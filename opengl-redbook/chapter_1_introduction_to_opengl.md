- glGen* commands are used for allocating names to the various types of opengl objects. A name is like a pointer type in C, in that until you allocate some memory and have the name reference it, the name is useless. The allocation scheme is called binding an object, is done using glBind*.

- Object binding: When you bind an object for the first time, OpenGL will internally allocate the memory it needs and make that object current, which means that any operations relevant to the bound object, like the vertex array object we are working with, will affect its state from that point on in the program's execution. After the first call to bind, the newly created  object will be initialized to its default state and will usually require some additional initialization to make it useful.

- Object binding happens in two cases
    - when you create and initialize the data you hold
    - everytime you want to use it and it's not currently bound
