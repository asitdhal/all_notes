## Vector

- A given position in space is defined by exactly one and only one unique `xyz` triplet. Those three values (x, y, and z) combined represent two imporatnt values: a direction and a magnitude. The `xyz` triplet is represented by a vector.  

- vec3 is a three component vector (x, y, z)
- vec4 is a four component vector (x, y, z, w) where w is added to make the vector homogeneous. w is usually 1.0.


### Direction of vector

- a direction from the origin toward a point in space. 

### Magnitude of Vector

- A vector can represent a magnitude(length of the vector). 
- A vector of length 1 is called *unit vector*. It's used to represent direction. 
- If a vector is not a unit vector and we want to scale it to make it one, we call that *normalization*. Normalizing a vector scales it such that its length is 1 and the vector is then said to be *normalized*.

## Vector operaations

### Dot product

-  The dot product between two (three component) vectors returns a scalar (just one value) that is the cosine of the angle between the two vectors scaled by the product of their length. 

## Coordinate Spaces in OpenGL

### 3D graphics coordinate space

Coordinate Space | What it represents
---------------- | --------------------
Model Space(also known as object space) | Position related to a local origin
World Space | Positions related to global origin
View Space | Positions relative to the viewer(also known as camera or eye space)
Clip Space | Positions of vertices after projection into a nonlinear homogeneous coordinate
Normalized device coordinate(NDC) space | Vertex coordinates are said to be in NDC after their clip space coordinates have been divided by their own w component
Window Space | Positions of vertices in pixels, relative to the origin of the window

### Object Coordinates

In object space, positions of vertices are interpreted relative to a local origin. The origin is often the point about which you might rotate it to place it into a new orientation. Logically, it's kept inside the model, because if it is far outside, because rotating the object about that point would apply significant translation as well as rotation. e.g, in a space model, the origin is usually tip of the nose or it's center of gravity.

### World Coordinates

In world space, coordinates are kept relative to a fixed, global origin. All objects live in a common frame. Ofter it is the space in which lighting and physics calculations are performed. e.g. in a space model, world space could be the center of a play-field or a nearby planet.

### View Coordinates

View coordinates are represented as seen by the observer regardless of any transformations that may occur, yo u can think of them as absolute coordinates. 

### Clip and NDC 

- Clip space is the coordinate space in which OpenGL performs clipping. 
- When your vertex shader writes to `gl_Position`, this coordinate is considered to be in clip space. Upon exiting clip space, all four of the vertex's components are divided through by w component. The result of this division is always considered to be in normalized device coordinate space. 