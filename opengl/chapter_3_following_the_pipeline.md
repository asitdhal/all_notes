## Passing Data to the Vertex Shader

- The vertex shader is the first *programmable* stage in the OpenGL pipeline and is the only mandatory stage in the graphics pipeline.
- Before vertex shader runs, a fixed function stage known as *vertex fetching*, or sometimes *vertex pulling*, is run. This automatically provides inputs to the vertex shader.

## Vertex Attributes

in keyword is used to bring inputs into the vertex shader. When a variable is declared with an in storage qualifier, it is essentially an input to the OpenGL graphics pipeline. It is automatically filled in by the fixed-function vertex fetch stage. The variable becomes known as a *vertex attribute*. 

We can tell the vertex fetch stage by using one of the many variants of the vertex attribute function `glVertexAttrib*()`.

```c
void glVertexAttrib4fv(GLuint index, const GLFloat *v);
```

The parameter index is used to reference the attribute and v is a pointer to the new data to put into the attribute. The `layout` qualifier in the shader code is the declaration of the offeset attribute. 

```cpp

#include <sb7.h>

class my_application : public sb7::application
{
public:
    GLuint compile_shaders()
    {
        GLuint vertex_shader;
        GLuint fragment_shader;
        GLuint program;

        static const GLchar *vertex_shader_source[] =
        {
            "#version 420 core                           \n" // this means use 4.5 shadder language
            "                                            \n"
            "layout (location = 0) in vec4 offset;       \n"
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   const vec4 vertices[3] = vec4[3](vec4( 0.25, -0.25, 0.5, 1.0), \n"
            "                                    vec4(-0.25, -0.25, 0.5, 1.0), \n"
            "                                    vec4( 0.25,  0.25, 0.5, 1.0));\n"
            "   gl_Position = vertices[gl_VertexID] + offset;                \n" // output position of the vertex
            "}                                           \n"
        };

        static const GLchar *fragment_shader_source[] =
        {
            "#version 420 core                           \n"
            "                                            \n"
            "out vec4 color;                             \n" // value of out parameter in a fragment shader is sent to the window
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   color = vec4(0.0, 0.0, 1.0, 0.0);        \n"
            "}                                           \n"
        };

        vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex_shader, 1, vertex_shader_source, NULL);
        glCompileShader(vertex_shader);

        fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment_shader, 1, fragment_shader_source, NULL);
        glCompileShader(fragment_shader);

        program = glCreateProgram();
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);
        glLinkProgram(program);

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);

        return program;
    }

    void startup()
    {
        fprintf(stdout, "startup\n");
        rendering_program = compile_shaders();
        glCreateVertexArrays(1, &vertex_array_object);
        glBindVertexArray(vertex_array_object);
    }

    void render(double currentTime)
    {
        fprintf(stdout, "render\n");
        // color :- red,green,blue,alpha
        // alpha used to encode the opacity of a fragment. if value is 0, it's transparent, 1, it's opaque,
        // static const GLfloat red[] = {1.0f, 0.0f, 0.0f, 1.0f};

        const GLfloat color[] = {static_cast<float>(sin(currentTime)) + 0.5f + 0.5f,
                                      static_cast<float>(cos(currentTime)) + 0.5f + 0.5f, 0.0f, 1.0f};
        glClearBufferfv(GL_COLOR, 0, color);

        glUseProgram(rendering_program);

        GLfloat attrib[] = { static_cast<float>(sin(currentTime)) * 0.5f,
                             static_cast<float>(cos(currentTime)) * 0.6f,
                             0.0f, 0.0f };
        glVertexAttrib4fv(0, attrib);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    void shutdown()
    {
        fprintf(stdout, "shutdown\n");
        glDeleteVertexArrays(1, &vertex_array_object);
        glDeleteProgram(rendering_program);
        glDeleteVertexArrays(1, &vertex_array_object);
    }

private:
    GLuint rendering_program;
    GLuint vertex_array_object;
};


DECLARE_MAIN(my_application)

```

## Passing Data from Stage to Stage

Data can be passed from one shader to the next shader using in and out keyword. Anything you write to an output variable in one shader is sent to a similarly named variable declared with the in keyword in the subsequent shader. 

```cpp
#include <sb7.h>


class my_application : public sb7::application
{
public:
    GLuint compile_shaders()
    {
        GLuint vertex_shader;
        GLuint fragment_shader;
        GLuint program;

        static const GLchar *vertex_shader_source[] =
        {
            "#version 420 core                           \n"
            "                                            \n"
            "layout (location = 0) in vec4 offset;       \n"
            "layout (location = 1) in vec4 color;        \n"
            "                                            \n"
            "out vec4 vs_color;                          \n"
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   const vec4 vertices[3] = vec4[3](vec4( 0.25, -0.25, 0.5, 1.0), \n"
            "                                    vec4(-0.25, -0.25, 0.5, 1.0), \n"
            "                                    vec4( 0.25,  0.25, 0.5, 1.0));\n"
            "   gl_Position = vertices[gl_VertexID] + offset;                \n"
            "   vs_color = color;                        \n"
            "}                                           \n"
        };

        static const GLchar *fragment_shader_source[] =
        {
            "#version 420 core                           \n"
            "                                            \n"
            "in vec4 vs_color;                           \n"
            "out vec4 color;                             \n" // value of out parameter in a fragment shader is sent to the window
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   color = vs_color;                        \n"
            "}                                           \n"
        };

        vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex_shader, 1, vertex_shader_source, NULL);
        glCompileShader(vertex_shader);

        fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment_shader, 1, fragment_shader_source, NULL);
        glCompileShader(fragment_shader);

        program = glCreateProgram();
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);
        glLinkProgram(program);

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);

        return program;
    }

    void startup()
    {
        fprintf(stdout, "startup\n");
        rendering_program = compile_shaders();
        glCreateVertexArrays(1, &vertex_array_object);
        glBindVertexArray(vertex_array_object);
    }

    void render(double currentTime)
    {
        fprintf(stdout, "render\n");
        const GLfloat color[] = {static_cast<float>(sin(currentTime)) + 0.5f + 0.5f,
                                      static_cast<float>(cos(currentTime)) + 0.5f + 0.5f, 0.0f, 1.0f};
        glClearBufferfv(GL_COLOR, 0, color);

        glUseProgram(rendering_program);

        GLfloat attribOne[] = { static_cast<float>(sin(currentTime)) * 0.5f,
                             static_cast<float>(cos(currentTime)) * 0.6f,
                             0.0f, 0.0f };
        GLfloat attributeTwo[] = {0.0f, 0.0f, 1.0f, 0.0f};
        glVertexAttrib4fv(0, attribOne);
        glVertexAttrib4fv(1, attributeTwo);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    void shutdown()
    {
        fprintf(stdout, "shutdown\n");
        glDeleteVertexArrays(1, &vertex_array_object);
        glDeleteProgram(rendering_program);
        glDeleteVertexArrays(1, &vertex_array_object);
    }

private:
    GLuint rendering_program;
    GLuint vertex_array_object;
};

DECLARE_MAIN(my_application)
```

## Interface Blocks

A number of variables can be grouped together into an *interface block*. These may include arrays, structures and other complex arrangement of variables. 

```cpp
#include <sb7.h>

class my_application : public sb7::application
{
public:
    GLuint compile_shaders()
    {
        GLuint vertex_shader;
        GLuint fragment_shader;
        GLuint program;

        static const GLchar *vertex_shader_source[] =
        {
            "#version 420 core                           \n"
            "                                            \n"
            "layout (location = 0) in vec4 offset;       \n"
            "layout (location = 1) in vec4 color;        \n"
            "                                            \n"
            "out VS_OUT                                  \n"
            "{                                           \n"
            "   vec4 color;                           \n"
            "} vs_out;                                   \n"
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   const vec4 vertices[3] = vec4[3](vec4( 0.25, -0.25, 0.5, 1.0), \n"
            "                                    vec4(-0.25, -0.25, 0.5, 1.0), \n"
            "                                    vec4( 0.25,  0.25, 0.5, 1.0));\n"
            "   gl_Position = vertices[gl_VertexID] + offset;                \n"
            "   vs_out.color = color;                    \n"
            "}                                           \n"
        };

        static const GLchar *fragment_shader_source[] =
        {
            "#version 420 core                           \n"
            "                                            \n"
            "in VS_OUT                                   \n"
            "{                                           \n"
            "   vec4 color;                              \n"
            "} fs_in;                                    \n"
            "out vec4 color;                             \n" // value of out parameter in a fragment shader is sent to the window
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   color = fs_in.color;                     \n"
            "}                                           \n"
        };

        vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex_shader, 1, vertex_shader_source, NULL);
        glCompileShader(vertex_shader);

        fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment_shader, 1, fragment_shader_source, NULL);
        glCompileShader(fragment_shader);

        program = glCreateProgram();
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);
        glLinkProgram(program);

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);

        return program;
    }

    void startup()
    {
        fprintf(stdout, "startup\n");
        rendering_program = compile_shaders();
        glCreateVertexArrays(1, &vertex_array_object);
        glBindVertexArray(vertex_array_object);
    }

    void render(double currentTime)
    {
        fprintf(stdout, "render\n");
        const GLfloat color[] = {static_cast<float>(sin(currentTime)) + 0.5f + 0.5f,
                                      static_cast<float>(cos(currentTime)) + 0.5f + 0.5f, 0.0f, 1.0f};
        glClearBufferfv(GL_COLOR, 0, color);

        glUseProgram(rendering_program);

        GLfloat attribOne[] = { static_cast<float>(sin(currentTime)) * 0.5f,
                             static_cast<float>(cos(currentTime)) * 0.6f,
                             0.0f, 0.0f };
        GLfloat attributeTwo[] = {0.0f, 0.0f, 1.0f, 0.0f};
        glVertexAttrib4fv(0, attribOne);
        glVertexAttrib4fv(1, attributeTwo);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    void shutdown()
    {
        fprintf(stdout, "shutdown\n");
        glDeleteVertexArrays(1, &vertex_array_object);
        glDeleteProgram(rendering_program);
        glDeleteVertexArrays(1, &vertex_array_object);
    }

private:
    GLuint rendering_program;
    GLuint vertex_array_object;
};

DECLARE_MAIN(my_application)
```

Interface blocks are matched between stages using block name, but referenced in shaders using the instance name. Matching interface blocks by block name but allowing block instances to have different names in each shader stage serves two important purposes.
	
1. It allows the name by which you refer to the block to be different in each stage, thereby avoiding confusing things.
2. It allows interfaces to go from being single items to arrays when crossing between certain shader stages.

## Tessellation

Tessellation is the process of breaking a high-order primitive(which is known as a *patch* in OpenGL) into many smaller, simpler primitives such as triangles for rendering. OpenGL includes a fixed-function, configurable tessellation engine that is able to break up quadrilaterals, triangles, and lines into potentially large number of smaller points, lines, or triangles that can be directly consumed by the normal rasterization hardware further down the pipeline. 

Logically, the testallation phase sits directly after the vertex shading stage in the OpenGL pipeline and is made up of three parts:

- the tessellation control shader
- the fixed-function tessellation engine
- tesselation evaluation shadder

### Tessellation Control Shaders

This shader takes input from verex shader and is reponsible for two tings

- the determination of the level of testallation that will be sent to the testallation engine.
- the generation of data that will be sent to the tessellation evaluation shader that is run after tesslation has occured.

Tessellation in OpenGL works by breaking down high-order surfaces known as *patches* into points, lines, or triangles. Each patch is formed from a number of *control points*. The number of control points per patch is configurable and set by calling `glPatchParameteri()` with pname set to GL_PATCH_VERTICES and value set to the number of control points that will be used to construct each patch.

```c
void glPatchParameteri(GLenum pname, GLint value);
```

When tessellation is active, the vertex shader runs once per control point, while the tessellation control shader runs in batches on groups of control points where the size of each batch is the same as the number of vertices per patch. That is, the vertices are used as control points and the result of the vertex shader is passed in batches to the tessellation control shader as its input.



### Tessellation Engine

- The tessellation engine is a fixed-function part of the OpenGL pipeline that takes high-order surface represented as patches and breaks them down into smaller primitives such as points, lines, or triangls.
- Before the tessellation engine receives a patch, the tessellation control shader processes the incoming control points and sets tessellation factors that are used to break down the patch.
- After the tessellation engine produces the output primitives, the vertices representing them are picked up by the tessellation evaluation shader.
- The tessellation engine is responsible for producing the parameters that are fed to the invocations of the tessellation evaluation shader, which it then uses to transform the resulting primitives and get them ready for rasterization.

### Tessellation Evaluation Shaders

- Once the fixed-function tessellation engine has run, it produces a number of output vertices representing the primitives it has generated. These are passed to tessellation evaluation shaders. 
- The tessellation evaluation shader runs an invocation for each vertex produced by the tessellator. When the tessellation levels are high, the tessellation evaluation shader could run an extremely large number of times. 

## Geometry Shaders

- The geometry shader is logically the last shader stage in the front end, sitting after the vertex and tessellation stages and before the rasterizer. The geometry shader runs once per primitive and has access to all of the input vertex data for all of the vertices that make up the primitive being processed.
- The geometry shader can increase or reduce the amount of data flowing through the pipeline in a programmatic way, by using `EmitVertex()` and `EndPrimitive()` that explicitly produce vertices  that are sent to primitive assembly and rasterization.
- The geometry shader can also change the primitive mode mid-pipeline. They can take triangles as input and produce a bunch of points or lines as output, or even create traiangles from independent points.

## Primitive Assembly, Clipping, and Rasterization

- After the front end of the pipeline has run(which includes vertex shading, tessellation, and geometry shading), a fixed-function part of the pipeline performs a series of tasks that take the vertex representation of our scene and converts it into a series of pixels, which in turn need to be colored and written to the screen. The first step of this process is called primitive assembly, which is grouping of vertices into lines and triangles. Primitive assembly still occurs for points, but it is trivial in that case.
- Once primitives have been constructed from their individual vertices, they are clipped against displayable region(window or screen), but can also be smaller area known as the viewport. 
- The part of the primitive that are determined to be potentially visible are sent to a fixed-function subsystem  called the rasterizer. This block determines which pixels are covered by the primitive(point, line, or triangle) and sends the list of pixels on to the next stage(fragment shader).

### Clipping

- As vertices exit the front end of the pipeline, their position is said to be in *clip space*. This is one of the many coordinate systems that can be used to represent positions. The gl_Position variable has a vec4 type(all four component vectors). This is known as *homogeneous* coordinate. The homegeneous coordinate system is used in projective geometry because much of the math ends up being simpler in homogeneous coordinate space than it does in regular Cartesian space. Homogeneous coordinates have one more component than their equivalent Cartesian coordinate, which is why our three-dimensional position vector is represented as a four-component variable.
- Although the output of the front end is a four-component homogeneous coordinate, clipping occurs in Cartesian space. To convert from homegeneous coordinates to Cartesian coordinates, OpenGL performs a *perspective division*, which involves dividing all four components of the position by the last w component. 
- After the prospective division, the resulting position is in *normalize device space*. In OpenGL, the visible region of normalized device space is the valume that extends from -1.0 to 1.o in the x and y dimensions and from 0.0 to 1,0 in the z dimension. Any geometry that is contained in this region may become visible to the user and anything outside of it should be discarded. 


### Viewport Transformation

- After clipping, all of the vertices of the geometry have coordinates that lie between -1.0 and 1.0 in the x and y dimensions. Along with a z coordinate that lies between 0.0 and 1.0, these are called normalized device coordinates. The window that you are drawing ti has coordinates that usually start from `(0,0)` at the bottom left and range to `(w-1, h-1)`, where w and h are width and height of the window in pixels. 

- OpenGL applies the *viewport transform*, which applies a scale and offset to the vertices normalized device coordinates to move them into window coordinates. 

### Culling

- Culling determines whether the triangle(in this case) faces forward or away from the viewer and can decide whether to actually go ahead and draw it based on the result of this computation. If the triangle faces toward the viewer, then it is considered to be *front-facing*; otherwise, it is said to be *back-facing*. It is common to discard traiangles that are back-facing because when an object is closed, any back-facing triangle will be hidden by another front-facing triangle.

## Fragment Shaders

- The fragment shader is responsible for determining the color of each fragment before it is sent to the framebuffer for possible composition into the window. After the rasterizer processes a primitive, it produces a list of fragment that need to be colored and passes this list to the fragment shader. 
- The fragment shader is responsible for performing calculations related to lighting, applying materialsm and even determining the depth of the fragment. 