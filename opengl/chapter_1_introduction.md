## Introduction

OpenGL is an interface that your application can use to access and control graphics subsystem of the device on which it runs. The device can be anything like

- high-end graphics workstation
- commodity desktop computer
- video game console
- mobile phone 

OpenGL provides an abstraction layer between your application and the underlying subsystem(hardware).

OpenGL must strike a balance between too high and too low an abstraction level.

- It must hide differences between various manufacturers' products and system specific traits such as screen resolution, processor architecture, installed os and so on.

- The level of abstraction must be low enough that programmer can gain access to the underlying hardware and make best use of it. 


## Graphics Pipeline

The commands from application program are taken by OpenGL and sent to underlying graphics hardware. There could be many commands lined up to execute on hardware(a status referred to as *in flight*), and some may even be partially completed. This allows their execution to be overlapped such that a later stage of one command might run concurrently with an earlier stage of another command. 

Computer graphics generally consists of many repititions of very similar tasks(such as figuring out what color a pixel should be), and these tasks are usually independent of one another(the coloring one pixel doesn't depend on any other). 

OpenGL can break up the work you give and work on its fundamental element *in parallel*.  Through a combination of *pipelining and parallelism*, incredible performance of modern graphics is realized.

Current GPUs consists of large numbers of small programmable processors called *shadder cores* that run mini-programs called *shadders*. Each core has a relatively low throughput, processing a single instruction of the shadder in one or more clock cycles and normally lacking advanced features such as out-of-order execution, branch prediction, super-scalar issues, and so on. A GPU can contain anywhere from a few tens to a few thousands of these cores, and together they can do immense amount of work. The graphics system is broken into a number of stages, each represented either by a shadder or by a fixed-function, possibly configurable processing blocks.

![alt text](images/graphics_pipeline.png)

In the above image, boxes with rounded corners are fixed function stages and boxes with square corners are programmable. 

## Primitives, pipelines and pixels

The fundamental unit of rendering in OpenGL is known as the *primitive*. OpenGL supports many types of primitives, but three basic renderable primitive types are points, lines, and triangles. Everything you see rendered on the screen is a collection of(perhaps cleverly colored) points, lines, and triangles. Application will normally break complex surfaces into a very large number of triangles and send them to OpenGL, where they are rendered using a hardware accelerator called a *rasterizer*. The rasterizer is a dedicated hardware that converts the three-dimensional representation of a triangle into a series of pixels that need to be drawn onto the screen.

The primitives are formed from collections of many vertices. A vertex is simply a point within a coordinate space(in our case, 3D). The graphics pipeline is broken down into two major parts. The first part is known as *front end*, processes vertices and primitives, eventually forming them into the points, lines, and triangles that will be handed off to the rasterizer. This is known as *primitive assembly*.  After going through the rasterizer, the geometry has been converted from what is essentially a vector representation into a large number of independent pixels. These are handed off to the *back end*, which includes depth and stencil testing, fragment shading, blendingm and updating of the output image.

