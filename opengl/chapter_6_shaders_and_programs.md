## Language

- GLSL is a C-like language, which is more suitable for graphics and parallel execution in general.
- In GLSL, matrix and vector types are first-class citizens. 

## Data Types

### Scalar Types
- Supported types are 

Type | bit size
---- | --------
`bool` | `true`/`false`
`int` | 32 bit  2's complement signed integer
`float` | 32 bit floating point
`double` | 64 bit floating point
`unsigned ` | 32 bit unsigned integer

## Compilation, Linking, and Examining Programs

- OpenGL implementation has a compiler and linker built in that will take your shader code, compile it to an internal binary form, and link it together so that it can be run on a graphics processor.

### Information about Compilation status

- `glGetShaderiv()`
- `glGetShaderInfoLog()`

