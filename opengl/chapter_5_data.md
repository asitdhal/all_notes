## Buffers

- Buffers are linear allocations of memory that can be used for a number of purposes. These are represented by *names*, which are essentially opaque handles that OpenGL uses to identify them.
- Before you can start using buffers, you have to ask OpenGL to reserve some names for you and then use them to allocate memory and put data into that memory. The memory allocated for a buffer object is called its *data store*. The data store of the buffer is where OpenGL stores its data.
- You can also put data into the buffers using OpenGL commands, or you can *map* the buffer object, which means that you can get a pointer that your application can use to write directly into the buffer.

### Creating Buffers and Allocating Memory

- Buffer object has to be created to represent allocation. Buffer objects are represented by a `GLuint` variable. This is called `name`. The following function is used to create a buffer object.

```c
void glCreateBuffers(GLsizei n, GLuint* buffers);
```
n:- number of buffer objects
buffers - address(es) of variable(s)

You can bind the buffer objects to the current OpenGL context by using the following function

```c
void glBindBuffer(GLenum target, GLuint buffer);
```

Before you can use them, you need to allocate their *data stores*, which is another term for the memory represented by buffer object. These are functions for this.

```c
void glBufferStorage(GLenum target, GLsizeiptr size, const void* data, GLbitfield flags); // first
void glNamedBufferStorage(GLenum target, GLsizeiptr size, const void* data, GLbitfield flags); // second
```

The first function affects the buffer object bound to the binding point specified by target. The second function directly affects the buffer specified by buffer. size is the size of buffer and data is the data in the buffer.

After the buffer allocation, the buffer is immutable(can't be reallocated and respecified). Size and usage flag can't be changed. If you want to resize a buffer, you need to delete it, create a new one, and set up new storage for that.

The flag parameter is

Flag | Description
---- | -----------
GL_DYNAMIC_STORAGE_BIT | Buffer contents can be updated directly
GL_MAP_READ_BIT | Buffer data store will be mapped for reading
GL_MAP_WRITE_BIT | Buffer data store will be mapped for writing
GL_MAP_PERSISTENT_BIT | Buffer data store can be mapped persistently
GL_MAP_COHERENT_BIT | Buffer maps are to be coherent
GL_CLIENT_STORAGE_BIT | If all conditions can be met, prefer storage local to the client(CPU) rather than to the server(GPU)


### GL_DYNAMIC_STORAGE_BIT

- GL_DYNAMIC_STORAGE_BIT tells that the developer wants to update the content of the buffers directly - perhaps once for every time that you use the data. If flag is not set, OpenGL will assume that you're not likely to need to change the contents of the buffer and might put the data somewhere that is less accessible. 

- This is required for glBufferSubData()

### Mapping flags

- Mapping is the process of getting a pointer that you can use from your application that represents the underlying data store of the buffer.

- GL_MAP_READ_BIT and GL_MAP_WRITE_BIT are used for read and write mapping.

- GL_MAP_PERSISTENT_BIT tells that you wish to map the buffer and then *leave mapped* while you call other drawing commands. If you don't use this bit, then OpenGL requires that you don't have the buffers mapped while you're using it from drawing commands. It has performance penalty.

- GL_MAP_COHEREN_BIT tells OpenGL that you want to be able to share data quite tightly with the GPU. 

```c
// name of the buffer
GLuint buffer;

// Creating a buffer
glCreateBuffers(1, &buffer);

// Specify the data store parameters for the buffer
glNamedBuffersStorage(
    buffer, // name of the buffer
    1024 * 1024, // size of space
    NULL, // no initial data
    GL_MAP_WRITE_BIT // allow map for writing
);

// now bund to the context
glBindBuffer(GL_ARRAY_BUFFER, buffer);
```

Here, we can pass data instead of NULL.

Another way to get data into a buffer is to give the buffer to OpenGL and tell it to copy data there. This allows you to dynamically update the content of a buffer after it has already been initialized. 

```c
void glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid * data);
void glNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, const GLvoid * data);
```

In this case, GL_DYNAMIC_STORAGE_BIT flag is required. 

```c
// this is the data that will place into the buffer object
static const float data[] =
{
    0.25, -0.25, 0.5, 1.0,
   -0.25, -0.25, 0.5, 1.0,
    0.25,  0.25, 0.5, 1.0
};

// Put the data into the buffer at offset zero
glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(data), data);
```

Another method for getting data into a buffer object is to ask OpenGL for a pointer to the memory that the buffer object represents and then copy the data there yourself. This is known as mapping the buffers.

```c
// this is the data that will place into the buffer object
static const float data[] =
{
    0.25, -0.25, 0.5, 1.0,
   -0.25, -0.25, 0.5, 1.0,
    0.25,  0.25, 0.5, 1.0
};

// get a pointer to the buffer's data store
void * ptr = glMapNamedBuffer(buffer, GL_WRITE_ONLY);

// copy our data into it...
memcpy(ptr, data, sizeof(data));

// Tell OpenGL that we're done with the pointer
glUnmapNamedBuffer(GL_ARRAY_BUFFER);
```


There are two pairs of mapping and unmapping buffers

```c
void *glMapBuffer(GLenum target, GLenum usage);
void *glMapNamedBuffer(GLuint buffer, GLenum usage);

void glUnmapBuffer(GLenum target);
void glUnmapNamedBuffer(GLuint buffer);
```

Mapping a buffer is useful if you don't have all the data handy when you call the function. 


The glMapBuffer() and glMapNamedBuffer() functions can sometimes be a little heavy handed. They map the entire buffer, and do not provide any information about the type of mapping operation to be performed besides the usage parameter. A more surgical approach can be taken by calling any of the following two functions.

```c
void *glMapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access);
void *glMapNamedBuffer(GLuint buffer, GLintptr offset, GLsizeiptr length, GLbitfield access);
```

### Filling and Copying Data in Buffers

## Feeding Vertex Shaders from Buffers

Let's create a vertex array object to store our vertex array state and bind it to our context so that we can use it.

```c
GLuint vao;
glCreateVertexArray(1, &vao);
glBindVertexArray(vao);
```

Rather than using hard-coded data in the vertex attribute and ask OpenGL to fill in automatically using the data stored in a buffer object that we supply. Each vertex attribute gets to fetch data from a buffer bound to one of several *vertex buffer bindings*. To set the binding that a vertex attribute uses to reference a buffer, call the `glVertexArrayAttribBinding()` function. This tells when the vertex array object named vaobj is bound, the vertex attribute at the index specified in attribindex should source data from the buffer bound at bindingindex.

```c
void glVertexArrayAttribBinding(GLuint vaobj, GLuint attribindex, GLunit bindingindex);
```

To tell OpenGL which buffer object our data is in and where in that buffer object the data resides, we use `glVertexArrayVertexBuffer()` function to bind a buffer to one of the vertex buffer bindings. We use the `glVertexArrayAttribFormat()` function to describe layout and format of the data, and finally we enable automatic filling of the attribute by calling `glEnableVertexAttribArray()`.

```c
void glVertexArrayVertexBuffer(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
```


