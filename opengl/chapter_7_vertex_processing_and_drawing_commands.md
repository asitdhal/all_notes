## Vertex Processing

Before the shader runs, OpenGL will fetch the inputs to the vertex shader in the *vertex fetch* stage. Your vertex shader's responsibility is to set the position of the vertex that will be fed to the next stage in the pipeline.

### Vertex Shader Inputs

- OpenGL supports a large number of vertex attributes, and each can have it's own format, data type, number of components, and so on. Also opengl can read the data for each attribute from a different buffer object, 

- 