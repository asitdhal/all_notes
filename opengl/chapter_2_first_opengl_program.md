## Hello-World

```cpp
#include <sb7.h>

class my_application : public sb7::application
{
public:
    void render(double currentTime)
    {
        // color :- red,green,blue,alpha
        // alpha used to encode the opacity of a fragment. if value is 0, it's transparent, 1, it's opaque,
        // static const GLfloat red[] = {1.0f, 0.0f, 0.0f, 1.0f};

        const GLfloat red[] = {static_cast<float>(sin(currentTime)) + 0.5f + 0.5f,
                                      static_cast<float>(cos(currentTime)) + 0.5f + 0.5f, 0.0f, 1.0f};
        glClearBufferfv(GL_COLOR, 0, red);
    }
};

DECLARE_MAIN(my_application)
```





## Shadder 

OpenGL shadders are written in a language called the OpenGL Shading Language, or GLSL. The compiler for this language is built into OpenGL. The source code for your shader is placed into a *shadder object* and compiled, and then multiple shader objects can be linked together to form a program object. Each program object can contain shaders for one or more shader stages. The shader stages are 

- vertex shaders
- tessellation control and evaluation shaders
- geometry shaders
- fragment shaders
- compute shaders

The minimal useful pipeline configuration consists of only a vertex shader, but if you want to see any pixels, you need a fragment shader.

```cpp
#include <sb7.h>


class my_application : public sb7::application
{
public:
    GLuint compile_shaders()
    {
        GLuint vertex_shader;
        GLuint fragment_shader;
        GLuint program;

        static const GLchar *vertex_shader_source[] =
        {
            "#version 420 core                           \n" // this means use 4.5 shadder language
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   gl_Position = vec4(0.0, 0.0, 0.5, 1.0);  \n" // output position of the vertex
            "}                                           \n"
        };

        static const GLchar *fragment_shader_source[] =
        {
            "#version 420 core                           \n"
            "                                            \n"
            "out vec4 color;                             \n" // value of out parameter in a fragment shader is sent to the window
            "                                            \n"
            "void main(void)                             \n"
            "{                                           \n"
            "   color = vec4(0.0, 0.8, 1.0, 1.0);        \n"
            "}                                           \n"
        };

        vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex_shader, 1, vertex_shader_source, NULL);
        glCompileShader(vertex_shader);

        fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment_shader, 1, fragment_shader_source, NULL);
        glCompileShader(fragment_shader);

        program = glCreateProgram();
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);
        glLinkProgram(program);

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);

        return program;
    }

    void startup()
    {
        fprintf(stdout, "startup\n");
        rendering_program = compile_shaders();
        glCreateVertexArrays(1, &vertex_array_object);
        glBindVertexArray(vertex_array_object);
    }

    void render(double currentTime)
    {
        fprintf(stdout, "render\n");
        // color :- red,green,blue,alpha
        // alpha used to encode the opacity of a fragment. if value is 0, it's transparent, 1, it's opaque,
        // static const GLfloat red[] = {1.0f, 0.0f, 0.0f, 1.0f};

        const GLfloat color[] = {static_cast<float>(sin(currentTime)) + 0.5f + 0.5f,
                                      static_cast<float>(cos(currentTime)) + 0.5f + 0.5f, 0.0f, 1.0f};
        glClearBufferfv(GL_COLOR, 0, color);
        glUseProgram(rendering_program);
        glDrawArrays(GL_POINTS, 0, 1);
    }

    void shutdown()
    {
        fprintf(stdout, "shutdown\n");
        glDeleteVertexArrays(1, &vertex_array_object);
        glDeleteProgram(rendering_program);
        glDeleteVertexArrays(1, &vertex_array_object);
    }

private:
    GLuint rendering_program;
    GLuint vertex_array_object;
};

DECLARE_MAIN(my_application)
```

In the vertex shader, `gl_Position` represents the output position of the vertex.  The value assigned places the vertex in OpenGL's *clip space*, which is the coordinate system expected by the next stage of the OpenGL pipeline. In the fragment shaders, the value of output variables will be sent to the windowing or screen. The `out` keyword is used to declare the output variable. 

## Shader compilation process

- The shader source code is a constant string that are passed to the `glShaderSource()` function, which copies them into the shader objects that we created with `glCreateShader()`.
- The shader object stores a copy of our source code. When we call `glCompileShader()`, it compiles the GLSL shader source code into an intermediate binary representation that is also stored in the shader object. The program object represents the linked executable that we will use for rendering. 
- We attach our shaders to the program objects using `glAttachShader()` and then call `glLinkProgram()`, which links the object together into code that can be run on the graphics processor. 
- Attaching a shader object to a program object creates a reference to the shader; we can then delete it, knowing that the program object will hold onto the shader's content as long as it needs it.
- *Vertext Array Object(VAO)* is an object that represents the vertex fetch stage of the OpenGL pipeline and is used to supply input to the vertex shader. `glCreateVertexArrays()` is used to create a VAO and `glBindVertexArray()` is used to attach to our context. 