When a variable is declared using auto, auto plays the role of T in the template, and the type specifier for the variable acts as ParamType. 

```cpp
auto x = 27;
const auto cx = x;
const auto& rx = x;

template <typename T>
void func_for_x(T param); // conceptional template for deducing x's type
func_for_x(27); // conceptual call: param's deduced type is x's type, here it's int

template <typename T>
void func_for_cx(const T param); // conceptual template for deducing cx's type
func_for_cx(x); // conceptual call: param's deduced type is cx's type

template <typename T>
void func_for_rx(const T& param); // conceptual template for deducing rx's type 
func_for_rx(x); // conceptual call: param's deduced type is rx's type
```

### Template type deduction rules

1. case 1: ParamType is a pointer or a reference type, but not a universal reference
	1. If expr's type is a reference, ignore the reference part. 
	2. Then pattern-match expr's type against ParamType to determine T.
2. case 2: ParamType is a universal reference
	1. If expr is an lvalue, both T and ParamType are deduced to be lvalue references. It's only situation in template type deduction where T is deduced to be a reference. Although ParamType is declared using the syntax for an rvalue reference, it's deduced type is an lvalue reference.
	2. If expr is an rvalue, the "normal" rules apply.
3. case 3: ParamType is neither pointer nor a reference
	1. If expr's type is a reference, ignore the reference part.
	2. Even after that, expr is const or volatile, ignore that too.


```cpp
#include "cpp14_typename.hpp"

int main()
{
    auto x = 27; // case 3: auto-> int, x -> int
    const auto cx = x; // case 3: auto-> int, cx -> const int
    const auto& rx = cx; // case 1 auto -> int, rx -> const int&

    auto&& uref1 = x; // case 2, x is an int and lvalue, uref is int&
    auto&& uref2 = cx; // case 2 cx is const int and lvalue, uref2 is int&
    auto&& uref3 = 27; // 27 is int and rvalue, uref3 is int&&

    std::cout << "x=" << type_name<decltype(x)>() << '\n';
    std::cout << "cx=" << type_name<decltype(cx)>() << '\n';
    std::cout << "rx=" << type_name<decltype(rx)>() << '\n';
    std::cout << "uref1=" << type_name<decltype(uref1)>() << '\n';
    std::cout << "uref2=" << type_name<decltype(uref2)>() << '\n';
    std::cout << "uref3=" << type_name<decltype(uref3)>() << '\n';

    return 0;
}
```

output

```bash
x=int
cx=const int
rx=const int&
uref1=int&
uref2=const int&
uref3=int&&
```

In case of array and function, type is also decayed for auto.

```cpp
#include "cpp14_typename.hpp"

void someFunc(int, double) // type is void(int, double)
{
}

int main()
{
    const char name[] = "Asit Dhal"; // name is const char[10]
    auto arr1 = name; // arr1's type is const char*
    auto& arr2  = name; // arr2's type is const char (&)[10]


    auto func1 = someFunc; // type is void (*)(int, double)
    auto& func2 = someFunc; // type is void (&)(int, double)

    std::cout << "name=" << type_name<decltype(name)>() << '\n';
    std::cout << "arr1=" << type_name<decltype(arr1)>() << '\n';
    std::cout << "arr2=" << type_name<decltype(arr2)>() << '\n';
    std::cout << "someFunc=" << type_name<decltype(someFunc)>() << '\n';
    std::cout << "func1=" << type_name<decltype(func1)>() << '\n';
    std::cout << "func2=" << type_name<decltype(func2)>() << '\n';

    return 0;
}
```

output

```bash
name=const char[10]
arr1=const char*
arr2=const char(&)[10]
someFunc=void(int,double)
func1=void(__cdecl *)(int,double)
func2=void(__cdecl &)(int,double)
```

### std::initializer_list

C++11 supports 4-types of uniform initialization.

```cpp
int x1 = 27;
int x2(27);
int x3 = {27};
int x4{27};
```

We can replace int with auto.

```cpp
auto x1 = 27; // type is int, value is 27
auto x2(27);  // ditto
auto x3 = {27}; // type is std::initializer_list<int> and value is {27}
auto x4{27};  // ditto
```

But they don't have the same meaning. First two are essentially same. The second two declare a variable of type std::initializer_list<int> containing a single element with value 27.

When the initializer for an auto-declared variable is enclosed in braces, the deduced type is a std::initializer_list. If such a type can't be deduced(e.g. because the values in the braced initializer are of different types), the code will be rejected.


```cpp
auto x5 = {1, 2, 3, 4}; // x5's type is std::initializer_list<int>
```

Here, there are two kind's of initialization
* x5's type
* T of std::initializer_list must be deduced


So, this comes under template type deduction.

When an auto-declared variable is initialized with a braced initializer, the deduced type is an instantiation of std::initializer_list.  But if corresponding template is passed the same initializer, type deduction fails and the code is rejected.

```cpp
#include "cpp14_typename.hpp"

template <typename T>
void f1(T param)    // template with param declaration equivalent to x's declaration.
{
    std::cout << __FUNCSIG__ << '\n';
}

template <typename T>
void f2(std::initializer_list<T> initList)
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{
    auto x = {1, 2, 3, 4}; // x's type is std::initializer_list<int>
    std::cout << "x=" << type_name<decltype(x)>() << '\n';

    // f2({11, 23, 9}); // error, type deduction fails.

    f2({11, 23, 9}); // T deduced as int, and initList's type is std::initializer_list<int>

    return 0;
}
```

output

```bash
x=class std::initializer_list<int>
void __cdecl f2<int>(class std::initializer_list<int>)
```

The real difference between auto and template type deduction is that auto assumes that braced initializer represents a std::initializer_list, but template type deduction doesn't.

C++14 permits auto to indicate that a function's return type should be deduced, and C++14 lambdas may use auto in parameter declaration. However, these uses of auto employ template type deduction, not auto type deduction.

```cpp
auto createInitList()
{
    return {1, 2, 3}; // error: can't deduce type
}

std::vector<int> v;
auto resetV = [&](const auto& newValue) { v = newValue; }; //C++14

resetV({1, 2, 3, 4}); // error: can't deduce type
```