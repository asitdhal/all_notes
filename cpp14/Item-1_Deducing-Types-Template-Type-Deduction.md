```cpp
template <typename T>
void f(ParamType param;

f(expr);
```

Here compiler uses expr to deduce two types
* one for T
* one for ParamType(Paramtype may contain adornments, e.g., const or reference qualifiers)

The type deduced for T is dependent not just on the type expr, but also on the form of ParamType. 
1. Case 1: ParamType is a pointer or a reference type, but not a universal reference.
2. Case 2: ParamType is a universal reference
3. Case 3: ParamType is neither pointer nor a reference

### Case 1(ParamType is a pointer or a reference type, but not a universal reference)
	1. If expr's type is a reference, ignore the reference part. 
	2. Then pattern-match expr's type against ParamType to determine T.

```cpp
template <typename T>
void f(T& param) // param is a reference
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{
    int x = 27;      // x is an int
    const int cx = x;  // cx is a const int
    const int& rx = x; // rx is a reference to x as a const int

    f(x);  // T is an int, ParamType is int&
    f(cx); // T is a const int, ParamType is const int&
    f(rx); // T is a const int, ParamType is const int&.

    return 0;
}
```

output

```bash
void __cdecl f<int>(int &)
void __cdecl f<const int>(const int &)
void __cdecl f<const int>(const int &)
```

Here referenceness of rx is ignored, so T becomes a const int.

When you pass a const object to a reference parameter, they expect that object to remain unmodifiable, i.e., for the parameter to be a reference to const. So passing a const  object to a template taking T& is safe, the constness of the object becomes part of the type deduced for T.

```cpp
template <typename T>
void f(const T& param) // param is now a ref-to-const
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{

    int x = 27; // x is an int
    const int cx = x;  // cx is a const int
    const int& rx = x; // rx is a reference to x as a const int

    f(x); // T is int, param's type is const int&
    f(cx); // T is int, param's type is const int&
    f(rx); // T is int, param's type is const int&

    return 0;
}
```

output

```bash
void __cdecl f<int>(const int &)
void __cdecl f<int>(const int &)
void __cdecl f<int>(const int &)
```


```cpp
template <typename T>
void f(T* param) // param is now a pointer
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{
    int x = 27; // x is an int
    const int *px = &x; // px is a ptr to x as a const int

    f(&x); // T is int, param's type is int*
    f(px); // T is const int, param's type is const int*

    return 0;
}
```

output

```bash
void __cdecl f<int>(int *)
void __cdecl f<const int>(const int *)
```

### Case 2(ParamType is a universal reference)

Universal Reference: parameters are declared like rvalue reference(a function template taking a type parameter T, a universal reference's declared type is T&&).When universal references are in use, type deduction distinguishes between lvalue arguments and rvalue argument.

```cpp
template <typename T>
void f(ParamType param);

f(expr);    
```

* If expr is an lvalue, both T and ParamType are deduced to be lvalue references. It's only situation in template type deduction where T is deduced to be a reference. Although ParamType is declared using the syntax for an rvalue reference, it's deduced type is an lvalue reference.
* If expr is an rvalue, the "normal" rules apply.

```cpp
template <typename T>
void f(T&& param) // param is now a universal reference
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{
    int x = 27; // x is an int
    const int cx = x;  // cx is a const int
    const int& rx = x; // rx is a reference to x as a const int

    f(x);   // x is lvalue, so T is int&
            // param's type is also int&
    f(cx);  // cx is lavlue, so T is const int&
            // param's type is also const int&

    f(rx);  // rx is lvalue, so T is const int&
            // param's type is also const int&
    f(27);  // 27 is rvalue, so T is int
            // param's type is therefore int&&

    return 0;
}
```

output

```bash
void __cdecl f<int&>(int &)
void __cdecl f<const int&>(const int &)
void __cdecl f<const int&>(const int &)
void __cdecl f<int>(int &&)
```

### Case 3(ParamType is neither pointer nor a reference)
This is passed-by-value. Param will be a copy of whatever is passed in - a completely new object. 

* If expr's type is a reference, ignore the reference part.
* Even after that, expr is const or volatile, ignore that too.

```cpp
template <typename T>
void f(T param); // param is now passed by value

template <typename T>
void f(T param) // param is now passed by value
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{
    int x = 27; // x is an int
    const int cx = x;  // cx is a const int
    const int& rx = x; // rx is a reference to x as a const int

    f(x);  // both T and param's type is int
    f(cx);  // again both are int
    f(rx); // again both are still int
    const char* const ptr = "Fun with pointers"; // ptr is const pointer to const object
    f(ptr); // T is const char* and param type is a pointer to const char. ptr is not a const.

    return 0;
}
```

output

```bash
void __cdecl f<int>(int)
void __cdecl f<int>(int)
void __cdecl f<int>(int)
void __cdecl f<const char*>(const char *)
```


### Array Arguments

An array decays into a pointer to its first elements.

```cpp
const char name[] = "asit dhal"; // type of name is const char[13]
const char* ptrToName = name; // decay happens here
```

Array parameter declaration is treated as a pointer parameter declaration. So, the type of an array that's passed to a template function by 
value is deduced to be a pointer type. 

```cpp
template <typename T>
void f(T param)
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{
    const char name[] = "asit dhal"; // type of name is const char[13]
    f(name); // name is array, but T is deduced as const char*

    return 0;
}
```

output

```bash
void __cdecl f<const char*>(const char *)
```


Some functions can declare parameters that are references to arrays.

```cpp
template <typename T>
void f(T& param) // template with by reference parameter
{
    std::cout << __FUNCSIG__ << '\n';
}

int main()
{
    const char name[] = "asit dhal"; // type of name is const char[13]
    f(name); // name is array, T is const char[10]. Param's type is const char&[10]

    return 0;
}
```

output

```bash
void __cdecl f<const char[10]>(const char (&)[10])
```


We can reuse this to get the size of array at compile time.

```cpp
template <typename T, std::size_t N>
constexpr std::size_t arraySize(T(&)[N]) noexcept
{
    return N;
}
int main()
{
       int keyVals[] = { 1, 3, 7, 9, 11, 22, 35 };
       int mappedVals[arraySize(keyVals)];
       std::cout << "array size: " << arraySize(keyVals) << std::endl; // 7
       std::cout << "sizeof keyVals: " << sizeof(keyVals) << std::endl; // 28
       std::cout << "sizeof mappedVals: " << sizeof(mappedVals) << std::endl; // 28
}
```

output

```bash
array size: 7
sizeof keyVals: 28
sizeof mappedVals: 28
```


### Function Arguments

Function types can decay into function pointers.


```cpp
void someFunc(int, double) // someFunc is a function; type is void(int, double)
{
}

template <typename T>
void f1(T param)   // in f1, param passed by value
{
    std::cout << __FUNCSIG__ << '\n';
}

template <typename T>
void f2(T& param) // in f2, param passed by ref
{
    std::cout << __FUNCSIG__ << '\n';
}


int main()
{
    f1(someFunc); // param deduced to ptr-to-func; type is void(*)(int, double)

    f2(someFunc); // param deduced as ref-to-func; type is void (&)(int, double)

    return 0;
}
```

output

```bash
void __cdecl f1<void(__cdecl *)(int,double)>(void (__cdecl *)(int,double))
void __cdecl f2<void(int,double)>(void (__cdecl &)(int,double))
```
