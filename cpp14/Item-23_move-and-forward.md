std::move doesn't move anything. std::forward doesn't forward anything. Both do casts. std::move unconditionally casts its arguments to an rvalue. std::forward performs this cast only if a particular condition is fulfilled.

```cpp
template <typename T>
typename std::remove_reference<T>::type&&
move(T&& param)
{
    using ReturnType = typename remove_reference<T>::type&&;
    return static_cast<ReturnType>(param);
}
```

std::move takes a reference to an object(a universal reference) and it returns a reference to the same object.

The && part of the function's return type implies std::move returns an rvalue reference, if the type T happens to be an lvalue reference, T&& would become an lvalue reference. To prevent this from happening, the type trait std::remove_reference is applied to T, thus ensuring that "&&" is applied to a type that isn't a reference. That guarantees that std::move truely returns an rvalue reference. As rvalue references returned from functions are rvalues, thus, std::move casts its arguments to an rvalue.

```cpp
// C++14
template <typename T>
decltype(auto) move(T&& param)
{
    using ReturnType = typename remove_reference<T>::type&&;
    return static_cast<ReturnType>(param);
}
```

ravlues are candidates for moving, so applying std::move to an object tells the compiler that the object is eligible to be moved from. That's why std::move has the name it does, to make it easy to designate objects that may be moved from.

```cpp
class Annotation
{
public:
        explicit Annotation(const std::string text) : value(std::move(text)) // it doesn't do what it seems to
        {...}

private:
        std::string value;
};
```

text is cast to rvalue by std::move, but text is declared to be const std::string, so before the cast, text is an lvalue const std::string, and the result of the cast is an rvalue const std::string. the constness remains.

Let's look at std::string

```cpp
class std::string
{
public:
        string(const string& rhs);
        string(string&& rhs);
    ...
};
```

The rvalue can't be passed to string's move constructor. Because the move constructor takes an rvalue reference to a non-const std::string.  But, the rvalue can be passed to copy constructor, because lvalue-reference-to-const is permitted to bind to a const rvalue.  So, in the previous case copy constructor is invoked.

> Moving a value out of an object generally modifies the object, so the language should not permit const objects to be passed to functions that could modify them.

Guidelines
*  Don't declare objects const if you want to be able to move from them. Move requests on const objects are silently transformed into copy operations.
*  std::move not only doesn't actually move anything, it doesn't even guarantee that the object it's casting will be eligible to be moved. The only thing you know for sure about the result of applying std::move to an object is that it's an rvalue.


```cpp
void process(const Widget& lvalArg); // process lvalues
void process(Widget&& rvalArg); // process rvalues

template <typename T>
void logAndProcess(T&& param)
{
    auto now = std::chrono::system_clock::now();
    makeLogEntry("calling 'process'", now);
    process(std::forward<T>(param));
}

Widget w;
logAndProcess(w); // call with lvalue
logAndProcess(std::move(w));  // call with rvalue

```
