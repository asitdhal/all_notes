```cpp
typedef void (*FP)(int, const std::string&); // typedef
using FP = void(*)(int, const std::string&); // alias declaration
```

Alias declaration can be templatized, while typedefs cannot.

```cpp
template <typename T>
using MyAllocList = std::list<T, MyAlloc<T>>; 

MyAllocList<Widget> lw;
```
In typedef, it's much more difficult.

```cpp
template <typename T>
struct MyAllocList {
    typedef std::list<T, MyAlloc<T>> type;
};
MyAllocList<Widget>::type lw;
```

It gets worse. If you want to use the typedef inside a template for the purpose of creating linked list holding objects of a type specified by a template parameter, you have to precede the typedef name with typename.

```cpp
template <typename T>
class Widget
{
private:
    typename MyAllocList<T>::type list; 
};
```

Here MyAllocList<T>::type refers to a type that's dependent on a template type parameter(T). MyAllocList<T>::type is thus a dependent type.


If MyAllocList is defined as an alias template, this need for typename vanishes.

```cpp
template <typename T>
using MyAllocList = std::list<T, MyAlloc<T>>; 

template <typename T>
class Widget
{
private:
    MyAllocList<T> list;
    ...
};
```

When compilers process the Widget template and encounter the use of MyAllocList<T>, they know that MyAllocList<T> is the name of a type, because MyAllocList is an alias template: it must name a type. MyAllocList<T> is thus a non-dependent type, and a typename specifier is neither required nor permitted.

C++11 allows type transformation.

```cpp
std::remove_const_t<T>; // T from const T
std::remove_reference_t<T>; // T from T& and T&&
std::add_lvalue_reference_t<T>; // T& from T
```
