> The trust shall set you free, but under the right circumstances, a well-choosen lie can be equally liberating.

To declare an rvalue reference to some type T, you write T&&. 

```cpp
void f(Widget&& param); // rvalue reference
Widget&& var1 = Wiget(); // rvalue reference
auto&& var2 = var1; // not rvalue reference

template <typename T>
void f(std::vector<T>&& param); // rvalue reference

template <typename T>
void f(T&& param); // not rvalue reference
```

```T&&``` has two different meanings. One is rvalue reference. These references behave exactly the way you expect: they bind only to rvalues, their primary *raison d'être* is to identify objects that may be moved from.

The other meaning for ```T&&``` is either rvalue reference or lvalue reference. Such references look like rvalue references in the source code(i.e. ```T&&```), but they can behave as if they were lvalue references(i.e., ```T&```). This dual nature permits them to bind to rvalues(like rvalue references) as well as lvalue(like lvalue references). Furthermore, they can bind to const or non-const objects, volatile or non-volatile objects, both const and volatile objects. They can bind virtually to anything. That's why they are called universal references.

Universal references arise in two contexts

* function template parameters
* auto declarations

```cpp
template <typename T>
void f(T&& param); // param is universal reference

auto&& var2 = var1; // var2 is universal reference
``` 

These two have one thing common: the presence of type deduction. Here both param's and var2's type is deduced.

If you see T&& without any type deduction, you are looking at an rvalue reference.

Universal references must be initialiyed.
* If the initializer is an rvalue, the universal reference corresponds to an rvalue reference.
* If the initializer is an lvalue, the universal reference corresponds to an lvalue reference.

```cpp
template <typename T>
void f(T&& param); // param is universal reference

Widget w;
f(w); // lvalue passed to f, so paramType is Widget&(lvalue reference)

f(std::move(w));  // rvalue passed to f, so paramType is Widget&&(rvalue reference)
```

For a reference to be universal, type deduction is necessary, but it's not sufficient. The form of the reference declaration must also be correct, and that form must be precisely ```T&&```. 

```cpp
template <typename T>
void f(std::vector<T>&& param); // param is an rvalue reference
```

If f is invoked, the type T will be deduced. But the form of param's type is std::vector<T>&&. So, it's not an universal reference. You can pass an lvalue, it won't compile.

Even the simple presence of a const qualifier is enough to disqualify a reference from being universal.

```cpp
template <typename T>
void f(const T&& param); // param is an r-value reference
```

Sometimes a function parameter of type ```T&&``` may not gurantee the presence of type deduction. 

```cpp
template<class T, class Allocator = allocator<T>> // from c++ standard library
class vector {
public:
	void push_back(T&& x);
	...
};
```

Here, push_back can't exist without a particular vector instantiation. So, for this function, template argument deduction doesn't take place.

```cpp
std::vector<Widget> v;
```

After instantiation, it becomes something like this. push_back employees no type deduction.

```cpp
class vector<Widget, allocator<Widget>> {
public:
	void push_back(Widget&& x); // r-value reference
	...
};
```

In contrast, emplace_back does employ type deduction.

```cpp
template<class T, class Allocator = allocator<T>> {
public:
	template <class... Args>
	void emplace_back(Args&&... args);
	...
};
```

Here, the type parameter Args is independent of vector's type parameter T, so Args must be deduced each time emplace_back is called. 

### ```auto&&```

Variables declared with the type ```auto&&``` are universal references, because type deduction takes place and they have the correct form ```auto&&```. 

C++14 lambda expressions may declare ```auto&&``` parameters.

```cpp
auto timeFuncInvocation =
	[](auto&& func, auto&&... params)
	{
		start timer;
		std::forward<decltype(func)>(func)( // invoke func on params
			std::forward<decltype(params)>(params)...
			);
		stop timer and record elapsed time;
	}

```

