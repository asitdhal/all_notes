Given a name or an expression, decltype tells you the name's or expression's type.

```cpp
const int i = 0; // decltype(i) is const int
bool f(const Widget& w); // decltype(w) is const Widget&, decltype(f) is bool(const Widget&)

struct Point
{
    int x,y; // decltype(Point::x) is int, decltype(Point::y) is int
};

Widget w; // decltype(w) is Widget
```

Primary usages: declaring function templates where the function's return type depends on its parameter types. 

operator[] on a container of objects of type T typically returns a T&(std::vector, std::deque). But in case of std::vector<bool>, operator[] returns a brand new bool(not a reference).  decltype makes it easy to express that. 

```cpp
template <typename Container, typename index>
auto authAndAccess(Container& c, Index i)
    -> decltype(c[i])
{
    authenticate();
    return c[i];
}
```
Here auto has nothing to do with type deduction. It means trailing return type(return type will be following parameter list->) is used. A trailing return type has the advantage that the function's parameters can be used in the specification of the return type. 

C++11 permits return type for a single statement lambdas to be deduced. 
C++14 extends this to both all lambdas and all functions, including those with multiple statements(even multiple returns, provided all yield the same deduced type).

If you omit trailing return type, leaving just the leading auto. With that form of declaration, auto does mean that type deduction will take place. It means compiler will deduce the function's return type from the function's implementation.

```cpp
template <typename Container, typename Index>
auto authAndAccess(Container& c, Index i)
{
    authenticate();
    return c[i]; // return type is deduced from c[i]
}

std::deque<int> d;
authAndAccess(d, 5) = 10; // it won't work here
```

Reference-ness of an expression is  ignored during type deduction.   d[5] returns a reference int&, but auto type deduction for authAndAccess will strip off the reference, thus yielding a return type of int.  So, the above code fails.

We can use decltype type deduction for its return type where types are inferred. C++14 allows through decltype(auto) specifier, i.e. auto says the type is to be deduced, and decltype says that decltype rules should be used during the deduction. 


```cpp
template <typename Container, typename Index>
decltype(auto) authAndAccess(Container& c, Index i)
{
    authenticate();
    return c[i]; // return type is deduced from c[i]
}

std::deque<int> d;
authAndAccess(d, 5) = 10; // it works. will return T& or T based on the type
```

decltype can also be used to declare initializing expressions.


```cpp
Widget w;
const Widget& cw = w;
auto myWidget1 = cw; // auto type deduction: myWidget's type is Widget, this is not what we intended.
decltype(auto) myWidget2 = cw; decltype type deduction: myWidget2's type is const Widget&


template <typename Container, typename Index>
decltype(auto) authAndAccess(Container& c, Index i);
```

Here, the container is passed by lvalue-reference-to-non-const, because returning a reference to an element of the container permits clients to modify that container.  But this means it's not possible to pass rvalue containers to this function. Rvalues can't bind to lvalue references(unless they're lvalue-reference-to-const, which is not the case here). An rvalue container, being a temporary object, would typically be destroyed at the end of the statement containing the call to authAndAccess, and that means that a reference to an element in that container would dangle at the end of the statement that created it.

But, we may need a use case like this sometime. A client might simply want to make a copy of an element in the temporary container. e.g.

```cpp
std::deque<std::string> makeStringDeque(); // factory function

// make copy of 5th element of deque returned from makeStringDeque
auto s = authAndAccess(makeStringDeque(), 5);
```

We can use universal references to achieve this.

```cpp
// C++14 version
template <typename Container, typename Index>
decltype(auto) authAndAndAccess(Container&& c, Index i)
{
    authenticate();
    return std::forward<Container>(c)[i];
}

// C++11 version
template <typename Container, typename Index>
auto authAndAndAccess(Container&& c, Index i) -> decltype(std::forward<Container>(c)[i])
{
    authenticate();
    return std::forward<Container>(c)[i];
}
```

Applying decltype to a name yields that declared type for that name. Names are typically lvalue expressions, but that doesn't affect decltype's behavior. 

Lvalue expressions  more complicated that names, however, decltype generally ensures that the type reported is an lvalue reference. If an lvalue expression other than a name has type T, decltype reports that type as T&. Function returning lvalues, always return lvalue references.

```cpp
int x = 0;
x is the name of a variable, so decltype(x) is int. 
Wrapping the name x in parentheses - "(x)", yields an expression.


decltype(auto) f1()
{
    int x  = 0;
    ...
    return x; // decltype(x) is an int, so f1 returns int
}

decltype(auto) f2()
{
    int x = 0;
    ...
    return (x); // decltype((x)) is int&, so f2 returns int&.
}
```