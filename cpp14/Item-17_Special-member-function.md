C++98  has 4 special member functions

*  Default Constructor
*  Destructor
*  Copy constructor
*  Copy-Assignment Operator

C++11 has two more: 
* move constructor 
* move assignment operator

```cpp
class Widget
{
public:
    ...
    Widget(Widget&& rhs);
    Widget& operator=(Widget&& rhs);
    ...
};
```

The move operations are generated only if they're needed, and if they are generated, they perform "memberwise moves" on the non-static data members of the class.  "Memberwise moves" are, in reality, more like memberwise move requests, because types that aren't move-enabled will be moved by copy operations. 

The two copy operations are independent, declaring one doesn't prevent compilers from generating the others. But the two move operations are not independent. If you declare either, that will prevent compilers from generating the other.  Move operations won't be generated for any class that explicitly declares a copy operation. That goes in other direction too. Declaring a move operation in a class causes compilers to disable the copy operation. 

Rule of Three

> If you declare any of a copy constructor, copy assignment operator, or destructor, you should declare all three. 

So move operations are generated for classes only if these three things are true.
*  No copy operations are declared
*  No move operations are declared
*  No destructor is declared



If you have code that depends on the generation of copy operations in classes declaring a destructor or one of the copy operations, you should provide the others too.


```cpp
class Widget
{
public:
    ...
    ~Widget(); // user declared dtor
    Widget(const Widget&) = default; // default copy ctor behavior is ok
    Widget& operator=(const Widget&) = default; // default copy assign behavior is OK
};
```

Polymorphic base classes normally have virtual destructors, because if they don't, some operations yield undefined or misleading results. Declaring a destructor has a potentially significant side effect: it prevents the move operations from being generated. However, creation of the class's copy operations is unaffected. 

```cpp
class Base
{
public:
    virtual ~Base() = default; // make dtor virtual

    Base(Base&&) = default; // support moving
    Base& operator=(Base&&) = default;

    Base(const Base&) = default; // support copying
    Base& operator=(const Base&) = default;
};
```

If the class has member function templates for special member functions, normal special member functions will be genrated.

```cpp
class Widget
{
    ...
    template <typename T>
    Widget(const T& rhs); // construct Widget from anything

    template <typename T>
    Widget& operator=(const T& rhs); // assign Widget from anything

};
```
