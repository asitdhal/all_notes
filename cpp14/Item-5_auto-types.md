Auto variables have their type deduced from their initializer, so they must be initialized.  This solves a host of unintialized variable problems.

```cpp
int x1; // potentially uninitialized
auto x2; // error
auto x3 = 10; //fine
```

In somecases, auto is the only way to declare a variable.

```cpp
template <typename It>
void dwim(It b, It e)
{
	for(; b != e; ++b) {
		auto currVal = *b;
		// ...
	}
}
```

It can represent types only known to compiler.

```cpp
// In C++11
auto derefUPLess = [](const std::unique_ptr<Widget&> &p1, const std::unique_ptr<Widget&> &p2) {
	return *p1 < *p2;
}

// In c++14
auto derefUPLess = [](const auto& p1, const auto& p2) {
	return *p1 < *p2;
}

```

Auto ensures you don't assume variable types. Explicitly specifying types may lead to implicit conversion that you neither want nor expect.

```cpp
std::vector<int> v;
auto sz = v.size(); // type is std::vector<int>::size_type;
```


### std::function and lambda

* std::function can refer to any callable object. 

```cpp
// function
bool(const std::unique_ptr<Widget>&, const std::unique_ptr<Widget>&); 

std::function<bool(const std::unique_ptr<Widget>&, const std::unique_ptr<Widget>&)> func;
```

* lambdas yield collable objects, so closure can be stored in std::function objects.

```cpp
// In C++11
std::function<bool(const std::unique_ptr<Widget>&, const std::unique_ptr<Widget>&)> derefUPLess = [](const std::unique_ptr<Widget&> &p1, const std::unique_ptr<Widget&> &p2) {
	return *p1 < *p2;
}
```

An auto-declared variable holding a closure is not same type as the closure, and as such it uses only as much memory as the closure requires. The type of an std::function declared variable holding a closure is an instantiation of the std::function template, and that has fixed size for any given signature. When you assign a closure to std::function, if the allocated  memory is not enough, heap memory will be allocated to store the closure.

So, std::function has 3 disadvantages
* std::function object typically uses more memory than auto declared object.
* Due to implementation details that restricts inlining and yield indirect function calls, invoking a closure via a std::function object is almost certain to be slower than calling it via an auto-generated object.
* auto is a whole lot less work than writing type of std::function.
