> rvalue references should be *unconditionally cast* to rvalues(std::move) when forwarding them to other functions, because they're always bound to rvalues.

> universal references should be *conditionally cast* to rvalues(std::forward) when forwarding them, because they're only sometimes bound rvalues.

```cpp

class Widget {
public:
	template <typename T>
	void setName(T&& newName)
	{
		name = std::move(newName);  // universal reference, compiles but is bad
	}
	...

private:
	std::string name;
	std::shared_ptr<SomeDataStructure> p;
};

std::string getWidgetName(); // factory function

Widget w;

auto n = getWidgetName(); // n is local variable
w.setName(n); // moves n into w
... // n's value unknown
```

Here, local variable n is passed to w.setName. Because setName internally uses std::move to unconditionally cast its reference parameter to an rvalue, n's value will be moved into w.name and n will come back from the call to setName with an unspecified behavior. This will anger the developer.

We can overload to both const lvalue and for rvalue.

```cpp
class Widget {
public:
	void setName(const std::string& newName) // set from const lvalue
	{
		name = newName; 
	}

	void setName(std::string&& newName) // set from rvalue
	{
		name = std::move(newName);
	}
};
```

here, a temporary std::string objects would be created for setName's parameter to bind to, and this temporary std::string would then be moved into w's data member. A call to setName would thus entail execution of 
* one std::string constructor(to create the temporary)
* one std::string move assignment operator(to move newName into w.name)
* one std::string destructor(to destroy the temporary)

That's almost certainly a more expensive execution sequence than invoking only the std::string assignment operator taking a const char* pointer. 

Overloading on lvalues and rvalues is also the poor scalability of the design. If functions take many parameters, the number of overloads grow exponentially(2^n).  And the worst case scenario, some functions also take an unlimited number of parameters.

