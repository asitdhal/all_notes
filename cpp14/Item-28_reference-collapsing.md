```cpp
template <typename T>
void func(T&& param);
```

When an lvalue is passed as an argument, T is deduced to be an lvalue reference.
When an rvalue is passed as an argument, T is deduced to be a non-reference.

> lvalue -> lvalue reference
> rvalue -> non-reference

```cpp
class Widget
{
    int a;
};

Widget widgetFactory()
{
    return Widget();
}

template <typename T>
void func(T&& param)
{
    std::cout << __FUNCSIG__ << '\n';
}

int main(int argc, const char* argv[])
{
    Widget w;
    func(w); // T is deduced to be Widget&

    func(widgetFactory()); // T is deduced to be Widget
}
```

output

```cpp
void __cdecl func<classWidget&>(class Widget &)
void __cdecl func<class Widget>(class Widget &&)
```

If we take the type deduced for T(i.e., Widget&) and use it to instantiate the template we get this:

```cpp
void func(Widget& && param);
```

When the compiler generates references to references, reference collapsing dectates what happens next.

There are two kinds of references, so there are four possible reference-reference combinations
* lvalue to lvalue
* lvalue to rvalue
* rvalue to lvalue
* rvalue to rvalue

### Reference collapsing rule
> if either of the reference is an lvalue reference, the result is an lvalue reference. Otherwise, the result is an rvalue reference.

So, in the above example, it becomes an lvalue reference.

A common use case of std::forward looks like this

```cpp
template <typename T>
void f(T&& param)
{
	...
	someFunc(std::forward<T>(param)); 
}
```

std::forward's job is to cast param(an lvalue) to an rvalue if and only if T encodes that the argument passed to f was an rvalue, i.e., if T is a non-reference type.

std::forward's implementation

```cpp
template <typename T>
T&& forward(typename remove_reference<T>::type& param)
{
	return static_cast<T&&>(param);
}
```

Suppose the arguments passed to f is an lvalue of type Widget. T will deduced as Widget&, and the call to std::forward will instantiate as std::forward<Widget&>. Plugging Widget& into the std::forward implementation yields this:

```cpp
Widget& && forward(typename remove_reference<Widget&>::type& param)
{
	return static_cast<Widget& &&>(param);
}
```

remove_reference<Widget&>::type yields Widget.

```cpp
Widget& && forward(Widget& param)
{
	return static_cast<Widget& &&>(param);
}
```

After reference collapsing

```cpp
Widget& forward(Widget& param)
{
	return static_cast<Widget&>(param);
}
```

Here, you see an lvalue reference to std::forward yields lvalue reference.

Suppose the arguments passed to f is an rvalue of type Widget. T will be deduced as Widget. The call to std::forward will be instantiated as std::forward<Widget>. Plugging Widget into the std::forward implementation yields this:

```cpp
Widget&& forward(typename remove_reference<Widget>::type& param)
{
	return static_cast<Widget&&>(param);
}
```

remove_reference<Widget>::type yields same Widget.

```cpp
Widget&& forward(Widget param)
{
	return static_cast<Widget&&>(param);
}
```
There is no reference to references here, so no reference collapsing rule. So, final version is same. 

Rvalue references returned from functions are defined to be rvalues. So in this case, std::forward will turn f's parameter param(an lvalue) into an rvalue. 

### Reference collapsing in auto

```cpp
auto&& w1 = w;
```

initializes w1 with an lvalue, thus deducing the type Widget& for auto. Plugging Widget& in for auto in the declaration for w1 yields this reference-to-reference code.

```cpp
Widget& && w1 = w;
```

After reference collapsing, it becomes

```cpp
Widget& w1 = w;
```

So, w1 becomes an lvalue reference.

For this declaration,

```cpp
auto&& w2 = widgetFactory();
```

initializes w2 with an rvalue, causing the non-reference type Widget to be deduced for auto. 

```cpp
Widget&& w2 = widgetFactory();
```

There is no reference collapsing here, so w2 is an rvalue reference.

### Universal References

An universal reference is an rvalue reference in a context where two conditions are satisfied.

* Type deduction distinguishes lvalues from rvalues. Lvalue of type T are deduced to have type T&, while rvalues of type T yield T as their deduced type.
* Reference collapsing occurs.

### typedefs and alias declarations

```cpp
template <typename T>
class Widget {
public:
	typedef T&& RvalueRefToT;
	...
};
```

Suppose we instantiate Widget with an lvalue reference type.

```cpp
Widget<int&> w;
```

Substituting int& for T in the Widget template gives us the following typedef.

```cpp
typedef int& && RvalueRefToT;
```

Reference collapsing reduces it to this,

```cpp
typedef int& RvalueRefToT;
```
so RvalueRefToT is an lvalue reference.

### decltype

like above




