
```cpp
int x(0); // initializer is in parentheses
int y = 0; // initializer follows "="
int z{ 0 }; // initializer is in braces
int z = {0}; // initializer uses "=" and braces
```
Uniform initialization:  a single initialization syntax that can, at least in concept, be used anywhere and expressed everything. It's based on braces(braced initialization). Braced initialization is a syntactic construct and it can be used everywhere.

```cpp
std::vector<int> v{1, 3, 5};
```

Using braces, specifying the initial contents of a container is easy.

```cpp
class Widget {
    ...
private:
    int x{ 0 }; // x's default value is 0
    int y = 0; // also fine
    int z(0); // error!!
};
```

Braces can also be used to specify default initialization values for non-static data members.

```cpp
std::atomic<int> ai1{ 0 }; // fine
std::atomic<int> ai2(0); // fine
std::atomic<int> ai3 = 0; // error!
```

Uncopyable objects may be initialized using braces or parentheses, but not using "=".

```cpp
double x, y, z;
...
int sum1{ x + y + z }; // error sum of doubles may not be expressible as int
int sum2(x + y + z); // okay(value of expression truncated to an int)
int sum3 = x + y + z;  // ditto
```

Braced initialization prohibits implicit narrowing conversion among built-in types.  If the value of an expression in a braced initializer isn't guranteed to be expressible by the type of the object being initialized, compilers are required to complain.

Braces initialization is immune to C++ most vexing parse. A side effect of C++'s rule that anything that can be parsed as a declaration must be interpreted as one, the most vexing parse most frequently afflicts developers when they want to default-construct an object, but inadvertently end up declaring a function instead. 

```cpp
Widget w1(10); // call Widget ctor with argument 10
Widget w2(); // most vexing parse! declares a function named w2 that returns a widget!
Widget w3{}; // calls widget ctor with no args, functions can't be declared using braces for the parameter list, so default constructing an object using braces doesn't have issue.
```

Braced initialization shows strange behavior due to tangled relationships between std::initializer_list, braced initialization and constructor overload resolutions.

If an auto declared variable has a braced initialization, the type deduced is std::initializer_list. 

In constructor calls, parentheses and braces have the same meaning as long as std::initializer_list parameters are not involved.

```cpp
class Widget
{
public:
    Widget(int i, bool b); // ctors not declaring
    Widget(int i, double d); // std::initializer_list params
    ...
};

Widget w1(10, true); // calls first ctor
Widget w2{10, true}; // calls first ctor
Widget w3(10, 5.0); // calls second ctor
Widget w4{10, 5.0}; // calls second ctor
```

If one or more constructors declare a parameter of type std::initializer_list, calls using the braced initialization syntax strongly prefer the overloads  taking std::initializer_lists. 
If there's any way for compilers to construe a call using a braced initializer to be a constructor taking a std::initializer_list, compiler will employ that interpretation.

```cpp
class Widget
{
public:
    Widget(int i, bool b); // ctors not declaring
    Widget(int i, double d); // std::initializer_list params
    Widget(std::initializer_list<long double> il);   // real std::initializer_list as a ctor param
  ...
};

Widget w1(10, true); // calls first ctor
Widget w2{10, true}; // calls std::initializer_list ctor, 10 and true convert to long double
Widget w3(10, 5.0);  // calls second ctor
Widget w4{10, 5.0};  // calls std::initializer_list ctor, 10 and 5.0 convert to long double
```

Even what would normally be copy and move construction can be hijacked by std::initializer_list constructors.

```cpp
#include <iostream>
 
class Widget
{
public:
    Widget(int i, bool b)
    {
        std::cout << "Widget(int i, bool b)" << std::endl;
    }

    Widget(int i, double d)
    {
        std::cout << "Widget(int i, double d)" << std::endl;
    }
    
    Widget(std::initializer_list<long double> il)
    {
        std::cout << "Widget(std::initializer_list<long double> il)" << std::endl;
    }

    operator float() const
    {
        std::cout << "operator float()" << std::endl;
        return 1.0f;
    }

    Widget(const Widget& w)
    {
        std::cout << "Widget(const Widget& w)" << std::endl;
    }
  
    Widget(Widget&& w)
    {
        std::cout << "Widget(Widget&& w)" << std::endl;
    }
};
  
int main()
{
    Widget w4{ 10, 5.0 };
    Widget w5(w4);
    Widget w6{w4};
    Widget w7{std::move(w4)};
}

// op
Widget(std::initializer_list<long double> il)
Widget(const Widget& w)
operator float()
Widget(std::initializer_list<long double> il)
```

In GCC, here w4 converts to float and float converts to long double in std::initializer_list(neither copy nor move constructor is called). MSVC and Clang don't show this behavior.


Compiler's determination to match braced initializers with constructors taking std::initializer_lists is so strong, it prevails even if the best-match std::initializer_list constructors can't be called.

```cpp
class Widget
{
public:
    Widget(int i, bool b); // ctors not declaring
    Widget(int i, double d); // std::initializer_list params
    Widget(std::initializer_list<bool> il);   // real std::initializer_list as a ctor param
       ...
};

Widget w4{10, 5.0};  // error, requires narrowing conversions
```

The compiler will ignore the first two constructors(the second of which is offers an exact match on both argument types) and try to call the constructor taking a std::initializer_list<bool>. 

Only if there is no way to convert the types of the arguments in a braced initializer to the type in a std::initializer_list do compilers fall back on normal overload resolution. 

```cpp
class Widget
{
public:
    Widget(int i, bool b);
    Widget(int i, double d);
    Widget(std::initializer_list<std::string> il);   // real std::initializer_list as a ctor param
  ...
};

Widget w1(10, true); // calls first ctor
Widget w2{10, true}; // calls first ctor
Widget w3(10, 5.0); // calls second ctor
Widget w4{10, 5.0}; // calls second ctor
```

Empty braces mean no arguments, not an empty std::initializer_list. So, default constructor is always called. If you want to call a std::initializer_list constructors with an empty std::initializer_list, yo do it by making the empty braces a constructor argument.

```cpp
class Widget
{
public:
       Widget()
       {
              std::cout << "Widget()" << std::endl;
       }
       Widget(std::initializer_list<int> il)
       {
              std::cout << "Widget(std::initializer_list<bool> il)" << std::endl;
       }
};
int main()
{
       Widget w; // calls default constructor
       Widget w2{}; // calls default constructor
       Widget w3({}); // calls std::initializer_list ctor with empty list
}


int main()
{
       std::vector<int> v1(10, 20); // creates 10-element and all elements have value 20
       std::vector<int> v2{ 10, 20 }; // creates 2-element of value 10 and 20
       std::cout << "v1: " << v1.size() << std::endl;
       std::cout << "v2: " << v2.size() << std::endl;
}
```

If you need to be aware that if your set of overloaded constructors includes one or more functions taking std::initializer_list, client code using braced initialization may see only the std::initializer_list overloads. It's best to design your constructors so that the overload called isn't affected by whether clients use parentheses or braces. 

The difference with std::initializer_list constructor overloads is that a std::initializer_list overload doesn't just complete with other overloads, it overshadows them to the point where the other overloads may be hardly be considered. 
