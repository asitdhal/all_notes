In C++, atom of api design is *Overload sets*.

### Overload sets

> An overload set is a collection of functions in the same scope(namespace. class, etc.) of the same name such that if any is found by name resolution they all will be. 

#### Per C++ Guideline
C.162: Overload operations that are roughly equivalent: if they name the samething, they are doing the same thing.
C.163: Overload only for operations that are roughly equivalent.

#### Per Google C++ Style
Use overloaded functions (including constructors) only if a reader looking at a call site can get a good idea of what is happening without having to first figure out exactly which overload is being called.

#### Properties of a good overload set
* Correctness can be judged at the call site without knowing which overload is picked.
* A single comment can describe the full set.
* Each element of the set is doing "the same thing".

#### Good overload sets

* Varied arity
```cpp
std::string s1 = absl::StrCat("hello", name);
std::string s2 = absl::StrCat("hello", name, " ", n, " times");
```

* Varied(related) type
```cpp
void Foo(const char* s);
void Foo(const std::string& s) { Foo(s.c_str());}
```

* optimization: same semantics, same post conditions(on the vector)
```cpp
void vector<T>::push_back(const T&);
void vector<T>::push_back(T&&);

v.push_back("hello");
v.push_back(std::move(str));
```

Bad Example
```cpp
// If calling the version that takes int, return n+5
// otherwise, prints a friendly message
void BadOverload(int n);
void BadOverload(const std::string& s);
```

Most important overload set: copy + move. Because move is an optimization of copy.

When viewing your constructor as an overload set, we start to have a better idea of when "explicit" applies. Explicit should be used when the reader of that call site need to know that a (non-copy) constructor was called.

```cpp
void Fizz(const Foo& f);
void Fizz(const Bar& b);

class Foo {
public:
	Foo(const Foo& f);
	Foo(const Bar& b);
}
```

Here both Foo and Bar represent same plutonic idea. So, it's okay to not to provide explicit.

```cpp
void FizzFoo(const Foo& f);
void FizzBar(const Bar& b);

class Foo {
public:
	Foo(const Foo& f);
	explicit Foo(const Bar& b);
}
```

We can construct Foo from a bag of parameters, we should provide an explicit.

Don't use =delete in an overload set to describe lifetime requirements. Instead document requirements.

```cpp
Foo(const std::string& s); // s must live the next call
Foo(std::string&& s) = delete; // no temporaries
```

There is an API called DNAScan which only takes a string by move. 

```cpp
future<bool> DNAScan(Config, const std::string&) = delete;
future<bool> DNAScan(Config, std::string&&);
```
This is a bad API design. Because you can't predict all invocations. You can't know if copy will ever be used. Instead you should provide a type class.


### Sinks: To Overload or Not To Overload

Sink: accepting a T and storing it

How do I express a sink parameter ?

* Is this a generic, or am I sinking a specific type ?
* For the type(s) being sunk, how expensive is the function compared to a copy or move for that type ?
* Are there multiple parameters being sunk ?
* Do I know that it will always be a sink of exactly T or anything that converts to T ?
* Could allocation reuse dominate ?

const T& + T&& | T | const T&
---------------|---|---------
If the implementation is small compared to move constructing T, probably a good design. | If the implementation is likely larger cost than move-constructing T, probably good. | If the implementation is likely larger cost than copy-constructing T, probably good.
More Complex. Worse error message. Worse compilation performance. Combinitorial(in case of multiple parameters) | Must be sure that this will always be a sink for (exactly) T (not convertible to T). | Simple, understood, flexible.

### Non-Sink Overloads

* const char* + const std::string&
* std::string_view
* std::span (since c++20)

