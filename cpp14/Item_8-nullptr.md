The uncertainty regarding the behavior of f(NULL) is a reflection of the leeway granted to implementation regarding the type of NULL. If NULL is defined to be, say 0L(i.e., 0 as long), the call is ambiguous, because conversion from long to int, long to bool, and 0L to void* are considered equally good. The interesting thing about that call is the contradiction between the apparent meaning of the source code(calling f with NULL) and its actual meaning(calling f with some kind of integer). 

nullptr's advantage is that it doesn't have an integral type. To be honest, it doesn't have a pointer type, either, but you can think of it as a pointer of all types. nullptr's actual type is std::nullptr_t, and, in a wonderfully circular definition, std::nullptr_t is defined to be the type of nullptr. The type std::nullptr_t implicitly converts to all raw pointer types, and that's what makes nullptr act as if it were a pointer of all types.

Calling the overloaded function f with nullptr calls the void* overload, because can't be viewed as anything integral.


```cpp
void f(void* p)
{
       std::cout << "f(void* p)" << std::endl;
}
/*void f(int* p)
{
       std::cout << "f(int* p)" << std::endl;
}*/

int main()
{
       f(nullptr);
       int *p = new int;
       f(p);
}
```

```cpp
int f1(std::shared_ptr<Widget> spw); 
int f2(std::unique_ptr<Widget> upw);
int f3(Widget* pw);

template <typename FuncType, typename MuxType, typename PtrType>
decltype(auto) lookAndCall(FuncType func, MuxType& mutex, PtrType ptr) // c++14
{
    using MuxGuard = std::lock_guard<MuxType>;
    MuxGuard g(mutex);
    return func(ptr);
}

std::mutex f1m, f2m, f3m;

auto lookAndCall(f1, f1m, 0); // error
auto lookAndCall(f2, f2m, NULL); //error
auto lookAndCall(f3, f3m,  nullptr); // fine

```

In the first case, type of 0 is int. int is not compartible with std::shared_ptr. So, the code is rejected.
In the second case, type of NULL is also int.
In the third case, type is nullptr_t. Implicit conversion is possible.