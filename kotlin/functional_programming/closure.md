* When a subprogram can be nested, in addition to locals and globals, the referencing environment of a subprogram can include variables defined in all enclosing subprograms. This is not an issue if the subprogram can be called only in places where all of the enclosing scopes are active and visible.

* The problem arises when the subprogram can be passes as a parameter or assigned to a variable, thereby allowing it to be called from virtually anywhere in the program.  

*  A closure is a subprogram that references environment where it was defined. The referencing environment is needed if the subprogram can be called from any arbitary place in the program. 

* A closure is a persistent scope which holds on to local variables even after the code execution has moved out of that block. The scope object, and all its local variables, are tied to the function, and will persist as long as that function persists.

```kotlin
fun main(args: Array<String>) {

    val f = {
        var a = 1
        var inner = fun() {
            a++
            println("a = $a")
        }
        inner
    }

    val f2 = f()
    f2() // 2
    f2() // 3

    val f3 = f()
    f3() // 2
    f3() // 3
}

```
