#### Function Literals
* You can declare a function literal using curly braces.

```kotlin
fun main(args: Array<String>) {
    val p = { println("I am a function literal") }
    p();
}
```

* Function litearal can take arguments.
```kotlin
fun main(args: Array<String>) {
    val printMessage = { message: String -> println(message) }
    printMessage("hello")
    printMessage("world")
} 
```

* When a function literal is used in a place where the compiler can figure out the parameter type, we can omit the types.

* If there is only a single parameter and the type can be inferred, then the compiler will allow us to omit the parameter completely.

#### Lambda Expression
* A lambda expression or an anonymous function is a "function literal", i.e. a function that is not declared, but passed immediately as an expression.

```kotlin
listOf(1, 2, 3).filter { it > 1 } 

fun(a: String, b: String): String = a + b 

val ints = listOf(1, 2, 3)      
val evens = ints.filter(fun(k: Int) = k % 2 == 0) 

```
