* Named parameters allow us to be explicit about naming arguments when passed to a function. This has the benefit that for functions with many parameters, explicit naming makes the intent of each argument clear. This makes the call site more readable.

* To use named parameters, we put the parameter name before the argument value. Here is the function call again, this time with named parameters:

```kotlin
string.regionMatches(thisOffset = 14, other = "Red Ravens",  otherOffset = 4, length = 6, ignoreCase = true)
```

* Advantages
  * Readable
  * less error prone
  
* once a parameter has been named, all the following parameters must be named too.
* Named parameters also allow the parameter order to be changed to suit the caller.
