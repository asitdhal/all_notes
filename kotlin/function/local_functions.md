*  The functions declared inside other functions are called local or nested functions. Functions can even be nested multiple times.

```kotlin
fun fizzbuzz4(start: Int, end: Int): Unit {
  for (k in start..end) {
    fun isFizz(): Boolean = k % 3 == 0
    fun isBuzz(): Boolean = k % 5 == 0
    
    when {
      isFizz() && isBuzz() -> println("Fizz Buzz")
      isFizz() -> println("Fizz")
      isBuzz() -> println("Buzz")
      else -> println(k)
    }
  }
} 
```
