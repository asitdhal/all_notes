* A function must declare its return type; an exception exists only for functions that consist of a single expression. These are often referred to as one line or single line functions. Such functions can use a shortened syntax that omits the braces and uses the = symbol before the expression rather than the return keyword.

```kotlin
fun square(k: Int) = k * k 
```

* Return type is inferred by the compiler. The rationale behind this feature is that very short functions are easy to read, and the return value is a bit of extra noise that doesn't add much to the overall process. However, you can always include the return value if you think it makes things clearer.

```kotlin
fun square2(k: Int): Int = k * k 
```
