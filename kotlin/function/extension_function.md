* An extension function is declared by defining a top-level function as normal, but with the intended type prefixed before the function name. The type of the instance that the function will be used on is called the receiver type. The receiver type is said to be extended with the extension function.

```kotlin
fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
    val tmp = this[index1]
    this[index1] = this[index2]
    this[index2] = tmp
}

fun main(args: Array<String>) {
    val l = mutableListOf(1, 2, 3, 4, 5)
    println(l)
    l.swap(2, 3)
    println(l)

}
```
```
[1, 2, 3, 4, 5]
[1, 2, 4, 3, 5]
```

Here this is used to reference the receiver instance, that is, the object that the function was invoked on. Whenever we are inside an extension function, the this keyword always refers to the receiver instance, and the instances in the outer scope need to be qualified.

> Extensions do not actually modify classes they extend. By defining an extension, you do not insert new members into a class, but merely make new functions callable with the dot-notation on variables of this type. Extension functions are dispatched statically(they are not virtual by receiver type.).

```kotlin
open class C
class D: C()
fun C.foo() = "c function"
fun D.foo() = "d function"
fun printFoo(c: C) {
    println(c.foo())
}
fun main(args: Array<String>) {
    printFoo(D())
}
```

```
c function
``` 

In the above example, if you comment out extension function for C, it will produce error.

##### Extension function precedence
* Extension functions cannot override functions declared in a class or interface. If an extension function is defined with the exact same signature (the same name, parameters type and order, and return type), then the compiler will never invoke it.

* During compilation, when the compiler finds a function invocation, it will first look for a match in the member functions defined in the instance type as well as any member functions defined in superclasses and interfaces. If a match is found, then that member function is the one that is bound.

* Only if no matching member functions are found, the compiler will consider any extension imports in the scope. Consider the following definitions:

> So, if an extension function is defined on a class and it has same name, it must have different signature.

````kotlin
class Submarine {       
  fun fire(): Unit {         
    println("Firing torpedoes")
  }
  
  fun submerge(): Unit {
    println("Submerging")
  }
}

fun Submarine.fire(): Unit {
  println("Fire on board!")
}

fun Submarine.submerge(depth: Int): Unit {
  println("Submerging to a depth of $depth fathoms")
} 

fun main(args: Array<String>) {
  val sub = Submarine() 
  sub.fire()  //call member function
  sub.submerge()  //call member function
  sub.submerge(10)  //call exension function
}
```

#### Nullable Receiver
*  Extensions can be defined with a nullable receiver type. Such extensions can be called on an object variable even if its
value is null, and can check for this == null inside the body. 

```kotlin
class AnyClass { }

fun AnyClass?.prettyString(): String {
    if (this == null) return "no data"
    // after the null check, ' this' is autocast to a non-null type, so the toString() below
    // resolves to the member function of the Any class
    return toString()
}



fun main(args: Array<String>) {
    val p :AnyClass? = null
    println(p.prettyString())
}
```

#### Declaring Extensions as Members
* Inside a class, you can declare extensions for another class. Inside such an extension, there are multiple implicit receivers objects members of which can be accessed without a qualifier. The instance of the class in which the extension is declared is called *dispatch receiver*, and the instance of the receiver type of the extension method is called *extension receiver*.

```kotlin
class D {
    fun bar() { println("D::bar()")}
}

class C {
    fun baz() { println("C::baz()") }

    fun D.foo() {
        bar() // calls D. bar
        baz() // calls C. baz
    }
    fun caller(d: D) {
        d.foo() // call the extension function
    }
}

fun main(args: Array<String>) {
    val c = C()
    val d = D()
    c.caller(d)
}
```

```
D::bar()
C::baz()
```

* In case of a name conflict between the members of the dispatch receiver and the extension receiver, the extension receiver
takes precedence. To refer to the member of the dispatch receiver you can use the qualified this syntax.

```kotlin
class D {
    fun baz() { println("D::baz()") }
}

class C {
    fun baz() { println("C::baz()") }

    fun D.foo() {
        baz() // calls D.baz
        this@C.baz() //calls C.baz
    }
    fun caller(d: D) {
        d.foo() // call the extension function
    }
}

fun main(args: Array<String>) {
    val c = C()
    val d = D()
    c.caller(d)
}
```

```
D::baz()
C::baz()
```

* Extensions declared as members can be declared as open and overridden in subclasses. This means that the dispatch of such functions is virtual with regard to the dispatch receiver type, but static with regard to the extension receiver type.

```kotlin
open class D { }

class D1 : D() { }

open class C {
    open fun D.foo() {
        println("D.foo in C")
    }
    open fun D1. foo() {
        println("D1.foo in C")
    }
    fun caller(d: D) {
        d.foo() // call the extension function
    }
}

class C1 : C() {
    override fun D.foo() {
        println("D.foo in C1")
    }
    override fun D1. foo() {
        println("D1.foo in C1")
    }
}

fun main(args: Array<String>) {
    C(). caller(D()) // prints "D. foo in C"
    C1(). caller(D()) // prints "D. foo in C1" - dispatch receiver is resolved virtually
    C(). caller(D1()) // prints "D. foo in C" - extension receiver is resolved statically
}
```

```
D.foo in C
D.foo in C1
D.foo in C
```
