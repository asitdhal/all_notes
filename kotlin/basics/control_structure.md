#### If-Expression
* It Kotlin, **if** is an expression. It returns a value.

```kotlin
var max = a
if (a < b) max = b

// With else
var max: Int
if (a > b) {
    max = a
} else {
    max = b
}

// As expression
val max = if (a > b) a else b

```

* if branches can be blocks, and the last expression is the value of a block.

```kotlin
val max = if (a > b) {
    print("Choose a")
    a
} else {
    print("Choose b")
    b
}
``` 

* If you're using if as an expression rather than a statement (for example, returning its value or assigning it to a variable), the expression is required to have an else branch.

### When Expression
* when is like the switch operator of C-like languages.

```kotlin
when (x) {
    1 -> print("x == 1")
    2 -> print("x == 2")
    else -> { // Note the block
        print("x is neither 1 nor 2")
    }
}
```

* when matches its argument against all branches sequentially until some branch condition is satisfied. 

* when can be used either as an expression or as a statement. If it is used as an expression, the value of the satisfied branch becomes the value of the overall expression. If it is used as a statement, the values of individual branches are ignored.

* The else branch is evaluated if none of the other branch conditions are satisfied. If when is used as an expression, the else
branch is mandatory, unless the compiler can prove that all possible cases are covered with branch conditions.

* If many cases should be handled in the same way, the branch conditions may be combined with a comma.
```kotlin
when (x) {
    0, 1 -> print("x == 0 or x == 1")
    else -> print("otherwise")
}
```

* arbitary expression in when
```kotlin
when (x) {
    parseInt(s) -> print("s encodes x")
    else -> print("s does not encode x")
}
```

* when can be used to check a value for being in or ! in a range or a collection.
```kotlin
when (x) {
    in 1. . 10 -> print("x is in the range")
    in validNumbers -> print("x is valid")
    !in 10. . 20 -> print("x is outside the range")
    else -> print("none of the above")
}
```

* when can be used to check that a value is or ! is of a particular type. 
```kotlin
val hasPrefix = when(x) {
    is String -> x. startsWith("prefix")
    else -> false
}
```

* when can also be used as a replacement for an if-else if chain. 
```kotlin
when {
    x. isOdd() -> print("x is odd")
    x. isEven() -> print("x is even")
    else -> print("x is funny")
}
```

#### Loop 
* for loop iterates through anything that provides an iterator. The for loop body can be a block.
```kotlin
for (item in collection) print(item)

for (item: Int in ints) {
    // . . .
}
```

* for iterates through anything that provides an iterator, i.e.
    * has a member- or extension-function iterator() , whose return type
        * has a member- or extension-function next() , and
        * has a member- or extension-function hasNext() that returns Boolean .
* All above three functions need to be marked as operator .

* A for loop over an array is compiled to an index-based loop that does not create an iterator object. If you want to iterate through an array or a list with an index, you can do it this way:
```kotlin
for (i in array. indices) {
    print(array[i] )
}
```
* Or, withIndex library function:
```kotlin
for ((index, value) in array. withIndex()) {
    println("the element at $index is $value")
}
```

```kotlin
val list = listOf(1, 2, 3, 4) 
for (k in list) { 
    println(k) 
} 
 
val set = setOf(1, 2, 3, 4) 
for (k in set) { 
    println(k) 
} 
```

* Or, repeat an action x times
```kotlin
repeat(10) { i ->
    println("This line will be printed 10 times")
    println("We are on the ${i + 1}. loop iteration")
}
```

#### While
* same as while and do-while in any languages

```kotlin
while (x > 0) {
    x--
}
do {
    val y = retrieveData()
} while (y ! = null) // y is visible here!
```

#### Break and Continue
* Both break and continue constructs work same as traditional languages.

#### Labels 
* Labels have the form of an identifier followed by the @ sign, for example: abc@ , fooBar@ are valid labels (see the grammar). To label an expression, we just put a label in front of it.
```kotlin
fun main(args: Array<String>) {
    loop@ for (i in 1..3) {
        for (j in 1..7) {
            println("innser loop $i $j")
            if (j == 5) break@loop
            if (j == 3) continue@loop

        }
    }
}
```
```
innser loop 1 1
innser loop 1 2
innser loop 1 3
innser loop 2 1
innser loop 2 2
innser loop 2 3
innser loop 3 1
innser loop 3 2
innser loop 3 3
```