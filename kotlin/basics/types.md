#### Numbers
Kotlin handles numbers in a way close to Java, but not exactly the same. For example, there are no implicit widening conversions for numbers, and literals are slightly diﬀerent in some cases. Kotlin provides the following built-in types representing numbers (this is close to Java): 

| Type  | Bit width
|-------|----------
| Double | 64 
| Float | 32
| Long | 64 
| Int | 32 
| Short | 16 
| Byte | 8


#### Literal Constants 

* There are the following kinds of literal constants for integral values: 
    * Decimals: 123 
        * Longs are tagged by a capital L: 123L 
    * Hexadecimals: 0x0F 
    * Binaries: 0b00001011 
    * Octal literals are not supported. 
    
* Kotlin also supports a conventional notation for ﬂoating-point numbers
    * Doubles by default: 123.5, 123.5e10 Floats are tagged by f or F: 123.5f

* Underscores in numeric literals 

    ```kotlin
    val oneMillion = 1_000_000 
    val creditCardNumber = 1234_5678_9012_3456L 
    val socialSecurityNumber = 999_99_9999L 
    val hexBytes = 0xFF_EC_DE_5E 
    val bytes = 0b11010010_01101001_10010100_10010010
    ```

#### Representation

* On the Java platform, numbers are physically stored as JVM primitive types, unless we need a nullable number reference (e.g. Int?) or generics are involved. In the latter cases numbers are boxed. 

*  Boxing of numbers does not necessarily preserve identity, but it preserves equality.

    ```kotlin
    fun main(args : Array<String>) {
        val a: Int = 10000
        println(a === a) // Prints 'true'
        println(a == a) // Prints 'true'
        val boxedA: Int? = a
        val anotherBoxedA: Int? = a
        println(boxedA === anotherBoxedA) // !!!Prints 'false', identity lost
        println(boxedA == anotherBoxedA) // !!!Prints 'true', equality preseved
    }
    ```

#### Explicit Conversions 

* Due to diﬀerent representations, smaller types are not subtypes of bigger ones. If they were, we would have troubles.So not only identity, but even equality would have been lost silently all over the place. As a consequence, smaller types are NOT implicitly converted to bigger types. This means that we cannot assign a value of type Byte to an Int variable without an explicit conversion
    ```kotlin
    fun main(args : Array<String>) {
        val b: Byte = 1 // OK, literals are checked statically 
        val i: Int = b // ERROR
        // We can use explicit conversions to widen numbers
        val i: Int = b.toInt() // OK: explicitly widened
    }
    ```
* Every number type supports the following conversions.
    * toByte(): Byte 
    * toShort(): Short 
    * toInt(): Int 
    * toLong(): Long 
    * toFloat(): Float 
    * toDouble(): Double 
    * toChar(): Char


#### Operations
Kotlin supports the standard set of arithmetical operations over numbers, which are declared as members of appropriate classes (but the compiler optimizes the calls down to the corresponding instructions).  As of bitwise operations, there're no special characters for them, but just named functions that can be called in inﬁx form, for example:
    
   ```kotlin
   val x = (1 shl 2) and 0x000FF000
   ```

Here is the complete list of bitwise operations (available for Int and Long only): 
| operator | description
|----------|------------
|shl(bits) | signed shift left (Java's <<) 
|shr(bits) | signed shift right (Java's >>)
|ushr(bits)| unsigned shift right (Java's >>>)
|and(bits) | bitwise and 
|or(bits)  | bitwise or 
|xor(bits) | bitwise xor 
|inv() | bitwise inversion

#### Characters
* Characters are represented by the type Char. They can not be treated directly as numbers
    ```kotlin
    fun check(c: Char) {
        if (c == 1) { // ERROR: incompatible types 
            // ...    
        }
    }
    ```
* Character literals go in single quotes: '1'. Special characters can be escaped using a backslash. The following escape sequences are supported: \t, \b, \n, \r, \', \", \\ and \$. To encode any other character, use the Unicode escape sequence syntax: '\uFF00'.

* We can explicitly convert a character to an Int number:

    ```kotlin
    fun decimalDigitValue(c: Char): Int {
        if (c !in '0'..'9') 
            throw IllegalArgumentException("Out of range")
        return c.toInt() - '0'.toInt() // Explicit conversions to numbers
    }
    ```
* Like numbers, characters are boxed when a nullable reference is needed. Identity is not preserved by the boxing operation.

#### Booleans

* The type Boolean represents booleans, and has two values: true and false. 
* Booleans are boxed if a nullable reference is needed. 
* Built-in operations on booleans include 

    | operator | description
    |----------|------------
    |```||```  | lazy disjunction
    | ```&&``` | lazy conjunction 
    |```!``` | negation



 





