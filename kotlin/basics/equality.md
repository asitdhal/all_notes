#### Referential Equality
* When two separate references point to the exact same instance in memory, it's called referential equality.
* operator :  === operator (triple equals) or !== for negation
```kotlin
import java.io.File

fun main(args: Array<String>) {
    val a = File("/mobydick.doc")
    val b = File("/mobydick.doc")
    val sameRef = a === b
    println(sameRef)
}
```
Output
```
false
```

#### Structural Equality
* when two objects are separate instances in memory but have the same value, it's called structural equality.
* operator:  == operator or != for negation

```kotlin
import java.io.File

fun main(args: Array<String>) {
    val a = File("/mobydick.doc")
    val b = File("/mobydick.doc")
    val sameRef = a == b
    println(sameRef)
}
```

Output
```
true
```
