* The designers of Kotlin have decided the default behavior is to have the classes sealed for inheritance. So, there is a new keyword ```open``` which makes the class inheritable.

```kotlin
enum class CardType {       
  VISA, MASTERCARD, AMEX     
}      

open class Payment(val amount: BigDecimal)
class CardPayment(amount: BigDecimal, val number: String, val  expiryDate: DateTime, val type: CardType) : Payment(amount) 

class ChequePayment : Payment {       
  constructor(amount: BigDecimal, name: String, bankId: String) :  super(amount) {
    this.name = name        
    this.bankId = bankId
  }
  
  var name: String
  get() = this.name
  var bankId: String
  get()  = this.bankId
  
} 
```

* The open annotation on a class is the opposite of Java's final: it allows others to inherit from this class. By default, all classes in
Kotlin are final.

* If the class has a primary constructor, the base type can (and must) be initialized right there, using the parameters of the primary
constructor. If the class has no primary constructor, then each secondary constructor has to initialize the base type using the super
keyword, or to delegate to another constructor which does that. Note that in this case different secondary constructors can call
different constructors of the base type.

```kotlin
open class Base(p: Int)
class Derived(p: Int) : Base(p)
class AnotherDerived : Base {
  constructor(i: Int) : super(i)
}
```
* We can't inherit from more than one class(like Java).

* Every time you construct an entity that doesn't take any parent, it will automatically get Any as its parent. You will probably think Any is the Object class, the super/parent class of any class defined in Java. However, this is not the case. If you pay attention to the methods defined by the class Any you will notice it is a subset of those found for on the Java Object class.

* Adding the abstract keyword in front of the class definition will mark the class as abstract. An abstract class is a partially defined class; properties and methods that have no implementation must be implemented in a derived class, unless the derived class is meant to be an abstract class as well.

```kotlin
abstract class A {       
  abstract fun doSomething()     
} 
```


* Inheritance in case of interface

```kotlin
interface Drivable { 
  fun drive() 
} 

interface Sailable { 
  fun saill() 
} 

class AmphibiousCar(val name: String) : Drivable, Sailable { 
  override fun drive() { 
    println("Driving...") 
  } 
  override fun saill() { 
    println("Sailling...") 
  } 
} 
```

* There is no restriction on how many interfaces you can inherit from and the order in which you want to specify them.

#### Overriding Rules
* if a class inherits many implementations of the same
member from its immediate superclasses, it must override this member and provide its own implementation (perhaps, using
one of the inherited ones). To denote the supertype from which the inherited implementation is taken, we use super qualified
by the supertype name in angle brackets.

```kotlin
open class A {
    open fun f() { print("A") }
    fun a() { print("a") }
}

interface B {
    fun f() { print("B") } // interface members are ' open' by default
    fun b() { print("b") }
}

class C() : A(), B {
    // The compiler requires f() to be overridden:
    override fun f() {
        super<A>. f() // call to A. f()
        super<B>. f() // call to B. f()
    }
}

fun main(args: Array<String>) {
    val c = C()
    c.f()
}
```

#### Polymorphism
