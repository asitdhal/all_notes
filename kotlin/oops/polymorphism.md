* The power of polymorphism comes at runtime when objects of a derived class are treated as objects of the base class. This can happen for a method parameter or when it comes to storing a group of common elements in a collection or array. The peculiar thing here is that the object's declared type will not be identical with the actual runtime type when the code is executed. This sounds like there is some magic happening under the hood. All of this is happening through the use of virtual methods. Base classes may define and implement virtual methods, and derived classes can override them, thus providing their own implementation. This way, two distinct types behave differently when the same method is called. When the virtual method is called as your program is executed, the JVM looks up the runtime type of the instance and works out which method it should actually invoke. Later in the chapter, we will dedicate some space to discuss in a bit more detail how this is implemented under the bonnet.

```kotlin
abstract class Shape protected constructor() {       
  var XLocation: Int
  get() = this.XLocation
  set(value: Int) {
    this.XLocation = value
  }
  
  var YLocation: Int
  get() = this.XLocation
  set(value: Int) {
    this.XLocation = value
  }
  
  var Width: Double
  get() = this.Width
  set(value: Double) {
    this.Width = value
  }
  
  var Height: Double
  get() = this.Height
  set(value: Double) {
    this.Height = value
  }
  
  abstract fun isHit(x: Int, y: Int): Boolean
} 

class Ellipsis : Shape() {
  override fun isHit(x: Int, y: Int): Boolean {
    val xRadius = Width.toDouble / 2
    val yRadius = Height.toDouble / 2
    val centerX = XLocation + xRadius
    val centerY = YLocation + yRadius
    if (xRadius == 0.0 || yRadius == 0.0)
      return false
    val normalizedX = centerX - XLocation
    val normalizedY = centerY - YLocation
    return (normalizedX * normalizedX) / (xRadius * xRadius) +  (normalizedY * normalizedY) / (yRadius * yRadius) <= 1.0
  }
}

class Rectangle : Shape() {
  override fun isHit(x: Int, y: Int): Boolean {
    return x >= XLocation && x <= (XLocation + Width) && y >=  YLocation && y <= (YLocation  + Height)
  }
} 

fun main(args: Array<String>) {
  val e1 = Ellipsis()
  e1.Height = 10
  e1.Width = 12
  
  val e2 = Ellipsis()
  e2.XLocation = 100
  e2.YLocation = 96
  
  e1.Height = 21
  e1.Width = 19
  
  val r1 = Rectangle()
  r1.XLocation = 49
  r1.YLocation = 45
  r1.Width = 10
  r1.Height = 10
  
  val shapes = listOf<Shape>(e1, e2, r1)
  val selected:Shape? = shapes.firstOrNull {shape ->  shape.isHit(50, 52)}
  
  if(selected == null){
    println("There is no shape at point(50,52)")
  }
  else{         
    println("A shape of type ${selected.javaClass.simpleName} has  been selected.")
  }
} 
```

* Dynamic method resolution is handled via the vtable (that is, virtual table) mechanism. The actual approach might depend on the JVM implementation, but they will share the same logical implementation.

* When any object instance is created, its memory allocation lives on the heap. The actual size of the memory being allocated is slightly bigger than the sum of all the allocated fields, including all the base classes, all the way to the Any class. The runtime footprint will get an extra space added at the top of the memory block to hold a reference to the type descriptor information. For each class type you define, there will be an object allocated at runtime. This entry has been added as the first entry to always guarantee the location, thus avoiding the need to compute it at runtime. This type descriptor holds the list of methods defined along with other information related to it. This list starts with the top class in the hierarchy and goes all the way to the actual type whose instance it belongs to. The order is deterministic; again, another example of optimization. This is known as the vtable structure and is nothing more than an array with each element pointing out (referencing) the actual native code implementation that will be executed. During the program execution, the JIT-er (the just-in time compiler) is responsible for translating the bytecode produced by your compiler into native/assembly code. If a derived class decides to override a virtual method, its vtable entry will point out to the new implementation rather than the last class in the hierarchy providing it.
