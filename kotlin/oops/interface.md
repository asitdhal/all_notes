* An interface is nothing more than a contract; it contains definitions for a set of related functionalities. 
* The implementer of the interface has to adhere to the interface the contract and implement the required methods. 
* A Kotlin interface contains the __declarations of abstract methods__ as well as __method implementations__. Unlike abstract classes, __an interface cannot contain state__; however, __it can contain properties__.


   ```kotlin
   interface MyInterface {
       fun bar()
       fun foo() {
           println("default implementation")
       }
    }

    class Impl : MyInterface {
        override  fun bar() {
            println("interface implementation")
        }
    }

    fun main(args : Array<String>) {
        var obj = Impl()
        obj.foo()
        obj.bar()
    }
    ```

* A property declared in an interface can either be abstract, or it can provide implementations for accessors. Properties declared in interfaces can't have backing ﬁelds, and therefore accessors declared in interfaces can't reference them.
  
  ```kotlin
  TODO
  ```

* When we declare many types in our supertype list, it may appear that we inherit more than one implementation of the same method.In this case, you should provide implementation in the child class. Otherwise, the compiler will complain.

  ```kotlin
  package demo

  interface A {
    fun foo() = println("A")
    fun bar()
  }

  interface B {
    fun foo() = println("B")
    fun bar() = println("Bar")
  }

  class C : A {
    override fun bar() = println("bar")
  }

  class D : A, B {
    override fun foo() {
        super<A>.foo()
        super<B>.foo()
    }

    override fun bar() = super<B>.bar()
  }

  fun main(args : Array<String>) {
    var impl = D()
    impl.foo()
    impl.bar()
  }
  ```



```kotlin
interface Document {
  val version: Long
  val size: Long
  
  val name: String
  get() = "NoName"
  
  fun save(input: InputStream)
  fun load(stream: OutputStream)
  fun getDescription(): String {
      return "Document $name has $size byte(-s)"
  }
}

class DocumentImpl : Document {
  override val size: Long    
  get() = 0      
  override fun load(stream: OutputStream) { }
  override fun save(input: InputStream) { }
  override val version: Long
  get() = 0

}
```
