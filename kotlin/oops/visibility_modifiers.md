* When you define your class, the contained methods, properties, or fields can have various visibility levels.

| visibility modifier | description |
|:--------------------|:------------|
|Public | can be accessed from anywhere |
|Internal | can only be accessed from the module code  |
|Protected | can only be accessed from the class defining it and any derived classes |
|Private | can only be accessed from the scope of the class defining it |

* If the parent class specifies that a given field is open for being redefined (overwritten), the derived class will be able to modify the visibility level.

```kotlin
open class Container {       
  protected open val fieldA: String = "Some value"
}

class DerivedContainer : Container() {
  public override val fieldA: String = "Something else"     
} 
```

* Redefining the field doesn't mean it will replace the existing one when it comes to object allocation.
