* Data classes are intended for types that are meant to be data containers and nothing more. Compiler provides many things for Data class.

```kotlin
data class User(val name: String = "", val age: Int = 0)

fun main(args: Array<String>) {
    val u = User(name="Asit", age=30)
    println("Name: ${u.name}")
    println("Age: ${u.age}")
    println(u)
}
```
* The compiler automatically derives the following members from all properties declared in the primary constructor
    * ```equals()``` / ```hashCode()``` pair
    * ```toString()``` of the form ```User(name=John, age=42)```
    * ```componentN()``` functions corresponding to the properties in their order of declaration
    * ```copy()``` function

* If any of these functions is explicitly defined in the class body or inherited from the base types, it will not be generated.

* To ensure consistency and meaningful behavior of the generated code, data classes have to fulfil the following requirements.
    * The primary constructor needs to have at least one parameter
    * All primary constructor parameters need to be marked as val or var 
    * Data classes cannot be abstract, open, sealed or inner
    * Data classes may not extend other classes (but may implement interfaces)

* The fields can have visibility modifiers also. If private is used, the compiler won't generate getters and setters.

* A field can be made readonly, by declaring field using "val". "var" will make it read-write.
```kotlin
data class User(val name: String = "", var age: Int = 0)

fun main(args: Array<String>) {
    val u = User(name="Asit", age=30)
    println(u)
    u.age = 31
    println(u)
}
```

#### Copying
* It's often the case that we need to copy an object altering some of its properties, but keeping the rest unchanged. This is what
copy() function is generated for. 
* For the above User class, copy function is 
```kotlin
fun copy(name: String = this.name, age: Int = this.age) = User(name, age)
```

#### Data Classes and Destructuring Declarations
* For each property we specify for our data class, Kotlin will generate a componentN() function which maps to that property, where ’N’ represents the properties order in the definition. This allows destructuring.
```kotlin
val jane = User("Jane", 35)
val (name, age) = j ane
println("$name, $age years of age") // prints "Jane, 35 years of age"
```
* It also works with loops.
```kotlin
data class User(val name: String = "", var age: Int = 0, val sex: Boolean = false)

fun main(args: Array<String>) {
    val u1 = User(name="Asit", age=30, sex=false)
    val u2 = User(name="Gargi", age=20, sex=true)
    val listOfUsers: List<User> = listOf(u1, u2)
    for ((name, age, sex) in listOfUsers) { if (sex == true) println("Name: $name Age: $age") }
}
```