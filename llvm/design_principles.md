### Design Principles
* A central aspect of the design of LLVM is its IR. It uses Single-Static Assignments (SSA), with two important characteristics
	1. Code is organized as three-address instructions
	2. It has an infinite number of registers
* LLVM has multiple forms of representing the program. Throughout the compilation process, other intermediary data structures hold the program logic and help its translation across major checkpoints. Technically, they are also intermediate forms of program representation. For example, LLVM employs the following additional data structures across different compilation stages:
	1. When translating C or C++ to the LLVM IR, Clang will represent the program in the memory by using an Abstract Syntax Tree (AST) structure (the ```TranslationUnitDecl``` class).
	2. When translating the LLVM IR to a machine-specific assembly language, LLVM will first convert the program to a Directed Acyclic Graph (DAG) form to allow easy instruction selection (the ```SelectionDAG``` class) and then it will convert it back to a three-address representation to allow the instruction scheduling to happen (the ```MachineFunction``` class)
	3. To implement assemblers and linkers, LLVM uses a fourth intermediary data structure (the ```MCModule``` class) to hold the program representation in the context of object files.
* The LLVM IR is the most important one. It has the particularity of being not only an in-memory representation, but also being stored on disk.
* The compiler goes beyond applying optimizations at compile time, exploring optimization opportunities at the installation time, runtime, and idle time (when the program is not running). In this way, the optimization happens throughout its entire life, thereby explaining the name of this concept. For example, when the user is not running the program and the computer is idle, the operating system can launch a compiler daemon to process the profiling data collected during runtime to reoptimize the program for the specific use cases of this user.
*  The design decision of maintaining an on-disk representation of the compiler IR remained as an enabler of link-time optimizations, giving less attention to the original idea of lifelong program optimizations. Eventually, LLVM's core libraries formalized their lack of interest in becoming a platform by renouncing the acronym Low Level Virtual Machine, adopting just the name LLVM for historical reasons, making it clear that the LLVM project is geared to being a strong and practical C/C++ compiler rather than a Java platform competitor.
* Still, the on-disk representation alone has promising applications, besides link-time optimizations, that some groups are fighting to bring to the real world. For example, the FreeBSD community wants to embed program executables with its LLVM program representation to allow install-time or offline microarchitectural optimizations. In this scenario, even if the program was compiled to a generic x86, when the user installs the program, for example, on the specific Intel Haswell x86 processor, the LLVM infrastructure can use the LLVM representation of the binary and specialize it to use new instructions supported on Haswell. Even though this is a new idea that is currently being assessed, it demonstrates that the on-disk LLVM representation allows for radical new solutions. The expectations are for microarchitectural optimizations because the full platform independence seen in Java looks impractical in LLVM and this possibility is currently explored only on external projects (see PNaCl, Chromium's Portable Native Client).
* As a compiler IR, the two basic principles of the LLVM IR that guided the development of the core libraries are the following:
	1. SSA representation and infinite registers that allow fast optimizations.
	2. Easy link-time optimizations by storing entire programs in an on-disk IR representation
* LLVM might refer to any of the following:
	1. ```The LLVM project/infrastructure```: This is an umbrella for several projects that, together, form a complete compiler: frontends, backends, optimizers, assemblers, linkers, libc++, compiler-rt, and a JIT engine. The word "LLVM" has this meaning, for example, in the following sentence: "LLVM is comprised of several projects".
	2. ```An LLVM-based compiler```: This is a compiler built partially or completely with the LLVM infrastructure. For example, a compiler might use LLVM for the frontend and backend but use GCC and GNU system libraries to perform the final link. LLVM has this meaning in the following sentence, for example: "I used LLVM to compile C programs to a MIPS platform".
	3. ```LLVM libraries```: This is the reusable code portion of the LLVM infrastructure. For example, LLVM has this meaning in the sentence: "My project uses LLVM to generate code through its Just-in-Time compilation framework".
	4. ```LLVM core```: The optimizations that happen at the intermediate language level and the backend algorithms form the LLVM core where the project started. LLVM has this meaning in the following sentence: "LLVM and Clang are two different projects".
	5. ```The LLVM IR```: This is the LLVM compiler intermediate representation. LLVM has this meaning when used in sentences such as "I built a frontend that translates my own language to LLVM".
* Important parts of the LLVM infrastructure
	* ```Frontend```: This is the compiler step that translates computer-programming languages, such as C, C++, and Objective-C, into the LLVM compiler IR. This includes a lexical analyzer, a syntax parser, a semantic analyzer, and the LLVM IR code generator. The Clang project implements all frontend-related steps while providing a plugin interface and a separate static analyzer tool to allow deep analyses.
	* ```IR```: The LLVM IR has both human-readable and binary-encoded representations. Tools and libraries provide interfaces to IR construction, assembling, and disassembling. The LLVM optimizer also operates on the IR where most part of optimizations is applied.
	* ```Backend```: This is the step that is responsible for code generation. It converts LLVM IR to target-specific assembly code or object code binaries. Register allocation, loop transformations, peephole optimizers, and target-specific optimizations/transformations belong to the backend.

	![LLVM infrastructure](llvm_infrastructure.jpg)
* The interaction between each of these compiler parts can happen in the following two ways:
	* ```In memory```: This happens via a single supervisor tool, such as Clang, that uses each LLVM component as a library and depends on the data structures allocated in the memory to feed the output of a stage to the input of another.
	* ```Through files```: This happens via a user who launches smaller standalone tools that write the result of a particular component to a file on disk, depending on the user to launch the next tool with this file as the input.

#### Interacting with the compiler driver
* The compiler driver is responsible for integrating all necessary libraries and tools in order to provide the user with a friendlier experience, freeing the user from the need to use individual compiler stages such as the frontend, backend, assembler, and linker. Once you feed your program source code to a compiler driver, it can generate an executable. In LLVM and Clang, the compiler driver is the ```clang ``` tool.
* hello.c program
	```c
	#include <stdio.h>

	int main()
	{  
		printf("Hello, World!\n");  
		return 0;
	}
	```
	To generate an executable for this simple program, use the following command:
	```sh
	$ clang hello.c –o hello
	```
	In order to see all subsequent tools called by the driver to complete your order, use the ```-###``` command argument:
	```sh
	$ clang -### hello.c –o hello
	clang version 3.4 (tags/RELEASE_34/final)
	Target: x86_64-apple-darwin11.4.2
	Thread model: posix
	"/bin/clang" -cc1 -triple x86_64-apple-macosx10.7.0 … -main-file-name hello.c (...) /examples/hello/hello.o -x c hello.c
	"/opt/local/bin/ld" (...) -o hello /examples/hello/hello.o (...)
	```

* ```-cc1 parameter```: disables the compiler-driver mode while  enabling the compiler mode. It also uses a myriad of arguments to  tweak the C/C++ options. Since LLVM components are libraries, the clang –cc1 is linked with the IR generation, the code generator for the target machine, and assembler libraries. Therefore, after parsing, clang –cc1 itself is able to call other libraries and supervise the compilation pipeline in the memory until the object file is ready.
* Afterwards, the Clang driver (different from the compiler clang -cc1) invokes the linker, which is an external tool, to generate the executable file, as shown in the preceding output line. It uses the system linker to complete the compilation because the LLVM linker, lld, is still under development.

#### Tools
* Some of these tools are as follows:
	* ```opt```: This is a tool that is aimed at optimizing a program at the IR level. The input must be an LLVM bitcode file (encoded LLVM IR) and the generated output file must have the same type.
	* ```llc```: This is a tool that converts the LLVM bitcode to a target-machine assembly language file or object file via a specific backend. You can pass arguments to select an optimization level, to turn on debugging options, and to enable or disable target-specific optimizations.
	* ```llvm-mc```: This tool is able to assemble instructions and generate object files for several object formats such as ELF, MachO, and PE. It can also disassemble the same objects, dumping the equivalent assembly information and the internal LLVM machine instruction data structures for such instructions.
	* ```lli```: This tool implements both an interpreter and a JIT compiler for the LLVM IR.
	* ```llvm-link```: This tool links together several LLVM bitcodes to produce a single LLVM bitcode that encompasses all inputs.
	* ```llvm-as```: This tool transforms human-readable LLVM IR files, called LLVM assemblies, into LLVM bitcodes.
	* ```llvm-dis```: This tool decodes LLVM bitcodes into LLVM assemblies.
* Examples:
	```c
	//main.c
	#include <stdio.h>
	int sum(int x, int y);
	int main()
	{    
		int r = sum(3, 4);    
		printf("r = %d\n", r);    
		return 0;
	}
	//sum.c
	int sum(int x, int y)
	{
    	return x+y;
	}
	```
	Compilation
	```sh
	$ clang main.c sum.c –o sum
	```
	By using standalone tools
	$ clang -emit-llvm -c main.c -o main.bc
	```sh
	$ clang -emit-llvm -c sum.c -o sum.bc
	```
	The ```–emit-llvm``` flag tells clang to generate either the LLVM bitcode or LLVM assembly files, depending on the presence of the ```-c``` or ```-S``` flag.
	To generate human readable code.
	```sh
	$ clang -emit-llvm –S -c main.c -o main.ll
	$ clang -emit-llvm –S -c sum.c -o sum.ll
	```
	.bc -> bitcode
	.ll -> human readable
	Generate target-specific object files from each LLVM bitcode and build the program executable by linking them with the system linker (part A of the next diagram):
	```sh
	$ llc -filetype=obj main.bc -o main.o
	$ llc -filetype=obj sum.bc -o sum.o
	$ clang main.o sum.o -o sum
	```
	First, link the two LLVM bitcodes into a final LLVM bitcode. Then, build the target-specific object file from the final bitcode and generate the program executable by calling the system linker (part B of the following diagram):
	```sh
	$ llvm-link main.bc sum.bc -o sum.linked.bc
	$ llc -filetype=obj sum.linked.bc -o sum.linked.o
	$ clang sum.linked.o -o sum
	```
	The ```-filetype=obj``` parameter specifies an object file output instead of the target assembly file. We use the Clang driver, ```clang```, to invoke the linker, but the system linker can be used directly if you know all parameters that your system linker requires to link with your system libraries.
	Linking IR files prior to the backend invocation (```llc```) allows the final produced IR to be further optimized with link-time optimizations provided by the opt tool.
	Alternatively, the llc tool can generate an assembly output, which can be further assembled using llvm-mc.
	![LLVM Tools](tools.jpg)

#### LLVM's basic libraries
* LLVM and Clang logic is carefully organized into the following libraries.
	* ```libLLVMCore```: This contains all the logic related to the LLVM IR: IR construction (data layout, instructions, basic blocks, and functions) and the IR verifier. It also provides the pass manager.
	* ```libLLVMAnalysis```: This groups several IR analysis passes, such as alias analysis, dependence analysis, constant folding, loop info, memory dependence analysis, and instruction simplify.
	* ```libLLVMCodeGen```: This implements target-independent code generation and machine level—the lower level version of the LLVM IR—analyses and transformations.
	* ```libLLVMTarget```: This provides access to the target machine information by generic target abstractions. These high-level abstractions provide the communication gateway between generic backend algorithms implemented in  ```libLLVMCodeGen``` and the target-specific logic that is reserved for the next library.
	* ```libLLVMX86CodeGen```: This has the x86 target-specific code generation information, transformation, and analysis passes, which compose the x86 backend. Note that there is a different library for each machine target, such as ```LLVMARMCodeGen``` and ```LLVMMipsCodeGen```, implementing ARM and MIPS backends, respectively.
	* ```libLLVMSupport```: This comprises a collection of general utilities. Error, integer and floating point handling, command-line parsing, debugging, file support, and string manipulation are examples of algorithms that are implemented in this library, which is universally used across LLVM components.
	* ```libclang```: This implements a C interface, as opposed to C++, which is the default implementation language of LLVM code, to access much of Clang's frontend functionalities—diagnostic reporting, AST traversing, code completion, mapping between cursors, and source code. Since it is a C, simpler interface, it allows projects written in other languages, such as Python, to use the Clang functionality more easily, albeit the C interface is designed to be more stable and allow external projects to depend on it. This only covers a subset of the C++ interface used by internal LLVM components.
	* ```libclangDriver```: This contains the set of classes used by the compiler driver tool to understand GCC-like command-line parameters to prepare jobs and to organize adequate parameters for external tools to finish different steps of the compilation. It can manage different strategies for the compilation, depending on the target platform.libclangAnalysis: This is a set of frontend level analyses provided by Clang. It features CFG and call-graph construction, reachable code, format string security, among others.
