### LLVM Program Structure
* LLVM IR is almost all doubly-linked lists.
* Module contains Functions/GlobalVariables
	- Module is unit of compilation/analysis/optimization
* Function contains BasicBlocks/Arguments
	- Functions roughly correspond to functions in C
* BasicBlock contains list of instructions
	- Each block ends in a control flow instruction
* Instruction is opcode + vector of operands
	- All operands have types
	- Instruction result is typed
* Compiler is organized as a series of ‘passes’:
	- Each pass is one analysis or transformation
* Four types of Pass:
	* ```ModulePass```: general inter procedural pass
	* ```CallGraphSCCPass```: bottom-up on the call graph
	* ```FunctionPass```: process a function at a time
	* ```BasicBlockPass```: process a basic block at a time
* Constraints imposed (e.g. FunctionPass):
	* FunctionPass can only look at “current function”
	* Cannot maintain state across functions
* Optimization of pass execution:
	* Process a function at a time instead of a pass at a time
	* Example:
	```
	three functions -> F, G, H
	two passes -> X & Y
	X(F)Y(F) X(G)Y(G) X(H)Y(H) not X(F)X(G)X(H) Y(F)Y(G)Y(H)
	```
### ModulePass
* If a pass is derived from ModulePass class , then that pass uses the entire program as a unit, referring to function bodies in no predictable order, or adding and removing functions. Because nothing is known about the behavior of ModulePass subclasses, no optimization can be done for their execution.
* A module pass can use function level passes (e.g. dominators) using the ```getAnalysis``` interface ```getAnalysis<DominatorTree>(llvm::Function *)``` to provide the function to retrieve analysis result for, if the function pass does not require any module or immutable passes. Note that this can only be done for functions for which the analysis ran, e.g. in the case of dominators you should only ask for the DominatorTree for function definitions, not declarations.
* Rule for deriving from ModulePass
	- overload the ```runOnModule``` method with the following signature.
	```
	virtual bool runOnModule(Module &M) = 0;
	```
	- It returns ```true``` if the module was modified by the transformation and ```false``` otherwise.
