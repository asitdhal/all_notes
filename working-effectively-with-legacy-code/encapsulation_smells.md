## Abstract

The principle of encapsulation advocates separation of concerns and information hiding. Hiding implementation details and hiding variations are two techniques that enable the effective application of the principle of encapsulation.

This is precisely what the principle of Encapsulation does—it hides details that are not really required for the user of the abstraction (the abstraction in this case is the car). In addition, the principle of Encapsulation helps an abstraction to hide variation in the implementation details. For instance, whether your car has a petrol engine or a diesel engine, it does not change the way you drive your car.

The principle of encapsulation complements the principle of abstraction through information hiding. Encapsulation disallows (or “hides”) the users of the abstraction from seeing internal details of the abstraction. In addition, encapsulation hides variation in implementation; the idea of hiding variation is explicitly stated by Gamma et al. [54]: “Encapsulate what varies.” Hence, there are two techniques that enable effective application of the principle of encapsulation:

- **Hide implementation details**. An abstraction exposes to its clients only “what the abstraction offers” and hides “how it is implemented.” The latter, that is, the details of the implementation include the internal representation of the abstraction (e.g., data members and data structures that the abstraction uses) and details of how the method is implemented (e.g., algorithms that the method uses).

- **Hide variations**. Hide implementation variations in types or hierarchies. With such variations hidden, it is easier to make changes to the abstraction’s implementation without much impact to the clients of the abstraction.

## Design Smells and the Violated Encapsulation Enabling Techniques

Design Smells |	Violated Enabling Technique
------------- | -------------------------
Deficient Encapsulation, Leaky Encapsulation | Hide implementation details
Missing Encapsulation, Unexploited Encapsulation | Hide variations

## Deficient Encapsulation

This smell occurs when the declared accessibility of one or more members of an abstraction is more permissive than actually required. For example, a class that makes its fields public suffers from Deficient Encapsulation.
An extreme form of this smell occurs when there exists global state (in the form of global variables, global data structures, etc.) that is accessible to all abstractions throughout the software system.

### Rationale

The primary intent behind the principle of encapsulation is to separate the interface and the implementation, which enables the two to change nearly independently. This separation of concerns allows the implementation details to be hidden from the clients who must depend only on the interface of the abstraction. If an abstraction exposes implementation details to the clients, it leads to undesirable coupling between the abstraction and its clients, which will impact the clients whenever the abstraction needs to change its implementation details. Providing more access than required can expose implementation details to the clients, thereby, violating the “principle of hiding.”

Having global variables and data structures is a more severe problem than providing lenient access to data members. This is because global state can be accessed and modified by any abstraction in the system, and this creates secret channels of communication between two abstractions that do not directly depend on each other. Such globally accessible variables and data structures could severely impact the understandability, reliability, and testability of the software system.

Since this smell occurs when the internal state of an abstraction is inadequately hidden, we name this smell Deficient Encapsulation.

### Potential Causes

- **Easier testability**: Often developers make private methods of an abstraction public to make them testable. Since private methods concern implementation details of an abstraction, making them public compromises the encapsulation of the abstraction.
- **Procedural thinking in object oriented context**: Sometimes, this smell occurs when developers from procedural programming background using an object oriented paradigm expose data as global variables when the data needs to be used by multiple abstractions.
- **Quick-fix solutions**: Delivery pressures often force developers to take shortcuts (which happens to be one of the main causes of technical debt). For instance, in situations where the data is shared between only a couple of abstractions, developers find it easier to expose that data globally than to create an abstraction and pass it as a parameter to a method.

### CASE STUDY

It is common to observe rampant occurrences of Deficient Encapsulation smell in real-world projects. Although it is easy to understand why fields may be declared public, it is interesting to note in many real-world projects that methods that should have been declared private are instead made public.

When we questioned some development teams about this, most of them said that it was a conscious design choice: when methods are declared private, it is difficult to test them, so they are typically made public!

However, exposing all of the methods as public not only compromises design quality but can also cause serious defects. For instance, clients of the class will start directly depending on implementation details of the class. This direct dependency makes it difficult to change or extend the design. It also impacts the reliability, since clients can directly manipulate the internal details of the abstractions. Hence, ensuring the integrity of the internal state of the abstractions becomes difficult (if not impossible). In summary, making internal methods publicly accessible in the name of testability can have serious consequences.

So, how can we test classes without compromising their encapsulation?

There are many ways in which tests can be written without exposing methods as public. One way is to use reflection—tests can use reflection to dynamically load classes, examine their internals, and invoke methods (including private ones). Another way is to adopt a language such as Groovy that allows direct access to all the members (and ignores access specifiers) of a Java class.

### ANECDOTE

One of the authors was involved in reviewing an application for post-processing violations of static analyzers, clone detectors, and metric tools that was being developed at a start-up. The application generated summary reports that helped architects and project managers understand the code quality problems in their projects.

The application had three logical modules:

- Parsing module: Classes responsible for parsing the results from static analyzers, metric tools, and clone detectors.
- Analytics module: Classes responsible for analyzing the data such as aggregating them based on some criteria such as criticality of the violations.
- Reporter module: Classes responsible for generating reports and providing a summary to the architects and managers.

While inspecting the design and codebase, the author found an interesting class named Data. The main reason why this class existed is because the classes in Reporting module needed to know (for example, to create a summary report) the details resulting from the processing done by the Parsing and Analytics modules. To elaborate, the Parsing and Analytics modules provided direct and useful results that were used for summarizing the results in the Reporting module. The code in Parsing and Analytics module dumped the data structures into the Data class, and the code in Reporting module read this data from the Data class and created a summary as part of the final report that was generated. However, the way that this was realized was by declaring all of the 23 fields of the Data class as public static (and non-final). The implication of such a design is that the details within Analytics module were unnecessarily available even to the Parsing module! This has an adverse impact on understandability and reliability. Such global data sharing provides an implicit form of coupling between the modules.

### Suggested Refactoring

In the case of a public data member, “encapsulate field” refactoring can be applied to make the field private and provide accessor methods (i.e., get and set methods) as required. 

### Impacted Quality Attributes

- **Understandability**: One of the main objectives of the principle of encapsulation is to shield clients from the complexity of an abstraction by hiding its internal details. However, when internal implementation details are exposed, the view of the abstraction may become complex and impact understandability. In the case when global data or data structures are used, it is very difficult to track and differentiate between read and write accesses. Furthermore, since the methods operating on the global data/data structures are not encapsulated along with the data, it is harder to understand the overall design.
- **Changeability and Extensibility**: Since the declared accessibility of members of the abstraction is more permissive, the clients of the abstraction may depend directly upon the implementation details of the abstraction. This direct dependency makes it difficult to change or extend the design. In the case of global variables, since encapsulation is lacking, any code can freely access or modify the data. This can result in unintended side effects. Furthermore, when defect fixes or feature enhancements are made, it can break the existing code. For these reasons, Deficient Encapsulation negatively impacts changeability and extensibility.
- **Reusability**: In the presence of Deficient Encapsulation smell, clients may directly depend on commonly accessible state. Hence, it is difficult to reuse the clients in a different context, since this will require the clients to be stripped of these dependencies.
- **Testability**: Globally accessible data and data structures are harder to test, since it is very difficult to determine how various code segments access or modify the common data. Furthermore, the abstractions that make use of the global data structures are also difficult to test because of the unpredictable state of the global data structures. These factors negatively impact testability of the design.
- **Reliability**: When an abstraction provides direct access to its data members, the responsibility of ensuring the integrity of the data and the overall abstraction is moved from the abstraction to each client of the abstraction. This increases the likelihood of the occurrence of runtime problems.

### Aliases

This smell is also known in literature as:

- “Hideable public attributes/methods”: This smell occurs when you see public attributes/methods that are never used from another class but instead are used within the own class (ideally, these methods should have been private).
- “Unencapsulated class”: A lot of global variables are being used by the class.
- “Class with unparameterized methods”: Most of the methods in class have no parameters and utilize class or global variables for processing.

### Practical Considerations

- **Lenient access in nested or anonymous classes**: It is perhaps acceptable to declare data members or implementation-level methods as public in case of nested/anonymous classes where the members are accessible only to the enclosing class.
- **Performance considerations**: Some designers make a conscious choice to make use of public data members citing efficiency reasons.

## Leaky Encapsulation

This smell arises when an abstraction “exposes” or “leaks” implementation details through its public interface. Since implementation details are exposed through the interface, it not only is harder to change the implementation but also allows clients to directly access the internals of the object (leading to potential state corruption).

### Rationale

For effective encapsulation, it is important to separate the interface of an abstraction (i.e., “what” aspect of the abstraction) from its implementation (i.e., “how” aspect of the abstraction). Furthermore, for applying the principle of hiding, implementation aspects of an abstraction should be hidden from the clients of the abstraction.

When implementation details of an abstraction are exposed through the public interface (i.e., there is a violation of the enabling technique “hide implementation details”):

- Changes made to the implementation may impact the client code.
- Exposed implementation details may allow clients to get handles to internal data structures through the public interface, thus allowing clients to corrupt the internal state of the abstraction accidentally or intentionally.

Since implementation aspects are not hidden, this smell violates the principle of encapsulation; furthermore, the abstraction “leaks” implementation details through its public interface. Hence, we term the smell Leaky Encapsulation.

### Potential Causes

- **Lack of awareness of what should be “hidden”**: It requires considerable experience and expertise to discern and separate the implementation and interface aspects of an abstraction from each other. Inexperienced designers often inadvertently leak implementation details through the public interface.
- **Viscosity**: It requires considerable thought and effort to create “leak-proof” interfaces. However, in practice, due to project pressures and deadlines, often designers or developers resort to quick and dirty hacks while designing interfaces, which leads to this smell.
- **Use of fine-grained interface**: Often, this smell occurs because fine-grained methods are directly provided in the public interface of a class. These fine-grained methods typically expose unnecessary implementation details to the clients of the class. A better approach is to have logical coarse-grained methods in the public interface that internally make use of fine-grained methods that are private to the implementation.

### ANECDOTE

A participant of the Smells Forum reported an example of this smell in an e-commerce application. In this application, the main logic involved processing of orders, which was abstracted in an OrderList class. The OrderList extended a custom tree data structure, since most use cases being tested by the team involved inserting and deleting orders.

When the application was first deployed, a customer filed a change request complaining that the application was not able to handle large number of orders and it “hanged.” Profiling the application showed OrderList to be a “bottleneck class.” The tree implementation internally used was not a self-balancing tree, and hence the tree often got skewed and looked more like a “list.” To fix this problem, a developer quickly added a public method named balance() in the OrderList class that would balance the tree. He also ensured that the balance() method was invoked from the appropriate places in the application to address the skewing of the tree. When he tested the application, this solution appeared to solve the performance problem and so he closed the change request.

In a few months, more customers reported similar performance problems. Taking a relook at the class revealed that most of the time orders were being “looked-up” and not inserted or deleted. Hence the team now considered replacing the internal tree implementation to a hash-table implementation.

However, the OrderList class was tied to the “tree” implementation because it extended a custom Tree class. Furthermore, changing the implementation to use a more suitable data structure such as a hash table was difficult because the clients of the OrderList class used implementation-specific methods such as balance()! Although refactoring was successfully performed to use hash table instead of unbalanced tree in the OrderList, it involved making changes in many places within the application.
There are two key insights that are revealed when we reflect on this anecdote:

- Having the OrderList extend the custom tree instead of using the custom tree forced the OrderList to unnecessarily expose implementation details to the clients of the OrderList class. Due to the choice of using inheritance over delegation, the clients of the OrderList could not be shielded from the changes that arose when a different data structure, i.e., a hash table was used inside the OrderList.
- Adding balance() method in the public interface of the class is clearly a work-around. Such workarounds or quick-fix solutions appear to solve the problem, but they often backfire and cause more problems. Hence, one must invest a lot of thought in the design process.

Design smells such as Leaky Encapsulation can cause serious problems in applications, and we need to guard against such costly mistakes.

### Suggested Refactoring

The suggested refactoring for this smell is that the interface should be changed in such a way that the implementation aspects are not exposed via the interface. For example, if details of the internal algorithms are exposed through the public interface, then refactor the public interface in such a way that the algorithm details are not exposed via the interface.

It is also not proper to return a handle to internal data structures to the clients of a class, since the clients can directly change the internal state by making changes through the handle. There are a few ways to solve the problem:

- Perform deep copy and return that cloned object (changes to the cloned object do not affect the original object).
- Create an immutable object and return that cloned object (by definition, an immutable object cannot be changed).

If low-level fine-grained methods are provided in the public interface, consider making them private to the implementation. Instead, logical coarse-grained methods that make use of the fine-grained methods can be introduced in the public interface.

### CASE STUDY

Consider the case of a software system in which a class implements operations on an image object. The operation to display an image completes in four steps that must be executed in a specific sequence, and the designer has created public methods corresponding to each of these steps: i.e., load(), process(), validate(), and show() in the Image class. Exposing these methods creates a constraint on using these four methods; specifically, the methods corresponding to these four steps—load, process, validate, and show—must be called in that specific sequence for the image object to be valid.

This constraint can create problems for the clients of the Image class. A novice developer may call validate() without calling process() on an image object. Each of the operations makes a certain assumption about the state of the image object, and if the methods are not called in the required sequence, then the operations will fail or leave the image object in an inconsistent state. Clearly, the Image class exposes “how” aspects of the abstraction as part of the public interface. It thus exhibits the Leaky Encapsulation smell.

How can this problem be addressed? One potential solution is to have a state flag to keep track of whether the operations are performed in a sequence. However, this is a really bad solution, since maintaining a state flag is cumbersome: if we maintain the state flag in the Image class, every operation must check for the proper transition and handle any exceptional condition.

At this juncture, we can take a step back and question why the four internal steps need to be exposed to the client who is concerned only with the display of the image. We can then see that the problem can be solved by exposing to the clients of the Image class only one method, say display(), which internally calls each of the four steps in sequence.

If we reflect upon this anecdote, we see that the underlying problem in this example is that the fine-grained steps were exposed as public methods. Hence, the key take-away here is that it is better to expose coarse-grained methods via the public interface rather than fine-grained methods that run the risk of exposing internal implementation details.

### Impacted Quality Attributes

- **Understandability**: One of the main objectives of the principle of encapsulation is to shield clients from the complexity of an abstraction by hiding its internal details. However, when internal implementation details are exposed in the case of a Leaky Encapsulation smell, the public interface of the abstraction may become more complex, affecting its understandability.
- **Changeability and Extensibility**: When an abstraction “exposes” or “leaks” implementation details through its public interface, the clients of the abstraction may depend directly upon its implementation details. This direct dependency makes it difficult to change or extend the design without breaking the client code.
- **Reusability**: Clients of the abstraction with Leaky Encapsulation smell may directly depend upon the implementation details of the abstraction. Hence, it is difficult to reuse the clients in a different context, since it will require stripping the client of these dependencies (for reference, see Command Pattern [54]).
- **Reliability**: When an abstraction “leaks” internal data structures, the integrity of the abstraction may be compromised, leading to runtime problems.

### Aliases

This smell is also known in literature as:
- Leaking implementation details in API [49]—This smell occurs when an API leaks implementation details that may result in confusing users or inhibiting freedom to change implementation.

### Practical Considerations

**Low-level classes**: Consider an example of embedded software that processes and plays audio in a mobile device. In such software, the data structures that store metadata about the audio stream (e.g., sampling rate, mono/stereo) need to be directly exposed to the middleware client. In such cases, when public interface is designed purposefully in this way, clients should be warned that the improper use of those public methods might result in violating the integrity of the object. Also, it is important to ensure that such classes are not part of the higher-level API, such as the one that is meant for use by the mobile application software, so that runtime problems can be avoided.


## Missing Encapsulation

This smell occurs when implementation variations are not encapsulated within an abstraction or hierarchy.
This smell usually manifests in the following forms:

- A client is tightly coupled to the different variations of a service it needs. Thus, the client is impacted when a new variation must be supported or an existing variation must be changed.
- Whenever there is an attempt to support a new variation in a hierarchy, there is an unnecessary “explosion of classes,” which increases the complexity of the design.

### Rationale

The Open Closed Principle (OCP) states that a type should be open for extension and closed for modification. In other words, it should be possible to change a type’s behavior by extending it and not by modifying it (see case study below). When the variations in implementation in a type or a hierarchy are not encapsulated separately, it leads to the violation of OCP.

### CASE STUDY ON OCP

Localization in Java is supported through the use of resource bundles (see java.util.ResourceBundle in Java 7). We can write code that is mostly independent of user’s locale and provide locale-specific information in resource bundles.

The underlying process of searching, resolving, and loading resource bundles is complex. For most purposes, the default implementation provided by the ResourceBundle class is sufficient. However, for sophisticated applications, there may be a need to use a custom resource bundle loading process. For instance, when there are numerous heavyweight locale-specific objects, you may want to cache the objects to avoid reloading them. To support your own resource bundle formats, search strategy, or custom caching mechanism, ResourceBundle provides an extension point in the form of ResourceBundle.Control class. Here is the description of this class from JDK documentation:

“ResourceBundle.Control defines a set of callback methods that are invoked by the ResourceBundle.getBundle factory methods during the bundle loading process… Applications can specify ResourceBundle.Control instances returned by the getControl factory methods or created from a subclass of ResourceBundle.Control to customize the bundle loading process.”

In summary, ResourceBundle supports a default process for locating and loading resource bundles that is sufficient for most needs. Furthermore, the class also provides support for customizing this bundle locating and loading process by providing a separate abstraction in the form of ResourceBundle.Control class. Thus, this design by default supports the standard policy and provides an extension point to plug-in a custom policy. Such a design follows OCP.

This example is also an illustration of what Alan Kay (Turing award winner and co-creator of Smalltalk) said about design: “Simple things should be simple, complex things should be possible.”

One of the enabling techniques for Encapsulation is “hide variations.” This is similar to the “Variation Encapsulation Principle (VEP)” which is advocated by Gamma et al. [54]. Thus, when a type (or hierarchy) fails to encapsulate the variation in the implementation, it implies that the principle of encapsulation has been either poorly applied or not applied at all. Hence, we term this smell Missing Encapsulation.

### Potential Causes

- **Lack of awareness of changing concerns**: Inexperienced designers are often unaware of principles such as OCP and can end up creating designs that are not flexible enough to adapt to changing requirements. They may also lack the experience to observe or foresee the concerns that could change in the future (based on the planned product roadmap); as a result, these concerns may not be properly encapsulated in the design.
- **Lack of refactoring**: As existing requirements change or new requirements emerge, the design needs to evolve holistically to accommodate the change. This is especially crucial when designers observe or foresee major problems such as explosion of classes. The lack of refactoring in such cases can lead to this smell.
- **Mixing up concerns**: When varying concerns that are largely orthogonal to each other are not separated but instead aggregated in a single hierarchy, it can result in an explosion of classes when the concerns vary.
- **Naive design decisions**: When designers naively use simplistic approaches such as creating a class for every combination of variations, it can result in unnecessarily complex designs.

### Suggested Refactoring

The generic refactoring suggestion for this smell is to encapsulate the variations. This is often achieved in practice through the application of relevant design patterns such as Strategy and Bridge

### Impacted Quality Attributes

- **Understandability**: When there are numerous services (many of which may not be used) embedded in an abstraction, the abstraction becomes complex and difficult to understand. Furthermore, in cases where variation is not hidden in a hierarchy, the explosion of classes that results increases the complexity of design. These factors impact the understandability of the design.
- **Changeability and Extensibility**: When variations in implementation in a type or a hierarchy are not encapsulated separately, clients are tightly coupled to these different variations. This has two effects when a new or different variation must be supported. First, it is harder to change the clients when they need to use a different or new variation. Second, it may result in explosion of classes (see Example 2 above). If variations were to be encapsulated separately, the clients would be shielded from changes in the variations. In the absence of such encapsulation, changeability and extensibility are impacted.
- **Changeability and Extensibility**: When variations in implementation in a type or a hierarchy are not encapsulated separately, clients are tightly coupled to these different variations. This has two effects when a new or different variation must be supported. First, it is harder to change the clients when they need to use a different or new variation. Second, it may result in explosion of classes (see Example 2 above). If variations were to be encapsulated separately, the clients would be shielded from changes in the variations. In the absence of such encapsulation, changeability and extensibility are impacted.
- **Reusability**: When services are embedded within an abstraction, the services cannot be reused in other contexts. When two or more orthogonal concerns are mixed up in the hierarchy, the resulting abstractions that belong to the hierarchy are harder to reuse. For instance, in Example 2, when encryption content type and algorithm are mixed together, we no longer have specific algorithm classes that could be reused in other contexts. These factors impact the reusability of the type or hierarchy.

### Aliases

This smell is also known in literature as:
- **Nested generalization**: This smell occurs when the first level in an inheritance hierarchy factors one generalization and the further levels “multiplies out” all possible combinations.
- **Class explosion**: This smell occurs when number of classes explode due to extensive use of multiple generalizations.
- **Combinatorial explosion**: This smell occurs when new classes need to be added to support each new family or variation.

## Unexploited Encapsulation

This smell arises when client code uses explicit type checks (using chained if-else or switch statements that check for the type of the object) instead of exploiting the variation in types already encapsulated within a hierarchy.

### Rationale

The presence of conditional statements (such as switch-case and if-else) in the client code that check a type explicitly and implement actions specific to each type is a well-known smell. Although this is a problem in general, consider the case when types are encapsulated in a hierarchy but are not leveraged. In other words, explicit type checks are being used instead of relying on dynamic polymorphism. This results in the following problems:

- Explicit type checks introduce tight coupling between the client and the concrete types, reducing the maintainability of the design. For instance, if a new type were to be introduced, the client will need to be updated with the new type check and associated actions.
- Clients need to perform explicit type check for all the relevant types in the hierarchy. If the client code misses checking one or more of the types in the hierarchy, it can lead to unexpected behavior at runtime. If runtime polymorphism were to be used instead, this problem could be avoided.
Such explicit type checks are an indication of the violation of the enabling technique “hide variations” and the principle of “encapsulate what varies” [54]. In the case of this smell, even though variation in types has been encapsulated within a hierarchy, it has not been exploited by the client code. Hence, we name this smell Unexploited Encapsulation.

### Potential Causes

- **Procedural thinking in object-oriented language**: Procedural languages such as C and Pascal do not support runtime polymorphism. Developers from procedural background think in terms of encoding types and use switch or chained if-else statements for choosing behavior. When the same approach is applied in an object-oriented language, it results in this smell.
- **Lack of application of object-oriented principles**: Inexperienced software developers may be aware of object-oriented design principles, but they do not have a firm enough grasp of these concepts to actually put them into practice. For instance, they may be familiar with the concept of hierarchy and polymorphism, but may not be aware of how to exploit these concepts properly to create a high-quality design. As a result, they may introduce type checks in their code.

### Suggested Refactoring

A straight-forward refactoring for this smell would be to get rid of conditional statements that determine behavior and introduce polymorphic methods in the hierarchy.

### Impacted Quality Attributes

- **Changeability and Extensibility**: A hierarchy helps encapsulate variation, and hence it is easy to modify existing variations or add support for new variations within that hierarchy without affecting the client code. With type-based switches, the client code needs to be changed whenever an existing type is changed or a new type is added. This adversely impacts changeability and extensibility.
- **Reusability**: Since the client code does type checks and executes certain functionality according to the type, reusability of the types in the hierarchy is impacted. This is because since some functionality is embedded in the client code, the types in the hierarchy do not contain all of the relevant functionality that may be needed to consider them as “reusable” units.
- **Testability**: When this smell is present, the same code segments may be replicated across the client code. This results in increased effort to test these fragments when compared to the effort if the code were to be provided within a hierarchy.
- **Reliability**: A common problem when using type-based checking is that programmers may overlook some of the checks. Such missing checks may manifest as defects.

### ANECDOTE

The authors heard an interesting story at a software engineering conference from one of the attendees who worked for a telecommunications company. In one of his projects, he came across a class named Pulse which has an association with a hierarchy rooted in Phone. There are two subclasses of the Phone class namely CellPhone and LandPhone. While analyzing the design fragment, he realized that the code in the Pulse class was performing explicit type checks for types in the Phone hierarchy.

When he asked a project team member about this explicit type checking, the team member revealed that earlier they were making a polymorphic call to the Phone hierarchy to compute the pulse charges. However, a later requirement necessitated the computation of the data charges incurred by a cell phone. This required the CellPhone class to additionally support computation of data charges.

Now, in this project, the ownership of the Phone hierarchy was with the Device Team and the ownership of the Pulse class was with the Billing Team. These two teams were geographically distributed across two continents. To prevent sweeping changes to critical portions of the code, it was decided that changes to the critical portions could be made only by the teams responsible for them. Thus, any changes to the Phone hierarchy (and this included the change to CellPhone to support computation of data charges) could be made only by the Device Team. Upon receiving the requirement, the Billing Team (which was responsible for the Pulse class) requested the Device Team to make changes to the CellPhone class. Since the Device Team already had a huge backlog of change requests, they could not fulfill this requirement in time for the product release. Left with little choice and under time pressure, the Billing Team implemented the requirement by explicitly checking for the type of Phone and calling existing methods of CellPhone to compute data charges in the Pulse class itself! This implementation led to the Unexploited Encapsulation smell.

The key learning from this anecdote is how the dynamics of two teams working on the same product may introduce smells. A proper coordination between the Device and Billing teams could have avoided this problem. Furthermore, it is understandable that the inefficient solution adopted by the Billing Team was necessary, given the time pressure, but adequate measures should have been taken to document it and ensure that this problem was rectified later. A follow-up should have been done after the product release to ensure that the CellPhone and Pulse classes were modified appropriately and the Unexploited Encapsulation smell was removed. However, over and over again, project teams forget about the shortcuts that they have used and fail to rectify their mistakes. This negligence builds up technical debt over time and eventually results in steep increase in maintenance costs for the project.

### Aliases

This smell is also known in literature as:

- **Simulated polymorphism**: This smell occurs when the code does conditional checks and chooses different behavior based on the type of the object to simulate polymorphism.
- **Improper use of switch statement**: This smell occurs when switch statements are used instead of employing runtime polymorphism.
- **instanceof checks**: This smell occurs when code has concentration of instanceof operators in a code block.
