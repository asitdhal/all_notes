## Abstract

A hierarchical organization of entities offers a powerful means to deal with complex relationships among real-world entities; it helps one to order the entities, deal with similarities and differences, and think in terms of generalizations and specializations.

> The principle of hierarchy advocates the creation of a hierarchical organization of abstractions using techniques such as classification, generalization, substitutability, and ordering.

Object-oriented languages support two kinds of hierarchies: type hierarchy (is-a relationship) and object hierarchy (part-of relationship). In this chapter, we concern ourselves with type hierarchies and use the term hierarchy synonymously with type hierarchy.We list below some key enabling techniques that we have gleaned from our experience that allow us to apply the principle of hierarchy in software design.

 1. **Apply meaningful classification**: Classify types to create a hierarchical organization using the following two steps:
		- **Identify commonalities and variations among types**: The commonalities are captured in a new abstraction which is referred as a supertype (this process is known as generalization). The variations are encapsulated in subtypes that inherit common behavior from supertypes. For a meaningful application of classification, the focus during this step should be more on capturing the commonalities and variation in the behavior of types rather than their data.
		- **Classify the supertypes and subtypes in levels to create a hierarchy**: The resulting hierarchy should have general types towards the root and the specialized types towards the leaves.

2. **Apply meaningful generalization**: As described in the previous enabling technique, generalization is the process wherein common behavior and elements between the subtypes are identified and factored out in supertypes. Meaningful generalization helps enhance reusability, understandability, and extensibility of the design. Further, generalization also helps reduce (or altogether eliminate) code duplication within types in the hierarchy.

3. **Ensure substitutability**: Ensure that the types in the hierarchy follow the Liskov’s Substitution Principle (LSP); in other words, a reference of a supertype can be substituted with objects of its subtypes without altering the behavior of the program. Adherence to this principle allows object-oriented programs to leverage dynamic polymorphism. This improves the changeability, extensibility, and reusability of the design.

4. **Avoid redundant paths**: An inheritance hierarchy allows us to explicitly express the logical relationship between related types. When there are redundant paths in an inheritance hierarchy, it unnecessarily complicates the hierarchy. This affects the understandability of the hierarchy and can also lead to runtime problems.

5. **Ensure proper ordering**: The key benefit of a hierarchical organization stems from its ability to provide us with a means to address the complex relationships among types. Hence, it is critical to express these relationships in an orderly fashion and maintain them consistently within the hierarchy. For example, in an inheritance hierarchy, it is intuitive to understand that a subtype depends on a supertype; however, when a supertype depends on its subtype(s), it is harder to understand the design since the ordering of dependencies is violated.

## Design Smells and the Hierarchy Enabling Techniques They Violate

Design Smell(s)	| Violated Enabling Technique
--------------- | ---------------------------
Missing Hierarchy, Unnecessary Hierarchy | Apply meaningful classification
Unfactored Hierarchy, Wide Hierarchy, Speculative Hierarchy, Deep Hierarchy | Apply meaningful generalization
Rebellious Hierarchy, Broken Hierarchy | Ensure substitutability
Multipath Hierarchy | Avoid redundant paths
Cyclic Hierarchy | Ensure proper ordering