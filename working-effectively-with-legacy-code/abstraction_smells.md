> The principle of abstraction advocates the simplification of entities through reduction and generalization: reduction is by elimination of unnecessary details and generalization is by identification and specification of common and important characteristics.

###  the principle of abstraction in software design

1. **Provide a crisp conceptual boundary and an identity.**:  Each abstraction should have a crisp and clear conceptual boundary and an identity. For instance, instead of “passing around” a group of data values representing a date, coordinates of a rectangle, or attributes of an image, they can be created as separate abstractions in the code.
2. **Map domain entities.**: There should be a mapping of vocabulary from the problem domain to the solution domain, i.e., objects recognized by the problem domain should be also represented in the solution domain. For instance, if there is a clip art that you can insert in a word processor application, it is easier to comprehend the design when it also has a corresponding abstraction named ClipArt.
3. **Ensure coherence and completeness.**: An abstraction should completely support a responsibility. For instance, consider a class representing a combo box. If the class supports disabling or hiding certain elements, it should provide an option for enabling or showing those elements as well; providing some of the methods and leaving a few would leave an abstraction incoherent and incomplete.
4. **Assign single and meaningful responsibility.**: Ensure that each abstraction has a unique non-trivial responsibility assigned to it. For instance, a class representing an image should not be overloaded with methods for converting one kind of image representation format to another format. Furthermore, it does not make sense to create a class named PI that serves as a placeholder for holding the value of the constant π.
5. **Avoid duplication.** Ensure that each abstraction—the name as well as its implementation—appears only once in design. For instance, you may require creating a class named List when designing a list control in Graphical User Interfaces, a linked-list data structure, or for maintaining a TO-DO list; however, naming all these abstractions as List will confuse the users of your design. Similarly, implementation duplication (copying the code for priority queue data structure for implementing a scheduler) will introduce additional effort in maintaining two pieces of the same code.

### Design Smells and the Abstraction Enabling Techniques They Violate

Design Smell | Violated Enabling Technique
------------ | ---------------------------
Missing Abstraction | Provide a crisp conceptual boundary and a unique identity
Imperative Abstraction | Map domain entities
Incomplete Abstraction | Ensure coherence and completeness
Multifaceted Abstraction, Unnecessary Abstraction, Unutilized Abstraction | Assign single and meaningful responsibility
Duplicate Abstraction | Avoid duplication

### Missing Abstraction

This smell arises when clumps of data or encoded strings are used instead of creating a class or an interface.

#### Rationale
An enabling technique for applying the principle of abstraction is to create entities with crisp conceptual boundaries and a unique identity. Since the abstraction is not explicitly identified and rather represented as raw data using primitive types or encoded strings, the principle of abstraction is clearly violated; hence, we name this smell Missing Abstraction.
Usually, it is observed that due to the lack of an abstraction, the associated data and behavior is spread across other abstractions. This results in two problems:

• It can expose implementation details to different abstractions, violating the principle of encapsulation.
• When data and associated behavior are spread across abstractions, it can lead to tight coupling between entities, resulting in brittle and non-reusable code. Hence, not creating necessary abstractions also violates the principle of modularization.

#### Potential Causes

1. **Inadequate design analysis**:  When careful thought is not applied during design, it is easy to overlook creating abstractions and use primitive type values or strings to “get the work done.” In our experience, this often occurs when software is developed under tight deadlines or resource constraints.
2. **Lack of refactoring**: As requirements change, software evolves and entities that were earlier represented using strings or primitive types may need to be refactored into classes or interfaces. When the existing clumps of data or encoded strings are retained as they are without refactoring them, it can lead to a Missing Abstraction smell.
3. **Misguided focus on minor performance gains**: This smell often results when designers compromise design quality for minor performance gains. For instance, we have observed developers using arrays directly in the code instead of creating appropriate abstractions since they feel that indexing arrays is faster than accessing members in objects. In most contexts, the performance gains due to such “optimizations” are minimal, and do not justify the resultant trade-off in design quality.

####  Suggested Refactoring
The refactoring for this smell is to create abstraction(s) that can internally make use of primitive type values or strings. For example, if a primitive type value is used as a “type-code,” then apply “replace type-code with class”; furthermore, create or move operations related to the data such as constructors, validation methods, and copying methods within the new class. 

#### Impacted Quality Attributes

1. **Understandability**: When a key entity is not represented as an abstraction and the logic that processes the entity is spread across the code base, it becomes difficult to understand the design.
2. **Changeability and Extensibility**: It is difficult to make enhancements or changes to the code when relevant abstractions are missing in design. First, it is difficult even to figure out the parts of the code that need to be modified to implement a change or enhancement. Second, for implementing a single change or enhancement, modifications need to be made at multiple places spread over the code base. These factors impact the changeability and extensibility of the design.
3. **Reusability and Testability**: Since some abstractions that correspond to domain or conceptual entities are missing, and the logic corresponding to the entities is spread in the code base, both reusability and testability of the design are impacted.
4. **Reliability**: An abstraction helps provide the infrastructure to ensure the correctness and integrity of its data and behavior. In the absence of an abstraction, the data and behavior is spread across the code base; hence, integrity of the data can be easily compromised. This impacts reliability.

#### Aliases
This smell is also known in literature as:

1. **Primitive obsession**: This smell occurs when primitive types are used for encoding dates, currency, etc. instead of creating classes.
2. **Data clumps**: This smell occurs when there are clumps of data items that occur together in lots of places instead of creating a class.

#### Practical Considerations

**Avoiding over-engineering**: Sometimes, entities are merely data elements and don’t have any behavior associated with them. In such cases, it may be over-engineering to represent them as classes or interfaces. Hence, a designer must carefully examine the application context before deciding to create an explicit abstraction. For example, check if the following are needed (a non-exhaustive list) to determine if creating an abstraction is warranted:

1. default initialization of data values using constructors
2. validation of the data values
3. support for pretty printing the data values
4. acquired resources (if any) are to be released


## Imperative Abstraction

This smell arises when an operation is turned into a class. This smell manifests as a class that has only one method defined within the class. At times, the class name itself may be identical to the one method defined within it. For instance, if you see class with name Read that contains only one method named read() with no data members, then the Read class has Imperative Abstraction smell. Often, it is also seen in the case of this smell that the data on which the method operates is located within a different class.
It should be noted that it is sometimes desirable to turn an operation into a class. 

### Rationale
The founding principle of object-orientation is to capture real world objects and represent them as abstractions. By following the enabling technique map domain entities, objects recognized in the problem domain need to be represented in the solution domain, too. Furthermore, each class representing an abstraction should encapsulate data and the associated methods. Defining functions or procedures explicitly as classes (when the data is located somewhere else) is a glorified form of structured programming rather than object-oriented programming. One-operation classes cannot be representative of an “abstraction,” especially when the associated data is placed somewhere else. Clearly, this is a violation of the principle of abstraction. Since these classes are ‘doing’ things instead of ‘being’ things, this smell is named Imperative Abstraction.

If operations are turned into classes, the design will suffer from an explosion of one-method classes and increase the complexity of the design. Furthermore, many of these methods that act on the same data would be separated into different classes and thus reduce the cohesiveness of the design. For these reasons, this smell also violates the principles of encapsulation and modularization.

### Potential Causes

**Procedural thinking**: A common cause for this smell is procedural thinking in an object-oriented setup. One of the authors was once involved in a project where a programmer from C background had designed a system in Java. In the resultant design, data that would have been encapsulated within “structs” in C was mapped to Java classes containing only public data members. The operations on the data were encapsulated in separate Java classes! The procedural thinking of his C experience resulted in the design of a procedural program that was, however, implemented in an object-oriented language.

### Suggested Refactoring

To refactor the Imperative Abstraction design smell, you have to either find or create an appropriate abstraction to house the method existing within the Imperative Abstraction. You also have to encapsulate the data needed by the method within the same abstraction to improve cohesion and reduce coupling.

### 3.2.5. Impacted Quality Attributes

1. **Understandability**: An abstraction with this smell does not have a direct mapping to the problem domain; it only represents a part of the behavior of a domain entity but does not represent the entity itself. Thus understandability of the abstraction is poor. Having numerous single-method classes leads to “class explosion” and complicates the design, and thus impacts the understandability of the overall design.

2. **Changeability and Extensibility**: The presence of an Imperative Abstraction smell does not impact the changeability and extensibility of the abstraction itself. However, if a modification or enhancement is required to a domain or conceptual entity (which is realized by multiple abstractions), it will require changes to multiple abstractions throughout the design. Thus, the changeability and extensibility of the overall design are impacted.

3. **Reusability—Consider**: the report generation functionality that was discussed in Example 1. If we want to reuse this functionality in a different context, we would have to take the CreateReport, CopyReport, and DisplayReport classes along with the class that serves to hold the report data, and adapt them to the new context. Clearly, this would require much more effort than if we were to reuse the Report class.

4. **Testability**: If an abstraction with Imperative Abstraction smell is self-sufficient, it is easy to test it. However, typically, in designs that have this smell, multiple abstractions with single operations need to be collectively tested. This impacts testability of the overall design.

### ANECDOTE
One of the anecdotes we came across in the Smells Forum concerned a toolkit development project written in Java. Just before the release of the product, experts external to the team performed a systematic design review to evaluate the design quality of the software.
During the review, it was observed that the name of a class was Diff and it had only one method named findDiff()! The findDiff() method would take two versions of a csv (comma separated values) file and report the difference between the files. It was obvious that the class had Imperative Abstraction smell.

A deeper look at the design revealed that most classes had names starting with a verb such as “generate,” “report,” “process,” and “parse.” These classes mostly had one public method with other methods serving as private helper methods. There was almost no use of class inheritance (interface inheritance was used, but mainly for implementing standard interfaces such as Comparable and Cloneable). This was a bigger finding—the design followed “functional decomposition” and not “object-oriented decomposition”!
Reflecting on the toolkit design revealed that object-oriented decomposition could have been more beneficial for the project, since such an approach would have helped making adaptations to variations as well as implementing new features easier. Since the toolkit consisted of tools that “do” things, developers unconsciously designed and evolved the software using a “functional decomposition” approach.
The design review results surprised the development team! They had never anticipated that the cause for one of the key structural problems in their project was a “procedural programming” mindset. Even though the team consisted of experienced designers, a major problem like this was overlooked.
However, changing the toolkit to follow an object-oriented decomposition approach would require rearchitecting the whole toolkit. Since there were already a large number of customers using the toolkit, it was a risky endeavor to take-up this reengineering activity. Hence, the management decided to live with the existing design.
Reflecting on this anecdote highlights the following key insights:

• Even when a person is part of and is familiar with a project, it is easy for him to overlook obvious design problems. Our experience shows that systematic and periodic design reviews by experts (external to the project) help identify opportunities for improving the 
design quality of the software [4].

• The presence of certain smells often indicates a much deeper problem in design. In this case, unraveling the cause behind the Imperative Abstraction smell revealed “functional decomposition” to be the deeper problem.

• It is ironic that the most critical problems are often found just before a release. It is very risky to “fix” the design so late in such cases. So, learning about smells and avoiding them throughout the development cycle is important in maintaining the quality of the design and avoiding technical debt.

###  Aliases
This smell is also known in literature as:

**Operation class**: This smell occurs when an operation that should have been a method within a class has been turned into a class itself.

### Reification
“Reification” is the promotion or elevation of something that is not an object into an object. When we reify behavior, it is possible to store it, pass it, or transform it. Reification improves flexibility of the system at the cost of introducing some complexity [52].
Many design patterns [54] employ reification. Examples:

* State pattern: Encoding a state-machine.
* Command pattern: Encoding requests as command objects. A permitted exception for this smell is when a Command pattern has been used to objectify method requests.
* Strategy pattern: Parameterizing a procedure in terms of an operation it uses.

In other words, when we consciously design in such a way to elevate non-objects to objects for better reusability, flexibility, and extensibility (i.e., for improving design quality), it is not a smell.

### Example 1

Consider the case of a large-sized financial application. This application employs classes named CreateReport, CopyReport, DisplayReport, etc. to deal with its report generation functionality. Each class has exactly one method definition named create, copy, display, etc., respectively, and suffers from Imperative Abstraction smell. The data items relating to a report such as name of the report, data elements that need to be displayed in the report, kind of report, etc. are housed in a “data class” named Report.
The smell not only increases the number of classes (in this case there are at least four classes when ideally one could have been used), but also increases the complexity involved in development and maintenance because of the unnecessary separation of cohesive methods.

![Nothing](images/imperative-abstraction-report.jpg)

For the report generation part of the financial application, a suggested refactoring is to move the methods in each of the classes suffering from Imperative Abstraction to the Report class itself. Moving all the report-related operations to the Report class makes the Report class a proper “abstraction” and also removes the Imperative Abstraction smell. The design becomes cleaner and less complex.

![Nothing](images/imperative-abstraction-report-after.jpg)


## Incomplete Abstraction

This smell arises when an abstraction does not support complementary or interrelated methods completely. For instance, the public interface of an abstraction may provide an initialize() method to allocate resources. However, a dispose() method that will allow clean-up of the resources before it is deleted or re-collected1 may be missing in the abstraction. In such a case, the abstraction suffers from Incomplete Abstraction smell because complementary or interrelated methods are not provided completely in its public interface.

### Rationale

One of the key enabling techniques for abstraction is to “create coherent and complete abstractions.” One of the ways in which coherence and completeness of an abstraction may be affected is when interrelated methods are not supported by the abstraction. For example, if we need to be able to add or remove elements in a data structure, the type abstracting that data structure should support both add() and remove() methods. Supporting only one of them makes the abstraction incomplete and incoherent in the context of those interrelated methods.

If an abstraction supports such interrelated methods only partially, it may force the users of the abstraction to implement the rest of the functionality. Sometimes, to overcome the problem of such an incomplete interface exposed by an abstraction, clients attempt to access the internal implementation details of the abstraction directly; in this way, encapsulation may be violated as a side-effect of not applying the principle of abstraction properly. Further, since one of the interrelated methods is implemented somewhere else, cohesion is compromised and the Single Responsibility Principle (SRP) (which per definition requires a responsibility to be completely encapsulated by a class) is violated. Since complementary or interrelated methods are not provided completely in the abstraction, this smell is named Incomplete Abstraction.

### Potential Causes

1. **Missing overall perspective**: When designers or developers create abstractions to add new features, their focus is usually limited to the actual specific requirements that need to be supported. In this process, they overlook if the abstraction supports all the interrelated methods completely or not.

2. **Not adhering to language or library conventions**: Consider the container java.util.HashMap in JDK. For retrieving an element based on the given input key, the get() method first compares the hash code of the input key (using the hashCode() method) with the mapped key found in the hash map; next, it invokes equals() method on the input key to check if it equals the mapped key. Now, if a class does not override hashCode() and equals() methods together and the objects of that class are used in HashMap, it is possible that the retrieval mechanism will not work correctly. Hence, one of the conventions required by the Java library is that these two methods should be overridden together in a class when the objects of that class are used with hash-based containers.
Such conventions also exist in other languages such as C# and C++. For instance, in C++, it is better to define either all of default constructor, copy constructor, assignment operator, and destructor (virtual destructor if the class would be used polymorphically) or none. If there are any missing definitions (e.g., virtual destructor), the abstraction is incomplete and could result in runtime problems.

Sometimes developers overlook such language or library conventions, and provide only partial set of methods. In such cases, it leads to Incomplete Abstraction smell.

### ANECDOTE

An attendee at one of the design forums shared an interesting anecdote that is relevant here. He was working on the development of a software tool that facilitated the configuration of smart devices. A week before the tool’s release, it was observed that the tool was characterized by a number of memory leaks. To quickly identify and address the cause of these runtime memory leaks, an expert on memory leaks was called in as a consultant.

Based on past experience, the consultant could guess that the reason for memory leaks was that there were some objects that were not getting cleaned up properly. So, he started examining how and when objects were getting deleted at runtime. He found out that while there was a systematic procedure to create objects, there was little or no attention paid to how those created objects were eventually deleted. Most developers had seemingly completely ignored the deletion of created objects!

To understand the reason, the consultant decided to dig deeper. He found out that the source of the problem was that the project architect had only listed “creation” methods in the design document that he had handed to the developers. When queried, the project architect said that he expected the software developers to “understand” that a method marked for object creation in his design was actually a placeholder for both “creation and deletion” methods. He stated that it is obvious that objects that are created need to be cleaned up and, therefore, the software developers writing the code should have taken care of object deletion as well.

When the consultant asked the software developers responsible for code construction about this, they said that they are told to “strictly follow the design drafted by the architect” so that there is no architecture erosion or over-engineering on their part. Therefore, they tended to overlook or seldom care about concerns not explicitly captured in the design.

There are two insights that emerge from this anecdote:

* Architects should be extremely careful and diligent about what is communicated in the design document. A stringent review of design is needed to expose issues such as the ones mentioned above.
* A healthy environment needs to exist in the project team that allows developers to give feedback to the architect(s) about the prescribed design. In fact, new age development methodologies such as those based on Agile promote the idea of “collective ownership” of the design and architecture. This means making all the team members responsible for the design and involving all of them in the design decisions that are made so that everyone’s understanding is enhanced and a high-quality design emerges.

### Suggested Refactoring

If you find interfaces or classes that are missing “complementary and symmetric” methods, it possibly indicates an Incomplete Abstraction. Table 3.2 lists some of the common pairs of operations. Note that these merely serve as exemplars of common usage and the actual names will vary depending on the context. For example, the names of operations in the context of a Stack would be push and pop, whereas in the context of data streams, it would be source and sink. The refactoring strategy for this smell is to preferably introduce the missing complementary operation(s) in the interface or class itself.

Also, if there are any language or library conventions that require providing interrelated methods together, the refactoring is to add any such missing methods in the abstraction.

### Impacted Quality Attributes

1. **Understandability**: Understandability of the abstraction is adversely impacted because it is difficult to comprehend why certain relevant method(s) are missing in the abstraction.
2. **Changeability and Extensibility**: Changeability and extensibility of the abstraction are not impacted. However, when complementary methods are separated across abstractions, changes or enhancements to one method in an abstraction may impact the other related abstractions.
3. **Reusability**: If some of the operations are not supported in an abstraction, it is harder to reuse the abstraction in a new context because the unsupported operations will need to be provided explicitly.
4. **Reliability**: An Incomplete Abstraction may not implement the required functionality, resulting in defects. Furthermore, when methods are missing, the clients may have to work around to provide the required functionality, which can be error-prone and lead to runtime problems. For instance, when a method for locking is provided in an abstraction without the related unlock method, the clients will not be able to release the lock. When clients try to implement the unlock functionality in their code, they may try to directly access the internal data structures of the abstraction; such direct access can lead to runtime errors.

### Aliases

This smell is also known in literature as:

1. **Class supports incomplete behavior**: This smell occurs when the public interface of a class is incomplete and does not support all the behavior needed by objects of that class.
2. **Half-hearted operations**: This smell occurs when interrelated methods provided in an incomplete or in an inconsistent way; this smell could lead to runtime problems.

### Practical Considerations

1. **Disallowing certain behavior**: Sometimes, a designer may make a conscious design decision to not provide symmetric or matching methods. For example, in a read-only collection, only add() method may be provided without the corresponding remove() method. In such a case, the abstraction may appear incomplete, but is not a smell.
2. **Using a single method instead of a method pair**: Sometimes, APIs choose to replace symmetrical methods with a method that takes a boolean argument (for instance, to enforce a particular naming convention such as JavaBeans naming convention that requires accessors to have prefixes “get,” “is,” or “set”). For example, classes such as java.awt.MenuItem and java.awt.Component originally supported disable() and enable() methods. These methods were deprecated and are now replaced with setEnabled(boolean) method. Similarly, java.awt.Component has the method setVisible(boolean) that deprecates the methods show() and hide(). One would be tempted to mark these classes as Incomplete Abstractions since they lack symmetric methods, i.e., getEnabled() and getVisible() respectively. However, since there is no need for corresponding getter methods (as these methods take a boolean argument), these classes do not have Incomplete Abstraction smell.



## Multifaceted Abstraction

This smell arises when an abstraction has more than one responsibility assigned to it.

### Rationale

An important enabling technique to effectively apply the principle of abstraction is to assign single and meaningful responsibility for each abstraction. In particular, the Single Responsibility Principle says that an abstraction should have a single well-defined responsibility and that responsibility should be entirely encapsulated within that abstraction. An abstraction that is suffering from Multifaced Abstraction has more than one responsibility assigned to it, and hence violates the principle of abstraction.

The realization of multiple responsibilities within a single abstraction leads to a low degree of cohesion among the methods of the abstraction. Ideally, these responsibilities should have been separated out into well-defined, distinct, and cohesive abstractions. Thus, the low degree of cohesion within a Multifaceted Abstraction also leads to the violation of the principle of modularization.

Furthermore, when an abstraction includes multiple responsibilities, it implies that the abstraction will be affected and needs to be changed for multiple reasons. Our experience shows that there is often a strong correlation between the frequency of change in elements of a design and the number of defects in that element. This means that a Multifaceted Abstraction may likely suffer from a greater number of defects. A Multifaceted Abstraction lowers the quality of the design and should be refactored to avoid building up the technical debt. Since the abstraction has multiple “faces” or “responsibilities”, it is named Multifaceted Abstraction.

### Potential Causes

1. **General-purpose abstractions**: When designers introduce an abstraction with a generic name (examples: Node, Component, Element, and Item), it often becomes a “placeholder” for providing all the functionality related (but not necessarily belonging) to that abstraction. Hence, general purpose abstractions often exhibit this smell.

2. **Evolution without periodic refactoring**: When a class undergoes extensive changes over a long period of time without refactoring, other responsibilities start getting introduced in these classes and design decay starts. In this way, negligence toward refactoring leads to the creation of monolithic blobs that exhibit multiple responsibilities.

3. **The burden of processes**: Sometimes the viscosity of the software and environment (discussed earlier in Section 2.2.5) serves to discourage the adoption of good practices. As discussed in the case of Insufficient Modularization smell (Section 5.2), to circumvent the long process that must be followed when a new class is added to the design, developers may choose to integrate new unrelated features within existing classes leading to Multifaceted Abstraction smell.

4. **Mixing up concerns**: When designers don’t give sufficient attention to the separation of different concerns (e.g., not separating domain logic from presentation logic), the resulting abstraction will have Multifaceted Abstraction smell.

### ANECDOTE

While working as an independent consultant, one of the authors was asked by a start-up company to help them identify refactoring candidates in a project that involved graphical displays in hand-held devices. The author ran many tools including object-oriented metrics, design smells detection, and clone detector tools on the code-base to identify refactoring candidates. One of the classes identified for refactoring was a class named Image. The class had 120 public methods realizing multiple responsibilities in more than 50,000 lines of code. When running a design smell detection tool, the tool showed Image class to have “Schizophrenic class” smell (the tool defined “Schizophrenic class” smell as a class having a large public interface with clients using disjoint group of methods from the class interface).

When the author proposed refactoring of the Image class, the team objected, saying that the Image class followed the SRP and hence there was no need to refactor it. Specifically, they argued that the class had a comprehensive set of methods that related to loading, rendering, manipulating, and storing the image, and all these methods were logically related to images.
In their argument, the project team completely missed the notion of “granularity” of responsibilities. If you take a look at an image processing application, almost everything is related to images: does it mean we should put everything in a single class named Image? Absolutely not. Each abstraction has a concrete, specific, and precise responsibility (instead of a coarse-grain high-level responsibility).

Coming back to the case under discussion, upon further probing, the team lead admitted that the class was one of the most frequently modified classes during defect fixes and feature enhancements. He also added that they changed the Image class for different reasons. The author and the team lead then had several discussions on how this problem could be avoided. Finally, the team lead accepted that there was an urgent need to refactor the Image class. As a result, four modules (or namespaces)—named ImageLoader, ImageRenderer, ImageProcessor, and ImageWriter (relating to loading, rendering, manipulating, and persisting functionality, respectively)—were extracted from the original Image class. With this refactoring, the main benefit was that the changes became localized. For example, if any change that related to image rendering was required, only the code in ImageRenderer module would need to be changed.

The main take-away that can be identified from this experience is that “responsibility” (as in Single “Responsibility” Principle) does not mean logical grouping of all functionality related to a concept. Rather “responsibility” refers to a concrete, specific, and precise responsibility that has one reason to change!

### Suggested Refactoring

When a class has multiple responsibilities, it will be non-cohesive. However, it is likely that you will find methods and fields that form logical clusters. These logical clusters are candidates for applying “extract class” refactoring. In particular, new class(es) can be created out of existing classes having Multifaceted Abstraction smell and the relevant methods and fields can be moved to the new class(es). Note that, depending on the context, the extracted class may either be used directly only by the original class, or may be used by the clients of the original class 

### Impacted Quality Attributes

1. **Understandability**: A Multifaceted Abstraction increases the cognitive load due to multiple aspects realized into the abstraction. This impacts the understandability of the abstraction.
2. **Changeability and Extensibility**: When an abstraction has multiple responsibilities, it is often difficult to figure out what all members within the abstraction need to be modified to address a change or an enhancement. Furthermore, a modification to a responsibility may impact unrelated responsibilities provided within the same abstraction, which may lead to ripple effects across the entire design.
3. **Reusability**: Ideally, a well-formed abstraction that performs a single responsibility has the potential to be reused as a unit. When an abstraction has multiple responsibilities, the entire abstraction must be used even if only one of the responsibilities needs to be reused. In such a case, the presence of unnecessary responsibilities may become a costly overhead that must be addressed, thus impacting the abstraction’s reusability. Furthermore, in an abstraction with multiple responsibilities, sometimes the responsibilities may be intertwined. In such a case, even if only a single responsibility needs to be reused, the overall behavior of the abstraction may be unpredictable affecting its reusability.
4. **Testability**: When an abstraction has multiple responsibilities, these responsibilities may be entwined with each other, making it difficult to test each responsibility separately. Thus, testability is impacted.
5. **Reliability**: The effects of modification to an abstraction with intertwined responsibilities may be unpredictable and lead to runtime problems.

### Aliases

This smell is also known in literature as:

1. **Divergent change**: This smell occurs when a class is changed for different reasons.
2. **Conceptualization abuse**: This smell occurs when two or more non-cohesive concepts have been packed into a single class of the system.
3. **Large class**: This smell occurs when a class has “too many” responsibilities.
4. **Lack of cohesion**: This smell occurs when there is a large type in a design with low cohesion, i.e., a “kitchen sink” type that represents many abstractions.


## Unnecessary Abstraction

This smell occurs when an abstraction that is actually not needed (and thus could have been avoided) gets introduced in a software design.

### Rationale

A key enabling technique to apply the principle of abstraction is to assign single and meaningful responsibility to entities. However, when abstractions are created unnecessarily or for mere convenience, they have trivial or no responsibility assigned to them, and hence violate the principle of abstraction. Since the abstraction is needlessly introduced in the design, this smell is named Unnecessary Abstraction.

### Potential Causes

1. **Procedural thinking in object-oriented languages**: According to Johnson et al. [60], programmers from procedural background who are new to object-oriented languages tend to produce classes that “do” things instead of “being” things. Classes in such designs tend to have just one or two methods with data located in separate “data classes.” Such classes created from procedural thinking usually do not have unique and meaningful responsibilities assigned to them.

2. **Using inappropriate language features for convenience**: Often, programmers introduce abstractions that were not originally intended in design for “getting-the-work-done” or just for “convenience.” Using “constant interfaces”3 instead of enums, for example, is convenient for programmers. By implementing constant interfaces in classes, programmers don’t have to explicitly use the type name to access the members and can directly access them in the classes. It results in unnecessary interfaces or classes that just serve as placeholders for constants without any behavior associated with the classes.

3. **Over-engineering**: Sometimes, this smell can occur when a design is over-engineered. Consider the example of a customer ID associated with a Customer object in a financial application. It may be overkill to create a class named CustomerID because the CustomerID object would merely serve as holder of data and will not have any non-trivial or meaningful behavior associated with it. A better design choice in this case would be to use a string to store the customer ID within a Customer object.

### Suggested Refactoring

The generic refactoring suggestions to address this smell include the following:

1. A class that isn’t doing enough to pay for itself should be eliminated.
2. Consider applying “inline class” refactoring to merge the class with another class.
3. If a class or interface is being introduced to encode constants, check if you can use a more suitable alternative language feature such as enumerations instead.

### Impacted Quality Attributes

1. **Understandability**: Having needless abstractions in the design increases its complexity unnecessarily and affects the understandability of the overall design.
2. **Reusability**: Abstractions are likely to be reusable when they have unique and well-defined responsibilities. An abstraction with trivial or no responsibility is less likely to be reused in a different context.

### Aliases

This smell is also known in literature as:

1. **Irrelevant class**: This smell occurs when a class does not have any meaningful behavior in the design.
2. **Lazy class/Freeloader**: This smell occurs when a class does “too little.”
3. **Small class**: This smell occurs when a class has no (or too few) variables or no (or too few) methods in it.
4. **Mini-class**: This smell occurs when a public, non-nested class defines less than three methods and less than three attributes (including constants) in it.
5. **No responsibility**: This smell arises when a class has no responsibility associated with it.
6. **Agent classes**: This smell arises when a class serve as an “agent” (i.e., they only pass messages from one class to another), indicating that the class may be unnecessary.

### Practical Considerations

1. **Delegating abstractions in design patterns**: Some design patterns (e.g., Mediator, Proxy, Façade, and Adapter) that employ delegation have a class that may appear to be an Unnecessary Abstraction. For example, in case of the Object Adapter pattern, the Adapter class may appear to merely delegate client requests to the appropriate method on the Adaptee [54]. However, the primary intention behind the Adapter class is to fulfill the specific, well-defined responsibility of adapting the Adaptee’s interface to the client needs. Hence, one has to carefully consider the context before deciding whether an abstraction that just performs delegation is unnecessary or not.

2. **Accommodating variations**: Consider the example of java.lang.Math and java.lang.StrictMath classes, which provide almost similar math-related functionality. The JavaDoc for StrictMath notes: “By default many of the Math methods simply call the equivalent method in StrictMath for their implementation.” It may appear from the JavaDoc description that Math is an Unnecessary Abstraction that simply delegates calls to StrictMath. However, although both support math-related functionality, StrictMath methods return exactly the same results irrespective of the platform because the implementation conforms to the relevant floating-point standard, whereas Math methods may use native hardware support for floating-point numbers and hence return slightly different results. Here, note that Math is less portable but can result in better performance when compared to StrictMath. Therefore, it is a conscious design decision to create two abstractions to accommodate variation in objectives, i.e., portability and performance.

### Unutilized Abstraction

This smell arises when an abstraction is left unused (either not directly used or not reachable). This smell manifests in two forms:

1. Unreferenced abstractions—Concrete classes that are not being used by anyone
2.  Orphan abstractions—Stand-alone interfaces/abstract classes that do not have any derived abstractions

### Rationale

One of the enabling techniques for applying the principle of abstraction is to assign a single and meaningful responsibility to an entity. When an abstraction is left unused in design, it does not serve a meaningful purpose in design, and hence violates the principle of abstraction.

Design should serve real needs and not imagined or speculative needs. Unrealized abstract classes and interfaces indicate unnecessary or speculative generalization, and hence are undesirable. This smell violates the principle YAGNI (You Aren’t Gonna Need It), which recommends not adding functionality until deemed necessary [53]. Since the abstraction is left unutilized in the design, this smell is named Unutilized Abstraction.

### Potential Causes

1. **Speculative design**: When designers attempt to make the design of a system “future-proof” or provide abstractions “just in case it is needed in future,” it can lead to this smell.

2. **Changing requirements**: When requirements keep changing, the abstractions created for satisfying an earlier requirement often may not be needed anymore. However, when the abstraction continues to remain in the design, it becomes an Unutilized Abstraction.

3. **Leftover garbage during maintenance**: When maintenance or refactoring activities are performed without cleaning up the old abstractions, it could result in unreferenced abstractions.

4. **Fear of breaking code**: Often, one of the reasons why developers do not delete old code is that they are not sure if any other class in the code is still using it. This is especially true in large code-bases where it is difficult to determine whether a piece of code is being used or not.

### ANECDOTE
During maintenance and refactoring, developers often refactor code bases that result in some of the old classes becoming obsolete. However, such code segments are often left in the code base (sometimes by mistake), making the source code bulge in size. Consider this documentation from the Java bug database for a set of classes in JDK source base (Check the original bug report “JDK-7161105: unused classes in jdk7u repository” here: http://bugs.sun.com/view_bug.do?bug_id=7161105):

“The ObjectFactory/SecuritySupport classes that were duplicated in many packages were replaced by a unified set under internal/utils. When the jaxp 1.4.5 source bundle was dropped, however, it was a copy of the source bundle on top of the original source, therefore leaving these classes behind unremoved.”
The left-over code was later removed from the source base. What is the lesson that we can learn from this bug report? During refactoring or maintenance, it is important to ensure that old code is deleted from the code base to avoid its bloating due to dead code and unused classes.

### Suggested Refactoring

The simplest refactoring is to remove the Unutilized Abstraction from the design. However, in the case of APIs, removing an abstraction is not feasible since there may be client code (or legacy code) that may be still referring to it. In these cases, such abstractions may be marked “obsolete” or “deprecated” to explicitly state that they must not be used by new clients.

### ANECDOTE

Unutilized Abstraction is a commonly occurring smell across development organizations. Interestingly, the main reason behind the occurrence of this smell is changing requirements. In one instance that the authors are aware of, the project was following the Scrum process. In each sprint, suitable tasks would be identified from the backlog for development. Just like in other real-world projects, after a certain sprint, the customer changed his requirements, and consequently a new feature was expected to replace the old one.

Before the next sprint commenced, the allocation of tasks for the development of the new feature was done. It turned out that the new task was assigned to a different developer. This developer, instead of reusing the existing implementation, overlooked it and reinvented the wheel. In other words, he created new classes that were doing the same thing but the old classes were never removed and were left unused leading to the Unutilized Abstraction smell.


### Impacted Quality Attributes

1. **Understandability**: Presence of unused abstractions in design pollutes the design space and increases cognitive load. This impacts understandability.
2. **Reliability**: The presence of unused abstractions can sometimes lead to runtime problems. For instance, when code in unused abstractions gets accidentally invoked, it can result in subtle bugs affecting the reliability of the software.

### Aliases

This smell is also known in literature as:

1. **Unused classes**: This smell occurs when a class has no direct references and when no calls to that class’s constructor are present in the code.
2. **Speculative generality**: This smell occurs when classes are introduced, speculating that they may be required sometime in future.

### Practical Considerations

**Unutilized Abstractions in APIs**: Class libraries and frameworks usually provide extension points in the form of abstract classes or interfaces. They may appear to be unused within the library or framework. However, since they are extension points that are intended to be used by clients, they cannot be considered as Unutilized Abstractions.


## Duplicate Abstraction

This smell arises when two or more abstractions have identical names or identical implementation or both. The design smell exists in two forms:

1. **Identical name**: This is when the names of two or more abstractions are identical. While two abstractions can accidentally have the same name, it needs to be analyzed whether they share similar behavior. If their underlying behavior is not related, the main concern is simply the identical names. This would impact understandability since two distinct abstractions have an identical name. However, in case the underlying behavior of the two abstractions is also identical, it is a more serious concern and could indicate the “identical implementation” form of Duplicate Abstraction.
2. **Identical implementation**: This is when two or more abstractions have semantically identical member definitions; however, the common elements in those implementations have not been captured and utilized in the design. Note that the methods in these abstractions might have similar implementation, but their signatures might be different. Additionally, identical implementation occurring in siblings within an inheritance hierarchy may point to an Unfactored Hierarchy smell (see Section 6.3).
This design smell also includes the combination of these forms. We have come across designs with “duplicate” abstractions that have identical names, identical method declarations (possibly with different signatures), and similar implementation.
Identical implementation could occur in four forms following the nature of “code clones” (fragments of code that are very similar). The sidebar “types of code clones” summarizes these four forms.

### TYPES OF CODE CLONES

Code clones can be similar textually in the code, or they could be similar in their functionally but could differ textually. Based on the level of similarity, code clones can be considered to be of four types [16]:

#### Textually Similar Clones

- **Type 1**: Two code fragments are clones of type-1 when the fragments are exactly identical except for variations in whitespace, layout, and comments.
- **Type 2**: Two code fragments are clones of type-2 when the fragments are syntactically identical except for variation in symbol names, whitespace, layout, and comments.
- **Type 3**: Two code fragments are clones of type-3 when the fragments are identical except some statements changed, added, or removed, in addition to variation in symbol names, whitespace, layout, and comments.

#### Functionally Similar Clones

- **Type 4**: Two code fragments are clones of type-4 when the fragments are semantically identical but implemented by syntactic variants.

### Rationale

**Avoid duplication** is an important enabling technique for the effective application of the principle of abstraction.
If two or more abstractions have an identical name, it affects understandability of the design. Developers of client code will be confused and unclear about the choice of the abstraction that should be used by their code.

If two or more abstractions have identical implementation (i.e., they have duplicate code), it becomes difficult to maintain them. Often, a change in the implementation of one of these abstractions will need to be reflected across all other duplicates. This introduces not only an overhead but also the possibility of subtle difficult-to-trace bugs. Duplication should be avoided as much as possible to reduce the extent of change required.

In summary, this smell indicates a violation of the DRY (Don’t Repeat Yourself) principle. The DRY principle mandates that every piece of knowledge must have a single unambiguous representation within a system. If the DRY principle is not followed, a modification of an element within the system requires modifications to other logically unrelated elements making maintainability a nightmare. Since there is duplication among abstractions in the design, this smell is named Duplicate Abstraction.

### Potential Causes

A Duplicate Abstraction smell can arise due to many reasons. While some of these reasons are generic, some are specific to a particular programming paradigm or platform.

- **Copy-paste programming**: The “get-the-work-done” mindset of a programmer leads him to copy and paste code instead of applying proper abstraction.
- **Ad hoc maintenance**: When the software undergoes haphazard fixes or enhancements over many years, it leaves “crufts”6 with lots of redundant code in it.
- **Lack of communication**: Often, in industrial software, code duplication occurs because different people work on the same code at different times in the life cycle of the software. They are not aware of existing classes or methods and end up re-inventing the wheel.
- **Classes declared non-extensible**: When a class that needs to be extended is declared non-extensible (for example, by declaring a class as final in Java), developers often resort to copying the entire code in a class to create a modified version of the class.

### ANECDOTE

An very unusual problem was reported by one of the attendees during one of our guest lectures on design smells. The attendee was involved as a consultant architect in a large Java project. He found that the project team had spent several hours debugging the following problem.

While invoking a method from a third-party library, an exception named MissingResourceException was getting thrown at times. To handle that exception, one of the team members had added a catch handler for that exception and logged that exception using a logging library. However, what was frustrating to the team was that even after adding the catch handler, the application sometimes crashed with the same exception!

To aid debugging, the attendee suggested that the logging code be removed and the stack trace be printed on the console. This was done and the application was executed repeatedly. Eventually, when the crash occurred again, it was found that the exception thrown was not java.util. MissingResourceException which the code was handing, but was a custom exception named MissingResourceException that was defined in a package in the third-party library. Since these exceptions are RuntimeExceptions, the compiler did not issue any error for the catch block for catching an exception that could never get thrown from that code block!

Although this appeared to be a programming mistake, a deeper reflection revealed that it was in fact a design problem: the design specification did not mention specifically the type of exception that should have been handled. This, in combination with the identical names of the two exception classes resulted in the programmer handling the wrong exception.

This incident also highlighted how careful one should be when designing an API. In this case, since the name of an exception defined by the third-party API clashes with an existing JDK exception, it resulted in problems for the clients of the third-party API.

In summary, the important lessons that can be learned from this experience are:

- Design problems can cause hard-to-find defects
- Naming is important; avoiding duplicate names is especially important!

### Suggested Refactoring

For identical name form, the suggested refactoring is to rename one of the abstractions to a unique name.
In the case of the identical implementation form of Duplicate Abstraction, if the implementations are exactly the same, one of the implementations can be removed. If the implementations are slightly different, then the common implementation in the duplicate abstractions can be factored out into a common class. This could be an existing supertype in the existing hierarchy (see refactoring for Unfactored Hierarchy (Section 6.3)) or an existing/new class which can be “referred to” or “used” by the duplicate abstractions.

### Impacted Quality Attributes

- **Understandability**: Developers can become confused about which abstraction to use when there are two or more abstractions with identical names or implementation. Further, duplicate implementations bloat the code. These factors impact the understandability of the design.
- **Changeability and Extensibility**: Change or enhancement involving one abstraction potentially requires making the same modification in the duplicate abstractions as well. Hence, changeability and extensibility are considerably impacted.
- **Reusability**: Duplicate abstractions often have slightly different implementations (especially Type 3 and Type 4 clones). The differences in implementations are usually due to the presence of context-specific elements embedded in the code. This makes the abstractions hard to reuse in other contexts; hence, reusability of the abstractions is impacted.
- **Reliability**: When two abstractions have identical names, a confused developer may end-up using the wrong abstraction. For example, he may type cast to a wrong type, leading to a runtime problem. In case of abstractions with identical implementations, a modification in one of the abstractions needs to be duplicated across the other copies failing which a defect may occur.

### Aliases

This smell is also known in literature as:

- “Alternative classes with different interfaces” [7]—This smell occurs when classes do similar things, but have different names.
- “Duplicate design artifacts” [74]—This smell occurs when equivalent design artifacts are replicated throughout the architecture.

### Practical Considerations

- **Accommodating variations**: One reason why duplicate abstractions may exist is to support synchronized and unsynchronized variants. A synchronized variant of an abstraction may have used synchronization constructs heavily and this may lead to creating separate abstractions (that suffer from Duplicate Abstraction smell) corresponding to the two variants. An example of this is seen in java.util.Vector and java.util.ArrayList classes that have similar method definitions. The main difference between these classes is that the former is thread-safe and the latter is not thread-safe.
- **Duplicate type names in different contexts**: It is hard to analyze and model large domains and create a unified domain model. In fact, “total unification of the domain model for a large system will not be feasible or cost-effective” [17]. One solution offered by Domain Driven Design is to divide the large system into “Bounded Contexts.” In this approach, the resulting models in different contexts may result in types with same names. Since Bounded Context is one of the patterns that help deal with the larger problem of modeling large domains, such types with same names in different contexts is acceptable.
- **Lack of language support for avoiding duplication**: Many methods and classes are duplicated in JDK because generics support is not available for primitive types. For example, the code for methods such as binarySearch, sort, etc. are duplicated seven times in the java.util.Arrays class because it is not possible to write a single generic method that takes different primitive type arrays. This results in the class suffering from a bloated interface.