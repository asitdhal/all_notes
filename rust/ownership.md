### Understanding Ownership

#### the stack and the heap
* Both the stack and the heap are parts of memory that is available to your code to use at runtime, but they are structured in different ways. The stack stores values in the order it gets them and removes the values in the opposite order. This is referred to as last in, first out. Think of a stack of plates: when you add more plates, you put them on top of the pile, and when you need a plate, you take one off the top. Adding or removing plates from the middle or bottom wouldn’t work as well! Adding data is called pushing onto the stack, and removing data is called popping off the stack.

* The stack is fast because of the way it accesses the data: it never has to search for a place to put new data or a place to get data from because that place is always the top. Another property that makes the stack fast is that all data on the stack must take up a known, fixed size.

* For data with a size unknown to us at compile time or a size that might change, we can store data on the heap instead. The heap is less organized: when we put data on the heap, we ask for some amount of space. The operating system finds an empty spot somewhere in the heap that is big enough, marks it as being in use, and returns to us a pointer, which is the address of that location. This process is called allocating on the heap, and sometimes we abbreviate the phrase as just “allocating.” Pushing values onto the stack is not considered allocating. Because the pointer is a known, fixed size, we can store the pointer on the stack, but when we want the actual data, we have to follow the pointe

* Accessing data in the heap is slower than accessing data on the stack because we have to follow a pointer to get there. Contemporary processors are faster if they jump around less in memory.

#### Ownership Rules

1. Each value in Rust has a variable that’s called its owner.
2. There can only be one owner at a time.
3. When the owner goes out of scope, the value will be dropped.

#### Variable Scope
* A scope is the range within a program for which an item is valid. 

```rust
{                      // s is not valid here, it’s not yet declared
    let s = "hello";   // s is valid from this point forward

    // do stuff with s
}                      // this scope is now over, and s is no longer valid
```

* The memory is automatically returned once the variable that owns it goes out of scope.

#### Ways Variables and Data Interact
* Multiple variables can interact with the same data in different ways in Rust. 

#### Copy
* If variable is created on stack, assigning one with other will lead to a copy. Usually, scalar values follows this rule.

```rust
fn main() {
    let mut x = 5;
    let mut y = x;
    println!("x={} y={}", x, y);
    x = x + 1;
    y = y + 2;
    println!("x={} y={}", x, y);

    let mut arr1= [1, 2, 3, 4, 5];
    let mut arr2= arr1;
    println!("arr1[2]={}", arr1[2]);
    println!("arr2[2]={}", arr2[2]);
    arr1[2] = 10;
    arr2[2] = 20;
    println!("arr1[2]={}", arr1[2]);
    println!("arr2[2]={}", arr2[2]);

    let mut pair1 = (true, 12);
    let mut pair2 = pair1;
    println!("pair1={:?}", pair1);
    println!("pair2={:?}", pair2);
    pair1.0 = false;
    pair2.1 = 100;
    println!("pair1={:?}", pair1);
    println!("pair2={:?}", pair2);
}
```
* Rust has a special annotation called the Copy trait that we can place on types like integers that are stored on the stack. If a type has the Copy trait, an older variable is still usable after assignment. Rust won’t let us annotate a type with the Copy trait if the type, or any of its parts, has implemented the Drop trait. If the type needs something special to happen when the value goes out of scope and we add the Copy annotation to that type, we’ll get a compile time error. 

#### Move

* If a variable is created on heap, assigning one with other will lead to a move. The first owner of the variable won't be able access the data after the move operation. When the last owner goes out of scope, memory is freed. So, rust never automatically deep copies data and avoids the performance loss.

```rust
fn main() {
    let s1 = String::from("hello world!");
    println!("s1={}", s1);
    let s2 = s1;
    println!("s2={}", s2);
    //println!("s1={}", s1); //error
}
```

#### Clone
* Clone makes deep copy and original owner can access data after the copy.

```rust
fn main() {
    let mut s1 = String::from("hello world!");
    println!("s1={}", s1);
    let mut s2 = s1.clone();
    println!("s2={}", s2);
    println!("s1={}", s1);

    s1.push_str(" ଓଡ଼ିଆ ଭାରି ବଢିଆ");
    s2.push_str(" we are good");
    println!("s2={}", s2);
    println!("s1={}", s1);
}
```

### Return Values and Scope
* Returning values can also transfer ownership.
* The ownership of a variable follows the same pattern every time: assigning a value to another variable moves it. When a variable that includes data on the heap goes out of scope, the value will be cleaned up by drop unless the data has been moved to be owned by another variable.

```rust
fn gives_ownership() -> String {
    let some_string = String::from("Hello");
    some_string
}

fn takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn main() {
    let s1 = gives_ownership();
    let s2 = String::from("ଓଡ଼ିଆ ଭାରି ବଢିଆ");
    let s3 = takes_and_gives_back(s2);

    println!("s1={}", s1);
    // println!("s2={}", s2); //error: doesn't own any more
    println!("s3={}", s3);
}
```

### References and Borrowing
* The & syntax lets us create a reference that refers to the value of a variable but does not own it. Because it does not own it, the value it points to will not be dropped when the reference goes out of scope.
* Functions that have references as parameters instead of the actual values mean we won’t need to return the values in order to give back ownership, since we never had ownership.
* We call having references as function parameters borrowing. 

```rust
fn calculate_length(s: &String) -> usize {
    s.len()
}

fn main() {
    let s1 = String::from("ଓଡ଼ିଆ ଭାରି ବଢିଆ");
    let len = calculate_length(&s1);

    println!("The length of s1={} is {}", s1, len);
}
```

#### Mutable References
```rust
fn change(s: &mut String) {
    s.push_str(" ଭାରି ବଢିଆ");
}

fn main() {
    let mut s = String::from("ଓଡ଼ିଆ");
    println!("s={}", s);
    change(&mut s);
    println!("s={}", s);
}
```

* But mutable references have one big restriction: you can only have one mutable reference to a particular piece of data in a particular scope. The benefit of having this restriction is that Rust can prevent data races at compile time. A data race is similar to a race condition and happens when these three behaviors occur:

    * Two or more pointers access the same data at the same time.
    * At least one of the pointers is being used to write to the data.
    * There’s no mechanism being used to synchronize access to the data.

```rust
fn main() {
    let mut s = String::from("hello");
    let r1 = &mut s;
    let r2 = &mut s; // error
}
```

* We also cannot have a mutable reference while we have an immutable one. Users of an immutable reference don’t expect the values to suddenly change out from under them! However, multiple immutable references are okay because no one who is just reading the data has the ability to affect anyone else’s reading of the data.

#### Dangling References
* In languages with pointers, it’s easy to erroneously create a dangling pointer, a pointer that references a location in memory that may have been given to someone else, by freeing some memory while preserving a pointer to that memory. In Rust, by contrast, the compiler guarantees that references will never be dangling references: if we have a reference to some data, the compiler will ensure that the data will not go out of scope before the reference to the data does.

#### The Rules of References

1. At any given time, you can have either but not both of:
    * One mutable reference.
    * Any number of immutable references.

2. References must always be valid.

### Slices
* Another data type that does not have ownership is the slice. Slices let you reference a contiguous sequence of elements in a collection rather than the whole collection.

* Slices also work for arrays

```rust
fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()
}

fn main() {
    let mut s = String::from("ଓଡ଼ିଆ ଭାରି ବଢିଆ");
    println!("s={}", s);
    let word = first_word(&s);
    println!("word={}", word);
    s.clear();
}
```

#### String Slices
* A string slice is a reference to part of a String. We can create slices using a range within brackets by specifying [starting_index..ending_index], where starting_index is the first position included in the slice and ending_index is one more than the last position included in the slice. Internally, the slice data structure stores the starting position and the length of the slice, which corresponds to ending_index minus starting_index. If you want to start at the first index (zero), you can drop the value before the two periods. 

* String slice range indices must occur at valid UTF-8 character boundaries. If you attempt to create a string slice in the middle of a multibyte character, your program will exit with an error. 

```rust
#![allow(dead_code)]

fn main() {
    let mut s = String::from("Hello Word!");
    println!("s={}", s);
    let s1 = &s[0..2];
    let s2 =&s[..2];
    let s3 = &s[3..];
    let s4 = &s[..];
    println!("first two={}", s1);
    println!("first two={}", s2);
    println!("from 3rd index={}", s3);
    println!("entire string={}", s4);
}
```

#### String Literasl as slices

```rust
let s = "Hello, world!";
```
* The type of s here is &str: it’s a slice pointing to that specific point of the binary. This is also why string literals are immutable; &str is an immutable reference.
* You can take slices of literals.

```rust
#![allow(dead_code)]

fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

fn main() {
    let s = String::from("Hello Word!");
    println!("s={}", s);
    let word = first_word(&s[..]);
    println!("word={}", word);

    let my_literal_string = "hello world!";
    let word_1 = first_word(my_literal_string); // this works
    let word_2 = first_word(&my_literal_string[..]);
    println!("literal string={}", my_literal_string);
    println!("word_1={}", word_1);
    println!("word_2={}", word_2);
}
```