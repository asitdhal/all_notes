### Structs
* To define a struct, we enter the keyword struct and name the entire struct. A struct’s name should describe the significance of the pieces of data being grouped together. Then, inside curly brackets, we define the names and types of the pieces of data, which we call fields.

* To use a struct after we’ve defined it, we create an instance of that struct by specifying concrete values for each of the fields. We create an instance by stating the name of the struct, and then add curly brackets containing key: value pairs where the keys are the names of the fields and the values are the data we want to store in those fields. We don’t have to specify the fields in the same order in which we declared them in the struct. 

* To get a specific value from a struct, we can use dot notation. 

* Rust doesn’t allow us to mark only certain fields as mutable. 

* As with any expression, we can construct a new instance of the struct as the last expression in the function body to implicitly return that new instance.

* Rust does include functionality to print out debugging information, but we have to explicitly opt-in to make that functionality available for our struct. To do that, we add the annotation #[derive(Debug)] just before the struct definition

```rust
#![allow(dead_code)]

#[derive(Debug)]
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: String, username: String) -> User {
    User {
        email: email,
        username: username,
        active: true,
        sign_in_count: 1,
    }
}

fn main() {
    let mut user1 = build_user( String::from("dhal.asitk@gmail.com"), String::from("dhal.asitk"));

    println!("user1={:?}", user1);
    user1.email = String::from("asitdhal_tud@outlook.com");
    println!("user1={:?}", user1);
}
```

#### Field init shorthand

* Because the parameter names and the struct field names are exactly the same in the previous example, we can use the field init shorthand syntax to rewrite build_user.

```rust
#![allow(dead_code)]

struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn main() {
    let mut user1 = build_user( String::from("dhal.asitk@gmail.com"), String::from("dhal.asitk"));

    println!("email={} username={}", user1.email, user1.username);
    user1.email = String::from("asitdhal_tud@outlook.com");
    println!("email={} username={}", user1.email, user1.username);
}
```

#### Struct update syntax
* It’s often useful to create a new instance of a struct that uses most of an old instance’s values, but changes some. We do this using struct update syntax.

```rust
#![allow(dead_code)]

struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn main() {
    let user1 = build_user( String::from("dhal.asitk@gmail.com"), String::from("dhal.asitk"));

    println!("email={} username={}", user1.email, user1.username);

    let user2 = User {
        email: String::from("asitdhal_tud@outlook.com"),
        username: String::from("asitdhal_tud"),
        ..user1
    };

    println!("email={} username={}", user2.email, user2.username);
}
```

#### Tuple Structs without Named Fields
* We can also define structs that look similar to tuples, called tuple structs, that have the added meaning the struct name provides, but don’t have names associated with their fields; rather, they just have the types of the fields. Tuple structs are useful when you want to give the whole tuple a name and make the tuple be a different type than other tuples, but naming each field as in a regular struct would be verbose or redundant.

* To define a tuple struct you start with the struct keyword and the struct name followed by the types in the tuple. 

*  Each struct we define is its own type, even though the fields within the struct have the same types. Otherwise, tuple struct instances behave like tuples: you can destructure them into their individual pieces and you can use a . followed by the index to access an individual value, and so on.

```rust
#![allow(dead_code)]

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn printColor(c: Color) {
    println!("(r,g,b)=({},{},{})", c.0, c.1, c.2);
}

fn printPoint(p: Point) {
    println!("(x,y,z)=({},{},{})", p.0, p.1, p.2);
}

fn main() {

    let black = Color(0,0,0);
    let white = Color(255, 255, 255);
    printColor(black);
    printColor(white);

    let origin = Point(0, 0, 0);
    let x_axis = Point(10, 0, 0);
    printPoint(origin);
    printPoint(x_axis);
}
```

#### Unit-Like Structs without Any Fields
* We can also define structs that don’t have any fields! These are called unit-like structs since they behave similarly to (), the unit type. Unit-like structs can be useful in situations such as when you need to implement a trait on some type, but you don’t have any data that you want to store in the type itself.


### Methods
* Methods are similar to functions,but they’re defined within the context of a struct or an enum or a trait object. Their first parameter is always self, which represents the instance of the struct the method is being called on.

* Method for a struct is always defined within impl block. The first parameter is always a &self. If we wanted to change the instance that we’ve called the method on as part of what the method does, we’d use &mut self as the first parameter. Having a method that takes ownership of the instance by using just self as the first parameter is rare; this technique is usually used when the method transforms self into something else and we want to prevent the caller from using the original instance after the transformation.

* Rust doesn’t have an equivalent to the -> operator; instead, Rust has a feature called automatic referencing and dereferencing. Calling methods is one of the few places in Rust that has this behavior.  Rust automatically adds in &, &mut, or * so object matches the signature of the method. 

* Each struct is allowed to have multiple impl blocks.

```rust
#![allow(dead_code)]

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}

fn main() {
    let rect1 = Rectangle { width: 40, height: 100};
    println!("The area of the rectange is {}", rect1.area());
}
```

* Methods can take additional parameters.

```rust
#![allow(dead_code)]

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle { width: size, height: size}
    }
}

fn main() {
    let rect1 = Rectangle { width: 40, height: 100};
    let rect2 = Rectangle { width: 10, height: 20};
    let square = Rectangle::square(20);
    println!("The area of the rectange is {}", rect1.area());
    println!("Can rect1 hold rect2 : {}", rect1.can_hold(&rect2));
    println!("The area of the square is {}", square.area());
}
```

#### Associated Functions
* Another useful feature of impl blocks is that we’re allowed to define functions within impl blocks that don’t take self as a parameter. These are called associated functions because they’re associated with the struct. They’re still functions, not methods, because they don’t have an instance of the struct to work with. 

* Associated functions are often used for constructors that will return a new instance of the struct. 

