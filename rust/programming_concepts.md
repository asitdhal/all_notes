### Statements and Expressions
* Statements are instructions that perform some action and do not return a value. Expressions evaluate to a resulting value.

* Creating a variable and assigning a value to it with the let keyword is a statement.  let y = 6; is a statement.
* Function definitions are also statements.
* The block that we use to create new scopes, {}, is an expression,

### Variables and Mutability
* By default variables are immutable.However, you still have the option to make your variables mutable. When a variable is immutable, that means once a value is bound to a name, you can’t change that value. 

```rust
fn main() {
    let x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}
```

This is an error.

* We can make them mutable by adding mut in front of the variable name. 

```rust
fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}
```

* advantage of mut:  In cases where you’re using large data structures, mutating an instance in place may be faster than copying and returning newly allocated instances. With smaller data structures, creating new instances and writing in a more functional programming style may be easier to reason about, so the lower performance might be a worthwhile penalty for gaining that clarity.

### Differences Between Variables and Constants
* Like immutable variables, constants are also values that are bound to a name and are not allowed to change, but there are a few differences between constants and variables.
    
    * We aren’t allowed to use mut with constants: constants aren’t only immutable by default, they’re always immutable.
    * We declare constants using the const keyword instead of the let keyword, and the type of the value must be annotated. 
    * Constants can be declared in any scope, including the global scope, which makes them useful for values that many parts of code need to know about.
    * Constants may only be set to a constant expression, not the result of a function call or any other value that could only be computed at runtime.

```rust
const MAX_POINTS: u32 = 100_000;

fn main() {
    println!("Maximum Points: {}", MAX_POINTS);
}
```

* Constants are valid for the entire time a program runs, within the scope they were declared in, making them a useful choice for values in your application domain that multiple parts of the program might need to know.


### Shadowing
*  We can declare a new variable with the same name as a previous variable, and the new variable shadows the previous variable.

```rust
fn main() {
    let x = 5;

    let x = x + 1;

    let x = x * 2;

    println!("The value of x is: {}", x);
}
```

* This is different than marking a variable as mut, because unless we use the let keyword again, we’ll get a compile-time error if we accidentally try to reassign to this variable. We can perform a few transformations on a value but have the variable be immutable after those transformations have been completed.

* The other difference between mut and shadowing is that because we’re effectively creating a new variable when we use the let keyword again, we can change the type of the value, but reuse the same name. 


### Data Types
#### Scalar Types

* A scalar type represents a single value. Rust has four primary scalar types: 
    * integers
    * floating-point numbers
    * booleans
    * characters

#### Integer Types
* Each integer variant in the Signed and Unsigned columns in the following table (for example, i16) can be used to declare the type of an integer value.

| Length | Signed |	Unsigned |
| ------ |:-------|---------:|
| 8-bit  |	i8    | u8       |
| 16-bit |	i16   |	u16      |
| 32-bit |	i32   |	u32      |
| 64-bit |	i64   |	u64      |
| arch 	 |  isize |	usize    |

* Each signed variant can store numbers from -(2n - 1) to 2n - 1 - 1 inclusive, where n is the number of bits that variant uses. Unsigned variants can store numbers from 0 to 2n - 1.

#### Floating-Point Types
* Rust’s floating-point types are f32 and f64.

#### Boolean Types
* A boolean type in Rust has two possible values: true and false. The boolean type in Rust is specified using bool.

#### Character Types
* The char type is specified with single quotes.

```rust
fn main() {
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';
    println!("{}", c);
    println!("{}", z);
    println!("{}", heart_eyed_cat);
}
```

* Rust’s char type represents a Unicode Scalar Value, which means it can represent a lot more than just ASCII. 


### Compound Types
* Compound types can group multiple values of other types into one type.
* Rust has two primitive compound types:
    * tuples
    * arrays

#### Grouping Values into Tuples
* A tuple is a general way of grouping together some number of other values with a variety of types into one compound type.

* We create a tuple by writing a comma-separated list of values inside parentheses. Each position in the tuple has a type, and the types of the different values in the tuple don’t have to be the same. 

* To get the individual values out of a tuple, we can use pattern matching to destructure a tuple value.

* In addition to destructuring through pattern matching, we can also access a tuple element directly by using a period (.) 

```rust
fn main() {
    let tup = (500, 6.4, 1);
    let (x,y,z) = tup;
    println!("the values are x={}, y={}, z={}", x, y, z);

    let five_hundred = tup.0;
    let six_point_four = tup.1;
    println!("five_hundred={} six_point_four={}", five_hundred, six_point_four);
}
```

#### Arrays
* Unlike a tuple, every element of an array must have the same type. Arrays in Rust are different than arrays in some other languages because arrays in Rust have a fixed length: once declared, they cannot grow or shrink in size.

* In Rust, the values going into an array are written as a comma-separated list inside square brackets.

* Arrays are useful when you want your data allocated on the stack rather than the heap, or when you want to ensure you always have a fixed number of elements. 

* We can access elements of an array using indexing. When we access an element of an array that is past the end of the array, we get a runtime error.

```rust
fn main() {
    let a = [1, 2, 3, 4];
    let index = 2;
    println!("a[index]= {}", a[index]);
    let beyond_end = 20;
    println!("a[beyond_end]= {}", a[beyond_end]); // error
}
```

### Functions
* Rust code uses snake case as the conventional style for function and variable names. In snake case, all letters are lowercase and underscores separate words. 

* Function definitions in Rust start with fn and have a set of parentheses after the function name. The curly brackets tell the compiler where the function body begins and ends.

```rust
fn another_function() {
    println!("another function");
}

fn main() {
    println!("main function");
    another_function();
}
```

#### Function Parameters
* Functions can also be defined to have parameters, which are special variables that are part of a function’s signature. When a function has parameters, we can provide it with concrete values for those parameters. 

* In function signatures, you must declare the type of each parameter. This is a deliberate decision in Rust’s design: requiring type annotations in function definitions means the compiler almost never needs you to use them elsewhere in the code to figure out what you mean.

```rust
fn another_function(x: i32, y: i32) {
    println!("another function x={} y={}", x, y);
}

fn main() {
    println!("main function");
    another_function(4, 5);
}
```

#### Function Bodies
* Function bodies are made up of a series of statements optionally ending in an expression.

#### Functions with Return Values
* Functions can return values to the code that calls them. We don’t name return values, but we do declare their type after an arrow (->). In Rust, the return value of the function is synonymous with the value of the final expression in the block of the body of a function. You can return early from a function by using the return keyword and specifying a value, but most functions return the last expression implicitly. 

```rust
fn sum(x: i32, y: i32) -> i32 {
    println!("x={} y={}", x, y);
    x + y
}

fn main() {
    let z = sum(4, 5);
    println!("sum={}", z);
}
```

### Control Flow
#### if Expressions
* We provide a condition and then state, “If this condition is met, run this block of code. If the condition is not met, do not run this block of code.”

```rust
fn main() {
    let number = 5;

    if number < 10 {
        println!("number is less than 10");
    } else {
        println!("number is not less than 10");
    }
}
```

* The condition is always a boolean value.

```rust
fn main() {
    let number = 3;

    if number { // error
        println!("number was three");
    }
}
```

#### Multiple Conditions with else if
* We can have multiple conditions by combining if and else in an else if expression.

```rust
fn main() {
    let number = 6;

    if number % 4 == 10 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisble by 4, 3 or 2");
    }
}
```

#### Using if in a let statement
* Because if is an expression, we can use it on the right side of a let statement.

```rust
fn main() {
    let condition = true;

    let number = if condition {
        5
    } else {
        6
    };

    println!("the value of number is {}", number);
}
```

*  The blocks of code evaluate to the last expression in them, and numbers by themselves are also expressions. In this case, the value of the whole if expression depends on which block of code executes. This means the values that have the potential to be results from each arm of the if must be the same type.

### Repetition with Loops
#### Repeating Code with loop

* The loop keyword tells Rust to execute a block of code over and over again forever or until you explicitly tell it to stop.

```rust
fn main() {
    loop {
        println!("again!");
    }
}
```

* You can place the break keyword within the loop to tell the program when to stop executing the loop.

#### Conditional Loops with while

```rust
fn main() {

    let mut number =  3;

    while number != 0 {
        println!("{} !", number);
        number = number - 1;
    }

    println!("LIFTOFF !!");
}
```

#### Looping Through a Collection with for

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;

    while index < 5 {
        println!("the value is: {}", a[index]);
        index = index + 1;
    }
}
```
