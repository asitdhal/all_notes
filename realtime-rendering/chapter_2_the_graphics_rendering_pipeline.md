## Introduction

- The Rendering Pipeline is a series of stages that take place in order to render an image to the screen.
- The main function of rendering pipeline is to generate, or render, a two-dimensional image, given a virtual camera, three-dimensional objects, light sources, and more. 
- The location and shape of the objects in the image are determined by 
    - their geometry
    - the characteristics of the environment
    - placement of the camera in that environment
- The appearance of the object is affected by
    - material properties
    - light sources
    - textures(images applied to surfaces)
    - shading equations


## Architecture

- The pipeline stages execute in parallel, with each stage dependent upon the result of the previous stage. Ideally, a nonpipelined system that is then divided into n pipelined stages could give a speedup of factor n. The pipeline stages execute in parallel, but they are stalled until the lowest stage has finished its task. The slowest stage is called *bottleneck* and the stage after that stage is called *starved*.

- A coarse division of the real-time rendering pipeline into four main stages
    - application
    - geometry processing
    - rasterization
    - pixel processing

- Each of these stages is usually a pipeline in itself, which means that it consists of several substages(called functional stages). A functional stage has a certain task to perform but does not specify the way that task is executed in the pipeline. A given implementation may combine two functional stages into one unit or execute using programmable cores, while it divides another, more time-consuming, functional stage into several hardware units.

- The rendering speed may be expressed in *frames per second(FPS)*, that is, the number of images rendered per second. This is usually used to express either the rate for a particular frame, or the average performance over some duration of use. It can also be represented using *Hertz(Hz)*, which is simply the notation for `1/seconds`, i.e. the frequency of update. Hertz is used for hardware, such as a display, which is set to afixed rate. It is also common to just state the time, in milliseconds(ms), that it takes to render an image. 

- The application stage is driven by the application and is therefore typically implemented in software running on general-purpose CPUs. These CPUs commonly include multiple cores that are capable of processing multiple *threads of execution* in parallel. This enables the CPUs to efficiently run the large variety of tasks that are the reponsibility of application stage. Some of the tasks traditionally performed on the CPU include
    - collision detection
    - global acceleration algorithms
    - animation
    - physics simulation
    
- The geometry processing stage deals with transforms, projections and all other types of geometry handling. This stage computes what is to be drawn, how it should be drawn, and where it should be drawn. The geometry stage is typically performed on a graphics processing unit(GPU) that contains many programmable cores as well as fixed-operation hardware. 

- The rasterization stage typically takes as input three vertices, forming a triangle, and finds all pixels that are considered inside that triangle, then forwards these to the next stage. This is entirely done on GPU.

- The pixel processing stage executes a program per pixel to determine its color and may perform depth testing to see whether it is visible or not. It may also perform per-pixel operations such as blending the newly computed color with previous color. This is entirely done on GPU.

## The Application Stage

- Developer has full control over this stage.
- Changes here can also affect the performance of subsequenct stages. e.g. an application stage algorithm could decrese number of triangles to be rendered.
- Some application work can be performed by the GPU, using a separate mode called a *computer shader*. This mode treats GPU as a highly parallel general processor.
- At the end of the application stage, the geometry to be rendered is fed to the geometry processing stage. These are rendering primitives, i.e., points, lines and triangles that might eventually end up on the screen. 

## Geometry Processing

- This stage is responsible for most of the per-triangle and per-vertex operations.
- This stage is further divided into the following functional stages
    - vertex shading
    - projection
    - clipping
    - screen mapping

### Vertex Shading

- There are two main tasks of vertex-shading
    - to compute the position for a vertex
    - to evaluate whatever the programmer may like to have as vertex output data, such as a normal and texture coordinates

- On its way to the screen, a model is transformed into several different *spaces* or *coordinate systems*. Originally, a model resides in its own model space(it has not been transformed at all). Each model can be associated with a *model transform* so that it can be positioned and oriented. It is possible to have several model transforms associated with a single model. This allows several copies(called *instances*) of the same model to have different locations, orientations, and sizes in the same scene, without requiring replication of the basic geometry.

- It is the vertices and the normals of the model that are transformed by the model transform. The coordinates of an object are called *model coordinates*, and after the model transform has been applied to these coordinates, the model is said to be located in *world coordinates* or in *world space*. The world space is unique, and after the models have been transformed with their prospective model transforms, all models exist in this same space.

- Only the models that the camera sees are rendered. The camera has a location in world space and a direction, which are used to place and aim the camera. To facilitate projection and clipping, the camera and all the models are transformed with the *view transform*. 

    - The purpose of the view transform is to place the camera at the origin and aim it, to make it look in the direction of negative z-axis, with the y-axis pointing upward and the x-axis pointing to the right.  
 
    - We use -z-axis conversion; some texts prefer looking down the -+z-axis. The transform between one to other is simple. The space thus delineated is called *camera space*, or *view space*, or *eye space*. 

- Example of view transform

    ![View Transform](images/chapter_2_4_view_transform.jpeg)

    In the above image, on the left, a top-down view shows the camera located and oriented as the user wants to be, in a world where the +z axis is up. The view transform reorients the world so that the camera is at the origin, looking along the negative z-axis, withe the camera's +y axis is up, as shown on the right. 

- To produce a realistic scene, it is not sufficient to render the shape and position of objects, but their apperance must be modeled as well. This description includes each object's material, as well as the effect of any light sources shining on the object. This operation of determining the effect of a light on a material is known as *shading*. 

- As part of vertex shading, rendering system perform *projection* and then clipping, which transforms the view volume into a unit cube with its extreme points at `(-1, -1, -1)` and `(1, 1, 1)`. Different ranges defining the same volume can and are used, for example `0 <= z <= 1`. The unit cube is called the *canonical view volume*. Projection is done first, and on the GPU it is done by the vertex shader. There are two commonly used projection methods, namely *orthographic* and *prospective* projection. 

- The view volume of orthographic viewing is normally a rectangular box, and the orthographic projection transforms the view volume into the unit cube. The main charactertic of orthographic projection is that parallel lines remain parallel after the transform. This transformation is a combination of a translation and a scaling.

- In prospective projection, the farther away an object lies from the camera, the smaller it appears after projection. Parallel lines may converse at the horizon. The perspective transform thus mimics the way we perceive obejects' size. Geometrically, the view volume is called a *frustum* and it is a truncated pyramid with rectangular base. The frustum is transformed into unit cube as well. 

- Both orthographic and perspective transforms can be constructed with 4 x 4 matrices, and after either transform , the models are said to be in *clip coordinates*. 


### Optional Vertex Processing

- There are a few optional stages that can take place on the GPU, in the following order 
    - tsessallation
    - geometry shading
    - stream output

- In tessellation stage, a curved surface can be generated with an appropriate number of triangles. Vertices can be used to describe a curved surface, such as a ball. Such surfaces can be specified by a set of patches, and each patch is made of a set of vertices. The tessallation stage consists of a series of stages itself - hull shader, tessellation, and domain shader - that converts these sets of patch vertices into (normally) larger sets of vertices that are then used to make new sets of triangles. The camera for the scene can be used to determine how many triangles are generated: many when the patch is close, few when it is far away.

- Geometric shader is like tessellation shader in that it takes in primitives of various sorts and can produce new vertices. It is a much simpler stage in that this creation is limited in scope and the types of output primitives are much more limited. The geometry shader can take each point and turn it into a square (made of two triangles) that faces the viewer and covers several pixels, so providing a more convincing primitives for us to shade.

- The stream output stage lets us use the GPU as a geometry engine. Instead of sending our processed vertices down the rest of the pipeline to be rendered to the screen, at this point we can optionally output these to an array for further processing. These data can be used by the CPU, or the GPU itself, in a later pass.

### Clipping

- Only the primitives wholly or partially inside the view volume need to be passed on to the rasterization stage, which then draws them on the screen.
    - A primitive that lies fully inside the view volume will be passed on to the next stage as is.
    - Primitives entirely outside the view volume are not passed on further, since they are not rendered.
    - Clipping is done on primitives which are partially inside the view volume. 
    
- A line that has one vertex outside and one inside the view volume should be clipped against the view volume, so that the vertex that is outside is replaced by a new vertex that is located at the intersection between the line and the view volume. The use of a projection matrix means that the transformed primitives are clipped against the unit cube. 

- The advantage of performing the view transformation and projection before the clipping is that it makes the clipping problem consistent; primitives are always clipped against the unit cube.

- In addition to the six clipping planes of the view volume, the user can define additional clipping planes to visibly chop objects. This is called *sectioning*.

- The clipping step uses the 4-value homogeneous coordinates produced by projection to perform clipping. Values do not normally interpolate linearly across a triangle in perspective space. The fourth coordinate is needed so tha data are properly interpolated and clipped when a perspective projection is used. Finally, *perspective division* is performed, which places the resulting triangles positions into three-dimensional *normalized device coordinate*. 

### Screen Mapping

- Only the (clipped) primitives inside the view volume are passed on to the screen mapping stage, and the coordinates are still three-dimensional when entering this stage. The x- and y-coordinates of each primitives are transformed to form *screen coordinates*. Screen coordinates together with the z-coordinates are called *window coordinates*. 
    


## Rasterization

- Given the transformed and projected vertices with their associated shading data(all from geometry processing), the goal of the next stage is to find all pixels - short for *picture elements* - that are inside the primitive, e.g. a triangle, being rendered. We call this process *rasterization*.

- Rasterization is split up into two functional substages:
    - triangle setup(also called primitive assembly)
    - triangle traversal

- Rasterization is also called *scan conversion*, is thus the conversion from two-dimensional vertices in screen space - each with a z-value(depth value) and various shading information associated with each vertex - into pixels on screen.

- Rasterization can also be thought of as a synchronization point between geometry processing and pixel processing, since it is here that triangles are formed from three vertices and eventually sent down to pixel processing.

- This is done by dedicated hardwired silicon die.

### Triangle Setup

- In this stage the differentials, edge equations, and other data for the triangle are computed. These data may be used for triangle traversal, as well as for interpolation of the various shading data produced by the geometry stage. Fixed-function hardware is used for this task.

### Traingle Traversal

- Here is where each pixel that has its center(or a sample) covered by the triangle is checked and a *fragment* generated for the part of the pixel that overlaps the triangle. Finding which samples or pixels are inside a triangle is often called *triangle traversal*. 

- Each triangle fragment's properties are generated using data interpolated among the three triangle vertices. These properties include fragment's depth, as well as any shading data from geometry stage. It is also here that persepctive-correct interpolation over the triangles is performed. All pixels or samples that are inside a primitive are then sent to the pixel processing stage.

## Pixel Processing

- At this stage, all pixels that are considered inside a triangle or other primitive have been found as a consequence of the combination of all the previous stages. Pixel processing is the stage where per-pixel or per-sample computations and operations are performed on pixels or samples that are inside a primitive.

- This is divided into two stages
    - pixel shading
    - merging

### Pixel Shading

- Any per-pixel shading computations are performed here, using the interpolated shading data as input. The end result is one or more colors to be passed on to the next stage.

- This is done by programmable GPU cores. So, the developer has to provide a program for pixel/fragment shader, which contains any desired computations.

- A large variety of techniques can be applied here, one of the most important technique is texturing. Texturing an object means gluing one or more images onto that object, for a variety of purposes. 

### Merging

- The information for each pixel is stored in the *color buffer*, which is a rectangular array of colors(red, blue, green). It is responsibility of merging stage to combine the fragment color produced by the pixel shading stage with color currently stored in the buffer. This stage is called ROP(*rester operation pipeline* or *render output unit*).

- The GPU subunit that performs this stage is typically not fully programmable, but highly configurable, enabling various effects.

- When the whole screen has been rendered, the color buffer should contain the colors of the primitives in the scene that are visible from the point of view of the camera. This is done with the z-buffer(also called *depth buffer*) algorithm. A z-buffer is the same size and shape as the color buffer, and for each pixel it stores the z-value to the currently closed primitive. 

- **z-buffer algorithm**

    1. When a primitive is rendered to a certain pixel, the z-value on that primitive at that pixel is being computed and compared to the contents of z-buffer at the same pixel. 

    2. If the new z-value is smaller than the z-value in the z-buffer, then the primitive that is being rendered is closer to the camera than the primitive that was previously closest to the camera at that pixel. Therefore, the z-vaue and the color of that pixel are updated with the z-value and color from the primitive that is being drawn. 

    3. If the computed z-value is greater than the z-value in the z-buffer, then the color buffer and the z-buffer are left untouched. 

- z-buffer algorithm has O(n) convergence(where n is the number of primitives being rendered). It works for any drawing primitive for which a z-value can be computed for each(relevant) pixel. This algorithm also allows most primitives to be rendered in any order. 

- Transparency is one of the major weakness of the basic z-buffer. The z-buffer stores only a single depth at each point on the screen, so it cannot be used for partially transparent primitives. These must be rendered after all opaque primitives, and in back-to-front order, or using a separate order-independent algorithm. 

- There are other channels and buffers that can be used to filter and capture fragment information. The *alpha channel* is associated with the color buffer and stores a related opacity value for each pixel. 

- The *stencil buffer* is an offscreen buffer used to record the locations of the rendered primitive. It typically contains 8 bits per pixel. Primitives can be rendered into the stencil buffer using various functions, and the buffer's contents can then be used to control rendering into the color buffer and z-buffer. 

- When the primitives have reached and passed rasterizer stage, those that are visible from the point of view of the camera are displayed on screen. The screen displays the contents of the color buffer. To avoid allowing the human viewer to see the primitives as they are being rasterized and sent to the screen, *double buffering* is used. This means that the rendering of a scene takes place off screen, in a *back buffer*. Once the scene has been rendered in the back buffer, the contents of the back buffer are swapped with the contents of the *front buffer* that was previously displayed on the screen. The swapping ofen occurs during *vertical retrace*, a time when it is safe to do so.
