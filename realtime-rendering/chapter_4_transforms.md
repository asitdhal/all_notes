## Instroduction

- A *transform* is an operation that takes entities such as points, vectors, or colors and converts them in some way. With them, you can position, reshape, and animate objects, lights and cameras. 

- A *linear transform* is one that preserves vector addition and scalar multiplication. Specifically,

    ```
    f(x) + f(y) = f(x+y)
    kf(x) = f(kx)
    ```

## Basic Transforms

### Translation

- A change from one location to another is represented by a translation matrix, T. This matrix translates an entity by a vector t = (t<sub>x</sub>,t<sub>y</sub>,t<sub>z</sub>). T is given by  

![alt text](images/chapter_4_1_1_Translation_Equation.jpeg)

The multiplication of a point p = (p<sub>x</sub>,p<sub>y</sub>,p<sub>z</sub>, 1) with T(t) yields a new point p'= (p<sub>x</sub> + t<sub>x</sub>, p<sub>y</sub> + t<sub>y</sub>, p<sub>z</sub> + t<sub>z</sub>, 1).  Any vector will be unaffected by a multiplication by T, because a direction vector cannot be translated. 

The effect of translation is shown below

![alt text](images/chapter_4_1_1_Translation_Example.jpeg)

The square on the left is transformed with a translation matrix T(5,2, 0), whereby the square is moved 5 distance on the right and 2 upward.


### Rotation

- A rotation transform rotates a vector(position or direction) by a given angle around a given axis passing through the origin. Like a translation matrix, it s a *rigid-body transform*, i.e. it preserves the distance between points transformed, and preserves handedness (i.e., it never causes left and right swap sides). 

- An *orientation matrix* is a rotation matrix associated with a camera view or object that defines its orientation in space, i.e., it's direction for up and forward.

- In two dimensions, the rotation matrix is simple to derive. Assume we have a vector v=(v<sub>x</sub>, v<sub>y</sub>) = (r cos θ, r sin θ). If we were to rotate that vector by Φ radians (counterclockwise), then we would get u = (r cos(θ+Φ), r sin (θ+Φ)), then this can be written as

![alt text](images/chapter_4_1_2_2d_Rotation_Equation.jpeg)



### Scaling

- A scaling matrix, S(s) = S(s<sub>x</sub>, s<sub>y</sub>, s<sub>z</sub>), scales an entity with factors s<sub>x</sub>, s<sub>y</sub> and s<sub>z</sub> along with the x-, y- and z-directions, respectively. This means that a scaling matrix can be used to large or dimish an object. The larger the s<sub>i</sub>, i ∈ {x, y, z}, the larger the scaled entity gets in the direction. Setting any of the components of s to 1 naturally avoids a change in scaling in that direction.

### Shearing

- Shearing is used in games to distort an entire scene to create a psychedelic effect or otherwise warp a model's appearance. 

- There are 6 basic shearing matrices
    - H<sub>xy</sub>(S)
    - H<sub>xz</sub>(S)
    - H<sub>yx</sub>(S)
    - H<sub>yz</sub>(S)
    - H<sub>zx</sub>(S)
    - H<sub>zy</sub>(S)

  The first subscript is used to denote which coordinate is being changed by the shear matrix, while the second subscript indicates the coordinate which does the shearing. 

### Concatenation of Transforms

- Due to the noncommutativity of the multiplication operation on matrices, the order in which the matrices occur matters. Concatenation of transforms is therefore said to be order-dependent.

- The obvious reason to concatenate a sequence of matrices into a single one is to gain efficiency. This single matrix is then applied to the vertices. The composite matrix is `C=TRS`. The scaling matrix S should be applied to the vertices first, and therefore appears to the right in the composition. The ordering implies that TRS<sub>p</sub> = (T(R(S<sub>p</sub>))).

### The Rigid-Body Transform

- When an object's orientation and location change, while the shape of the object generally is not affected, we call it rigid-body transform.  Such a transform consists of concataneations of only translations and rotations. It has the characteristics of preserving lengths, angles, and hardness.

### Normal Transform

- 