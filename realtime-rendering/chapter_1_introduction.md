## Introduction

- Realtime rendering is concerned with rapidly making images on the computer. An image appears on the screen, the viewer acts or reacts, and their feedback affects what is generated nex. This cycle of reaction and rendering happens at a rapid enough rate that the viewer does not see individual images, but rather becomes immersed in a dynamic process.

- The rate at which images are displayed is measured in frames per second(FPS) or Hertz(Hz). 
    - Video games aim for 30, 60, 72 or higher FPS: at these speeds the user focuses on action and reaction.
    - Movie projector show frames at 24 FPS but use a shutter system to display each frame two to four times to avoid flicker. This *refresh rate* is separate from the display rate and is expressed in Hertz(Hz). A shutter shutter that illuminates the frame three times has a 72 Hz refresh rate. LCD monitor also has separate refresh rate from display rate.
    - As little as 15 milliseconds of temporal delay can slow and interfere with interaction. 
    - If speed was the only creterion, any application that rapidly responded to user commands and drew anything on the screen would qualify. Rendering in real time normally means producing three domensional images.

## Geometric Definition

- basic rendering/drawing primitives : points, lines and triangles
- model/object : collection of genometric entities
    - examples: a car, building, line, etc.
    - usually contains a set of drawing primitives
    - may have a higher kind of geometrical representation(Bezier curves or surface)
    - can consists of other objects(a car has four doors, wheel)
- scene: a collection of models comprising everything that is included in the environment to be rendered
    - this also includes material description, lighting and viewing specification

