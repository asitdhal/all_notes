Texturing is a process that takes a surface and modifies its apperance at each location using some image, function or other data source. 

# Pipeline

* Texturing is a technique for efficiently modeling variations in a surface's material and finish. One way to think about texturing is to consider what happens for a single shaded pixel. 

* The shade is computed by taking into account the color of the material and the lights, among other factors.  If present, transparency also affects the sample. Texturing works by modifying the values used in the shading equation. The way these values are changed is normally based on the position on the surface. The pixels in the image texture are often called **texels**. The roughness texture modifies the roughness value, and the bump texture changes the direction of the shading normal, so each of these change the result of the shading equation.

* A location in a space is the starting point for the texturing process. This location can be in world space, but is more often in the model's frame of reference, so that as the model moves, the texture moves along with it. This point in space then has a *projector* function applied to it to obtain a set of numbers, called *texture coordinates*, that will be used for accessing the texture. This process is called *mapping*, which leads to the phrase *texture mapping*. Sometimes the texture image itself is called the *texture map*, though this is not strictly correct.

* One or more *corresponder* functions can be used to transform the texture coordinates to texture space. These texture-space locations are used to obtain values from the texture, i.e. they may be array indices into an image texture to retrieve a pixel. The retrieved values are then potentially transformed yet again by a *value transform* function, and finally these new values are used to modify some property of the surface, such as the material or shading normal. 


## Example: Brick wall example

1. The `(x,y,z)` position in the object's local frame of reference is found; say it is `(-2.3, 7.1, 88.2)`.
2. A projector function is then applied to this position.  The projector function here typically changes the `(x,y,z)` vector into a two-element vector `(u,v)`. The projector function used for this example is equivalent to an orthographic projection, acting something like a slide projector shining the brick wall image onto the triangle's surface. 
3. To return to the wall, a point on its surface could be transformed into a pair of values ranging from 0 to 1, say the values obtained are `(0.32, 0.29)`.



![Texture Pipleline](images/chapter_6_2_1_texture_pipeline.jpeg)

# The Projector Function

* The first step in the texture process is obtaining the surface's location and projecting it into texture coordinate space, usually two-dimensional `(u,v)` space. Modelling packages typically allow artists to define `(u,v)`-cooridnate per vertex. These may be initialized from projector functions or from mesh unwrapping algorithms. Projector functions typically work by converting a three-dimensional point in space into texture coordinates. Functions commonly used in modeling programs include spherical, cylinderical, and planar projections.

* Other inputs can be used to a projector function. The surface normal can be used to choose which of six planar projection directions is used for the surface. 

* Other projector functions are not projections at all, but are an implicit part of surface creation and tessellation. 

