## Shading Models

- The frst step in determining the apperance of a rendered object is to choose a *shading model* to describe how the object's color should vary based on factors such as surface orientation, view direction, and lighting.

- *Gooch shading model* is a form of non-photorealistic rendering, designed to increase legibility of details in technical illustrations.
    - The basic idea behind Gooch shading is to compare the surface normal to light's location.
    - If the normal points toward the light, a warmer tone is used to color the surface; if it points away, a cooler tone is used. 
    - Angles in between interpolate between these tones, which are based on a super-supplied surface color.

- Shading models often have properties used to control apperance variation. Setting the values of these properties is the next step in determining object appearance.

- Shading model is affected by the surface orintation relative to the view and lighting directions. These directions are commonly expressed as normalized (unit-length) vectors as illustrated in the following image.

![alt text](images/chapter_5_1_surface_normal.jpeg)



## Light Sources

- Lighting in the real world can be quite complex. There can be multiple light sources each with its own size, shape, color, and intensity; indirect lighting adds even more variation.

- Stylized shading models may use lighting in many different ways, depending on the needs of application and visual style. Some highly stylized models may have no concept of lighting at all, or may only use it to provide some simple directionality.

- The next step in lighting complexity is for the shading models to react to the presence or absence of light in a binary way. A surface shaded with such a model would have one apperance when lit and a different apperance when unaffected by light. This implies some criteria for distingushing the two cases: 
    - distance from light sources
    - shadowing
    - whether the surface is facing away from light source
    - some combination of above 

- It is a small step from the binary presence or absence of light to a continuous scale of light intensities. This could be expressed as a simple interpolation between absence and full presence, which implies a bounded range for the intensity, perhaps 0 to 1, or as an unbounded quantity that affects the shading in some other way.

- A light source does not affect a surface point if the light direction `l` is more than 90° from the surface normal `n`, in effect coming from underneath the surface. 

### Directional Lights

- Directional light is the simplest model of a light source. Both `l` and `c`<sub>light</sub> are constant over the scene, except that `c`<sub>light</sub> may be attenuated by shadowing. Directional lights have no location, are abstraction which work well when the distance to the light is large relative to the scene size. 

- The concept of a directional light can be somewhat extended to allow varying  the value of `c`<sub>light</sub> while the light direction `l` remains constant. This is most often done to bound the effect of the light to a particular part of the scene for performance or creative reasons.

### Punctual Lights

- A *punctual light* is not one that is on time for its appointments, but rather a light that has a location, unlike directional lights. Punctual lights consist of all sources of illumination that originate from a single, local position. 
- The term *point light* is used to mean a specific kind of emitter, one that shines light equally in all directions.

#### Point/Omni Lights

- Punctual lights that emit light uniformly in all directions are known as point light. 