## The Vertex Shader

- The vertex shader is the first stage directly under programmer control. Some data manipulation happens before this stage. In DirectX, it's called input assembly and in OpenGL it's called primitive assembly. During this stage, several streams of data can be woven together to form the set of vertices and primitives sent down the pipeline. 

- A triangle mesh is represented by a set of vertices, each associated with a specific position on the model surface. Besides position, there are other optional properties associated with each vertex, such as a color or texture coordinates. Surface normals are defined at mesh vertices as well. Each triangle has a well-defined surface normal, and it may seem to make more sense to use the triangle's normal directly for shading. When rendering, triangle meshes are often used to represent an underlying curved surface, and vertex normals are used to represent the orientation of this surface, rather than that of the triangle mesh itself. 

- The vertex shader is the first stage to process the triangle mesh. The data describing what triangles are formed is unavailable to the vertex shader. As its name implies, it deals exclusively with the incoming vertices. The vertex shader provides a way to modify, create, or ignore values associated with each triangle's vertex, such as its color, normal, texture coordinates, and position. Normally the vertex shader program transforms vertices from model space to homogeneous clip space. At a minimum, a vertex shader must always output this location.

- Usages for the vertex shader include:
    - Object generation, by creating a mesh only once and having it be deformed by the vertex shader.
    - Animating character's bodies and faces using skinning and morphing techniques.
    - Procedural deformations, such as the movement of flags, cloth, or water.
    - Particle creation, by sending degenerate meshes down the pipeline and having these be given an area as needed.
    - and many more

## The Tessellation Shader

- This stage allows us to render curved surfaces. 

- Advantages
    - The curved surface description is often more compact than providing the corresponding triangles themselves. (memory saving)
    - This feature can keep the bus between CPU and GPU from becoming the bottleneck for an animated character or object whose shape is changing each frame. The surfaces can be rendered efficiently by having an appropriate number of triangles generated for the given view.
    - e.g.:- a ball far from the camera, only a few triangles are needed. Up close, it may look best represented  with thousands of triangles. 
    - This ability to control the *level of detail* can also allow an application to control its performance, e.g. using lower quality mesh on weaker GPUs in order to maintain frame rate.

- It has three sub stages
    1. hull shader (or tsellation control shader)
    2. tessalator
    3. domain shader (or tessellation evaluation shader)

### hull shader

- The input to hull shader is a special patch primitive. This consists of several control points defining a subdivision surface, Bezier patch, or other type of curved element. The hull shader has two functions.
    - It tells the tessellator how many triangles should be generated and in what configuration.
    - It performs processing on each of the control points.

- Optionally, a hull shader can modify the incoming patch description, adding or removing control points as desired. The hull shader outputs its set of control points, along with the tessellation control data, to the domain shader.

### tessalator

- The tessalator is a fixed-function stage in the pipeline, only used with tessellation shaders. It has the task of adding several new vertices for the domain shader to process. The hull shader sends the tessellation information about what type of tessellation surface is desired: triangle, quadrilateral, or isoline. Isolines are sets of line strips, sometimes used for hair rendering. 

- The other important values sent by the hull shader are the tessellation factors(*tessellation levels* in OpenGL). There are of two types: inner and outer edge. The two inner factors determine how much tessellation occurs inside the triangle or quadrilateral. The outer factors determine how much each exterior edge is split. By allowing separate controls, we can have adjacent curved surfaces' edges match in tessellation, regardless of how the interiors are tessellated. Matching edges avoids cracks or other shading artifacts where patches meet. The vertices are assigned barycentric coordinates, which are values that specify a relative location for each point on the desired surface.

- The hull shader always outputs a patch, a set of control point locations. It can signal that a patch is to be discarded by sending the tessellator an outer tessellation level of zero o less(or not-a-number, NaN). Otherwise, the tessellator generates a mesh and sends it to the domain shader. The control points for the curved surface from the hull shader are used by each invocation of the domain shader to compute the output values for each vertex. The domain shader has a data flow pattern like that of a vertex shader, with each input vertex from the tessellator being processed and generating a corresponding output vertex. The triangles formed are then passed on down the pipeline.

- The patch passed into a hull shader will often undergo little or no modification. This shader may also use the patch's estimated distance or screen size to compute tessellation factors on the fly, as for terrain rendering. Alternatively, the hull shader may simply pass on a fixed set of values for all patches that the application computes and provides. The tessellation performs an involved but fixed-function process of generating the vertices, giving them positions, and specifying what triangles and lines they form. 

## The Geometry Shader

- The geometry shader can turn primitives into other primitives, something the tessellation stage cannot do. e.g. a triangle mesh could be transformed to a wireframe view by having each triangle create line edges. Alternatively, the lines could be replaced by quadrilaterals facing the viewer, so making a wireframe rendering with thicker edges. 

- The input to the geometry shader is a single object and its associated vertices. The object typically consists of triangles in a strip, a line segment, or simply a point. 