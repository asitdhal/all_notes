### Introduction

 * Golang ```net/http``` has two major components for HTTP processing
  	1. ```ServeMux```: a multiplexer(http router) which compares incoming HTTP requests against a list of predefined URI resources and then dispatches the request to the associated handler.
  	2. Handler: Handles the HTTP request.
	http.Handler interface signature
 	```go
	type Handler interface {
		ServeHTTP(ResponseWriter, *Request)
	}
	```
	```ResponseWriter```: interface writes response headers and bodies into the HTTP response.
	```Request```: pointer to struct contains information about HTTP request
	Any object which implements the handler interface can become a handler.
* Example
	```go
	package main

	import (
    	"net/http"
	)

	func main() {
    	mux := http.NewServeMux() //creates an empty ServeMux object
    	fs := http.FileServer(http.Dir("public")) //handler
    	mux.Handle("/", fs) // registers the URL path "/" with the handler
    	http.ListenAndServe(":8080", mux) //create HTTP server
	}
	```
* ```ListenAndServe``` creates an HTTP server. It takes a ```ServeMux``` object.
	signature
	```go
	func ListenAndServe(addr string, handler Handler) error
	```
	The ServeMux type also has a ServeHTTP method, which means that it satisfies the http.Handler interface so that a ServeMux object can be passed as a second argument for the ListenAndServe function. Keep in mind that an instance of a ServeMux is an implementation of the http.Handler interface. If you pass nil as the second argument for ListenAndServe, a DefaultServeMux will be used for the http.Handler. DefaultServeMux is an instance of ServeMux, so it is also a handler.
