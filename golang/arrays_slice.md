## Arrays
An array is a numbered and fixed length sequence of data items of the same 
single type. 

```golang
var identifier [len]type 
```
Here, len  is a constant expression and must evaluate to a non-negative integer.

> len is the part of array, so [5]int and [10]int are of different type.

```golang
var arr1 [5]int
var arr2 = new([5]int) 
```
Here, arr1 is of type ```[5]int``` and  arr2 is of  type ```*[5]int```.

> Arrays are value type in Golang. 

Assigning one array to another will create a new copy in the memory. Passing an array as an argument to a function will create another copy in the memory. 

```golang

func f( a [3]int) { fmt.Println(a)}
func fp( a *[3]int) { fmt.Println(a)}

func main() {
    var arr [3]int;
    f(arr) //passes a copy 
    f(&arr) //passes a pointer
}
```

### Array literals
```golang
var arrAge = [5]int { 18, 20, 15, 22, 16}
var arrAge2 = [10]int { 18, 20, 15}
```
Here, arrAge is an array of 5 ints, with values mentioned.
arrAge2 is an array of 10 ints, with first 3 values being 18, 10, 15.

```golang
var arrLazy = [...]int { 18, 20, 15, 22, 16}
```
Here, arrLazy is an array of 5 ints, with values mentioned. The size is counted
by the compiler.

```golang
var arrKeyVal = [5]string { 3: "Asit", 4: "Amit"}
```
Here arrKeyVal is an array of 5 strings and 3rd and 4th member being 
"Asit" and "Amit".



### for-loop construct to loop over an array
```golang
var arr1 [5]int 
for i := 0; i<len(arr1); i++ {
    arr1[i] = i + 100
}
```

### for-range construct to loop over an array
```golang
var arr1 [5]int 
for i := range arr1 {
    arr1[i] = i + 100
}
```
Here, i is the index
```golang
var arr1 [5]int 
for _, val := range arr1 {
    fmt.Println(val)
}
```
here, val is the each elemenent.

### Multi Dimensional Array
```golang
const (
    X = 5
    Y = 10
)
var arr1 [X][Y]int 
for y := 0; y < Y; y++ {
    for x := 0; x < X; x++ {
        arr1[x][y] = x+y
    }
}
```
You can do like this, ```[len][len]type```


## Slices
- A slice is a reference to a contiguous segment(section) of an array (which will 
call the underlying array, and which is usually anonymous), so a slice is a 
reference type.
- Slice provide a dynamic window onto the underlying array.
- ```len()``` :- length of slice
- ```cap()``` :- maximum length of slice(length of underlying array)
- always true ```0 <= len(s) <= cap(s)```
- declaration & initialization
```
var identifier []type //length field is not needed
var slice1 []type = arr1[start:end] //slice will have elements from index start to end-1
var slice2 []type = arr1[:] //complete array
slice3 := [3]int{1, 2, 3}[:] //or [...]int{1, 2, 3}[:] or []int{1, 2, 3}
```
- Slice can only be moved forward. ```s2 = s2[-1:]``` is an error 
### Difference between slice and array
- slice is a refernce type and aray is a value type
- two slice can share same data, two arrays will have always distinct storage of date

### Internal Representation
- A slice is a structure of 3 fields.
    - a pointer to the underlying array
    - length of the slice
    - capacity of the slice
- As long as the slice is kept around, garbage collector can't realease the array.
However, it can create problem if a huge array is kept in the memory, but very few elements are accessed.

### Usages
```golang
func sum(a []int) int {
    s := 0
    for i := 0; i<len(a); i++ {
        s += a[i]
    }
    return s
}

func main() {
    var arr1 [6]int;
    for i :=0; i<len(arr1); i++ {
        arr1[i] = i
    }

    var slice1 []int = arr1[2:5]
    for i := 0; i<len(slice1); i++ {
        fmt.Printf("index: %d data: %d\n", i, slice1[i])
    }

    fmt.Println(sum(slice1)
}
```

### make()
make() allows us to create a slice when the underlying array is not defined.
```golang
slice1 := make([]type, len)
slice2 := make([]type, len, cap)
```
In the first case,
len is the length of the array. Here, the following statement is true
```golang
cap(slice1) = len(slice1) = len 
```

In the second case,
slice2 doesn't occupy the entire underlying array. Following two satements create the same thing.
```golang
make([]type, len, cap)  
new([cap]type)[0:len]
```

### for-range construct
same as  array

### copy() and append()
- copy() is just old school copy
````
n = copy(destination, src)
````
- n is number of elements copied, and equal to minimum number between len(src)
and len(destination). So, it may skip element of destination is smaller than the source. 
- It will overwrite if destination has some data.


- slice can be resliced beyond capacity. So, append() can be used to overcome this limitation.
    - append() appends zero or more vaues to a slice and returns the resulting slice.
    - allocates a new, sufficiently large slice 
    - always succeeds, unless the computer runs out of memory
````
x = append(x, y...)
````

````golang
s1_from := []int{1, 2, 3}
s1_to := make([]int, 10)
n := copy(s1_to, s1_from)
fmt.Println(s1_to)
fmt.Println(n)

s2 := []int{1, 2, 3}
s2 = append(s1, 4, 5,6)
fmt.Println(s2)
````
- append() is a very powerful function, can simulate many things

Index | Description | simulation
------|------------|------------
1 | append a slice a to a new slice b | ```a = append(a, b...)```
2 | copy a slice to a new slice b  | ```b = make([]T, len(a))```
 |        | ```copy(b,a)```
3| delete an item at index i |```a = append(a[:i], a[i+1:]...)```
4| cut from index i till j out of slice a | ```a = append(a[:i], a[j:]...)```
5| extend slice a with a new slice of length j | ```a = append(a, make([]T,j)...)```
6| insert item x at index i | ```a = append(a[:i], append([]T{x}, a[i:]...)...)```
7| insert a new slice of length j at index i | ```a = append(a[:i], append(make([]T, j), a[i:]...)...)```
8| insert an existing slice b at index i | ```a = append(a[:i], append(b, a[i:]...)...)```
9| pop from  the stack | ```x, a = a[len(a)-1], a[:len(a)-1]```
10| push into a stack | ```a = append(a, x)```




