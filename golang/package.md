##### Package System
- Design and maintenance of large programs practical by grouping related features together into units that can be easily understood and changes, independent of the other packages of the program.
- This modularity allows packages to be shared and reused by different projects. The boundaries of modules should be clearly defined.
- Each package defines a distinct space that encloses its identifiers. Each name is associated with a particular package, letting us choose short, clear names for the types, functions, and so on that we use most often, without creating conflicts with other parts of the program.
- Packages also provide __encapsulation__  by controlling which names are visible or exported outside the package. Restricting visibility also hides variables so that clients can access and update them only through exported functions that preserve internal invariants or enforce mutual exclusion in a concurrent program.
- In a package, if something starts with a capital letter, that means other packages will be able to see. So, variables and functions beginning with capital letter are exported(globally visible).
- When a file is changed, we must recompile the file's package and potentially all the packages that depend on it.

##### Reason for fast Go compilation
* Unused imports are a compile time error. So, a lot of unnecessary codes are not considered.
* All imports must be explicitly listed at the beginning of each source file, so the compiler does not have to read and process an entire file to determine its dependencies.
* The dependencies of a package form a directed acyclic graph, and because there are no cycles, packages can be compiled separately and perhaps in parallel. It forces the programmer to think earlier about a larger-scale issue (package boundaries) that if left until later may never be addressed satisfactorily.
* The object file for a compiled Go package records export information not just for the package itself, but for its dependencies too. The export data is the first thing in the object file, so the compiler can stop reading as soon as it reaches the end of that section.

##### Import Paths
- Each package is identified by a unique string called its __import__ __path__.
```go
import (
	"fmt"
	"math/rand"
	"github.com/go-sql-driver/mysql"
)
```
- Import path should be globally unique for packages to be shared or published.

##### Package Declaration
- The package declaration is required at the start of every Go source file. Its main purpose is to determine the default identifier for that package (called the _$package$_ _$name$_) when it is imported by another package.
- Every file of the math/rand package starts with package rand, so when you import this package, you can access its members as rand.Int, rand.Float64, and so on.
```go
package main
import (
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println(rand.Int())
}
```
- Conventionally, the package name is the last segment of the import path. Exceptions.
	* A package defining a command(an executable Go program) always has the name main, regardless of the package's import path. This is a signal to go build that it must invoke the linker to make an executable file.
	* Some files in the directory may have the suffix _test on the their package name if the file name ends with _test.go. Such a directory may define two packages: the usual one, plus another one called an external test package. The _test suffix signals to go test that it must build both packages, and it indicates which files belong to each package. External test packages are used to avoid cycles in the import graph arising from dependencies of the test.
	* Some tools for dependency management append version number suffixes to package import paths.
###### Import Declaration
- A Go source file may contain zero or more import declaration immediately after the package declaration and before the first non-import declaration. Two forms.
```go
import "fmt"
import "os"
```
```go
import (
	"fmt"
	"os"
)
```
- Imported packages may be grouped by introducing blank lines; such groupings usually indicate different domains. The order is not significant, but by convention the lines of each group are sorted alphabetically.
```go
import (
	"fmt"
	"html/template"
	"os"

	"golang.org/x/net/html"
	"golang.org/x/net/ipv4"
)
```
- If we need to import two packages whose names are same into a third package, the import declaration must specify an alternative name for at least one of them to avoid conflict. This is called a __renaming__ __import__.
```go
import (
	"crypto/rand"
	mrand "math/rand"
)
```
- If name of the imported package is unwiedly, as is sometimes the case for automatically generated code, an abbreviated name may be more convenient.
- Choosing an alternative name can help avoid conflicts with common local variable names.
- Each import declaration establishes a dependency from the current package to the imported package. The go build tool reports an error if these dependencies form a cycle.
*  To suppress the "unused import" error we would otherwise encounter, we must use a renaming import in which the alternative name is _, the black identifier. As usual, the blank identifier can never be referenced.
```go
import _ "image/png"
```
* This is most often used to implement a compile-time mechanism whereby the main program can enable optional features by blank-importing additional packages.

##### Packages and Naming
* Package name
	* short, but not cryptic
	* descriptive and unambiguous
	* avoid choosing a name which is commonly used for related local variables.
	* singular form

##### Initialization of a package
* Program execution begins by
	* importing the packages
	* initializing the main package
	* invoking the function main()
* A package with no imports is initialized by assigning initial values to all its package-level variables and then calling any package-level ```init()``` function defined in its source.
* A package may contain multiple ```init()``` functions, even within a single source file; they execute in unspecified order. It is best practice if the determination of a package's values depend on other values or functions found in the same package.
* Order of initialization is
  ```imported packages -> local variables -> init function(s) -> main```
* ```init()``` function cannot be called.


##### Building Packages
* The ```go build``` command compiles each argument package.
* If package is
	* a library: the result is discarded. This merely checks that the package is free of compiler errors.
	* a named main: go build invokes the linker to create an executable in the current directory; the name of executable is taken from the last segment of the package's import path.
* By default, the ```go build``` command builds the requested package and all its dependencies, then throw away all compiled except the final executable, if any.
* Both dependency analysis and the compilation are surprisingly fast, but as project grow to dozens of packages and hundreds of thousands of lines of code, the time to recompile dependencies can become noticeable, potentially several seconds, even when those dependencies haven't changed at all.
* The ```go install``` command is very similar to ```go build```, except that it saves the compiled code for each package and command instead of throwing it away. Compiled package are saved beneath the ```$GOPATH/pkg``` directory corresponding to the src directory in which the source resides, and command executables are saved in the ```$GOPATH/bin``` directory. Thereafter, ```go build``` and ```go install``` do not run the compiler for those packages and commands if they have not changed, making subsequent builds much faster.
