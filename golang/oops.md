##### Object Oriented Programming
* __Object__ is a value or variable that has methods, and a __method__ is a function associated with a particular type. An __object__ __oriented__ __program__ is one that uses methods to express the properties and operations of each data structure so that clients need not access the object's representation directly.
* A __method__ is declared with a variant of the ordinary function declaration in which an extra parameter appears before the function name. The parameter attaches the function to the type of that parameter.

##### Methods
```go
package main

import (
	"math"
	"fmt"
)

type Point struct { X, Y float64 }

func (p Point) Distance(q Point) float64 {
	return math.Hypot(q.X-p.X, q.X-p.Y)
}

func main() {
	p := Point{1, 2}
	fmt.Println(p.Distance())
}
```
* The extra parameter p is called the method's _receiver_, a legacy from early object-oriented languages that described calling a method as "sending a message to an object".
* There is no _self_ or _this_ in Go, instead receiver name is used. Since receiver name will be used frequently, it should be short and consistent.
* The expression ```p.Distance``` is called a _selector_, because this selects the appropriate Distance method for the receiver p of type ```Point```.
* Selectors are also used to select fields of struct types, as in ```p.X```. Since methods and fields inhabit the same name space, declaring a method X on the struct type ```Point``` would be ambiguous and the compiler will reject it.
* All methods of a given type must have unique names, but different types can use same name for a method.
* Methods may be declared on any named type defined in the same package, so long as its underlying type is neither a pointer nor an interface.

###### Methods with a Pointer Receiver
* Calling a function makes a copy of each argument value.
* Address of the variable using a pointer should be passed in the following situations.
	* if a function needs to update a variable
	* if an argument is so large that we wish to avoid copying it.
```go

type Point struct { X, Y float64 }

func (p *Point) ScaleBy(factor float64) {
	p.X *= factor
	p.Y *= factor
}

func main() {
	//1
	p1 := Point{1, 2}
	pptr := &p1
	pptr.ScaleBy(2)

	//2
	p2 := Point{1, 2}
	(&p2).ScaleBy(2)

	//3
	p3 := &Point{1, 2}
	p3.ScaleBy(2)

	//4
	p4 := Point{1, 2}
	p4.ScaleBy(2) //implicit conversion from p4 to &p4

}
```
* If any method of a type has a pointer receiver, then all methods of that type should have a pointer receiver, even ones that don't strictly need it.
* If the receiver p is a _variable_ of type ```Point```, but the method requires a ```*Point``` receiver, we can use ```p.ScaleBy(2)```. Here the compiler will perform an implicit ```&p``` on the variable.

##### Constructor
* Each fundamental type has 0 as initial value. If initial values are not provided at initialization, all fields will have the zero value.
* There is a generic constructor for any class instance.
* ```new``` allocates zeroed storage for a new item or type whatever and then returns a pointer to it.
```go
package main
import "fmt"

type Point struct {
  Label         string
  X, Y float64
}

func main() {

  var p1 Point
  var p2 = Point{"Center", 10.0, 20.0}
  var p3 = Point{X: 100.0, Y: 184.9}
  var ptr1 = new(Point)

  fmt.Println(p1)    // {0 0}
  fmt.Println(p2)    // {Center 10 20}
  fmt.Println(p3)    // {100 184.9}
  fmt.Println(*ptr1) // {0 0}
}
```

##### Composition(Struct Embedding)
* Inheritance can be used by embedding a ```struct``` into another ```struct```.
* After embedding, the base fields and methods are directly available in the derived ```struct```. Internally a hidden field is created, named as the base-struct-name.
* Base fields and methods are directly available as if they were declared in the derived ```struct```, but base fields and methods can be "shadowed". Shadowing means defining another field or method with the same name (and signature) of a base field or method. Once shadowed, the only way to access the base member is to use the hidden field named as the base-struct-name. This avoids __diamond__ __problem__..
```go
package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

type Women struct {
	Person        // embedding
	MaritalStatus bool
	Name          string //shadowed
}

func main() {
	a := Women{Person{"Gargi", 28}, true, "Gargi Dhal"}
	fmt.Println(a)
	fmt.Println(a.Name)
	fmt.Println(a.Person.Name)

	b := Women{Person: Person{Name: "Gargi2", Age: 29},
		MaritalStatus: true, Name: "Gargi2 Dhal"}
	fmt.Println(b)
	fmt.Println(b.Name)
	fmt.Println(b.Person.Name)

	person := Person{Name: "Gargi3", Age: 30}
	c := Women{person, true, "Gargi3 Dhal"}
	fmt.Println(c)
	fmt.Println(c.Name)
	fmt.Println(c.Person.Name)
}
```

##### Multiple Inheritance
```go
package main

import (
	"fmt"
)

type NamedObj struct {
	Name string
}

type Shape struct {
	NamedObj  //inheritance
	color     int32
	isRegular bool
}

type Point struct {
	x, y float64
}

type Rectangle struct {
	NamedObj            //multiple inheritance
	Shape               //^^
	center        Point //standard composition
	Width, Height float64
}

func main() {

	var aRect Rectangle = Rectangle{NamedObj{"name1"},
		Shape{NamedObj{"name2"}, 0, true},
		Point{0, 0},
		20, 2.5}

	fmt.Println(aRect.Name)
	fmt.Println(aRect.Shape)
	fmt.Println(aRect.Shape.Name)
}

```

##### Interfaces
* Golang interfaces are __satisfied__ __implicitly__. There is no need to declare all interfaces that a given concrete type satisfies; simply possessing the necessary methods is enough. This design lets you create new interfaces that are satisfied by existing concrete type without changing the exiting types, which is particularly useful for types defined  in packages that you don't control.
* A __concrete__ __type__ specifies the exact representation of its values and exposes the intrinsic operations of that representation, such as arithmetic for numbers, or indexing, append, and range for slices. When you have a value of a concrete type, you know exactly what it is and what you can do with it.
* An interface is an __abstract__ __type__. It doesn't expose the representation or internal structure of its values, or the set of basic operations they support; it reveals only some of their methods. When you have a value of an interface type, you know nothing about what it is; you know only what it can do, or more precisely, what behavior are provided by its methods.
* An __interface__ __type__ specifies a set of methods that a concrete type must possess to be considered an instance of that interface.
* An ```interface``` can be embedded in another ```interface```(like ```struct```).
```go
package io

type Reader interface {
	Read(p []byte) (n int, err error)
}

type Closer interface {
	Close() error
}

type ReadWriter interface {
	Reader
	Writer
}

type ReadWriteCloser interface {
	Reader
	Writer
	Closer
}
```
* A type __satisfies__ an interface if it possesses all the methods the interface requires.

```go
var w io.Writer
w = os.Stdout //OK *os.File has a write method
w = new(bytes.Buffer) //OK *bytes.Buffer has a write method
w = time.Second // compiler error
```
* Interface values may be compared using ```==``` and ```!=```. Two interfaces are equal if
	* both are nil
	* their dynamic types are identical and their dynamic values are equal according to the usual behavior of ```==```  for that type.
* Because interface values are comparable, they may be used as the keys of a map or as the operand of a switch statement.

##### Value and Behavior of interface
Let's consider following lines of code
```go
var w io.Writer //line 1
w = os.Stdout   //line 2
w = new(bytes.Buffer)
w = nil
```
After Line 1
* In go, interface are well initialized. The zero value of interface has both of its type and value component set to nil. Now, this is a nil interface.
![alt text](nil_interface.PNG "A nil interface")
* Calling this interface will cause a panic. The nil can be checked using
	```w == nil``` or ```w != nil```.

After Line 2
* This interface involves an implicit conversion from a concrete type to an interface type. This captures the type and value of its operand.
![alt text](interface_with_data.PNG)
* The interface value's dynamic type is set to the type descriptor for the pointer type ```*os.File```, and its dynamic value holds a copy of  ```os.Stdout```, which is a pointer to the ```os.File``` variable representing the standard output of the process.
* Calling the Write method on an interface value containing an ```*os.File``` pointer causes the ```(*os.File).Write``` method to be called.
```go
w.Write([]byte("hello")) //hello
```
* The dynamic type of an interface value cannot be known at compile time, so a call through an interface must use __dynamic__ __dispatch__. The compiler generates code to get the address of the method named ```Write``` from the type descriptor, then make an indirect call to that address. The receiver argument for the call is a copy of the interface's dynamic value, ```os.Stdout```
##### Empty Interface
* An empty interface type ```interface{}``` places no demand on the types that satisfy it, we can assign any value to the empty interface.
```go
var any interface{}
any = true
any = 12.34
any = "hello"
any = map[string]int {"one": 1}
any = new(bytes.Buffer)
```

##### Type Assertions
* A __type__ __assertion__ is an operation applied to an interface value.  Syntactically, it looks like ```x.(T)```, where x is an expression of an interface type and T is a type, called the "asserted" type.
* A type assertion checks that the dynamic type of its operand matches the asserted type.
* The type must either be the concrete type held by the interface, or a second interface type that the value can be converted to.
* There two different styles
	1. If the asserted type T is a concrete type, then the type assertion checks whether x's dynamic type is identical to T. If this check succeeds, the result of the type assertion is x's dynamic value, whose type is of course T. In short, a type assertion to a concrete type extracts the concrete value from its operand. If the check fails, then the operation panics.
	```go
	var w io.Writer
	w = os.Stdout
	f := w.(*os.File) //success: f == os.Stdout
	c := w.(*bytes.Buffer) //panic: interface holds *os.File, not *bytes.Buffer
	```
	2. If the asserted type T is an interface type, then the type assertion checks whether x's dynamic type satisfies T. If this check succeeds, the dynamic value is not extracted; the result is still an interface value with same type and value component, but the result has interface type T. In short, a type assertion to an interface type changes the type of the expression, making a different set of methods accessible, but it preserves the dynamic type and value component inside the interface value.
	```go
	var w io.Writer
	w = os.Stdout
	rw := w.(io.ReadWriter)
	```
* If the operand is a nil interface, the type assertion fails.
* If the type assertion appears in an assignment in which two results are expected, the operation does not panic on failure, but instead returns an additional second result, a boolean indicating success.
```go
var w io.Writer = os.Stdout
f, ok := w.(*os.File)       //success: ok, f == os.Stdout
b, ok := w.(*bytes.Buffer)  //failure: !ok, b == nil
```
* When the operand of a type assertion is a variable, rather than invent another name for new local variable, the original name is reused, shadowing the original.
```go
var w io.Writer = os.Stdout
if w, ok := w.(*os.File); ok {
	//...use w
}
```

##### Type Switches
* Interfaces are used in two distinct styles.
	1. An interface's methods express the similarities of the concrete types that satisfies the interfaces, but hide the representation details and intrinsic operations of those concrete types. The emphasis is on the methods, not on the concrete types.
	```
	io.Reader
	io.Writer
	fmt.Stringer
	```
	2. The interface can hold values of a variety of concrete types and consider the interface to be the union of those types. Type assertions are used to discriminate among these types dynamically and treat each case differently. Here, the emphasis is on concrete types that satisfy the interface, not on the interface's methods. There is no hiding of information.
* A type switch looks like an ordinary switch statement in which the operand is ```x.(type)```, that's literally the keyword ```type```. And each case has one or more types. A type switch enables a multi-way branch based on the interface value's dynamic type.
* An example of Golang API for SQL database looks like
```go
import "database/sql"

func listTracks(db sql.DB, artist string, minYear, maxYear int) {
	result, err := db.Exe(
		"select * from tracks where artist =? and ? <= year and <= ?",
		artist, minYear, maxYear)
	)
}
```
Here, the Exec method replaces each '?' in the query string with an SQL literal denoting the corresponding argument value, which may be boolean, a number, a string, or nil.
```go
func sqlQuote(x interface{}) string {
	switch x := x.(type) {
	case nil:
		return "NULL"
	case int, uint:
		return fmt.Sprintf("%d", x) //x has type interface{} here
	case bool:
		if x {
			return "TRUE"
		}
		return "FALSE"
	case string:
		return sqlQuoteString(x) //not fully shown here
	default:
		panic(fmt.Sprintf("unexpected type %T: %v", x, x))
	}
}
```
In the above code, sqlQuote accepts an argument of any type, but the function runs to completion only if the argument's type matches one of the cases in the type switch; Although the type of x is interface{}, we consider it a __discriminated__ __union__ of ```int, uint, bool, string and nil```.
