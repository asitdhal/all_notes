##### Goroutines
* Each concurrently executing activity is called goroutines.
* When a program starts, its only goroutine is the one that calls the main function, so we call it main goroutine.
* go statement
```go
f() // call f(); wait for it to return
go f() //create a new goroutine that calls f(); don't wait
```

##### Channels
* Channels are connection between goroutines. A channel is a communication mechanism that lets one goroutine send values to another goroutine.
* Each channel is a conduit for values of a particular type, called the channel's _element type_.
* Channels take care of all communication between goroutines, avoiding all pitfall of shared memory; the very act of communication through a channel guarantees synchronization.
* Data is passed around on channels: only one goroutine has access to a data item at any given time: so data races cannot occur by design. The ownership of data item is passed around.
* Channels have dual purpose
	1. communication - the exchange of a value
	2. synchronization - gurantee that two goroutines are at known state at any time.
* To create a channel
```go
ch1 := make(chan int) //ch1 has type 'chan int'
ch2 := make(chan chan int) //ch2 has type chan of chan of int
```
* A channel is a _reference_ to the data structure created by make. When we copy a channel or pass one as an argument to a function, we are copying a reference, so caller and callee refer to the same data structure. Like other reference type, the zero value of channel is nil.
* Two channels of same type can be compared using ```==```. The comparison is true if both are reference to the same channel data structure. A channel may also be compared to nil.
* Thre operations
  	1. send
  	2. receive
  	3. close

  ```send``` and ```receive``` operations are atomic, they complete without interruption.
  ```go
  ch <- x  //a send statement
  x = <-ch //a receive expression in an assignment statement
  <-ch     //a receive statement; result is discarded
  ```
* ```close``` operation sets a flag indicating that no more values will ever be sent on this channel; subsequent attempts to send will panic. Received operation on a closed channel yield the values that have been sent until no more values are left; any receive operations thereafter complete immediately and yield the zero value of channel's element type.
```go
close(ch)
```
* buffered and unbuffered channel
```go
ch = make(chan int)      //unbuffered channel
ch = make(chan int, 0)   //unbuffered channel
ch = make(chan int, 3)   //buffered channel with capacity 3
```

###### Unbuffered Channels
* A send operation on an unbuffered channel blocks the sending goroutine until another goroutine executes a corresponding receive on the same channel, at which point the value is transmitted and both goroutines may continue. Conversely, if the receive operation was attempted first, the receiving goroutine is blocked until another goroutine performs a send on the same channel.
* Communication over an unbuffered channel causes the sending and receiving goroutines to _synchronize_. When a value is sent on unbuffered channel, the receipt of the value _happens before_ the reawakening of the sending goroutine.
```go
package main

import (
	"io"
	"log"
	"net"
	"os"
)

func mustCopy(dst io.Writer, src io.Reader) {
	if _, err := io.Copy(dst, src); err != nil {
		log.Fatal(err)
	}
}

func main() {
	conn, err := net.Dial("tcp", "localhost:8000")
	if err != nil {
		log.Fatal(err)
	}
	done := make(chan struct{})
	go func() {
		io.Copy(os.Stdout, conn)
		log.Println("done")
		done <- struct{}{}
	}()

	mustCopy(conn, os.Stdin)
	conn.Close()
	<- done
}
```
* Messages sent over channel have 2 important aspects.
	1. a value
	2. moment at which it occured

  Messages are called event when the 2nd aspect is stressed. When the event carries no additional information, it's sole purpose is synchronization. In this case we use a channel of type ```struct{}```  

##### Pipelines
* A channel connects two goroutines and output of one goroutine is input to other. This is called _pipeline_.
![alt text](pipeline.jpg)
```go
package main

import "fmt"

func main() {
	naturals := make(chan int)
	squares := make(chan int)

	//counter
	go func() {
		for x := 0; x < 100; x++ {
			naturals <- x
		}
		close(naturals)
	}()

	//square
	go func() {
		for x := range naturals {
			squares <- x * x
		}
		close(squares)
	}()

	for x := range squares {
		fmt.Println(x)
	}
}
```
##### Unidirectional Channel Type(fan-out-fan-in)
* When a channel is supplied as a function parameter, it is nearly always with the intent that it be used exclusively for sending or exclusively for receiving.
* To prevent misuse, Golang provides _unidirectional channel_ types that expose only one or the other of the send and receive operations.
  	* ```chan<- int```: a send only channel
  	* ```<-chan int```: a receive only channel
```go
package main

import "fmt"

func counter(out chan<- int) {
	for x := 0; x < 100; x++ {
		out <- x
	}
	close(out)
}

func squarer(out chan<- int, in <-chan int) {
	for x := range in {
		out <- x * x
	}
	close(out)
}

func printer(in <-chan int) {
	for x := range in {
		fmt.Println(x)
	}
}

func main() {
	naturals := make(chan int)
	squares := make(chan int)

	go counter(naturals)
	go squarer(squares, naturals)
	printer(squares)

}
```

###### Buffered Channel
* It has queue of element, maximum capacity is determined when its created, by the capacity agrumnet to make.
```go
ch = make(chan string, 3)
```
* A send operation on a buffered channel inserts an element at the back of the queue, and a receive operation removes an element from the front.
* Channel's buffer decouples the sending and receiving goroutine. If buffer is full
	* full: the send operation blocks its goroutine until space is made available by another goroutine's receiver.
	* empty: a receive operation blocks until a value is sent by another goroutine.
	* neither full nor empty: either a send or a receive operation could proceed without blocking
	```go
	ch = make(chan string, 3)
	ch <- "A"
	ch <- "B"
	ch <- "C"
	fmt.Println(<-ch)
	```
* Channel's capacity: by using built-in cap function.
```go
fmt.Println(cap(ch)) // 3
```
* Channel's len: usually stale

###### Buffered vs Unbuffered Channel
* Unbuffered channel gives stronger synchronization guarantees because every send operation is synchronized with its corresponding receive operation; with buffered channel this operation is decoupled.
* When we know an upper bound on the number of values that will be sent on a channel, it's not unusual to create buffer channel of that size and perform all sends before the first value is received. In this case, failure to allocate sufficient buffer capacity would cause the program to deadlock.

##### Multiplexing with Select
* The general form of select statement is shown below.
  ```go
  //three channels ch1, ch2, ch3
  select {
  case <-ch1:
	 //..
  case x := <-ch2:
	//..
  case ch3 <- y:
	//..
  }
  ```
  Like a switch statement, it has a number of case and an optional default.
  * Each case specifies a communication(a send or receive operation on some channel) and an associated block of statements.
* A select waits until a communication for some case is ready to proceed. It then performs that communication and executes the case's associated statements; the other communications do not happen.
* Rocket countdown example
```go
package main

import (
	"fmt"
	"time"
	"os"
)

func launch() {
	fmt.Println("huuaaaa...")
}

func main() {
	//abort channel
	abort := make(chan struct{})
	go func() {
		os.Stdin.Read(make([]byte, 1)) //read a single byte
		abort <- struct{}{}
	}

	fmt.Println("Commencing countdown. Press return to abort..")
	tick := time.Tick(1 * time.Second)
	for countdown := 10; coundown > 0; coundown-- {
		fmt.Println(countdown)
		select {
		case <-tick:
			//do nothing
		case <-abort:
			fmt.Println("Launch aborted!")
			return
		}
	}
	launch()
}
```
* A ```select``` with no cases, ```select{}``` waits forever.
* If multiple cases are ready, select picks one at random, which ensures that every channel has an equal chance of being selected.
* Sometimes we want to try to send or receive on channel but avoid blocking if the channel is not ready - a non blocking communication. A select statement can do that too. A select may have a default, which specifies what to do when none of the other communications can proceed immediately.
_Polling_ using select statement.
  ```go
  select {
  case <-abort:
	fmt.Println("Launch aborted!\n")
	return
  default:
	//do nothing
  }
  ```
##### Semaphore
1. 
##### Channel Axioms
1. A send to a nil channel blocks forever, because the zero value for an uninitialized channel is nil.
   ```go
   package main

   func main() {
	   var c chan string
	   c <- "let's get started" // deadlock
   }
   ```
2. A receive from a nil channel blocks forever
   	```go
   	package main

	import "fmt"

	func main() {
		var c chan string
		fmt.Println(<-c) // deadlock
	}
   	```
3. A send to a closed channel panics
	```go
	package main

	import "fmt"

	func main() {
    	var c = make(chan int, 100)
        for i := 0; i < 10; i++ {
        	go func() {
            	for j := 0; j < 10; j++ {
                	c <- j
                }
                close(c)
			    c <- 101 //this will panic
            }()
        }
        for i := range c {
            fmt.Println(i)
        }
	}
	```
4. A receive from a closed channel returns the zero value immediately
   ```go
   package main

   import "fmt"

   func main() {
	  c := make(chan int, 3)
	  c <- 1
      c <- 2
	  c <- 3
	  close(c)
	  for i := 0; i < 4; i++ {
	     fmt.Printf("%d ", <-c) // prints 1 2 3 0
	  }
   }
   ```
5. A closed channel never blocks. It always returns the zero value of the type. This property can be used in the range over channel idiom to exit the loop once a channel has been drained.
	```go
	package main

	import "fmt"

	func main() {
        ch := make(chan bool, 2)
        ch <- true
        ch <- true
        close(ch)

        for v := range ch {
    		fmt.Println(v) // called twice
        }
	}
	```
