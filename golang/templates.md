### Golang Template
* A template engine is a software application which uses predefined template files to generate dynamic HTML pages. The primary objective is to separate view from logic. 
* In golang, the handler calls the template engine, passing it the template(s) to be used, usually as a list of template files and the dynamic data. The template engine then generates the HTML and writes it to the ResponseWriter, which adds it to the HTTP response sent back to the client.
![alt text](go_templ.png)

* A template is a string or file containing one or more portions enclosed in double braces, ```{{...}}```, called __actions__. Most of the string is printed literally, but actions trigger other behaviors.

* Two steps to generate HTML from template
	1.  Parse the text-formatted template source to create a parsed template struct.
	2.  Execute the parsed template, passing a ResponseWriter and some data to it. This triggers the template engine to combine the parsed template with the data to generate the final HTML that’s passed to the ResponseWriter. 
	![alt text](go_templ2.png)
	```HTML
	<html>
  	<head>
    	<title>Hello World</title>
  	</head>
  	<body>
    	{{ . }}
  	</body>
	</html>
	```

	```go
	package main

	import (
    	"net/http"
    	"html/template"
	)

	func process(w http.ResponseWriter, r *http.Request) {
    	t, _ := template.ParseFiles("tmpl.html") //setp 1
    	t.Execute(w, "Hello World!") //step 2
	}

	func main() {
    	server := http.Server{
        	Addr: "127.0.0.1:8080",
    	}
    	http.HandleFunc("/process", process)
    	server.ListenAndServe()
	}
	```
* Parsing templates
 	1. ```template.ParseFiles()``` is a variadic function call. It takes variable number arguments. 
	2. ```template.ParseGlob()``` can be used also. This uses pattern matching.
	```go
	t, _ := template.ParseFiles("t1.html", "t2.html")
	t, _ := template.ParseGlob("*.html")	
	```
	3. ```template.Parse()``` method can be used on strings.
	```go
	tmpl := `<!DOCTYPE html>
	<html>
	<head>
		<title>Hello World!</title>
	</head>
	<body>
		{{ . }}
	</body>
	</html>
	`
	t := template.New("tmpl.html")
	t, _ = t.Parse(tmpl)
	t.Execute(w, "Hello World!")
	```
	* Error Handling: ```template.Must()``` wraps around a function that returns a pointer to a template and an error, and panics if the error is not a nil.
	```go
	t  := template.Must(template.ParseFiles("tmpl.html"))
	```
* Executing templates
	* The Execute method applies a parsed template to the specified data object (here a Note object) and writes the output to an output writer.
	```go
	t.Execute(w, "Hello World!")
	```
	* The ExecuteTemplate() method works if the template contains multiple parsed template object.
	```go
	t, _ := template.ParseFiles("t1.html", "t2.html") //here t is a template set which contains two templates
	t.Execute(w, "Hello World!") //this one executes t1
	t.ExecuteTemplate(w, "t2.html", "Hello World!") //this one executes t2
	```
* Actions
	* Within an action, there is a notion of the current value, referred as "dot" and written as ".", a period. The dot initially refers to the template's parameter.
		1. Conditional Actions
		2. Iterator Actions
		3. Set Actions
		4. Include Actions
	* Conditional Actions
	```syntax
	{{ if arg }}
  		some content
	{{ else }}
  		other content
	{{ end }}
	```
	* Iterator Actions
	```syntax
	{{ range array }}
  		Dot is set to the element {{ . }}
	{{ end }}
	```
	* Set Actions
	```syntax
	{{ with arg }}
  		Dot is set to arg
	{{ end }}
	```
	The dot (.) between {{ with arg }} and {{ end }} is now pointing to arg. 
	A fallback variant
	```syntax
	{{ with arg }}
  		Dot is set to arg
	{{ else }}
  		Fallback if arg is empty
	{{ end }}
	```
	* Include Actions
	```syntax
	{{ template "name" arg }}
	```
	name template will be replaced in the called place.
* Variable can also be used in the template.
	```go
	$variable := value
	{{ range $key, $value := . }}
  		The key is {{ $key }} and the value is {{ $value }}
	{{ end }}
	```
* Pipelines are arguments, functions, and methods chained together in a sequence. This works much like the Unix pipeline. 
	```go
	{{ p1 | p2 | p3 }}
	{{ 12.3456 | printf "%.2f" }}
	```
#### Template Functions
* Functions can also be used in go template engine. The Go template engine has a set of built-in functions that are pretty basic. It also allows us to define custome function.
To create custome functions,
	1.  Create a FuncMap map, which has the name of the function as the key and the actual function as the value.
	2.  Attach the FuncMap to the template.
Here, FuncMap must be attached before the template is parsed. This is because when you parse the template you must already know the functions used within   the template, which you won’t unless the template has the FuncMap. So, the general flow is.

		![alt text](func_map_template.png)