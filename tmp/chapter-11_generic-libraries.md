## Callbacks 

Several types, called function object types

* pointer-to-function types
* class types with an overloaded operator(), including lambdas
* class types with a conversion function yielding a pointer-to-function or reference-to-function.

```cpp
#include <iostream>
#include <vector>

// if we make op as a const, like Callable const&, the const will be ignored
template <typename Iter, typename Callable>
void forEach(Iter current, Iter end, Callable op)
{
    while (current != end) {
        // if op is of class type, the following statement will be transformed to
        // op.operator()(*current)
        op(*current);
        current++;
    }
}

void func(int i)
{
    std::cout << "func() called for: " << i << '\n';
}

class FuncObj
{
 public:
    // usually this is made as const
    void operator()(int i) const {
        std::cout << "FuncObj::op() called for: " << i << '\n';
    }
};

int main()
{
    std::vector<int> primes = {2, 3, 5, 7, 11, 13, 17, 19 };
    
    // func is decayed into a function pointer when passed as a value
    forEach(primes.begin(), primes.end(), func);
    
    // explicitly pointer to function is passed
    forEach(primes.begin(), primes.end(), &func);
    
    forEach(primes.begin(), primes.end(), FuncObj());
    
    // lambdas that start with [](no capture) produce a conversion operator to a
    // function pointer.
    forEach(primes.begin(), primes.end(), [](int i) {
        std::cout << "lambda called for: " << i << '\n';
    });
}
```

## std::invoke

`std::invoke()` can be called with the given callable followed by the additional given parameters along with the referenced element. `std::invoke()` handles this as follows.

* If the callable is a pointer to member, it uses the first additional argument as the `this` object. All remaining additional parameters are just passed as arguments to the callable.

* Otherwise, all additional parameters are just passed as arguments to the callable.

```cpp
#include <iostream>
#include <vector>
#include <utility>
#include <functional>

template <typename Iter, typename Callable, typename... Args>
void forEach(Iter current, Iter end, Callable op, Args const&... args)
{
    while (current != end) {
        std::invoke(op, args..., *current);
        current++;
    }
}

class MyClass
{
 public:
    // usually this is made as const
    void memfunc(int i) const {
        std::cout << "MyClass::memfunc() called for: " << i << '\n';
    }
};

int main()
{
    std::vector<int> primes = {2, 3, 5, 7, 11, 13, 17, 19 };
    
    // lambda as the callable and an additional argument
    forEach(primes.begin(), primes.end(), [](std::string const& prefix, int i) {
        std::cout << prefix << i << '\n';
    },
    "- value: ");
    
    MyClass obj;
    forEach(primes.begin(), primes.end(), &MyClass::memfunc, obj);
}
```

## Wrapping Function Calls

We can support move semantics by perfect forwarding both the callable and all passed arguments.

```cpp

```