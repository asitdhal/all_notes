C++11 has three different ways to pass by refernce.

* ```X const&(const lvalue reference)```: parameter refers to the passed object, without the ability to modify it.
* ```X&(nonconst lvalue reference)```: parameter refers to the passed object, with the ability to modify it.
* ```X&&(rvalue reference)```: the parameter refers to the passed object, with move semantics, meaning that you can modify or "steal" the value.

Passing by value is recommended unless there are strong reasons like

* copying is not possible.
* parameters are used to return data.
* templates just forward the parameters to somewhere else by keeping all the properties of the original arguments.
* there are significant performance improvements.

### Pass by Value

```cpp
template <typename T>
void printV(T arg)
{
	// do things
}
```

the above code becomes this when called for a string

```cpp
void printV(std::string arg)
{
	// do things
}

std::string s = "hi";
printV(s);
```

here copy is a deep copy. The potential copy constructor is not always called.

```cpp

std::string returnString()
{
	std::string someString = "ha ha ha";
	return someString;
}

void printV(std::string arg)
{
	// do things
}

std::string s = "hi";
printV(s); // copy constructor
printV(std::string("hi")); // copying usually optimized away(if not, move constructor)
printV(returnString()); // copying usually optimized away(if not, move constructor)
printV(std::move(s)); // move constructor
```

when an lvalue is passed, copy constructor is used.

when a pravlue(temporary objects created on the fly or returned by another function) is passed, compilers usually optimize passing the argument so that no copy constructoris called at all. This is only available in C++17. Before that compilers used to use move semantics.

when passing an xvalue(an existing nonconstant object with std::move()), we force to call the move constructor by signalling that we no longer need the value of s.

### Passing by Value Decays

When passing arguments to a parameter by value, the type decays. It means, raw array convertes to pointers, qualifiers such as const and volatile are removed.

```cpp
std::string const c = "hi"; 
printV(c); // c decays so that arg has type std::string

printV("hi"); // decays to pointer so that arg has type char const*

int arr[4];
printV(arr); // decays to pointer so that arg has type char const*
```

### Passing by Const Reference

```cpp
template <typename T>
void printR(T const& arg)
{
	// do things
}
```

```cpp

std::string returnString()
{
	std::string someString = "ha ha ha";
	return someString;
}

std::string s = "hi";
printR(s); // no copy
printR(std::string("hi")); // no copy
printR(returnString()); // no copy
printR(std::move(s)); // no copy
```

Passing an argument by reference is implemented by passing the address of the argument. Addresses are encoded compactly, and therefore transferring an address from the caller to the callee is efficient itself. 

### Passing by Reference Does not decay

raw arrays are not decayed to ponters and const/volatile are not removed.
However, the call parameter is declared as T const&, the template parameter T itself is not deduced as const.


```cpp
template <typename T>
void printR(T const& arg)
{
	// do things
}

std::string const c = "hi";
printR(c); // T deduced as std::string, arg is std::string const&

printR("hi"); // T deduced as char[3], arg is char const(&)[3]

int arr[4];
printR(arr); // T deduced as int[4], arg is int const(&)[4]
```

### Passing by Nonconstant Reference

```cpp
template <typename T>
void outR(T& arg)
{

}
```

```cpp

std::string returnString()
{
	std::string someString = "ha ha ha";
	return someString;
}

std::string s = "hi";
outR(s); // OK: T deduced as std::string, arg is std::string&
outR(std::string("hi")); // ERROR: not allowed to pass a temporary(prvalue)
outR(returnString()); // ERROR: not allowed to pass a temporary(prvalue)
outR(std::move(s)); // ERROR: not allowed to pass an xvalue
```

Array also doesn't decay.

```cpp
int arr[4];
outR(arr); // OK: T deduced as int[4], arg is int(&)[4]
```

You can modify elements and, for example, deal with the size of the array.

```cpp
template <typename T>
void outR(T& args)
{
	if (std::is_array<T>::value) {
		std::cout << "got array of " << std::extent<T>::value << " elems\n";
	}
	...
}
```

If you pass a const argument, the deduction might result in arg becoming a declaration of a constant reference, which means that passing an rvalue is suddenly allowed, where an lvalue is expected.

```cpp

std::string returnConstString();
std::string const c = "hi";
outR(c); // OK: T deduced as std::string, arg is std::string const
outR(returnConstString()); // OK: T deduced as std::string const
outR("hi"); // OK: T deduced as char const[3]
```

Of couse, you can't modify the value inside the function.

You can disable passing const objects to nonconst references.

* Use static assertion to trigger a compile-time erroe.

```cpp
template <typename T>
void outR(T& arg)
{
	static_assert(!std::is_const<T>::value, "out parameter of foo<T>(T&) is const");
	...
}
```

* Disable the template for this case either by using std::enable_if<>

```cpp
template <typename T, typename = std::enable_if<!std::is_const<T>::value>
void outR(T& arg)
{
	static_assert(!std::is_const<T>::value, "out parameter of foo<T>(T&) is const");
	...
}
```

### Passing by Forwarding Reference

```cpp
template <typename T>
void passR(T&& arg) {
	...
}

```

You can pass everything to a forwarding reference and, as usual when passing by reference, no copy gets created.

```cpp
std::string s = "hi";
passR(s); // OK: T deduced as std::string&(also type of arg)
passR(std::string("hi")); // OK: T deduced as std::string, arg is std::string&&
passR(returnString()); // OK: T deduced as std::string, arg is std::string&&
passR(std::move(s)); // OK: T deduced as std::string, arg is std::string&&
passR(arr); // OK: T deduced as int(&)[4](also type of arg)
```

### std::ref() and std::cref()

When a template is declared to take arguments by value, the caller can use `std::cref()` and `std::ref()` to pass the argument by reference.

```cpp
template <typename T>
void printT(T arg) {
	...
}

std::string s = "hello";
printT(s); // pass s by reference
printT(std::cref(s)); // pass s "as if by reference"
```

It creates an object of type `std::reference_wrapper<>` referring to the original argument and passes this object by value. The wrapper more or less supports only one operation: an implicit type conversion back to the original type, yielding the original value. 

```cpp
#include <functional>
#include <string>
#include <iostream>

void printString(std::string const& s)
{
	std::cout << s << "\n";
}

template <typename T>
void printT(T arg)
{
	printString(arg);
}

int main()
{
	std::string s = "hello";
	printT(s); // print s passed by value
	printT(std::cref(s)); // print s passed "as if by reference"
}
```

The compiler has to know that an implicit conversion back to the original type is necessary. For this reason, `std::ref()` and `std::cref()` usually work fine only if you pass objects through generic code. 

```cpp
template <typename T>
void printV(T arg)
{
	std::cout << arg << '\n';
}

std::string s = "hello";
printV(s); // ok
printV(std::cref(s)); // ERROR: no operator << for reference wrapper defined
```

This will also fail, you can't compare a reference wrapper with a char const* or std::string.

```cpp
template <typename T1, typename T2>
bool isLess(T1 args, T2 arg2)
{
	return arg1 < arg2;
}

std::string s = "hello";
if (isLess(std::cref(s) < "world")) {} // ERROR
if (isLess(std::cref(s) < std::string("world") )) {} // ERROR

```

> The effect of the class `std::reference_wrapper<>` is to be able to use a reference as a "first class object", which can copy and therefore pass by value to function templates. You can also use it in classes, to hold references to objects in containers. But you always finally need a conversion back to the underlying type.

### Dealing with String Literals and Raw Arrays

Difference between call by value and call by reference

* Call-by-value decays so that they become pointers to the element type.
* Any form of call-by-reference does not decay so that the arguments become references that still refer to arrays.


When decaying arrays to pointers, you lose the ability to distinguish between handling pointers to elements from handling passed arrays. 
When dealing with parameters where string literals may be passed, not decaying can be a problem, because string literals of different size have different types.

```cpp
template <typename T>
void foo(T const& arg1, T const& arg2)
{
	...
}

foo("hi", "guy"); // ERROR
```

But, this works

```cpp
template <typename T>
void foo(T arg1, T arg2)
{
	...
}

foo("hi", "guy"); // OK
```

