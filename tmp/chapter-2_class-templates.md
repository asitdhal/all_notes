Code is instantiated only for template(member) functions that are called. For class templates, member functions are instantiated only if they are used.

```cpp
#include <iostream>
#include <vector>
#include <cassert>
#include <string>

template <typename T>
class Stack
{
public:       
	void push(T const& elem);
	void pop();
	T const& top() const;
	bool empty() const { return m_elems.empty(); }
	void printOn(std::ostream& strm) const

private:
	std::vector<T> m_elems;
};

int main()
{
	Stack<int> intStack;
	intStack.push(10);
	intStack.push(90);
	intStack.push(67);
	std::cout << intStack.top() << "\n";
	return 0;
}

template <typename T>
void Stack<T>::push(T const& elem)
{
	m_elems.push_back(elem);
}

template <typename T>
void Stack<T>::pop()
{       
	assert(!m_elems.empty());       
	m_elems.pop_back();
}

template <typename T>
T const& Stack<T>::top() const
{       
	assert(!m_elems.empty());       
	return m_elems.back();
}

template <typename T>
void Stack<T>::printOn(std::ostream& strm) const
{       
	for (T const& elem : m_elems)
	{              
		strm << elem << " ";       
	}
}
```

Template arguments only have to provide all necessary operations that are needed(instead of that could be needed). 

```cpp
Stack<std::pair<int, int>> pairStack;
pairStack.push({ 4, 5 });
pairStack.push({ 6, 7 });
std::cout << pairStack.top().first << ", " << pairStack.top().second <<  "\n";
// std::cout << pairStack.printOn(std::cout); //error
```

Here, std::pair doesn't overload << operator. You can create object, but  compilation will fail if you call printOn.

### Friends Function

When trying to declare the friend function and define it afterwards, things become more complicated.
1. We can implicitly declare a new function template, which must use a different template parameter, such as U.

```cpp
template <typename T>
class Stack
{    
	...
	template <typename U>
	friend std::ostream& operator<< (std::ostream&, Stack<U> const&);
};
```

Neither using T  again nor skipping the template parameter declaration would work(either the inner T hides the outer T or we declare a nontemplate function in namespace scope).

2. We can forward declare the output operator for a Stack<T> to be a template.

```cpp
template <typename T>
class Stack;

template <typename T>
std::ostream& operator<< (std::ostream&, Stack<T> const&);

template <typename T>
class Stack
{    
	...
	friend std::ostream& operator<< <T>(std::ostream&, Stack<T> const&);
};
```

### Specialization of Class Templa

```cpp
testemplate <>
class Stack<std::string>
{
	.... // all member functions must be reimplemented
};
```

### Partial Specilization

```cpp
#include <iostream>
#include <vector>
#include <cassert>
#include <string>
#include <utility>

template <typename T>
class Stack
{
public:
	void push(T const& elem);
	void pop();
	T const& top() const;
	bool empty() const { return m_elems.empty(); }
	void printOn(std::ostream& strm) const;
private:
	std::vector<T> m_elems;
};

template <typename T>
class Stack<T*>
{
public:
	void push(T*);
	T* pop();
	T* top() const;
	bool empty() const { return m_elems.empty(); }
private:
	std::vector <T*> m_elems;
};

int main()
{
	Stack<int*> intPtrStack;
	intPtrStack.push(new int(10));
	intPtrStack.push(new int(112));

	auto* elem = intPtrStack.pop();
	std::cout << *elem << "\n";
	std::cout << *intPtrStack.top() << "\n";
	delete elem;
	return 0;
}

template <typename T>
void Stack<T>::push(T const& elem)
{
	m_elems.push_back(elem);
}

template <typename T>
void Stack<T>::pop()
{
	assert(!m_elems.empty());
	m_elems.pop_back();
}

template <typename T>
T const& Stack<T>::top() const
{
	assert(!m_elems.empty());
	return m_elems.back();
}

template <typename T>
void Stack<T>::printOn(std::ostream& strm) const
{
	for (T const& elem : m_elems)
	{
		strm << elem << " ";
	}
}

template <typename T>
void Stack<T*>::push(T* elem)
{
	m_elems.push_back(elem);
}

template <typename T>
T* Stack<T*>::pop()
{
	assert(!m_elems.empty());
	T* p = m_elems.back();
	m_elems.pop_back();
	return p;
}

template <typename T>
T* Stack<T*>::top() const
{
	assert(!m_elems.empty());
	return m_elems.back();
}
```

### Default Class Template Arguments

```cpp
template <typename T, typename Cont = std::vector<T>>
class Stack
{
public:
	void push(T elem);
	...
	...
private:
	Cont elems;
};

template <typename T, typename Cont>
void Stack<T, Cont>::push(T const& elem)
{    
	...
}

int main()
{
	Stack<int> intStack;
	Stack<double, std::deque<double>> dblStack;
	...
}
```

### Type Aliases

A new name can be defined for a complete type

1.  typedef

```cpp
typedef Stack<int> IntStack; //typedef declaration
void foo(IntStack const& s) ; //IntStack is a typedef-name        
```

2. using

```cpp
using IntStack = Stack<int>; //alias declaration
void foo(IntStack const& s);  //IntStack is a type alias
```

### Alias Templates

An alias declaration can be templated to provide a convenient name for a family of types(not possible using typedef)

```cpp
template <typename T>
using DequeStack = Stack<T, std::deque<T>>;
```

Alias templates are especially helpful to define shortcuts for types that are members of class templates.

```cpp
struct C
{
	typedef ... iterator;
	...
};
// or

struct MyType
{
	using iterator = ...;    
	...
};

// then

template <typename T>
using MyTypeIterator = typename MyType<T>::iterator;

// allows the use of
MyTypeIterator<int> pos;

// instead of the following
typename MyType<T>::iterator pos; // typename is necessary, because the member is a type
```

### Type Traits Suffix \_t

In C++14, you can write

```cpp
std::add_const_t<T> 
```

instead of

```cpp
typename std::add_const<T>::type ; // was in C++11
```

because the standard library defines

```cpp
namespace std
{
    template <typename T> 
	using add_const_t = typename add_const<T>::type;
}
```

### Class Template Argument Deduction

Until C++17, you always had to pass all template parameter type to class templates(unless they have default values). Since C++17, the constraint that you always have to specify the template arguments explicitly was relaxed. So, you can skip to define the template arguments explicitly, if the constructor is able to deduce all template parameters.

By providing constructors, that pass some initial arguments, you can support deduction of the element type of a stack. 

```cpp
template <typename T>
class Stack
{
private:
	std::vector<T> m_elems;

public:
	Stack() = default;
	Stack(T const& elem) : m_elems({elem}) {}
	...
	...
};

Stack intStack = 0; // Stack<int> deduced
Stack intStack2 = intStack; // copy constructor
```

Due to the definition of the int constructor,  you have to request the default constructors to be available with its default behavior, because the default constructor is available only if no other constructor is defined.

```cpp
Stack() = default;
```

The argument elem is passed to m_elems with braces around to initialize the vector m_elems with an initializer list with elem as the only argument.

```cpp
: m_elems({elem})
```

Unlike for function templates, class template arguments may not be deduced only partially.In principle, the above should work for a string literal.

```cpp
Stack stringStack = "bottom"; //Stack<char const[7]> deduced 
```

In general, when passing arguments of a template type T by reference, the parameter doesn't  decay, which is the term for the mechanism to convert a raw array type to the corresponding raw pointer type. This means we realize aStack

```cpp
<char const[7]>
```

This is not what we wanted. Our stack should support strings of variable length(not always 7).

When passing arguments of a template type T by value, the parameter decays, which is the term for the mechanism to convert RAW array type to the corresponding raw pointer type.  So, the call parameter T of the constructor is deduced to be char const* so that the whole class is deduced to be Stack<char const*>. So we need to declare the constructor so that the argument is passed by value.

```cpp
template <typename T>
class Stack
{
private:
	std::vector<T> m_elems;  // elements
public:
	Stack(T elem) : m_elems ( {elem}) // decay will happen now
	{}
};
```

### Deduction Guides

Specific deduction guides can be defined to provide additional or fix existing class template argument deduction. e.g. You can define that whenever a string literal or C string is passed, the stack is instantiated for std::string.

```cpp
Stack(char const*) -> Stack<std::string>;
```

### Templatized Aggregates

Aggregate classes are classes/structs with no

* user-provided, explicit, or inherited constructors
* no private or protected nonstatic data members
* no virtual functions
* no virtual, private, or protected base classes


Aggregate classes can also be templates.

```cpp
template <typename T>
struct ValueWithComment
{
    T value;
    std::string comment;
};

ValueWithComment<int> vc;
vc.value = 42;
vc.comment = "initial value";
```

Since it doesn't have a constructor, it can't perform argument deduction. In C++17, you can even define deduction guides for aggregate class templates.

```cpp
ValueWithComment(char const*, char const*) -> ValueWithComment<std::string>;
```
