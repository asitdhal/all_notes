### Variadic Templates

```cpp
#include <iostream>

void print()
{
    std::cout << "\n";
}

template <typename T, typename... Types>
void print(T arg1, Types... args)
{
    std::cout << arg1 << " ";
    print(args...);
}

int main()
{
    print(1, 2, 3, "asit", 4.5, false);
    return 0;
}
```

Here, Types is the parameter pack.

If two function templates only differ by a trailing parameter pack, the function template without the trailing parameter pack is preferred.

```cpp
#include <iostream>

template <typename T>
void print(T arg)
{
    std::cout << arg << "\n";
}

template <typename T, typename... Types>
void print(T arg1, Types... args)
{
    print(arg1);
    print(args...);
}

int main()
{
    print(1, 2, 3, "asit", 4.5, false);
    return 0;
}
```

C++11 also introduced a new form of the sizeof operator for variadic templates: sizeof... . It expands to the number of elements a parameter pack contains.

```cpp
#include <iostream>

template <typename T, typename... Types>
void print(T arg1, Types... args)
{
    std::cout << "No of remaining types: " << sizeof...(Types) << '\n';
    std::cout << "No of remaining args : " << sizeof...(args) << '\n';
}

int main()
{
    print(0, 1, 2, 3, "asit", "kumar", 4.5, false);
    return 0;
}
```

So, you might think to skip the last function call.

```cpp
#include <iostream>

template <typename T, typename... Types>
void print(T arg1, Types... args)
{
    std::cout << arg1 << '\n';
    if (sizeof...(args) > 0) {
        print(args...);
    }
}

int main()
{
    print(0, 1, 2, 3, "asit", "kumar", 4.5, false);
    return 0;
}
```

This approach doesn't work because in general both branches of all if statements in function templates are instantiated. Whether the instantiated code is useful is a run-time decision, while the instantiation of the call is a compile-time decision. Here, if you call the `print()` function template for one(last) argument, the statement with the call of `print(args...)` still is instantiated for no argument, and if there is no function `print()` for no arguments provided, this is an error.

### Fold Expressions

In C++17, there is a feature called fold expression to compute the result of using binary operator over all the arguments of a parameter pack(with an optional initial value).

```cpp
#include <iostream>

template <typename... T>
auto foldSum(T... args)
{
    return (... + args);
}

int main()
{
    std::cout << foldSum(1, 2, 3, 4, 5, 100) << "\n";
    return 0;
}
```

Fold Expression | Evaluation
--------- | -----------
`(... op pack)` | `(((pack1 op pack2) op pack3)... op packN)`
`( pack op ...)` | `(pack1 op (... (packN-1 op packN)))`
`( init op ... op pack)` | `(((init op pack1) op pack2)... op packN)`
`( pack op ... op init)` | `(pack1 op (... (packN op init)))`


If the parameter pack is empty, the expression is usually ill-formed (with the exception that for the following).

Operator | empty parameter pack value |
---------|----------- |
`&&` | `true` |
&#x7c;&#x7c; | `false` |
`,` | `void()` |

