## Empty Base class optimization(EBCO)

Empty classes don't need any bits of memory at run time.

Empty class can contain

* type members
* nonvirtual function members
* static data members

Other members do require memory at runtime.

```cpp
#include <iostream>

class EmptyClass {};

int main()
{
    std::cout << sizeof(EmptyClass) << "\n";
    return 0;
}

``` 

It will print "1".

The size is at least one because size zero is not good for pointer arithmatic. 

## Empty Base Class Optimization(EBCO)

> When an empty class is used as a base class, no space needs to be allocated for it provided that it does not cause it to be allocated to the same address as another object or subobject of the same type. 


```cpp
#include <iostream>

class Empty {
    using Int = int;
};

class EmptyToo : public Empty { };

class EmptyThree : public EmptyToo { };

int main()
{
    std::cout << "sizeof(Empty): " << sizeof(Empty) << "\n"; // output 1
    std::cout << "sizeof(EmptyToo): " << sizeof(EmptyToo) << "\n"; // output 1
    std::cout << "sizeof(EmptyThree): " << sizeof(EmptyThree) << "\n"; // output 1
    return 0;
}
```

Let's see another example

```cpp
#include <iostream>

class Empty {
    using Int = int;
};

class EmptyToo : public Empty { };

class NonEmpty : public Empty, public EmptyToo { };

int main()
{
    std::cout << "sizeof(Empty): " << sizeof(Empty) << "\n"; //outputs 1
    std::cout << "sizeof(EmptyToo): " << sizeof(EmptyToo) << "\n"; // outputs 1
    std::cout << "sizeof(NonEmpty): " << sizeof(NonEmpty) << "\n"; // outputs 2 in gcc, 1 in msvc(msvc is crap)
    return 0;
}
```

The base classes Empty and EmptyToo of NonEmpty can end up at the same address as the base class Empty of the NonEmpty. In other words, two subobjects of the same type would end up at the same offset. This is not permitted by object layout rules of C++. 

## Members as Base Class

## Curiously Recurring Template Pattern(CRTP)

CRTP refers to a general class of techniques that consists of passing a derived class as a template argument to one of its own base classes.  It looks like the following.

```cpp
template <typename Derived>
class CuriousBase {
	...
};

class Curious : public CuriousBase<Curious> {
	...
};
```

If the derived class needs to be templatized

```cpp
template <typename Derived>
class CuriousBase {
	...
};

template <typename T>
class CuriousTemplate : public CuriousBase<CuriousCurious<T>> {
	...
};
```

By passing the derived class down to its base class via a template parameter, the base class can customize its own behavior to the derived class without requiring the use of virtual functions. This makes CRTP useful to factor out implementations that can only be member functions or are dependent on the derived class's identity.

Example

```cpp
#include <iostream>
#include <cstddef>

template <typename CountedType>
class ObjectCounter {
private:
    inline static std::size_t count = 0;

protected:
    ObjectCounter() { ++count; }
    ObjectCounter(ObjectCounter<CountedType> const&) { ++count; }
    ObjectCounter(ObjectCounter<CountedType> &&) { ++count; }
    ~ObjectCounter() { --count; }

public:
    static std::size_t live() { return count; }
};

template <typename CharT>
class MyString: public ObjectCounter<MyString<CharT>> {

};

int main()
{
    MyString<char> s1, s2;
    MyString<wchar_t> ws;
    std::cout << "num of MyString<char>: " << MyString<char>::live() << "\n";
    std::cout << "num of MyString<wchar_t>: " << ws.live() << "\n";
    return 0;
}
```


## Policy and Policy Classes

### Multiple Inheritance

Analyzing the reason why multiple inheritance fails to allow the creation of flexible designs provides interesting ideas for reaching a sound solution. The problems with assembling separate features by using multiple inheritance are as follows.

* Mechincs: There is no boileprate code to assemble the inherited components in a controlled manner. The language applies simple superposition in combining the base classes and establishes a set of simple rules for accessing their members. This is unacceptable except for simplest cases. Most of the time, you need to carefully orchestrate the workings of the inherited classes to obtain the desired behavior.

* Type information: The base classes don't have enough type information to carry out their tasks.

<!--* State manipulation: various -->

### Templates

### Drawbacks:-

* You cannot specialize structure. Only member functions can be specialized.

* Partial specialization of member functions doesn't scale. 

* Multiple default values are not allowed.

Multiple Inheritance and templates foster complementary tradeoffs. 

* Multiple inheritance has scarce mechanics; templates have rich mechanics.

* Multiple inheritance loses type information and templates retain type information.

* Specilization of templates does not scale, but multiple inheritance scales quite nicely.

* You can provide only one default for a template member function, but you can write an unbounded number of base classes.


### Policy

A policy defines a class interface or a class template interface. The interface consists of one or all of the following

* inner type definitions
* member functions
* member variables

Policies have much in common with traits but differ in that  they put less emphasis on type and more emphasis on behavior. 

### Example

A Creator policy is a class template of type T. It has a member function called `Creator`(takes no argument and returns a pointer to T).
Let's define some Creator policies. 

```cpp

template <typename T>
struct OpNewCreator
{
	static T* Create()
	{
		return new T;
	}
};


template <typename T>
struct MallocCreator
{
	static T* Create()
	{
		void* buf = std::malloc(sizeof(T));
		if (!buf) return nullptr;
		return ::new(buf) T;
	}
};

template <typename T>
struct PrototypeCreator
{
	PrototypeCreator(T* pObj=nullptr): pPrototype_(pObj) {}

	T* Create()
	{
		return pPrototype_ ? pPrototype_->Clone() : nullptr;
	}

	void SetPrototype(T* pObj)
	{
		pPrototype_ = pObj;
	}

	T* GetPrototype() { return pPrototype_; }

private:
	T* pPrototype_;
};

```

The implementations are called as policy classes.

We can design a class that exploits the Creator policy. Such a class will either contain or inherit one of the three classes defined previously.

```cpp
// Library Code
template <typename CreatorPolicy>
class WidgetManager : public CreatorPolicy {
	...
};
```

The class that use one or more policies are called hosts or host classes. 

Client code

```cpp
// Application code
typedef WidgetManager<OpNewCreator<Widget>> MyWidgetMgr;
```

### Implementing Policy Classes with Template Template Parameters

The user must pass OpNewCreator's template argument explicitly. The host class already knows, or can easily deduce, the template argument of the policy class. In the above example, WidgetManager always manages object of type Widget, so requiring the user to specify Widget again in the instantiation of OpNewCreator is redundant and potentially dangerous.

```cpp
// Library Code
template <template <class Created> class CreationPolicy>
class WidgetManager : public CreationPolicy<Widget>
{
	...
};
```

Created is a formal argument to the CreationPolicy and can simply be omitted.


Client code

```cpp
// Application code
typedef WidgetManager<OpNewCreator> MyWidgetMgr;
```

The host class has access to the template so that the host can instantiate it with a different type. Assume WidgetManager also needs to create objects of type Gadget using the same creation policy. 

```cpp
// Library Code
template <template <class Created> class CreationPolicy>
class WidgetManager : public CreationPolicy<Widget>
{
	...
	void doSomething()
	{
		Gadget* pW = CreationPolicy<Gadget>().Create();
	}
};
```

You can also provide a default policy to make use of frequently used policies.

```cpp
template <template <class Created> class CreationPolicy=OpNewOperator>
class WidgetManager : public CreationPolicy<Widget>
{
...
};
```

Advantages:-

* You can change policies from the outside as easily as changing a template argument when you instantiate WidgetManager. 

* You can provide your own policies that are specific to your concrete application. 


Disadvantages:-

* Policy is unsuitable for dynamic binding and binary interfaces, so in essence policies and classic interfaces don't compete.


### Implementing Policy Classes with Template Member Functions

We can redefine the Creator policy to prescribe a regular (nontemplate) class that exposes a template function Create<T>. 

```cpp
struct OpNewOperator
{
	template <class T>
	static T* Create()
	{
		return new T;
	}
};
```

This is error prone and hard to use.

### Enriched Policies

The WidgetManager inherits its policy class and because GetPrototype and SetPrototype are public members of PrototypeCreator, the two functions propagate through WidgetManager and are directly accessible to clients.

```cpp
typedef WidgetManager<PrototypeCreator> MyWidgetManager;
...
Widget* pPrototype = ...;
MyWidgetManager mgr;
mgr.SetPrototype(pPrototype);
... use mgr ...
```

Unlike regular interfaces, policies give ability to add functionality to a host class in a type safe manner.


## Barton-Nackman Trick

Suppose we have a class template Array for which we want to define the equality operator ==. One possibility is to declare the operator of the class template, but this is not good practice because the 


