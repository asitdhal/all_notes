The generic code that forwards the basic property of passed arguments:

* Modifyable objects should be forwarded so that they still can be modified.
* Constant objects should be forwarded as read-only objects.
* Movable objects(objects we can "steal" from because they are about to expire) should be forwarded as movable objects.

```cpp
#include <utility>
#include <iostream>
#include <vector>

class X
{

};

void g(X&)
{
    std::cout << __FUNCSIG__ << "for variables" << '\n';
}

void g(X const&)
{
    std::cout << __FUNCSIG__ << "for constants" << '\n';
}

void g(X&&)
{
    std::cout << __FUNCSIG__ << "for movable objects" << '\n';
}

void f(X& val)
{
    g(val); // val is non-const lvalue => calls g(X&)
}

void f(X const& val)
{
    g(val); // val is const lvalue => calls g(X const&)
}

void f(X&& val)
{
    g(std::move(val)); // val is non-const lavlue => needs std::move to call g(X&&)
}

int main(int argc, const char* argv[])
{
    X v; // create a variable
    X const c; // create a constant

    f(v); 
    f(c);
    f(X());
    f(std::move(v));
}

```

output

```bash
void __cdecl g(class X &)for variables
void __cdecl g(const class X &)for constants
void __cdecl g(class X &&)for movable objects
void __cdecl g(class X &&)for movable objects
```

If we want to combine all three cases in generic code

```cpp
template <typename T>
void f(T val)
{
	g(T);
}
```

The above code will work for first 2 cases, but not for the third case.

That's why C++11 has introduced perfect forwarding parameters.

```cpp
template <typename T>
void f(T&& val)
{
	g(std::forward<T>(val)); // perfect forward val to g()
}
```

After perfect forwarding, the program looks like this.

```cpp
#include <utility>
#include <iostream>
#include <vector>

class X
{

};

void g(X&)
{
    std::cout << __FUNCSIG__ << "for variables" << '\n';
}

void g(X const&)
{
    std::cout << __FUNCSIG__ << "for constants" << '\n';
}

void g(X&&)
{
    std::cout << __FUNCSIG__ << "for movable objects" << '\n';
}

template <typename T>
void f(T& val)
{
    g(std::forward<T>(val)); // calls the right g() for any passed argument val
}

int main(int argc, const char* argv[])
{
    X v; // create a variable
    X const c; // create a constant

    f(v); 
    f(c);
    f(X());
    f(std::move(v));
}

```


### Difference between std::move and std::forward

* std::move() has no template parameter and "triggers" move semantics for the passed argument.
* std::forward<>() forwards potential move semantics depending on a passed template argument.

### Difference between universal reference(T&&) and rvalue reference(X&&)

* r-value reference can only be bound to a movable object(a prvalue, such as temporary object, and an xvalue, such as an object passed with std::move()). It is always mutable and you can always "steal" its value.
* universal reference can be bound to a mutable, immutable(i.e. const), or movable object. Inside the function definition, the parameter may be mutable, immutable, or refer to a value you can "steal" the information from.

### Special Member function templates

```cpp
#include <utility>
#include <iostream>
#include <string>

class Person
{
private:
    std::string m_name;

public:
	// caller can still use string object after the call is made
    explicit Person(std::string const& n) : m_name(n)
    {
        std::cout << __FUNCSIG__ << '\n';
        std::cout << "copying string-CONSTR for name=" << m_name << '\n';
    }

	//the constructor steals the value from
    explicit Person(std::string&& n) : m_name(std::move(n))
    {
        std::cout << __FUNCSIG__ << '\n';
        std::cout << "moving string-CONSTR for name=" << m_name << '\n';
    }

    Person(Person const& p) : m_name(p.m_name)
    {
        std::cout << __FUNCSIG__ << '\n';
        std::cout << "COPY-CONSTR Person for name=" << m_name << '\n';
    }

    Person(Person&& p) : m_name(std::move(p.m_name))
    {
        std::cout << __FUNCSIG__ << '\n';
        std::cout << "MOVE-CONSTR Person for name=" << m_name << '\n';
    }
};

int main(int argc, const char* argv[])
{
    std::string s = "asit";
    Person p1(s); // init with string object => calls copying string-CONSTR
    Person p2("tmp"); // init with string object => calls moving string-CONSTR
    Person p3(p1); // copy Person => class COPY-CONSTR
    Person p4(std::move(p1)); // move Person => class MOVE-CONSTR
}
```

output

```bash
__cdecl Person::Person(const class std::basic_string<char,struct std::char_traits<char>,class std::allocator<char> > &)
copying string-CONSTR for name=asit
__cdecl Person::Person(class std::basic_string<char,struct std::char_traits<char>,class std::allocator<char> > &&)
moving string-CONSTR for name=tmp
__cdecl Person::Person(const class Person &)
COPY-CONSTR Person for name=asit
__cdecl Person::Person(class Person &&)
MOVE-CONSTR Person for name=asit
```

Now, the same function with generic constructors

