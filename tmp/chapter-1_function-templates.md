```cpp
template <comma-separated-list-of-parameters>
```

The list of parameters is typename T.  T is the type parameter. The type parameter represents an arbitrary type that is determined by the caller when the caller calls the function.  You can also use class keyowrd(but definitely not struct keyword).
<,> tokens are used as brackets.
The keyword typename introduces a type parameter.

```cpp
template <typename T>
T max(T a, T b)
{
    return b < a ? a: b;
}

int main()
{
    std::string s1 = "mathematics";
    std::string s2 = "math";
    std::cout << ::max(s1, s2) << "\n";
    return 0;
}
```

Here max template is qualified with `::`. This is to ensure our max() template is found in the global namespace.

The process of replacing template parameters by concrete type is called *instantiation*.  Any type, including void can be used to instantiate.

### Two-Phase translation

The templates are "compiled" in two phases.
1. Without instantiation at definition time, the template code itself is checked for correctness ignoring the template parameters. This includes
  * Syntax errors are discovered, such as missing semicolons.
  * Using unknown names(type names, function names, ...) that don't depend on template parameters are discovered.
  * Static assertions that don't depend on template parameters are checked.
2. At instantiation time, the template code is checked(again) to ensure that all code is valid. Now, all parts that depend on template parameters are double-checked.



The fact that names are checked twice is called two-phase lookup.

Two-phase translation leads to an important problem in the handling of templates in practices. When a function template is used in a way that triggers its instantiation, a compiler will (at some point) need to see that template's definition. This breaks the usual compile and link distinction for ordinary functions, when the declaration of a function is sufficient to compile its use. That's why templates are always inside a header file.


### Type conversions During Type Deduction

* When declaring call parameters by reference, even trivial conversions do not apply to type deduction. Two arguments declared with same template parameter T must match exactly.
* When declaring call parameters by value, only trivial conversions that decay are supported.
* Qualifications with const or volatile are ignored
* References convert to the referenced type
* Raw arrays or functions convert to the corresponding pointer type. 
* For two arguments declared with the same template parameter T, the decayed type must match.

```cpp
template <typename T>
T max (T a, T b)
{
    std::cout << __PRETTY_FUNCTION__ << '\n';
    return b < a ? a : b;
}

int main()
{
    int i = 10;
    int const c = 42;
    max(i, c); // T max(T, T) [with T = int] 
    max(c, c); // T max(T, T) [with T = int] 
    
    int& ir = i;
    max(i, ir); // T max(T, T) [with T = int] 
    
    int arr[4];
    max(&i, arr); // T max(T, T) [with T = int] 

    return 0;
}
```

Automatic type conversions are limited during type deduction.

```cxx
template <typename T>
T max (T a, T b)
{
    std::cout << __PRETTY_FUNCTION__ << '\n';
    return b < a ? a : b;
}

int main()
{
    max(4, 7.2); // error 
    std::string s;
    max("hello", s); // error
    return 0; 
}
```
Since automatic type conversions are not allowed, in the first case there is a conflict between int and float. In the 2nd case, there is a conflict between `const char[6]` and `std::string`.

How to handle the error

1. cast arguments so that they both match

```cpp
max(static_cast<double>(4), 7.2); 
```
2. Specify(qualify) explicitly the type of T to prevent the compiler from attempting type deduction.

```cpp
max<double>(4, 7.2); 
```

3. Specify different types of parameters.

### Type Deduction for Default Arguments

Type deduction does not work for default call arguments.

```cpp
template <typename T>
void f(T = "");
...
...

f(1); // OK: deduced T to be int, so that it calls f<int>(1)
f();  // Error: cannot deduce T
```

Here you have to declare a default argument for the template parameter.

```cpp
template <typename T = std::string>
void f(T= "");

f(); // OK
```

### Multiple Template Parameters

Function templates have two distinct sets of parameters.
1. Template Parameters: declared in angle brackets before the function template name
```cpp
template <typename T> // T is a template parameter
```
2. Call Parameters: declared in parentheses after the function template name
```cpp
T max(T a, T b); // and b are call parameters
```

```cpp
template <typename T1, typename T2> // T1, T2 are template parameter
T1 max (T1 a, T2 b)     // a and b are call parameters
{
    return b < a ? a : b;
}
```

Here, if you use one of the parameter types as return type, the argument for the other parameter might get converted to this type, regardless of the caller's intention. Thus the return type depends on the call argument order. There are three way to deal with this problem.

* Introduce a third template parameter for the return type.
* Let the compiler find out the return type.
* Declare the return type to be the "common type" of the two parameter types.


### Template Parameters for Return Types

```cpp
template <typename T>
T max(T a, T b)
{
    return a > b ? a:b;
}
```

Template argument deduction allows us to call function templates with syntax identical to that of a calling an ordinary function.

```cpp
::max(10, 20); //ok
```

When there is no connection between template and call parameters and when template parameters cannot be determined, you must specify the template argument explicitly with the call. 

```cpp
::max<double>(10, 20.3); //ok
```

Here, you can introduce a third template argument type to define the return type of a function template. To deduce the third parameter, you have to specify the template argument list explicitly. 

```cpp
template <typename T1, typename T2, typename RT>
RT max(T1 a, T2 b);

::max<int, double, double>(4, 7.2); //ok, but tedious
```

Another approach is to specify only the  first arguments explicitly and to allow the deduction process to deliver the rest. In general, you must specify all the argument types up to the last argument type that cannot be determined implicitly. Thus, if you change the order of the template parameters in our example, the caller needs to specify only the return type.

```cpp
template <typename RT, typename T1, typename T2>
RT max(T1 a, T2 b);

::max<double>(10, 20.1); //ok, return type is double, T1 and T2 are deduced.
```

### Deducing the Return Type

If a return type depends on template parameters, the simplest and the best approach to deduce the return type is to let the compiler find out.  The use of auto for the return type without a corresponding trailing return type(at least C++14) indicates that the actual return type must be deduced from the return statement in the function body.  Deducing the return type from the function body has to be possible. Therefore, the code must be available and multiple return statements have to match.


```cpp
template <typename T1, typename T2>
auto max(T1 a, T2 b)
{
    return a > b  ? a : b;
}
```

Before C++14, it is only possible to let the compiler determine the return type by more or less making the implementation of the function part of its declaration. In C++11, trailing return type syntax allows us to use the call parameters.

```cpp
template <typename T1, typename T2>
auto max(T1 a, T2 b) -> decltype(b<a?a:b)
{
    return b < a ? a : b;
}
```

Here, the function prototype is a declaration, the implementation does not necessarily have to match. In fact, using true as the condition for operator?: is enough.

```cpp
template <typename T1, typename T2>
auto max(T1 a, T2 b) -> decltype(true?a:b);
```

It might happen that the return type is a reference type, because under some condition T might be a reference.  And you should return the type decayed from T.

```cpp
#include <type_traits>

template <typename T1, typename T2>
auto max(T1 a, T2 b) -> typename std::decay_t<decltype(true?a:b)
{
    return b < a ? a:b;
}

```

An initialization of type auto always decays. This also applies to return values when the return type is just auto.

### Return Type as Common Type

`std::common_type_t<T1, T2>` yields the common type of two (or more) different types passed as template argument. Internally, it chooses the returning type according to the language rules of operator `?:` or specialization for specific types.

```cpp
template <typename T1, typename T2>
std::common_type_t<T1, T2> max(T1 a, T2 b)
{
    return b < a ? a:b;
}
```

### Default Template Arguments

If you want to combine the approaches to define the return type with the ability to have multiple parameter types, you can introduce a template parameter RT for the return type with the common type of the two arguments as default.

1. use operator?:

```cpp
template <typename T1, typename T2,
        typename RT = std::decay_t<decltype(true? T1(): T2()>> // default construction required
RT max(T1 a, T2 b)
{
    return b < a ? a:b;
}
```

2. use std::common_type_t


```cpp
template <typename T1, typename T2,
        typename RT = std::common_type_t<T1, T2>>
RT max(T1 a, T2 b)
{
    return b < a ? a:b;
}

auto a = ::max(7.8, 2);
auto b = ::max<double, int, double>(7.8, 2);
```

`std::comman_type_t` decays, so return value can't become a reference.

We would need the ability to have the return type as the first template parameter, while still being able to deduce it from the argument types. In principle, it is possible to have default arguments for leading function template parameters even if parameters without default arguments follow.

```cpp
template <typename RT = long, typename T1, typename T2>
RT max(T1 a, T2 b)
{
    return b < a ? a: b;
}

int i=10;
long l = 20;
::max(i,l); // returns long(default argument of template parameter for return type)
::max<int>(4, 32);// returns int as explicitly requested.
```

### Overloading Function Templates

```cpp
#include <iostream>
#include <type_traits>
int max(int a, int b)
{
       std::cout << "max(int, int)\n";
       return b < a ? a : b;
}
template <typename T>
T max(T a, T b)
{
       std::cout << "max(T, T)\n";
       return a > b ? a : b;
}
int main()
{
    std::cout << ::max(7, 42) << "\n"; // both int values match non-template function properly
    std::cout << ::max(7.2, 4.2) << "\n"; // calls the max<double> by argument deduction
    std::cout << ::max('a', 'b') << "\n"; // calls the max<char> by argument deduction
    std::cout << ::max<>(7, 42) << "\n"; // calls the max<int> by argument deduction
    std::cout << ::max<double>(7, 42) << "\n";
    std::cout << ::max('a', 42.7) << "\n"; // non template function is called
    return 0;
}
```

It is also possible to specify explicitly an empty template argument list.  This syntax  indicates  that only templates may resolve a call, but all the template parameters should be deduced  from call arguments.

Automatic type conversion is not considered for deduced template parameters, but only considered  for ordinary function parameters.

```cpp
#include <iostream>
#include <type_traits>
template <typename T1, typename T2>
auto max(T1 a, T2 b)
{
       std::cout << "auto max(T1 a, T2 b)\n";
       return a > b ? a : b;
}
template <typename RT, typename T1, typename T2>
RT max(T1 a, T2 b)
{
       std::cout << "RT max(T1 a, T2 b)\n";
       return a > b ? a : b;
}
int main()
{
    std::cout << ::max(7, 4.2) << "\n"; // uses first template
       std::cout << ::max<long double>(7.2, 4) << "\n"; // uses 2nd template
       // std::cout << ::max<int>(4, 7.2) << "\n"; // error, both matches
       return 0;
}
```

When overloading function templates, you should ensure that only one of them matches for any call.

```cpp
template <typename T1, typename T2>
auto max(T1 a, T2 b)
{
    std::cout << __PRETTY_FUNCTION__ << '\n';
    return b < a ? a : b;
}

template <typename RT, typename T1, typename T2>
RT max(T1 a, T2 b)
{
    std::cout << __PRETTY_FUNCTION__ << '\n';
    return b < a ? a : b;
}

int main()
{
    ::max(4, 7.2); // auto max(T1, T2) [with T1 = int; T2 = double] 
    ::max<long double>(7.2, 4); // RT max(T1, T2) [with RT = long double; T1 = double; T2 = int] 
    
    ::max<int>(4, 7.2); // error, both function template match
    return 0; 
}
```

### Pass by Value or by Reference

In general, passing by reference is recommended for types other than cheap simple types(fundamental types or std::string_view), because no necessary copies are created.

But, passing by value are generally chosen

1. simple syntax
2. compiler optimises better
3. move semantics often make copies cheap
4. And sometimes there is no copy or move at all


In addition, for templates, specific aspects come into play:

1. A template might be used for simple and complex types, so choosing the approach for complex types might be counter productive for simple types.
2. As a caller you can often still decide to pass arguments by reference, using std::ref() and std::cref().
3. Although passing string literals or raw arrays can become a problem, passing them by reference often considered as a bigger problem. 
