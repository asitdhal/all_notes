#!/bin/bash

git add -u
git status
git commit -m "generated files on `date +'%Y-%m-%d %H:%M:%S'`"
git remote | xargs -L1 -I R git push R master
