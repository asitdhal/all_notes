> Refactoring is a "behavior-preserving transformation".

> Refactoring is a change made to the internal structure of software to make it easier to understand and chaper to modify without changing its observable behavior.


The process of refactoring involves

* removal of duplication
* the simplication of complex-logic
* clarification of unclear code

When you refactor, you relentlessly poke and prod your code to improve its design. 

To refactor safely, you must either manually test that your changes didn't break anything or run automated tests. 

Refactoring in small steps helps prevent introduction of defects. Large refactorings are implemented in small steps.

It's best to refactor continuously, rather than in phases. If your boss needs you to finish a feature before a demo that just got scheduled for tomorrow, finish the feature and refactor later.

### Motivations for refactoring

* *Make it easier to add new code*
	* If you quickly program the feature without regard to how well it fits with an existing design, then you incur design debth. You need to refactor later.
	* If you can modify the existing design so it can easily and gracefully accomodate the new feature, you need to analyze what will need to change to best accomodate the new feature and make whatever changes are necessary.

* *Improve the design of existing code*
	* Continuous refactoring involves constantly sniffing for coding smells and removing smells immediately after finding them. 

* *Gain a better understanding of code*

* *Make coding less annoying*
	* like to break apart the enoromous class into smaller classes.

### Many eyes

To get the best refactoring results, you'll want the help of many eyes. This is one reason why extreme programming suggests the practices of pair-programming and collective code ownership.

### Human-readable code

Any fool can write code that a computer can understand. Good programmers write code that humans can understand.

### Clean code

To keep code clean

* you must continuously remove duplication and simplify and clarify code. 
* you must not tolerate messes in code, and we must not backslide into bad habits.

### Small Steps

If you take steps that are too large and the struggle for minutes, hours, or even days to get back to a green bar, refactoring will be slow. Each big step generated failures in the unit tests, which took a good deal of time to fix, not to mention that some of the fixes needed to be undonr during later steps.

If the bar stays red for too long, then you should backtrack and begin again. 

### Design Debt

In financial terms, if you don't pay off a debt, you incur late fees. If you don't pay your late fees, you incur higher late fees.The more you don't pay, the worse your fees and payments become. After sometime, getting out of debt becomes an impossible dream. This term for software engineering is called design debt.

Design debt occurs when you don't consistently do three things

* Remove duplication
* Simplify your code
* Clarify your code's intent

### Composite and Test-Driven Refactorings

Composite Refactorings: higher level refactoring composed of low-level refactoring.

Low-level refactoring :- moving code around.

Test-Driven Refactorings :- applying test driven development to produce replacement code and then swap out old code for new code. 

### Benefits of Composite Refactorings

* They describe an overall plan for a refactoring sequence: The mechanics of a composite refactoring describe the sequence of low-level refactorings you can apply to improve a design in a particular way.

* They suggest nonobvious design directions: Composite Refactorings begin at a source and take you to a destination. The destination may or may not be obvious, given your source. Much depends on your familiarity with patterns, each of which defines a destination as well as the forces that suggest the need to go towards or to that destination.

* They provide insights into implementation patterns: Because there is no right way to implement a pattern, it's useful to consider alternative pattern implementations.