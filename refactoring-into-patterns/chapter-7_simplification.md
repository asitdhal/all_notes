## Compose Method

Intent: You can't rapidly understand a method's logic.

Solution: Transform the logic into a small number of intention-revealing steps at the same level of detail.

```cpp
void add(Object element)
{
	if (!readOnly)
	{
		int newSize = size + 1;
		if (newSize > elements.len()) {
			Object[] newElements = new Object[elements.len() + 10];
			for(int i = 0; i < size; i++) {
				newElements[i] = elements[i];
			}
			elements = newElements;
		}
		elements[size++] = element;
	}
}
```

After Compose Method

```cpp
void add(Object element)
{
	if (readOnly) {
		return;
	}
	
	if (atCapacity()) {
		grow();
	}

	addElement(element);
}
```

A *composed method* is a small, simple method that you can understand in seconds. When your code has many Composed Methods, it tends to be easy to use, read, and extend. It's name communicates *what* it does, while its body comminicates *how* it does. 

* Most refactorings to *Composed Method* involve applying *Extract Method* several times until the Composed Method does most(if not all) of its work via calls to other methods. 

* The most difficult part is deciding what code to include or not include in an extracted method. If you extract too much code into a method, you'll have a hard time naming the method to adequately describe what it does. In that case, just apply *Inline Method* to get the code back into the original method, and then explore other ways to break it up.

* If you apply this refactoring on numerous methods within the same class, you may find that the class has an overabundance of small, private methods. In that case, you may see an opportunity to apply *Extract Class*.


### Guidelines for Compose Methods

* Think small: Composed Methods are rarely more than ten lines of code and are usually about five lines.
* Remove duplication and dead code: Reduce the amount of code in the method by getting rid of blatant and/or subtle code duplication or code that isn't being used.
* Communicate Intention: Name your variables, methods, and parameters clearly so they communicate their purposes.
* Simplify: Transform your code so it's as simple as possible. 
* Use the same level of detail: When you break up one method into chunks of behavior, make the chunks operate at similar levels of detail. e.g. if you have a piece of detailed conditional logic mixed in with some high level method calls, you have code at different levels of detail. Push the detail into a well-named method, at the same level of detail as the other methods in the Composed method.


## Replace Conditional Logic with Strategy

Intent: Conditional logic in a method controls which of several variants of a calculation are executed.

Solution: Create a Strategy for each variant and make the method delegate the calculation to a Stragery instance.


![Before Strategy Pattern](Conditional_Logic_Before_Strategy.jpg)

Here capital() has a lot of conditional logic.

```cpp
double capital()
{
	if (expiry == nullptr && maturiry != nullptr) {
		return commitment * duration() * riskFactor();
	}
	if (expiry != nullptr && maturiry == nullptr) {
		if (getUnusedPercentage() != 1.0 ) {
			return commitment * getUnusedPercentage() * duration() * riskFactor();
		} else {
			return (outstandingRiskAmount() * duration() * riskFactor()) +
				(unusedRiskAmount() * duration() * unusedRiskFactor());
		}
	}
	return 0.0;
}
```

![After Strategy Pattern](Conditional_Logic_After_Strategy.jpg)

After refactoring to strategy pattern,

```cpp
double capital()
{
	return capitalStrategy().capital(this);
}
```

Replace conditional logic with strategy involves object composition: you produce a family of classes for each variation of the algorithm and outfit the host class with one Strategy instance to which the host delegates at runtime. 

An inheritance-based solution can be achieved by applying Replace Conditional with Polymorphism. The prerequisite for that refactoring is an inheritance structure. If subclasses exist and each variation of the algorithm maps easily to specific subclasses, this is probably your preferred refactoring. If the conditionals in the algorithim are controlled by one type code, it may be easy to create subclasses of the algorithm's host class, one for each type code. If there is no such type code, you'll likely be better off refactoring to Strategy. 

![Strategy Pattern](Stategy_Pattern.jpg)

If clients need to swap one calculation type for another at runtime, you'd better avoid an inheritance-based approach because this means changing the type of object a client is working with rather than simply substituting one Strategy instance for another.

When implementing a Strategy-based design, you'll want to consider how your context class will obtain its Strategy. If you don't have many combinations of Strategies and context classes, it's good preactice to shield client code from having to worry about both instantiating a Strategy instance and outfitting a context with a Strategy instance.

Since the most strategies require data in order to do their calculations, you'll need to determine how to make that data available to the strategy.

1. Pass the context as a parameter to the strategy's constructor or calculation method. This may involve making context methods public so the strategy can obtain information it needs. 

2. Pass the necessary data from the context to the strategy via calculation method parameters. It involves the least coupling between the context and the strategy, but the data will be passed to every concrete strategy regardless of whether particular strategies need that data.

Strategy pattern makes use of IS-A relationship. So you can change the behavior at run time too.


## Move Embellishment to Decorator

Intent: Code provides an embellishment to a class's core responsibility.

Solution: Move the embellishment code to a Decorator.

When you add new code to an old class, the new code may embellish the core responsibility or primary behavior of an existing class. The Decorator Pattern offers a good remedy: place each embellishment in its own class and let that class wrap the type of object it needs to embellish so that clients may wrap the embellishment around objects at runtime, when special case behavior is needed.

Decorators are "transparent enclosure": it must implement all of the public method of the classes it decorates. Transparent enclosures wrap objects in a way that is transparent to the objects being wrapped(i.e., a decoratee doesn't know it has been decorated).

Since a Decorator and its decoratee share the same interface, Decorators are transparent to client code that uses them. Such transparency works well for client code, unless that code is deliberately checking the dentity of an object. 

## Replace State-Altering Conditionals with State

Intent: The conditional expressions that control an object's state transitions are complex.

Solution: Replace the conditionals with State Classes that handle specific states and transitions between them.


## Replace Conditional Dispatcher with Command

Intent: Conditional logic is used to dispatch requestes and execute actions.

Solution: Create a Command for each action. Store the Commands in a collection and replace the conditional logic with code to fetch and execute Commands.

A *conditional dispatcher* is a conditional statement(such as a switch) that performs request routing and handling. Some dispatchers are small and route a small number of requests to small chunks of handler logic. On the other hand, if number of dispatchers are large, you can use command pattern. The two most common reasons to refactor from a conditional dispatcher to a Command-based solutions are the following.

1. Not enough runtime flexibility: Conditional dispatcher doesn't allow for dynamic configurations because all of its routing and handling logic is hard coded into a single conditional statement.

2. A bloated body of code: Some conditional dispatchers become enormous and unwidely as they evolve to handle new requests or as their handler logic becomes ever more complex with new responsibilities. Extracting the handler logic into different methods doesn't help enough because the class that contains the dispatcher and extracted handler methods is still too large to work with.

The command pattern is implemented

* place each piece of request-handling logic in a separate "command" class that has a common method, like `execute()` or `run()` for executing its encapsulated handler logic.

* once you have a family of such commands, you can use a collection to store and retrieve instances of them; add, remove, or change instances; and execute instances by invoking their execution methods.

* specify, queue, and execute requests at different times. 

* support undo.

* support logging changes so that they can be reapplied in case of a system crash. 

* structure a system around high-level operations built on primitive operations.


Example :-

We have a class called CatalogApp which dispatches request. The conditional dispatcher code looks like this.

```cpp
if (actionName.equals(NEW_WORKSHOP)) {
	// lots of code to create a new workshop
} else if (actionName.equals(ALL_WORKSHOPS)) {
	// lots of code to display information about all workshops
} //... many more "else if" statements
```

After refactoring

![Command Pattern](Command_Pattern.jpg)

```cpp
handler = lookupHandlerBy(actionName);
return handler.execute(...);
```

> When you have an interface with a signle method that returns nothing, there is a good chance that it is a command pattern.

### Participants

* Command
	- declares an interface for executing an operation

* ConcreteCommand
	- defines a binding between a Receiver object and an action
	- implements execute() by invoking the corresponding operation(s) on Receiver.

* Client(Application)
	- creates a ConcreteCommand object and sets its receiver

* Invoker(MenuItem)
	- asks the command to carry out the request

* Receiver(Document, Application)
	- knows how to perform the operations associated with carrying out a request. Any class may serve as a Receiver.

### Collaborations

- The client creates a ConcreteCommand object and specifies its receiver.
- An Invoker object stores the ConcreteCommand object.
- The invoker issues a request by calling execute() on the command. When commands are undoable, ConcreteCommand stores state for undoing the command prior to invoking execute().
- The ConcreteCommand object invokes operations on its receiver to carry out the request.

### Consequences

- Command decouples the object that invokes the operation from the one that knows how to perform it.
- Commands are first-class objects. They can be manipulated and extended like any other object.
- You can assemble commands into a composite command(e.g. MacroCommand). Composite commands are an instance of the Composite pattern.



## Observer Pattern

