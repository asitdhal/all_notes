> Each pattern is a three-part rule, which expresses a relation between a certain context, a problem, and a solution.

### Pattern Happy

The overuse of patterns tends to result from being patterns happy. We are patterns happy when we become so enamoured of patterns that we simply must use them in our code. 

### There are many ways to implement a Pattern

Every pattern describes a problem which occurs over and over again in our environment, and then describes the core of the solution to that problem, in such a way that you can use this solution a million times over, without ever doing it same way twice.

