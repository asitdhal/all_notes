### Over-engineering

When you make your code more flexible or sophisticated than it needs to be, you over-engineer it. Some people do it, because they think it's best to make a design more flexible or sophisticated today, so it can accomodate the needs of tomorrow. That sounds reasonable - if you happen to be psychic.

The problem here, if the prediction is wrong, then

* you waste precious time and money
* the extra code is never removed, because you think one day the code might be needed.
* accumulated over sophisticated code makes a maintenance nightmare(team needs to know the code, new developers need to understand it)
* everyone works in his own comforatable area of the system, rarely seeking elsewhere for code that already does what he needs(lot of duplicated code).


### Patterns of panacea

Power of patterns may lead you to lose sight of simpler ways to write code.

### Under-engineering

We under-engineer when we produce poorly designed software. 

* don't have time, don't make time, aren't given time to refactor.
* no knowledge about good software design.
* required to quickly add new features to existing systems.
* made to work on too many projects at once.

Continuius under-engineering leads to the "fast, slow, slower" rhythm of software development.

### TDD and Continuous Refactoring

TDD and Continuous Refactoring enable the efficient evolution of working code by turning programming into a dialogue.

* Ask: You ask a question of a system by writing test.
* Respond: You respond to the question by writing code to pass the test.
* Refine: You refine your response by consolidating ideas, weeding out inessentials, and clarifying ambiguities.
* Repeat: You keep the dialog going by asking the next question.

In TDD, instead of spending lots of time thinking about a design that would work for every nuance of a system, you now spend seconds or minutes making a primitive piece of behavior work correctly before refactoring and evolving it to the next necessary level of sophistication.

Another TDD and Continuous Refactoring is "red, green, refactoring"

1. Red: You create a test that expresses what you expect your code to do. The test fails(turns red), because you haven't created code to make the test pass.
2. Green: You program whatever if expedient to make the test pass(turn green). 
3. Refactor: You improve the design of the code that passed the test.

### Refactoring and Patterns

Refactoring is done to or towards patterns, being careful not to produce overly flexible or unnecessary sophisticated solutions. The motivation for applying pattern-directed refactorings are identical to the general motivation for implementing low-level refactoring:

* to reduce or remove duplication
* to simplify what is complicated
* to make code better at communicating its intention

All these informations can be found in the motivation(intent) of the dp.