## Chain Constructors

Intent: You have multiple constructors that contain duplicate code.

Solution: Chain the constructors together to obtain the least amount of duplicate code.

Before refactoring

```cpp
public class Loan {

	public Loan(float notional, float outstanding, int rating, Date expiry)
	{
		this.trategy = new TermROC();
		this.notional = notional;
		this.outstanding = outstanding;
		this.rating = rating;
		this.expiry = expiry;
	}

	public Loan(float notional, float outstanding, int rating, Date expiry, Date maturity)
	{
		this.trategy = new RevolvingTermROC();
		this.notional = notional;
		this.outstanding = outstanding;
		this.rating = rating;
		this.expiry = expiry;
		this.maturity = maturity;
	}	

	public Loan(CapitalStrategy strategy, float notional, float outstanding, int rating, Date expiry, Date maturity)
	{
		this.trategy = strategy;
		this.notional = notional;
		this.outstanding = outstanding;
		this.rating = rating;
		this.expiry = expiry;
		this.maturity = maturity;
	}
};
```

After chain of constructors

```cpp
public class Loan {

	public Loan(float notional, float outstanding, int rating, Date expiry)
	{
		this(new TermROC(), notional, outstanding, rating, expiry, nullptr);
	}

	public Loan(float notional, float outstanding, int rating, Date expiry, Date maturity)
	{
		this(new RevolvingTermROC(), notional, outstanding, rating, expiry, maturity);
	}	

	public Loan(CapitalStrategy strategy, float notional, float outstanding, int rating, Date expiry, Date maturity)
	{
		this.trategy = strategy;
		this.notional = notional;
		this.outstanding = outstanding;
		this.rating = rating;
		this.expiry = expiry;
		this.maturity = maturity;
	}
};
```

Constructor Chaining: specific constructors call more general-purpose constructors until a final constructor is reached. If you have one constructor at the end of every chain, it is a catch-all constructor, because it handles every constructor call. 
