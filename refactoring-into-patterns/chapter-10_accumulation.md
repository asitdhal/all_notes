## Move Accumulation to Collecting Parameter

Intent: You have a single bulky method that accumulates information to a local variable.

Solution: Accumulate results to a Collecting Parameter that gets passed to extracted methods.

![Move_Accumulation_to_Collecting_Parameter](Move_Accumulation_to_Collecting_Parameter.jpg)

The toString is very bulky

```java
String result = new String();
result += "<" + tagName + " " + attributes + ">";
Iterator it = children.iterator();
while (it.hasNext()) {
	TagNode node = (TagNode)it.next();
	result += node.toString();
}

if (!tagValue.equals(""))
	result += tagValue;
result += "</" + tagName + ">";
return result;
```

After refactoring

![Move_Accumulation_to_Collecting_Parameter](Move_Accumulation_to_Collecting_Parameter2.jpg)

where appendContentsTo is 

```java
writeOpenTagTo(result);
writeChildrenTo(result);
writeValueTo(result);
writeEndTagTo(result);
```

Collecting Parameter

```java
StringBuffer result = new StringBuffer("");
appendContentsTo(result);
return result.toString();
```

A Collecting Parameter is an object that you pass to methods in order to collect information from those methods. Insted of having each of the methods return a result, which you combine into a final result, you incrementally accumulate a result by passing a Collecting Parameter to each of the methods. The methods write their information to the Collecting Parameter, which accumulates all of the results.


## Move Accumulation to Visitor

Intent: A method accumulates information from heterogeneous classes.

Solution: Move the accumulation task to a Visitor that can visit each class to accumulate the information.


![Move_Accumulation_to_Visitor](Move_Accumulation_to_Visitor.jpg)

You have a class called TextExtractor which can extract information from a list of related class which implement a common interface(and implements iterator pattern). The extractText() method is bulky.

```java
StringBuffer results = new StringBuffer();
NodeIterator nodes = parser.elements();
while (nodes.hasMoreNodes()) {
	Node node = nodes.nextNode();
	if (node instanceof StringNode) {
		// add contents to results
	} else if (node instanceof LiinkTag) {
		// add contents to results
	} else if (node instanceof Tag) {
		// add contents to results
	}
}
return results.toString();
```

