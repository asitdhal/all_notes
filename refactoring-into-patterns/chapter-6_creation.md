A class has too many constructors, clients will have difficult time knowing which constructor to call. You can refactor by applying *Replace Constructors with Creation Methods*.

Creation Method:- A static or non-static method method that creates and returns an object instance.

Factory Method:- useful for polymorphic creation.

Unlike a creation method, a factory method may not be static and must be implemented by at least two classes, typically a superclass and a subclass.  All creation methods are factory methods, but reverse is not true always. 

A class that is a factory is one that implements one or more Creation Methods. 


![Creation Method](Creation_Method.png)

### Replace Constructors with Creation Methods

Problem: Constructors on a class make it hard to decide which constructors to call during development.

Solution: Replace constructors with intention-revealing Creation Methods that return Object instance.

#### Problems with constructors

* Constructors don't communicate efficiently or effectively, the more constructors you have, the easier it is for programmers to choose the wrong one.

* If you need a new constructor to a class with same signature as an existing constructor, you are out of luck.

* Some constructors are not used(dead constructor), but still live in the code base. Because no one knows if it used anywhere or not.

### Advantage of creation method

* no naming constraint, so easy to clarify the intent by giving a proper name.

* easy to find dead methods because of the unique names.

```cpp
class Loan
{
public:
	Loan(double commitment, int riskRating, Date maturity) :
		Loan(commitment, 0.0, riskRating, maturity, nullptr)
	{
	}

	Loan(double commitment, int riskRating, Date maturity, Date expiry) :
		Loan(commitment, 0.0, riskRating, maturity, expiry)
	{
	}

	Loan(double commitment, double outstanding, int riskRating, Date maturity, Date expiry) :
		Loan(nullptr, commitment, outstanding, riskRating, maturity, expiry)
	{
	}

	Loan(CapitalStrategy *capitalStrategy, double commitment, double outstanding, int riskRating, Date maturity, Date expiry) :
		Loan(capitalStrategy, commitment, outstanding, riskRating, maturity, expiry)
	{
	}

	Loan(CapitalStrategy *capitalStrategy, double commitment, double outstanding, int riskRating, Date maturity, Date expiry) :
		m_commitment = commitment,
		m_outstanding = outstanding,
		m_riskRating = riskRating,
		m_maturity = maturity,
		m_expiry = expiry,
		m_capitalStrategy = capitalStrategy
	{
		if ( !m_capitalStrategy ) {
			if (m_expiry == 0) {
				this.m_capitalStrategy = new CapitalStrategyTermLoan();
			} else if (m_maturity == 0) {
				this.m_capitalStrategy = new CapitalStrategyRevolver();
			} else {
				this.m_capitalStrategy = new CapitalStrategyRCTL();
			}
		}
	}

	...
	...
};

// client code

class CapitalCalculationTests
{
public:
	void testTermLoanPayments()
	{
		Loan termloan(commitment, riskRating, maturity);
	}
};

```

Steps to refactor

1. find all constructor usages in client code
2. do an extract method to produce a public, static method called createdLoan
3. Apply move methods
4. find all calles and replace the constructor
5. make sure nullptr is not passed to constructor(it reduces readability)

```cpp
class Loan
{
public:
	static Loan createTermLoan(double commitment, int riskRating, Date maturity)
	{
		return Loan(commitment, 0.0, riskRating, maturity, 0)
	}

	static Loan createTermLoanWithExpiry(double commitment, int riskRating, Date maturity, Date expiry) :
		Loan(commitment, 0.0, riskRating, maturity, expiry)
	{
	}

	
	static Loan createTermLoanWithAdjustedCapitalStrategy(CapitalStrategy *capitalStrategy, double commitment, double outstanding, int riskRating, Date maturity, Date expiry) :
		Loan(capitalStrategy, commitment, outstanding, riskRating, maturity, expiry)
	{
	}

private:
	Loan(CapitalStrategy *capitalStrategy, double commitment, double outstanding, int riskRating, Date maturity, Date expiry) :
		m_commitment = commitment,
		m_outstanding = outstanding,
		m_riskRating = riskRating,
		m_maturity = maturity,
		m_expiry = expiry,
		m_capitalStrategy = capitalStrategy
	{
		if ( !m_capitalStrategy ) {
			if (m_expiry == 0) {
				this.m_capitalStrategy = new CapitalStrategyTermLoan();
			} else if (m_maturity == 0) {
				this.m_capitalStrategy = new CapitalStrategyRevolver();
			} else {
				this.m_capitalStrategy = new CapitalStrategyRCTL();
			}
		}
	}

	Loan(double commitment, double outstanding, int riskRating, Date maturity, Date expiry) :
		Loan(nullptr, commitment, outstanding, riskRating, maturity, expiry)
	{
	}

	...
	...
};

```

#### Variation

Sometimes you need something on the order of 50 Creation Methods to account for every object configuration supported by your class. Writing 50 methods is not fun, neither recommended. 

You can write Creation Methods for the most popular configuration and leave some public constructors around to handle the rest of cases. It also make sense to consider using parameters to cut down on number of Creation Methods.


### Move Creation Knowledge to Factory

Problem: Data and code used to instantiate a class is sprawled across numerous classes.

Solution: Move the creation knowledge into a single Factory class.

Motivation:

* When the knowledge for creating an object is spread out across numerous classes, you have *creation sprawl*. Creation sprawl is the placement of creational responsibilities in classes that ought not to be playing any role in an object's creation. 

e.g. A client needed to configure an object based on some preference, yet lacked access to the object's creation code. The client passes it configuration preferences to one object, which hands them off to another object, which holds onto then until the creation code. It spreads creation code and data far and wide.

A factory uses one class to encapsulate both creation logic and client's instantiation/configuration preferences. A client can tell a Factory instance how to instantiate/configure an object, and then then the same Factory instance may be used at runtime to perform the instantiation/configuration. 

e.g.: a NodeFactory creates StringNode instances and may be configured by clients to embellish those instances with a DecodingStringNode Decorator.

![Node Factory](node_factory.png)

A factory need not be implemented exclusively by a concrete class. You can use an interface to define a Factory and make an existing class implement that interface.  This approach is useful when you want other areas of a system to communicate with an instance of the existing class exclusively through its Factory interface.

If the creation logic inside a Factory becomes too complex, perhaps due to supporting too many creation options, it may make sense to evolve it into an Abstract Factory. Onece that's done, clients can configure a system to use a particular ConcreteFactory or let the system use a default ConcreteFactory.

![Abstract Factory](abstract_factory.png)

### Encapsulate Classes with Factory

Problem: Clients directly instantiate classes that reside in one package and implement a common interface.

Solution: Make the class constructor non-public and let clients create instances of them using a Factory.

Motivation:

A client's ability to directly instantiate classes is useful so long as the client needs to know about the very existence of those classes. But in some cases, client doesn't need to know.

* the client doesn't need that knowledge.
* the classes live in one package and implement one interface, and those conditions aren't likely to change.

In these cases, the classes in the package could be hidden from clients outside the package by giving a Factory the responsibility of creating and returning instances that implement a common interface.

Advantages of these kinds of design:

* it enforces "program to an interface, not an implementation".
* it provides a way to reduce the "conceptual weight" of a package by hiding classes that don't need to be publicly visible outside their package.
* it simplifies the construction of available kinds of instances by making the set available through a Factory's intention revealing Creation Methods.

Disadvantages:

* it involves a dependency cycle: whenever you create a new subclass or add/modify an existing subclass constructor, you must add a new Creation Method to your Factory. 
* if the code is available as binary, then this should not be  used. 

### Introduce Polymorphic Creation with Factory Method

Problem: Classes in a hierarchy implement a method similarly, except for an object creation step.

Solution: Make a single superclass version of the method that calls a factory method to handle the instantiation.

Motivation:

If you wish to form a Factory Method, you need the following:

* A type(defined by an interface, abstract class, or class) to identify the set of classes that Factory Method implementation may instantiate and return.

* The set of classes that implement that type.

* Several classes that implement the Factory Method, making local decisions about which of the set of classes to instantiate, initialize, and return.

Factory Methods are usually implemented within a class hierarchy, though they may be implemented by classes that simply share a common interface. It is common for an abstract class to either declare a Factory Method and force subclasses to override it or to provide a default Factory Method implementation and let subclasses inherit or override that implementation.

Because the signature of a Factory Method must be the same for all implementers of the Factory Method, you may be forced to pass unnecessary parameters to some Factory Method implementers.

Let's consider the following example.

![Polymorphic Creation Without Factory](Polymorphic_Creation_Without_Factory.jpg)

Here, similar methods are used to instantiate the concrete classes.

```java
builder = new DOMBuilder("orders");
```

```java
builder = new XMLBuilder("orders");
```

We can combine these two methods into one factory method.

![Polymorphic Creation With Factory](Polymorphic_Creation_With_Factory.jpg)

Now

```java
void AbstractBuilderTest::testAddAboveRoot()
{
	...
	builder = createBuilder("orders");
	...
}
```

```java
OutputBuilder DOMBuilderTest::createBuilder(String rootName)
{
	return new DOMBuilder(rootName);
}
```

```java
OutputBuilder XMLBuilderTest::createBuilder(String rootName)
{
	return new XMLBuilder(rootName);
}
```

This is called the abstract factory pattern. The abstract factory pattern provides an interface for creating families of related or dependent
objects without specifying their concrete classes.


### Encapsulate Composite with Builder

Problem: Building a Composite is repetitive, complicated, or error-prone.

Solution: Simplify the build by letting a Builder handle the details.

Motivation:

A common motivation for refactoring to a Builder is to simplify client code that creates complex object. When difficult or tedioud parts of creation are implemented by a Builder, a client can direct the Builder's creation work without having to know how that work is accomplished. Builders often ecnapsulate Composites.

e.g. Let's say, we have to add a child node to a parent node, a client must do the following:

* Instantiate a new node
* Initialize the new node
* Correctly add the new node to the right parent node.

This is error prone, because you can either forget any one step. This process is repetative because it requires performing the same batch of construction steps over and over again. 

Another motivation for encapsulating a Composite with a Builder is to decouple client code from Composite code. 
Here is an example, how the creation of the DOM Composite, orderTag, is tightly coupled to the DOM's `Document`, `DocumentImpl`, `Element`, and `Text` interface and classes. This makes it difficult to change Composite implementations.

![Composite Structure For DOM](Composite_Structure_For_Dom.jpg)

```java
Document doc = new DocumentImpl();
Element orderTag = doc.createElement("order");
orderTag.setAttribute("id", order.getOrderId());
Element productTag = doc.createElement("product");
productTag.setAttribute("id", product.getId());
Text productName = doc.createTextNode(product.getName());
productTag.appendChild(productName);
orderTag.appendChild(productTag);
```

We can create a builder called DOM builder which will create different parts of DOM components.

![Composite Structure With Builder For DOM](Composite_Structure_With_Builder_For_Dom.jpg)

```java
DOMBuilder orderBuilder = new DOMBuilder("order");
orderBuilder.addAttribute("id", order.getOrderId());
orderBuilder.addChild("product");
orderBuilder.addAttribute("id", product.getId());
orderBuilder.addValue(product.getName());
```



Builder Pattern: Separates the construction of a complex object from it's internal representation so that the same construction process can create different representations. While creating  different representations of a complex object is useful service, it's the only service a Builder provides. 


![Before Composite Builder](Before_Composite_Builder.jpg)


![After Composite Builder](After_Composite_Builder.jpg)

#### Builder example

Intent: Separate the construction of a complex object from its representation so that the same construction process can create different representations.

Applicability:

* the algorithm for creating a complex object should be independent of the parts that make up the objects and how they're reassembled.
* the construction process must allow different representations for  the object that's constructed.


Participants

* Builder
	- specifies an abstract interface for creating parts of a Product object

* ConcreteBuilder
	- constructs and assembles parts of the product by implementing the Builder interface.
	- defines and keeps track of the representation it creates
	- provides an interface for retrieving the product.

* Director
	- constructs an object using the Builder interface.

* Product
	- represents the complex object under construction. ConcreteBuilder builds the product's internal representation and defines the process by which it's assembled.
	- includes classes that define the constituent parts, including interfaces for assembling the parts into the final results.




Let's say you need a vacation planner. The structure is something like this.

![Vacation Planner](Vacation_Plan_Structure.jpg)

Each vacation is planned over some number of days. And each day can have any combination of hotel reservations, tickets, meals and special events.

We can encapsulate the iteration into a separate object and hid the internal representation of the collection from the client.

![Vacation Planner](Vacation_Plan_Structure_With_Builder.jpg)

Sample client code

```cpp
builder.buildDay(date);
builder.addHotel(date, "Grand Facadian");
builder.addTickets("Patterns an Ice");

// plan the rest of vacation

Planner yourPlanner = builder.getVacationPlanner();
```

#### Composite Pattern

The composite pattern allows you compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of obejcts uniformly.

Let's say you have a menu to implement.

![Menu Structure](Menu_Structure.jpg)

* The commposite pattern allows us to build structures of objects in the form of trees that contain both compositions of objects and individual objects as nodes. A composite contains components. Components come in two flavors: composites and leaf elements. A Composite holds a set of children; those children may be other composite or leaf elements.

* Using a composite structure, we can apply the same operations over both composites and individual objects. In other words, in most cases we can ignore the differences between compositions of objects and individual objects.

![Menu Composite Structure](Menu_Composite_Structure.jpg)

