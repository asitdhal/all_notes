### Intent

Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and composition of objects uniformly.

### Motivation:

* You will build complex diagrams out of simple components. The user can group components to form larger components, which in turn can be grouped to form still larger components. The problem here, the code that uses these classes must treat primitive and container objects differently, even if most of the time the user treats them identically.  Having to distinguish these objects makes the application more complex.  The composite pattern describes how to use recursive composition so that clients don't have to make this distinction.

* The key to the composite pattern in an abstract class that represents both primitives and their containers.  This abstract class also declares operations that all composite objects share, such as operations for accessing and managing its children.

Typical composite object structure of recusrively composed Graphics objects.

![Composite Object Structure](Composite_Object_Structure.jpg)


### Applicability

Use when

* you want to represent part-whole hierarchies of objects.
* you want clients to be able to ignore the difference between compositions of objects and individual objects. Clients will treat all objects in the composite structure uniformly.


### Participants

* Component
	* declare the interface for objects in the composition
	* implements default behavior for the interface common to all classes, as appropriate.
	* declares as interface for accessing and managing its child components.
	* (optional) defines an interface for accessing a component's parent in the recursive structure, and implements it if that's appropriate.

* Leaf(Rectangle, Line, Text, etc.)
	* represents leaf objects in the composition. A leaf has no children.
	* defines behavior for primitive objects in the composition

* Composite(Picture)
	* defines behavior for components having children
	* stores child components.
	* implements child-related operations in the component interface.

* Client
	* manipulate objects in the composition through the component interface.

### Collaborations

* Clients use the Component class interface to interact with objects in the composite structure. If the recipient is aLeaft, then the request is handled directly. If the recipient is a Composite, then it usually forwards requests to its child components, possibly performing additional operations before and/or after forwarding.

