Everyday Efficiency: in place construction

### What happens when you move something ?
   
- Moving from a string usually isn’t any faster than copying from it. Most strings are usually small strings, so they created in the small string buffer. When you move it, the source will be set to null. So moving is slower than copying.
- Moves only matter for objects on the heap. 


### Copy Elision/Return value optimisation

- When the caller calls the callee, there is an extra hidden parameter which is the address of the return value(in callee’s stack frame). If the callee can construct what it can return directly in the caller’s stack frame, there is no need to construct and copy at the point of return.
- RVO rules: what is returned has to be either:
  - a temporary(prvalue) - guaranteed in C++17
  - the name of the stack variable

- RVO ability: sometimes, the callee can’t construct the object in place
  - if there is no opportunity to
    
    ```cpp
    std::string sad_function(std::string s)
    {
      s += "No RVO for you";
      return s;
    }
    ```
    In the above case, the compiler will move.
  
  - if it’s not of the right type
  ```cpp
    std::string sad_function(std::string s)
    {
      s += "No RVO for you";
      return std::move(s);
    }
  ```
    An rvalue-ref is not same type.  Don’t return std::move(x). In most cases you will get a move when you didn’t need anything.
  
  - if the callee doesn’t know enough
  ```cpp
    std::string undecided_function()
    {
      std::string happpy = "Hooray";
      std::string sad = "thau thau";
      if (get_happiness() > 0.5) {
        return happy;
      else
        return sad;
    }
    ```


### Putting stuff into the vector push_back vs emplace_back
    
```cpp
void push_back(const T& x);
void push_back(T&& x);
    
template <class... Args>
reference emplace_back(Args&&... args);
```
```cpp
std::vector<std::string> v;
const char* s = "Hello";
    
v.push_back(s); // case 1
v.emplace_back(s); // case 2
```

case 1: A temporary `std::string` will be constructed and moved to the vector.
case 2: the const char* will be passed to emplace_back and the string will be created in place.

`push_back` - will do a copy or move
`emplace_back` - in place construction

`emplace_back` does perfect forwarding, so it can call explicit constructor.

```cpp
// S has an explicit constructor from int
std::vector<S> v;
v.push_back(1); // compiler error
v.emplace_back(1); // works due to perfect forwarding
```

#### Type of one thing to type of another thing

```cpp
std::array<Arg, 3> a = { Arg{}, Arg{}, Arg{} };
    
std::vector<S> v;
v.reserve(a.size());
std::copy(a.cbegin(), a.cend(), std::back_inserter(v));
```

`back_inserter` calls push_back.  So here

- a construct for S
- a move from temporary of S
- a destruct of temporary

```cpp
std::array a = {1, 2, 3, 4, 5};
std::vector <S> v;
v.reserve(a.size());
std::copy(a.cbegin(), a.cend(), std::back_inserter(v)); 
```

Here, it won’t work. Because back_inserter can’t call explicit constructor. 

The fix is

```cpp
std::transform(a.cbegin(), a.cend(), std::back_inserter(v), [](int i) { return S{i};  });
```

Here, the lambda will give a r-value and back_inserter will call push_back on the rvalue. 

As of now, std library doesn’t have an equivalent of back_inserter for emplace back.

```cpp
// back_emplacer
template <typename Container>
struct back_emplace_iterator
{
  explicit back_emplace_iterator(Container& c): c(&c) {}
  back_emplace_iterator& operator++() { return *this; }
  back_emplace_iterator& operator*() { return *this; }
  template <typename Arg>
  back_emplace_iterator& operator=(Arg&& arg) 
  { 
    c->emplace_back(std::forward<Arg>(arg));
    return *this;
  }
private:
  Container *c;
};
    
template <typename Container>
auto back_emplacer(Container& c)
{
  return back_emplace_iterator<Container>(c);
}
```

```cpp
// now
std::copy(a.cbegin(), a.cend(), back_emplacer(v));
```

```cpp
std::vector<std::string_view> tokens;
std::string_view token = /* suff */;
tokens.emplace_back(std::move(token));
```
Here, std::move is superfluous. Because token is a value type.

```cpp
m_headers.emplace_back(std::string(headerData, numBytes)); // superfluous 
m_headers.emplace_back(headerData, numBytes); // better
```

Don’t explicitly call a constructor with emplace_back. It’s a code smell to explicitly asking constructor at construction site.


#### Recommendations

- `push_back` is perfectly fine for rvalues.
- use `emplace_back` only when you need its powers.
  - in-place construction(including nullary construction)
  - a reference to what's added(c++17)
- never pass an explicit temporary to `emplace_back`
- use `piecewise_construct`/`forward_as_tuple`to forward args through pair

### Intializer_list with map

It’s perfectly possible to initialize a map with an initializer_list.

```cpp
// s has an implicit constructor from Arg
    
using M = std::map<int, S>;
M m { {0, Arg{} } }; //temporary will be created and moved
```

```cpp
// call an N-ary function on each lot N args passed in
template <size_t N, typename F, typename... Ts>
void for_each_n_args(F&& f, Ts&&... ts);
```    

```cpp
using M = std::map<int, S>;
M m;
std::for_each_n_args<2>(
  [&](auto&& k, auto&& v) {
    m.emplace(std::forward<decltype(k)>(k),
              std::forward<decltype(v)>(v)); },
   0, 1); // we can also call explicit constructor here Or, in-place construction
```

```cpp
using M = std::map<int, S>;
M m;
std::for_each_n_args<3>(
  [&](auto&& k, auto&&... v) {
    m.emplace(
      std::piecewise_construct,
      std::forward_as_tuple(std::forward<decltype(k)>(k)),
      std::forward_as_tuple(std::forward<decltype(v)>(v)...)); },
  0, 1, 2); // explicit multi-arg value constructor
```

Putting things into a map: operator[]

```cpp
// has an implicit constructor from Arg and explicit constructor from int
m[0] = S{1}; 
m[1] = Arg{};
```

If that thing is already in the map, there will a construction of temporary, move assignment and destruction of temporary.



Vector of pair = map

what do you do if part of your pair has a multi-argument constructor ?

```cpp
struct Value { Value(int, std::string, double); };
    
std::vector<std::pair<int, Value>> v;
    
// this is very common!
v.push_back(std::make_pair(1, Value{42, "hello", 3.14}));
    
// this is no better
v.emplace_back(std::make_pair(1, Value{42, "hello", 3.14}));
```

In all these examples, temporaries are created.

pair has a constructor that will handle your multi-argument constructor.

```cpp
template <class... Args1, class... Args2>
pair(piecewise_construct_t,
  tuple<Args1...> firrst_args,
  tuple<Args2...> second_args);
    
template <class... Types>
constexpr tuple<Types&&...> forward_as_tuple(Types&&... args) noexcept;
```
piecewise_construct_t is a tag type.

so the fix is
```cpp
v.emplace_back(
  std::piecewise_construct,
  std::forward_as_tuple(1), // arg to int "constructor"
  std::forward_as_tuple(42, "hello", 3.14)); // args to Value constructor
```

### Initializer list

When you write:

```cpp
std::vector<int> v{ 1, 2, 3 };
```

It’s as if you wrote:

```cpp
const int a[] = {1, 2, 3};
std::vector<int> v = std::initializer_list<int>(a, a+3);
```

initializer_list is a view into a const array.

initializer_list has const storage.

```cpp
template <int...Is>
auto f()()
{
  // wrong
  return std::initializer_list<int>{Is...};
}
    
void fine() 
{
  for (int i: {1, 2, 3})
    std::cout << i << '\n';
}
    
void works_fine_until_it_explodes() 
{
  for (int i: f<1, 2, 3>())
    std::cout << i << '\n';
}
```

initializer_list has a const storage, so move from won’t work.


```cpp
std::vector<S> v = { S{1}, S{2}, S{3} };
```
3 constructs, 3 copies, 3 destructs

```cpp
S a[3] = { S{1}, S{2}, S{3} };
std::vector<S> v(std::make_move_iterator(std::begin(a)),
    std::make_move_iterator(std::end(a)));
```
3 constructs, 3 moves, 3 destructs


```cpp
std::vector<S> v;
v.reserve(3);
v.emplace_back(1);
v.emplace_back(2);
v.emplace_back(3);
```

Here only 3 constructs.

#### Recommendations
- use `initializer_list` only for literal types.
- consider using `array` and manually moving?
- probably don't use `initializer_list` for anything that'll get run more than once.

### Putting stuff into other things (like optional, variant, any)

```cpp
template <class... Args>
constexpr explicit optional(in_place_t, Args&&... args);
    
template <class T, class... Args>
constexpr explicit variant(in_place_type_t<T>, Args&&... args);
template <size_t I, class... Args>
constexpr explicit variant(in_place_index_t<I>, Args&&... args);
    
template <class ValueType, class... Args>
explicit any(in_place_type_t<ValueType>, Args&&... args);
```

optional construction

implicit constructor

```cpp
std::optional<S> opt = Arg{}
```

explicit constructor(naive method)

```cpp
std::optional<S> opt = S{1};
```
Here, you will construct then move.

explicit constructor(in-place method)

```cpp
std::optional<S> opt(std::in_place, 1);
```
optional assignment

implicit constructor

```cpp
std::optional<S> opt;
opt = Arg{};
```
explicit constructor(naive method)

```cpp
std::optional<S> opt;
opt = S{1};
```
explicit constructor(in-place method)

```cpp
std::optional<S> opt;
opt.emplace(1);
```
variant construction

implicit constructor

```cpp
std::variant<int, S> v = Arg{};
```
explicit constructor(naive method)
```cpp
std::variant<int, S> v = S{1};
```
explicit constructor(oops method)

```cpp
std::variant<int, S>  v = 1;
```
in-place construction

```cpp
std::variant<int, S> v(std::in_place_type<S>, 1);
std::variant<int, S> v(std::in_place_index<1>, 1);
```
variant assignment

```cpp
std::variant<int, S> v;
    
v = Arg{}; // fine
v = S{1}; // constructs a temporary
v = 1;  // oops
```
Implicity-typed variant construction/assignment can be dangerous

```cpp
int main()
{
  std::variant<bool, std::string> v = "Hello";
  std::cout << "index is " << v.index() << "\n"; // 0 due to narrowing
}
```

Sometimes, narrowing can happen.  
