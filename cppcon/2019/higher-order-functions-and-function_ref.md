# Higher order functions

```cpp
template <typename F>
void call_twice(F&& f)
{
    f();
    f();
}

int main()
{
    call_twice([](){ std::cout << "called!\n";});
    return 0;
}
```

- takes a function object as an argument.
- implementation technique: `template` parameter

```cpp
auto greater_than(int threshold)
{
	return [threshold](int x)
	{
		return x > threshold;
	};
}

int main()
{
	std::vector<int> v{0, 4, 1, 11, 5, 9};
	assert(std::count_if(v.begin(), v.end(), greater_than(5)) == 2);
}
```

- Returns a `FunctionObject` invocable with an int.
- Implementation technique: closure + auto return type


#### Erase-remove idiom

```cpp
int main()
{
	std::vector<entity> entities;

	entities.erase(
		std::remove_if(entities.begin(),
					   entities.end(),
					   [](const entity &e) { return !e._active;} ),
		entities.end()
	);
}
```
- Moves kept elements to the beginning of the range.
- Relative order of elements is preserved.


#### Variant visitation using lambdas

```cpp
using event = std::variant<connect, disconnect, heartbeat>;

void process(event &&e)
{
	std::visit(
		overload ([](connect)  { std::cout << "process connect\n"; },
				  [](disconnect)  { std::cout << "process disconnect\n"; },
				  [](heartbeat)  { std::cout << "process heartbeat\n"; }),
		e);
}

int main()
{
	process(event{connect{}});
	process(event{heartbeat{}});
	process(event{disconnect{}});
}

```

#### Avoid repetition

```cpp
void widget::update()
{
	for (auto &c: this->_children)
		if (c->visible())
			c->recalculate_focus();

	for (auto &c: this->_children)
		if (c->visible())
			c->recalculate_bounds();

	for (auto &c: this->_children)
		if (c->visible())
			c->update();
}
```

After lambda refactoring

```cpp
void widget::update()
{
	const auto for_visible_children = [this](auto&& f)
	{
		for (auto &c: this->_children)
			if (c->visible())
				f(*c);
	}
	for_visible_children([](auto &c) { c.recalculate_focus(); });
	for_visible_children([](auto &c) { c.recalculate_bounds(); });
	for_visible_children([](auto &c) { c.update(); });
}
```

#### inversion of contraol flow

- Pass an action/predicate to a function which deals with the control flow.
- separate what happens from how it happens

```cpp
struct physics_component
{
	vec2f _pos, _vel, _acc;
};

std::vector<physics_component> components{ /*....*/ };

std::for_each(std::execution::par_unseq,
			  components.begin(),
			  components.end(),  //control flow
			  [](auto &c)
			  {
			  		//actions
			  		c._vel += c._acc;
			  		c._pos += c._vel;
			  });
```

- Decoupling control flow from desired action
	- can be reused & tested separately

- e.g printing comma separated elements

Initial version

```cpp
template <typename T>
void print(const std::vector<T> &v)
{
	if (std::empty(v)) { return; }
	std::cout << *v.begin();

	for (auto it = std::next(v.begin()); it != v.end(); ++it)
	{
		std::cout << ", ";
		std::cout << *it;
	}
}
```

Then identity the structure

```cpp
template <typename T>
void print(const std::vector<T> &v)
{
	if (std::empty(v)) { return; }
	/* action */

	for (auto it = std::next(v.begin()); it != v.end(); ++it)
	{
		/* separation */
		/* action     */
	}
}
```

Create an abstraction, which probides
	- control flow
	- actions

```cpp
template <typename Range, typename F, typename FSep>
void for_separated(Range && range, F && f, FSep && f_sep)
{
	if (std::empty(range)) { return; }
	f(*range.begin());

	for (auto it = std::next(range.begin()); it != range.end(); ++it)
	{
		f_sep();
		f(*it);
	}
}
```

Now Redefine print

```cpp
template <typename T>
void print(const std::vector<T>& v)
{
	for_separated(v,
				  [](const const& x) { std::cout << x; },
				  []{ std::cout << ", "; });
}

```

Or, something else

```cpp
const auto wide_print = [](const auto& sentence)
{
	for_separated(sentence,
		[](const auto& x) { std::cout << x; },
		[]{ std::cout << ' ';});
};

wide_print("helloworld"); // h e l l o w o r l d
```

```cpp
template <typename Range, typename Pred, typename F>
void consume_if(Range&& range, Pred&& pred, F&& f)
{
	for (auto it = std::begin(range); it != std::end(range);)
	{
		if (pred(*it))
		{
			f(*it);
			it = range.erase(it);
		}
		else
		{
			**it;
		}
	}
}

consume_if(_systems,
		   [](auto& system) { return system.is_initialized(); },
		   change_state_to(state::ready_to_sync));

```

### asynchronicity

- Currently the easiest way to express asynchronous callbacks
	- std::future
	- std::thread

```cpp
auto graph = all
{
	[]{ return http_get_request("animals.com/cat.png"); },
	[]{ return http_get_request("animals.com/dog.png"); }
}
.then([](std::tuple<data, data> payload)
{
	std::apply(stich, payload);
});
```

## Lambdas

### Lambda capture

- `[t=title]() { decltype(title)...use(t) }`
- `[title]() { decltype(title)...use(title) }`
- `[&t=title]() { use(t) }`
- `[&title]() { use(title) }`
- capture all by copy `[=]() { use(title); }`
- capture all by reference `[&]() { use(title); }`
- globals and static are not captured, neither are unevaluated operands

### Other features of lambdas

- Convertible to raw function pointer (if captureless)
```cpp
int (*fp)(int) = [](int x) { return x+1; };
```

The unary +  operator forces any expression to "scalar" type. 
```cpp
template <class T> void fn(T t);
fn(+[](int x) { return x+1; }); 
```

- default-constructible (if captureless)
```cpp
auto lam = [](int x) { return x + 1; };
decltype(lam) copy; //ok new in c++20
```

- constexpr by default, but not noexcept by default
```cpp
static_assert(lam(42) == 43); //ok
static_assert(not noexcept(lam(42))); // new in c++20
```

### ways of capturing this

- `[t]() { this->work(); }`  deprecated in c++20
- `[this]() { this->work(); }`
- `[&]() { this->work(); }`
- `[*this]() { this->work(); }` // in c++17
- capture *this by move* has no shorthand equivalent.
	- `[obj = std::move(*this)]() { obj.work(): }`

### how to forward inside lambda

 
```cpp
auto one = [](auto&&... args) {
	return product(std::forward<decltype(args)>(args)...);
}
```

```cpp
auto two = []<class... Ts>(Ts&&... args) { // in c++20
	return product(std::forward<Ts>(args)...);
}
```

### lambdas may be copyable or movable

```cpp
std::unique_ptr<int> prop;
auto lamb = [p=std::move(prop)]() {};
auto lamb2 = std::move(lamb); //ok
auto lamb3 = lamb; // error
```

- A lambda's type is copyable, movable, or neither depending as it's captures are copyable, movable, or neither.

`std::function` is always copyable. That's why some lambdas can't be stored in a `std::function`.

Consider placing the single instance of non-copyable lambda on the heap and sharing access to it.

```cpp
auto lamp = /*only movable lambda*/;
std::function<void()> f3 = [
	p = std::make_shared<decltype(lamb)>(std::move(lamb))
	](){ (*p)(); };
```


