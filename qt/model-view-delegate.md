### Model View Architecture

* MVC has three kinds of objects.
	* Model: application object -> communicates with the source of data and provides an interface for the other components in the architecture.
	* View: Screen presentation -> the view obtains model indexes from the model(these are references to items of data). The view can retrive items of data from the data source by using model index.
	* Controller: the way the user interface reacts to user input -> delegate renders the items of data. When an item is edited, the delegate communicates with the model directly using model indexes.

* Model/View Architecture
	* view and controller objects are combined,
	* separates the way data is stored from the way it is presented to the user

* Models, views, and delegates communicate with each other using signals and slots.
	* Signals from model inform the view about changes to the data held by the data source.
	* Signals from the view provide information about the user's interaction while the items being displayed.
	* Signals from the delegate are used during editing to tell the model and view about the state of the editor.

* All item models are based on the ```QAbstractItemModel```, this basically defines an interface that is used by views and delegates to access the data.  When implementing new models for list and table-like data structures, the ```QAbstractListModel``` and ```QAbstractTableModel``` classes are better. If these standard models do not meet your requirements, you can subclass ```QAbstractItemModel```, ```QAbstractListModel``` or ```QAbstractTableModel``` to create your custom models.

* All views are based on the abstract class  ```QAbstractItemView```, this basically defines an interface that is used by delegate to show the data. QListView, QTableView and QTreeView can be used to display list of items, tabular data and hierarchical list.

* ```QAbstractItemDelegate``` is the abstract base class for delegates. The default delegate implementation is provided by ```QStyledItemDelegate``` and used as the default delegate by Qt's standard views and this can be used as the base class when implementing custom delegates.

* If your model is sortable, i.e, if it reimplements the ```QAbstractItemModel::sort()``` function, both QTableView and QTreeView provide an API that allows you to sort your model data programmatically. If the model doesn't have the required interface or if you want to use a list view to present your data, the approach is to use a proxy model to transform the structure of your model before presenting the data in the view.

* A number of convinience classes are derived from the standard view classes for the benefitt of applications that rely on Qt's item-based item view and table classes. e.g. QListWidget, QTreeWidget and QTableWidget. These classes are not supposed to be derived.

### Model Classes
* No matter how the items of data are stored in any underlying data structure, all subclasses of ```QAbstractItemModel``` represent the data as a hierachical structure containing tables of items. Views use this convention to access items of data in the model.

#### Model Indexes
* To ensure that the representation of the data is kept separate from the way it is accessed, the concept of a model index is introduced. Each piece of information that can be obtained via a model is represented by a model index. Views and delegates use these indexes to request items of data to display. So all models must implement ```model()``` function.

* Only the model needs to know how to obtain data, and the type of data managed by the model can be defined fairly generally. Model indexes contain a pointer to the model that created them, and this prevens confusion when working with more than one model.

```cpp
QAbstractItemModel *model = index.model();
```

* Model indexes provide temporary references to piece of information, and can be used to retrieve or modify data via the model. Since models may reorgnaize their internal structures from time to time, model indexes may become invalid, and should not be stored. If a long term reference to a piece of information is required, a persistent model index must be created. This provides a reference to the information that the model keeps up to date. Temporary model indexes are provided by the ```QModelIndex``` class, and persistent model indexes are provided by the ```QPersistentModelIndex``` class.
```cpp
QModelIndex Model::index(int row, int column, const QModelIndex &parent=QModelIndex());
```

* To Obtain the model index that corresponds to an item of data, three properties must be specified to the model
	* a row number
	* a column number
	* the model index of a parent item

#### Rows and Columns
* We can retrieve information about any given item by  specifying its row and column numbers to the model, and we receive an index that represents the item:
```cpp
QModelIndex index = model->index(row, col, ...);
```
![GitHub Logo](/modelview-tablemodel.png)

* The above diagram shows a representation of a basic model in which each item is located by a pair of row and column numbers. Single level data structures like lists and tables do not need any other information to be provided.

```cpp
QModelIndex indexA = model->index(0, 0, QModelIndex());
QModelIndex indexB = model->index(1, 1, QModelIndex());
QModelIndex indexC = model->index(2, 1, QModelIndex());
```

#### Parents of items
* In case of tree views, each item can also be the parent of another table of items. In this case, we should provide a parent model index to refer an item.
```cpp
QModelIndex index = model->index(row, col, index);
```
![GitHub Logo](/modelview-treemodel.png)

* The above diagram shows a representation of a tree model in which each item is refereed to by a parent, a row number, and a column number.
```cpp
QModelIndex indexA = model->index(0, 0, QModelIndex());
QModelIndex indexB = model->index(1, 1, indexA);
QModelIndex indexC = model->index(2, 1, QModelIndex());
```

* When supplied with a model index, we first check whether it is valid. If it is not, we assume that a top-level item is being referred to; otherwise, we obtain the data pointer from the model index with its internalPointer() function and use it to reference the undelying data object. Note that all the model indexes that we construct will contain a pointer to an existing TreeItem, so we can guarantee that any valid model indexes that we receive will contain a valid data pointer. 

#### Item Roles
* Items in a model can perform various roles for other components, allowing different kinds of data to be supplied for different situation. By supplying appropriate item data for each role, models can provide hints to views and delegates about how items should be presented to the user.
* We can ask the model for the item's data by passing it the model index corresponding to the items, and by specifying a role to obtain the type of data we want.
```cpp
QVariant value = model->data(index, role);
```

### View classes
* Views typically manage the overall layout of the data obtained from models. They may render individual items of data themselves, or use delegates to handle both rendering and editing features.
* Views do the following things
	- present the data to the user
	- navigation between items
	- some aspects of item selection
	- context menus
	- drag and drop
* Some views, such as ```QTableView``` and ```QTreeView```, display headers as well as items. These are also implemented by a view class, ```QHeaderView```. Headers usually access the same model as the view that contains them. They retrieve data from the model using the ```QAbstractItemModel::headerData()``` function, and usually display header information in the form of a label.
* Providing multiple views onto the same model is simply a matter of setting the same model for each view. The use of signal and slots in the model/view architecture means that changes to the model can be propagated to al the attached views, ensuring that we can always access the same data regardless of the view being used.
* The mechanism for handling selections of items within views is provided by the ```QItemSelectionModel``` class. All the standard views construct their own selection models by default. The selection model can be accessed through getters and setters. When we use more than one view onto the same model it is often desirable that both the model's data and the user's selection are shown consistently in all views. Since the view classes allow their internal selection models to be replaced, we can achieve a unified selection between views with the following line.
```cpp
secondTableView->setSelectionModel(firstTableView->selectionModel());
```

### Delegate Classes
* The standard interface for controlling delegates is defined in the ```QAbstractItemDelegate``` class. Delegates are expected to be able to render their contents themselves by implementing the ```paint()``` and ```sizeHint()``` function.
* A simple widget-based delegates can subclass ```QItemDelegate``` instead of ```QAbstractItemDelegate```, and take advantage of the default implementations of these functions.
* Views have getters and setter for delegates.
```cpp
itemDelegate() // getters for delegate
setItemDelegate() // setters for delegates
```

#### Providing an editor
* When a table view needs to provide an editor, it asks the delegate to provide an editor widget that is appropriate for he item being modified. The ```createEditor()``` function is supplied with everything that the delegate needs to be able to set up a suitable widget. The view takes the responsibility of the editor and destroys whenever it's not needed.
* The delegate's default event filter is installed on the editor to ensure that it provides the standard editing shortcuts that users expects.
* The view ensures that the editor's data and geometry are set correctly by calling functions that we define for these purposes. We can create different editors depending on the model index supplied by the view. e.g ```QSplinBox``` for integer column and ```QLineEdit``` for string column.
* The delegate must provide a function to copy model data into the editor.
```cpp
voidSpinBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	int value = index.model()->data(index, Qt::EditRole).toInt();
	QSplinBox *spinBox = static_cast<QSpinBox*>(editor);
	spinBox->setValue(value);
}
```

#### Submitting data to the model
* When the user has finished editing the value in the spin box, the view asks the delegate to store he edited value in the model by calling the ```setModeldata()``` function.
* The standard ```QItemDelegate``` class informs the view when it has finished editing by emitting the ```closeEditor()``` signal.
* All the operations on data are performed through the interface provided by ```QAbstractItemModel```. This makes the delegate mostly independent from the type of data it manipulated.

#### Updating the editor'S geometry
* It is the responsibility of the delegate to manage the editor's geometry. The geometry must be set when
	* the editor is created
	* the item's size or position in the view is changed.
* The view provides all the necessary geometry information inside a view option object.

### Creating New Models
* The separation of functionality between the model/view components allows models to be created that can take advantage of existing views.
* ```QAbstractItemModel``` class provides interface to support data sources
	- arrange information in hierarchical structures
	- data to be inserted, removed, modified or sorted in some way
	- drag and drop operations
* ```QAbstractTableModel``` and ```QAbstractListModel``` classes provide support for interfaces to simpler non-hierarchical data structures.

#### QAbstractItemModel


#### Designing a model

	| data structure | Model |
	| -------------- | ----- |
	| list | QAbstractListModel |
	| table | QAbstractTableModel |
	| hierarchical data | QAbstractItemModel  |

#### A read-only model implementation with QStringListModel
* We need to inherit from QStringListModel and provide the following minimum functions
	* constructor
	* rowCount() : returns number of strings in the QStringList
	* data() : returns the item of data corresponding to the index argument
* Apart from that we can provide an optionally headerData() function.
```cpp
class StringListModel : public QStringListModel
{
public:
    StringListModel(const QStringList &stringList, QObject *parent=nullptr);
    int rowCount(const QModelIndex &index = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
private:
    QStringList m_stringList;
};

```


### Handling Selections in Item Views
* Information about the items selected in a view is stored in an instance of the QItemSelectionModel class. This maintains model indexes for items in a single model, and is independent of any views. In case of multiple views on to a model, it is possible to share selectiogn between views.
* Selection are made up of selection ranges.
	* For large ranges, only starting and ending model indexes are used.
	* For non-contiguous ranges, more than one selection range is used.


### Item view convinience classes
* Three item view convinience classes.
	- ```QListWidget```
	- ```QTreeWidget```
	- ```QTableWidget```
* Each class inherits the behavior of the ```QAbstractItemView``` class which implements common behavior for item selection and header management.

#### List Widgets
* ```QListWidget``` displays single level lists of items and a number of ```QListWidgetItem```.

```cpp
QListWidget *listWidget = new QListWidget(this);
```

* List items can be added directly to the list widget when they are constructed. 

```cpp
QListWidgetItem *newItem = new QListWidgetItem;
newItem->setText(itemText);
listWidget->insertItem(row, newItem);
```

* Each item in a list can display a text label and an icon. 

```cpp
newItem->setToolTip(toolTipText);
newItem->setStatusTip(toolTipText);
newItem->setWhatsThis(whatsThisText);
```

* By default, items in the list are presented in the order of their creation. List items can be sorted according to the creteria given in Qt::SortOrder.

```cpp
listWidget->sortItems(Qt::AscendingOrder);
listWidget->sortItems(Qt::DescendingOrder);
```

#### Tree Widgets
* Trees or hierarchical lists of items are provided by the ```QTreeWidget``` and ```QTreeWidgetItem``` classes. Each items in the tree widget can have child items of its own, and can display a number of columns of information.

* Tree widgets are created just like any other widget.

```cpp
QTreeWidge *treeWidget = new QTreeWidget(this);
```

* Before items can be added to the tree widget, the number of columns must be set. 

```cpp
treeWidget->setColumnCount(2);
QStringList headers;
headers << tr("Subject") << tr("Default");
treeWidget->setHeaderLabels(headers);
```

* Tree widgets deal with top-level items slightly differently to other items from deeper within the tree. Items can be removed from the top level of the tree by calling the tree widget's ```takeTopLevelItem()``` function, but items from lower levels are removed by calling their parent item's ```takeChild()``` function. Items  are inserted in the top level of the tree with the ```insertTopLevelItem()``` function. At lower levels in the tree, the parent item's ```insertChild()``` function is used.


* We can remove the current item in the tree widget regardless of its location.

```cpp
	QTreeWidgetItem* parent = currentItem->parent();
	int index;
	if(parent) {
		index = parent->indexOfChild(treeWidget->currentItem());
		delete parent->takeChild(index);
	} else {
		index = treeWidget->indexOfTopLevelItem(treeWidget->currentItem());
		delete treeWidget->takeTopLevelItem(index);
	}
```

#### Table widgets
* Tables of items similar to those found in spreadsheet applications are constructed with the ```QTableWidget``` and ```QTableWidgetItem```.

* Tables can be created with a set numbers of rows and columns, or these can be added to an unsized tables as they are needed.

```cpp
	QTableWidgetItem* newItem = new QTableWidgetItem(tr("%1").arg(pow(row, column+1)));
	tableWidget->setItem(row, column, newItem);
```

* Horizontal and vertical headers can be added to the table by constructing items outside the table and using them as headers.

```cpp
	QTableWidgetItem* valuesHeaderItem = new QTableWidgetItem(tr("Values"));
	tableWidget->setHorizontalHeaderItem(0, valuesHeaderItem);
```

#### Hidden Items
* Items can be hidden by ```setItemHidden()``` and hiddeness can be checked by ```isItemHidden()```.
* This is item based, the same function is available for all three convenience classes.

#### Selections
* The way items are selected is controlled by the widget's selection mode (```QAbstractItemView::SelectionMode```).
* There are three modes.
	- Single Item Selections:- one item is selected, the current item and the selected item are same.
	- Multi-item Selections
	- Extended Selections :- many adjacent items can be selected.

#### Searching
* All three item view convenience classes provide a common ```findItems()``` function to make this as consistent and simple as possible.
* Items are searched for by the text that they contain according to creteria specified by a selection of values from ```Qt::MatchFlags```.