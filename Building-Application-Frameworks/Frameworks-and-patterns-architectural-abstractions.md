## Introduction

- *Reuse* :-  The effictiveness originates from the qualitative and economic benefits reuse accomplishes. Using previously developed and tested components when building an application saves development efforts and reduces the risk of introducing errors into the system.

- *Abstraction* :- Abstractions let the developer work with fewer elements - abstracting from details. This facilitates communication about the software. This also facilitates reuse.

- It is often hard to reuse software designs or implementations directly, because variations in the different application contexts imply that variation of the designs and implementations are likewise needed. When the potential reusable designs and implementations are abstracted from specific contexts and represented by generalized concepts and designs, they typically have much wider application scope.

- A software architecture is a description of subsystems and components of a software system and the relationships between them. Subsystems and components are typically specified in different views to show the relevant functional and non-functional properties of a software system.

- Software architecture is often interpreted as
	- the choice of architectural abstractions and language mechanisms
	- the use of choosen elements

![Nothing](assets/conecptual_frameworks.png)


## Architectural Abstractions

> Interface design and functional factoring constitute the key intellectual content of software and are far more difficult to create or re-create than code.

- *Interface design* and *functional factoring* can be perceived as aspects of *architecture*. Architecture is concerned with the design and building of structure - not with the individual building blocks that bring the structures into existence. Interface design of software (classes) is concerned with the external properties of a component as opposed to its implementation. Functional refactoring is concerned with the distribution of functionality(reponsibility) between the various components. The distribution expresses the logical structure of software.

- The following four issues are considered to be characteristic aspects of an architectural abstraction:
	- **Structure** :- Specficies the interface of a set of basic elements as well as the static and/or dynamic structures that relate them in a composition.
	- **Functionality** :- Provides useful functionality.
	- **Abstraction** :- Identifies and names a composition of elements with a certain internal structure and a certain functionality.
	- **Reuse** :- Can be applied multiple times.

## Object-Oriented Patterns