- Design and implementation of complex software remain expensive and error-prone. Much of the cost and effort stem from the continuous rediscovery and reinvention of core concepts and components across the software industry. 

- Frameworks are an obejct oriented reuse techniques. 
	- structure of framework:- a framework is a reusable design of all or part of a system that is represented by a set of abstract classes and the way their instances interact.
	- purpose of framework:- a framework is the skeleton of an application that can be customized by an application developer.

## What is an application framework ?

- A framework is a reusable design of a system that describes how the system is decomposed into a set of interacting objects. The framework describes both the component objects and how these objects interact. It describes the interface of each objects and the flow of control between them. It describes how the system's responsibilities are mapped onto its objects.

- The most important part of a framework is the way that a system is divided  into its components. Frameworks also reuse implementation, but that is less important than reuse of the internal interfaces of a system and the way that its functions are divided among its components. The high level design is the main intellectual content of software, and frameworks are a way to reuse it.

- Each object in the framework is described by an *abstract class*. Since abstract class has no instances, it is used as a template for creating subclasses rather than as a template for creating objects. Frameworks use them as designs of their components because they both define the interface of the components and provide a skeleton that can be extended to implement the components.

- An abstract class provides part of the implementation of its subclasses. The abstract class can either leave some methods unimplemented(abstract methods) or provide a default implementation(hook methods). A concrete class must implement all the abstract methods of its abstract subclass and may implement any of the hook methods. It will then be able to use all the methods it inherits from its abstract superclass.

- Frameworks take advantage of all three of the distinguishing characteristic of object-oriented programming language: data abstraction, polymorphism and inheritance.
	- Like an abstract data type, an abstract class represents an interface behind which implementation can change.
	- *Polymorphism* is the ability for a single variable or procedure parameter to take on values of several types. Object-oriented polymorphism lets a developer mix and match components, lets an object change its collaborators at runtime, and makes its possible to build generic objects that can work with a wide range of components. 
	- *Inheritance* makes its easy to make a new component.

- A framework describes the architecture of an object-oriented system; the kinds of objects in it, and how they interact. It describes how a particular kind of program, such as a user interface or network communication software, is decomposed into objects. It is represented by a set of classes(usually abstract), one for each kind of object, but the interaction patterns between objects are just as much part of framework as the classes.

- One of the characteristics of frameworks is *inversion of control*. Traditionally, a developer reused components from a library by writing a main program that called the components whenever necessary. The developer decided when to call the components and was responsible for the overall structure and flow of control of the program. In case inversion of control, the main program is reused, and the developer decided what is plugged into it and might even make some new components that are plugged in. The developer's code gets called by the framework code. The framework determines the overall structure and flow of control of the program.

- A framework reuses code because it makes it easy to build an application from a library of existing components. These components can be easily used with each other because they all use the interfaces of the framework. A framework also reuses code because a new component can inherit most of its implementation from an abstract superclass. But resuse is best when you don't have to understand the component you are reusing, and inheritance requires a deeper understanding of a class that is using it as a component, as it is better to reuse existing components than to make a new one.

- A framework reuses analysis. It describes the kinds of objects that are important and provides a vocabulary for talking about a problem. An expert in a particular framework sees the world in terms of the framework and will naturally divide it into the same components. Two expert users of the same framework will find it easier to understand each other's designs, since they will come up with similar components and will describe the systems they want to build in similar ways.


## Benefits

- Modularity
- Reusability
- Extensibility
- Inversion of Control

## Classifying Application Frameworks

### Classification based on scope

- System infrastructure frameworks: These frameworks simplify the development of portable and efficient system infrastructure such as operating system and communication frameworks, and frameworks for user interfaces and language processing tools. 

- Middleware Integration Frameworks: These frameworks are commonly used to integrate distributed applications and components. Middleware integration frameworks are designed to enhance the ability of software developers to modularize, reuse, and extend their software infrastructure to work seamlessly in a distributed environment.

- Enterprise application frameworks

### Classification based on techniques used to extend them

- Whitebox Frameworks: rely heavily on OO language feature like inheritance and dynamic binding in order to achieve extensibility. Existing functionality is reused and extended by
	- inheriting from framework base classes
	- overriding predefined hook methods
	These frameworks tend to produce systems that are tightly coupled to specific details of the framework's inheritance hierarchies. 

- Blackbox Frameworks: support extensibility by defining interfaces for components that can be plugged into framework via object composition. Existing functionality is reused by
	- defining components that conforms to a particular interface
	- integrating these components into the framework using patterns like Strategy and Functors. 
	These are structured using object composition and delegation rather than inheritance. 

- Graybox Frameworks: these are designed to avoid the disadvantages presented by whitebox and blackbox frameworks. It has enough flexibility and extensibility, and also has the ability to hide unnecessary information from the application developers.


## Reuse: Components versus Designs

- A component that does one thing  well is easy to use, but can be used in fewer cases. A component with many parameters and options can be used more often, but will be harder to learn to use.  Reuse techniques ranges from the simple and inflexible to complex and powerful. 

- A component represents code reuse. Reuse experts often claim that design reuse is more important than code reuse, mostly because it can be applied in more contexts and so is more common. Also, it is applied earlier in the development process and so can have a larger impact on a project. But most design reuse is informal. One of the main problems with reusing design information is capturing and expressing it. There is no standard design notation and there are no standard catalogs of design to reuse. 

- Frameworks are an intermediate form, part code reuse and part design reuse. Frameworks eliminate the need of a new design notation by using an object-oriented programming languages as the design notation. 

- Reuse(and frameworks) save time and money during development and also brings uniformity. GUI frameworks give a similar look and feel, reusable network interface allows all applications that use it follows the same protocol. Framework also allows the customer to build open systems, so they can mix and match components from different vendors.


## Application Frameworks versus Other Reuse Techniques

- The more customizable a component is, the more likely it is to work in a particular situation, but the more work it takes to use it and to learn to use it.

- Frameworks are a component in the sense that vendors sell them as products, and an application might use several frameworks bought from various vendors. But frameworks are much more customizable than most components. As a consequence, using a framework takes work even when you are familier with it, and learning a new framework takes work even when you are familiar with it, and learning a new framework is hard.

- It is probably better to think of frameworks and components as different, but cooperating, technologies. 
	- Frameworks provide a reusable context for components. Each components makes assumptions about its environment.A framework will provide a standard way for components to handle errors, to exchange data, and to invoke operations on each other.
	- Frameworks make it easier to develop new components out of smaller components(a widget). They also provide the specificatoions for new components and a template for their implementation.

- Frameworks are similar to application generators. Application generators are based on a high-level, domain-specific language that is compiled to a standard architecture. Designing a reusable class library is a lot like designing a programming language, except that the only concrete syntax is that of the language it is implemented in. A framework is already a standard architecture. It is common to combine frameworks and a domain-specific language. Programs in tha language are translated into a set of objects in the framework.

- Frameworks are more abstract and flexible(and harder to learn) than components, but more concrete and easier to reuse than a raw design(but less flexible and less likely to be applicable). They are most comparable to reuse techniques that reuse both code and design, such as application generators and templates. 

## How to Use Application Frameworks

- An application developed using a framework has three parts:
	- framework
	- the concrete subclasses of the framework classes
	- everything else

- Everything else includes
	- a script that specifies which concrete classes will be used and how they will be interconnected
	- objects that have no relationship to the framework
	- objects that use one or more framework objects, but that are not called by framework objects

- Objects that are called by framework objects will have to participate in the collaborative model of the framework and so are part of framework.

- If application programmers can use a framework by connecting components without having to look at their implementation, then the framework is a *blackbox framework*. Framework that rely upon inheritance usually require more knowledge on the part of developers and so are called *whitebox framework*. Blackbox frameworks are easier to use and whitebox frameworks are often more powerful in the hands of experts.  It is common for a framework to be used in a blackbox way most of the time and to be extended when the occasion demands.

## How to Develop Application Frameworks

- The design of a framework is like the design of the most reusable software. 
	- It starts with domain analysis which collects a number of examples. 
	- The first version of the framework is usually designed to be able to implement the examples and is usually a whitebox framework. 
	- Then the framework is used to build applications. These applications point out weak points in the framework, which are parts of the framework that are hard to change.
	- Experience leads to improvements in the framework, which often make it more blackbox. 
	- Eventually the framework is good enough that suggestions for improvement are rare. 
	- At some point, the developers have to decide that framework is finished and release it.

- Reasons for framework iterations
	- Domain analysis: Unless the domain is mature, it is hard for experts to explain it. Mistakes in domain analysis are discovered when a system is built, which leads to iteration.
	- The explicit parts of a design of a framework are likely to change. Interfaces and shared invariants are hard. The only way to learn what changes is by experience.
	- Frameworks are abstractions, so the design of the framework depends on the original example. Each example that is considered makes the framework more general and reusable. Paper design are not sufficiently detailed to evaluate the framework. A better notation for describing frameworks might let more of the iteration take place during framework design.

- A framework should not be used widely until it has proven itself, because the more widely a framework is used, the more expensive it is to change it.

- Because frameworks require iteration and deep understanding of an application domain, it is hard to create them on schedule. Thus framework design should never be on the critical path of an important project.

- The tension between framework design and application design leads to two models of the process of framework design.
	- Framework designers and application designers are the same people, they divide their time into phases in which they extend the framework by applying it and phases in which they revise the framework by consolidating earlier extensions. 
	- Framework designers and application designers are different group of people, the framework designers test their framework by using it, but also rely on the main users of the framework for feedback.