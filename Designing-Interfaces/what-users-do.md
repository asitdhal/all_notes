* First step in designing interface is to figure out what its users are really trying to accomplish.
	- Filling out a form:- goal is to renew the driving license

* To do design well, you need to take many "softer" factors into account: like information about the user
	- gut reactions
	- preferences
	- social context
	- beliefs
	- values
   Among these softer factors, you may find the critical feature or design factor that makes your application more appealing and successful.

* Empirical discovery is one of the way to obtain this information. To get the design started, you'll need to characterize the kinds of people who will be using  the design.
	- The target audience for, say, a new mobile phone app will differ dramatically from the target audience for a piece of scientific software. Even if the same person uses both, his expectations for each are different - a researcher using a scientific software might tolerate a less-polished interface in exchange for high functionality, whereas that same person may stop using the mobile app if he finds its UI to be too hard to use after a few days.

	- In this case you will like to learn
		- their goals in using the software.
		- the specific tasks they undertake in persuit of those goals.
		- the language and words they use to describe what they're doing
		- their skill at using software similar to what you're designing
		- their attitudes toward the kind of things you're designing, and how different designs might affect those attitudes.

* The subject-specific vocabulary you use in your interface should match your users' level of knowledge; 
	- if some users won't know that vocabulary, give them a way to learn the unfamiliar terms.
	- if their level of interest might be low, respect that, and don't ask for too much effort for too little reward.

* You need to choose how much freedom your users have to act arbitarily.
	- At one end of the scale might be a software installation wizard: the user is carried through it with no opportunity to use anything other than Next, Previous, or Cancel. It's tightly focused and specific, but quite efficient - and satisfying, to the extend that it works and is quick. 
	- At the other end might be an application such as Excel, an "open floorplan" interface that exposes a huge number of features in one place. At any given time, the user has about 872 things that he can do next, but that's considered good, because self-directed, skilled users can do a lot with that interface.

* Software designed for intermediate-to-expert users includes:
	- Photoshop
	- Excel
	- Qt-Creator

   These applications have complex functionality, but they don't generally walk the user through tasks step by step. They assume users already know what to do, they optimize for efficient operation, not learnability. They tend to be document-centered or list driven. 

* Software designed for occasional users includes:
	- Installation wizards
	- Windows controls for setting desktop background

  These applications are restrained in functionality but helpful about explaining it along the way. They present simplified interfaces, assuming no prior knowledge of document- or list-centered application styles. They key is that users aren't motivated to work hard at learning these applications - it's usually just not worth it.

* The following applications lies in between
	- facebook
	- powerpoint
	- blog writing tools
   
   These applications are designed to serve people on both ends adequately - to help new users learn the tool(and satisfy their need for instant gratification), while enabling frequent-user intermediates to get things done smoothly. Their designers probably knew that people wouldn't take a three-day course to learn an email client. Yet the interfaces hold up under repeated usage. People quickly learn the basics, reach a proficiency level that satisfies them, and don't bother learning more until they are motivated to do so for specific purposes.


## Interface Patterns

### Safe Exploration

> Let me explore without getting lost or getting into trouble.

When someone feels like she can explore an interface and not suffer dire consequences, she's likely to learn more and feel more positive about it. Good sofware allows people to try something unfamiliar, back out, and try something else, all without stress.

Dire consequences can be mere annoyance(anything that can deter someone from trying things out voluntarily).
	
	- clicking away pop-up
	- reentering data that was mistakenly erased

When you design almost any kind of software interface, make many avenues of exploration available for users to experiment with, without costing the user anything.
	
	- providing undo operation
	- occasional users always expect Back button to go to main page, so you should not provide surprise feature with a back button

### Instant Gratification

> I want to accomplish something now, not later.

People like to see immediate results from the actions they take - it's human nature. If someone starts using an application and gets a "success experience" within the first few seconds, that's gratifying. He'll be more likely to keep using it, even if gets harder later.

You can predict the first thing a new user is likely to do, you should design the UI to make that first thing stunningly easy. This also means you shouldn't hide introductory functionality behind anything that needs to be read or waited for, such as registration, long sets of instructions, slow-to-load screens, advertisements, and so on. These are discouraging because they block users from finishing that first task quickly.

### Satisficing

> This is good enough. I don't want to spend more time learning to do it better.

Satisficing is actually a very rational behavior, once you appreciate the mental work necessary to "parse" a complicated interface. If an interface presents an abvious option or two that the user sees  immediately, he'll try it. Chances are good that it will be the right choice, and if not, thare's little cost in backing out and trying something else.

- use "call to actions" in the interface. Give direction on what to do first: type here, drag an image here, tap here to begin, and so forth.
- Make labels short, plainly worded, and quick to read. 
- Use the layout of the interface to communicate meaning.
- Make it easy to move around the interface, especially for going back to where a wrong choice might have been made hastily. Provide "escape hatches".
- Keep in mind that a complicated interface impose a large cognitive cost on new users. Visual complexity will often tempt nonexperts to satisfice: they look for the first thing that may work.

Long ago, a user may have learned Path A to do something, and even though a later version of the system offers Path B as a better alternative, he sees no benefit in learning it - that takes effort, after all - and keeps using the less-efficient Path. Breaking old habits and learning something new takes energy, and a small improvement may not be worth the cost to the user.

### Changes in Midstream

> I changed my mind about what I was doing.

The designers should make choices available. Don't lock users into a choice-poor environment with no connections to other pages or functionality unless there's a good reasons to do so. 

You can also make it easy for someone to start a process, stop in the middle, and come back to it later to pick up where he left off - a property often called *reentrance*. To support reentrance, you can make dialogs and web forms remember values typed previously, and they don't usually need to be modal; if they are not modal, they can be dragged aside on the screen for later use. 

### Deferred Choices

> I don't want to answer that now; just let me finish.

This follows from people's desire for instant gratification. If you ask a task-focused user unnecessary questions in the process, he may prefer to skip the questions and come back to them later.

- Don't accost the user with too many upfront choices in the first place.
- On the forms that he does have to use, clearly mark the required fields, and don't make too many of them required. Let him move on without answering the optional ones.
- Sometimes you can separate the few important questions or options from others that are less important. Present the short list; hide the long list.
- Use Good Defaults whenever possible, to give users some reasonable default answers to start with. But keep in mind that prefilled answers still require the user to look at them, just in case they need to be changed. 

### Incremental Construction

> Let me change this. That doesn't look right; let me change it again. That's better.

When people create things, they don't usually do it all in a precise order. The user starts with some small piece of it, works on it, steps back and looks at it, tests it, fixes what's wrong, and starts to build other parts of it. Or, may be she starts over, if she really doesn't like it. Builder-style interfaces need to support that style of work. Make it easy for users to build small pieces. Keep the interface responsive to quick changes and saves. 

Feedback is critical: constantly show the user what the whole thing looks and behaves like, while the user works. 

### Habituation

> That gesture works everywhere else; why doesn't it work here, too?

When one uses an interface repeatedly, some frequent physical actions become reflexive: pressing Ctrl-S to save a document, clicking the Back button to leave a web page, pressing Return to close a modal dialog box. The user no longer needs to think consciously about these actions. They've become habitual.

Consistency is required across platform and within application. Some applications are evil because they establish an exception that some gesture will do Action X, except in one special mode where it suddenly does Action Y. Don't do that. It's a sure bet that users will make mistakes, and the more experienced they are - that is, the more habituated they are - the more likely they are to make that mistake.

