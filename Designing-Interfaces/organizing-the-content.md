*Information architecture(IA)* is the art of organizing an information space. It encompasses many things: presenting, searching, browsing, labeling, categorizing, sorting, manipulating, and strategically hiding information. 

## The Big Picture

At the highest level of your application design, you'll make decisions about the whole package, like

- interaction model
- desktop metaphor
- simple model of a traditional website
- richly interactive sites

## Show One Single Thing

You need to manage the user's interaction with one thing. There might be small-scale tools clustered around the content - scrollers and sliders, sign-in box, global navigation, headers and footers, and so forth - but they are minor and easily designed. Your design might take one of these shapes:

- A long, vertically scrolled page of flowed text.
- A zoomable interface for very large, fine-grained artifacts, such as maps, images, or information graphics.
- The "media player" idiom, including video and audio players.

As you design this interface, consider the following patterns and techniques to support the design:

- Alternative Views: to show the content in more than one way.
- Many Workspaces: in case people want to see more than one place, state, or document at one time.
- Deep-linked State: With this, a user can save a certain place or state within the contents so that he can come back to it later or send someone else a URL.
- Sharing Widget and other social patterns


## Show a List of Things

Lists present rich challenges in information architecture.

- how long is the list ?
- is it flat or hierarchical ?
- what kind of hierarchical ?
- is it ordered ?
- can the user change that ordering dynamically ?
- should it be filtered or searched ?
- what information or operations are associated with each list item, and when and how should they be shown ?

Some patterns for designing an interface around a list

- Feature, Search, and Browse
- News Stream
- Picture Manager

## Provide Tools to Create a Thing






### Feature, Search, and Browse

Put three elements on the main page of the site or app
- a featured article or product
- a search box
- a list of items or categories that can be borrowed

Searching and browsing go hand in hand as two ways to find desired items. Featured item are how you "hook" the user. They are more interesting than just category lists and search boxes, especially when you are appealing images and headlines.

### Picture Manager

Use thumbnails, item views, and a browsing interface to create a familiar structure for managing photos, videos, and other pictorial items.

This is also a *guild* of patterns - a set of patterns linked together and supporting each other in predictable ways. Patterns and other components that often play parts in this guild include.

- Thumbnail Grid
- One-Window Drilldown
- Two-Panel Selector
- Pyramid
- Tabs and Collapsible Panels
- Button Groups
- Trees or outlines
- Keyboard Only
- Sharing Widget
- Search box


Set up two principal views: a Thumbnail Grid of the items in the list, and a large view of a single item. Users will go back and forth between these. Design a browsing interface and associate it with the Thumbnail Grid to let users explore a large collection easily.


#### The Thumbnail Grid

- Use this pattern to show a sequence of items.
- Many Picture Managers show a small amount of metadata with each item, such as its filename or author, but do this with care, as it clutters the interface. 
- You might offer a control to adjust the size of the thumbnails.
- There may also be a way to sort the items by different creteria, such as date, label, or rating, or to filter it, etc.
- When a user clicks on an item, show it immediately in the single-item view. Applications often let the user traverse the grid with the keyboard.
- If the user owns the items, offer ways to move, reorder, and delete items at this level in the interface. This implies having a multiple-selection interface, such as Shift-select, checkboxes, or lassoing a group of items with the pointer. Cut, copy, and paste should also work in applications.
- You can also offer slideshow or playlist functionalities to all users.

#### The Single-Item View

-  Show a large view of the selected image.  
